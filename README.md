# Regular Show: Paintwar


BACKEND SETUP
=============
Copy the entire source/backend/ directory (EXCEPT for the regshowpaintwardomestic_2013-10-03.sql file) to the target backend directory.


DATABASE SETUP
--------------
- 1) Create a database using  source/backend/regshowpaintwardomestic_2013-10-03.sql.
- 2) Edit source/backend/meekrodb.2.1.class.php:  

	//...
	public static $dbName = 'DB_NAME';
	public static $user = 'USER';
	public static $password = 'PASSWORD';
	public static $host = 'DB_HOST';
	public static $port = null;
	public static $encoding = 'utf8_unicode_ci';
	//...



FRONTEND SETUP
==============
Copy the entire source/frontend/bin/ directory to the target fronted directory.


TO SET UP INTERACTION WITH MSIB BACKEND
---------------------------------------

Edit source/frontend/bin/config/config.xml:  

- // To set appId (Line 32)  
	<term key="app_id">regshowpaintwar</term>

- // To set the path to the Amfphp backend directory (line 38):  
	<term key="db_php_path">http://127.0.0.1/games/regshowpaintballbackend/repo/domestic/Amfphp/</term>

TO CONFIGURE GUEST AND LOGIN MODE
---------------------------------

- // To set guest mode available (true/false) (Line 49)  
	<term key="allow_guests">true</term>

- // To use in game login (true: AS calls, false: JS) (line 52):  
	<term key="use_ingame_login">false</term>
