package players 
{
import obstacles.Obstacle;

/**
	 * ...
	 * @author ...
	 */
	public class PlayerState_Hide extends PlayerState
	{	
		protected var _playerPositionRelativeToObstacle	: String	= "right";
		protected var _obstacle							: Obstacle	= null;
		
		public function PlayerState_Hide(player:Player) 
		{
			super(player);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= true;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function EnterWithArgs(args:Array):void {
			super.EnterWithArgs(args);
			if (args != null && args.length > 0)	_playerPositionRelativeToObstacle = args[0];
			else {
				Debugger.Log("WARNING! - Player hide: enter state without args");
				_playerPositionRelativeToObstacle	= CollisionManager.LEFT;
			}
			
			_obstacle = null;
			if (args != null && args.length > 1)	_obstacle = args[1] as Obstacle;
			
			Enter();
		}
		
		override public function Enter():void {
			super.Enter();
			
			_player.speedX = _player.speedZ = _player.speedY	= 0;
			
			switch(_playerPositionRelativeToObstacle)
			{
				case CollisionManager.LEFT:
					_player.FaceRight();
					if (_obstacle != null && Config.getValue("obstacle_" + _obstacle.configPrefix + "force_hide_center_depth") == "true") _player.TeleportToZ(_obstacle.z + 1);
					break;
				case CollisionManager.RIGHT:
					_player.FaceLeft();
					if (_obstacle != null && Config.getValue("obstacle_" + _obstacle.configPrefix + "force_hide_center_depth") == "true") _player.TeleportToZ(_obstacle.z + 1);
					break;
				case CollisionManager.BACK:
					_player.FaceFront();
					if (_obstacle != null && Config.getValue("obstacle_" + _obstacle.configPrefix + "force_hide_center_width") == "true") _player.TeleportToX(_obstacle.x);
					break;
				case CollisionManager.FRONT:
					_player.FaceBack();
					if (_obstacle != null && Config.getValue("obstacle_" + _obstacle.configPrefix + "force_hide_center_width") == "true") _player.TeleportToX(_obstacle.x);
					break;
			}
			_player.ViewGoToAndStop("hide_" + _player.facing);
			
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			//Debugger.Log("EN HIDE - _playerPositionRelativeToObstacle: " + _playerPositionRelativeToObstacle);
			//Debugger.Log("GameInput.right: " + GameInput.right);
			if (	(_playerPositionRelativeToObstacle == CollisionManager.LEFT && !GameInput.right)
				||	(_playerPositionRelativeToObstacle == CollisionManager.RIGHT && !GameInput.left)
				||	(_playerPositionRelativeToObstacle == CollisionManager.BACK && !GameInput.down)
				||	(_playerPositionRelativeToObstacle == CollisionManager.FRONT && !GameInput.up))
			{
				_player.SetState(Player.ST_IDLE);
				return;
			}
			
			if (GameInput.action1Hit) {
				_player.SetState(Player.ST_FIRE);
				return;
			}
			
			if (GameInput.action2Hit){
				_player.tryToThrowGrenade();
				return;
			}
		}
		
	}

}
