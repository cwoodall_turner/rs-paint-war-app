package players 
{
import communication.database.DBManager;

import flash.display.MovieClip;

import obstacles.Obstacle;

import shots.Shot;

import specialassets.SpecialAsset;

import ui.HUDManager;

/**
	 * ...
	 * @author ...
	 */
	public class Player extends GameObject {
		
		public static const ST_IDLE					: String	= "idle";
		public static const ST_RUN					: String	= "run";
		public static const ST_FIRE					: String	= "fire";
		public static const ST_GRENADE				: String	= "grenade";
		public static const ST_HIDE					: String	= "hide";
		public static const ST_HIT					: String	= "hit";
		public static const ST_KILLED				: String	= "killed";
		public static const ST_STOP					: String	= "stop";
		public static const ST_SPAWN				: String	= "spawn";
		
		public static const ID_MORDECAI				: String	= "mordecai";
		public static const ID_RIGBY				: String	= "rigby";
		public static const ID_MUSCLEMAN			: String	= "muscleman";
		public static const ID_GUY					: String	= "guy";
		
		protected var _number	: int	= 1;
		
		public function Player(view:GameObjectView = null, shadow:MovieClip = null, $id:String = "player", $number:int = 1) {
			super(view, shadow, $id);
			_number	= $number;
			_id		= $id;
			_targetId		= Targets.PLAYER;
			_maxEnergy		= Config.getValue(configPrefix + "max_energy");
			_heightForFire	= Config.getValue(configPrefix + "height_for_fire");
			Debugger.Log("Player _maxEnergy: " + _maxEnergy);
			
			SetState(ST_READY);
		}
		
		
		public function againstObstacle(playerPositionRelativeToObstacle:String, obstacle:Obstacle):void 
		{
			(_currentState as PlayerState).againstObstacle(playerPositionRelativeToObstacle, obstacle);
		}
		
		override protected function CreateStates():void 
		{
			super.CreateStates();
			_states[ST_SPAWN]				=	new PlayerState_Spawn(this);
			_states[ST_IDLE]				=	new PlayerState_Idle(this);
			_states[ST_RUN]					=	new PlayerState_Run(this);
			_states[ST_READY]				=	new PlayerState_Ready(this);
			_states[ST_FIRE]				=	new PlayerState_Fire(this);
			_states[ST_GRENADE]				=	new PlayerState_Grenade(this);
			_states[ST_HIDE]				=	new PlayerState_Hide(this);
			_states[ST_HIT]					=	new PlayerState_Hit(this);
			_states[ST_KILLED]				=	new PlayerState_Killed(this);
			_states[ST_STOP]				=	new PlayerState_Stop(this);
		}
		
		override public function Update(dtMs:int):void {
			//Debugger.Log("Player Update. Player: " + this);
			super.Update(dtMs);
			_currentState.Update(dtMs);
		}
		
		override public function Reset():void {
			super.Reset();
			SetState(ST_READY);
		}
		
		
		public function tryToThrowGrenade():void 
		{
			if (hasGrenades)
			{
				SetState(Player.ST_GRENADE);
			}
			else
			{
				HUDManager.Instance.onTryToThrowEmptyGrenade();
			}
		}
		
		
/* ========================================================*/
/* ============= GAME OBJECTS HITS/COLLISIONS =============*/
/* ========================================================*/
		
		public function HitByShot(shot:Shot):void {
			_lastHitBy			= shot;
			_lastHitByAttackSeq	= shot.attackSeq;
			//Debugger.Log("PLAYER LOST " + enemy.attackPower + " HP");
			//energy -= shot.attackPower * Config.player_damage_received_factor;
			energy -= shot.attackPower;
		}
		
		
		
/* =============================================*/
/* ============= SETTERS & GETTERS =============*/
/* =============================================*/
		
		public function get shotRef():MovieClip {
			if (!_innerMc) _innerMc = _view.getChildByName("inner_mc") as MovieClip;
			return _innerMc.getChildByName("shotRef_mc") as MovieClip;
		}
		public function get number():int 
		{
			return _number;
		}
		
		override public function set teamId(value:String):void
		{
			Debugger.Log("SET PLAYER ID: " + teamId);
			super.teamId = value;
			SpecialAsset.setSpecialState(View, "specialassets::Vincha", teamId);
		}
		
		public function get hasGrenades():Boolean
		{
			var numGrenades	: int = DBManager.instance.user.getVar("grenades");
			return (!isNaN(numGrenades) && numGrenades > 0);
		}
		
	}

}
