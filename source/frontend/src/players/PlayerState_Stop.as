package players 
{
import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class PlayerState_Stop extends PlayerState {
		
		public function PlayerState_Stop(player:Player) 
		{
			super(player);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= false;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function Enter():void {
			super.Enter();
			
			_player.speedX = _player.speedZ = _player.speedY	= 0;
			_player.ViewGoToAndStop("stop_" + _player.facing);
		}
		
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			if (GameInput.left || GameInput.right || GameInput.up || GameInput.down) {
				_player.SetState(Player.ST_RUN);
				return;
			}
			
			if (GameInput.action1Hit) {
				_player.SetState(Player.ST_FIRE);
				return;
			}
			
			if (GameInput.action2Hit)
			{
				_player.tryToThrowGrenade();
				return;
			}
			
			if (MovieClipUtils.AnimationEnded(_player.innerMc))
			{
				_player.SetState(Player.ST_IDLE);
				return;
			}
		}
	}

}
