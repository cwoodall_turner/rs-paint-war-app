package players 
{
	/**
	 * ...
	 * @author ...
	 */
	public class PlayerState_Ready extends PlayerState
	{
		
		public function PlayerState_Ready(player:Player) 
		{
			super(player);
			_onReEnterPlayInnerMcFromStart	= false;
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= false;
			_gObj.canGrabPowerups	= false;
		}
		
		override public function Enter():void {
			super.Enter();
			_player.visible	= false;
			//_player.TeleportToX( -1000);
			_player.ViewGoToAndStop("ready");
		}
		
		
		override public function Leave():void {
			super.Enter();
			_player.visible	= true;
		}
		
	}

}
