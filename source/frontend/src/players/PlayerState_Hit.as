package players 
{
import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class PlayerState_Hit extends PlayerState {
		
		public function PlayerState_Hit(player:Player) 
		{
			super(player);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= false;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function Enter():void {
			super.Enter();
			
			_player.speedX = _player.speedZ = _player.speedY	= 0;
			_player.ViewGoToAndStop("hit_" + _player.facing);
		}
		
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (MovieClipUtils.AnimationEnded(_player.innerMc))
			{
				_player.SetState(Player.ST_IDLE);
			}
		}
	}

}
