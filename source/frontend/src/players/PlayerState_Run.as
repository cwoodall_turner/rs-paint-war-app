package players 
{
import obstacles.Obstacle;

/**
	 * ...
	 * @author ...
	 */
	public class PlayerState_Run extends PlayerState
	{	
		protected var _speedHorizontal			: Number	= 1;
		protected var _speedHorizontalDiagonal	: Number	= 1;
		protected var _speedVertical			: Number	= 1;
		protected var _speedVerticalDiagonal	: Number	= 1;
		
		public function PlayerState_Run(player:Player) 
		{
			super(player);
			
			_speedHorizontal			= Config.getValue(_player.configPrefix + "run-speed-horizontal");
			_speedVertical				= Config.getValue(_player.configPrefix + "run-speed-vertical");
			_speedHorizontalDiagonal	= Config.getValue(_player.configPrefix + "run-speed-horizontal-diagonal");
			_speedVerticalDiagonal		= Config.getValue(_player.configPrefix + "run-speed-vertical-diagonal");
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= true;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function Enter():void {
			super.Enter();
			
			updateView();
			
			MovePlayer(33); // TMP
		}
		
		private function updateView():void 
		{
			if (GameInput.right) {
				_player.FaceRight();
			} else if (GameInput.left) {
				_player.FaceLeft();
			} else if (GameInput.up) {
				_player.FaceBack();
			} else if (GameInput.down) {
				_player.FaceFront();
			}
			_player.ViewGoToAndStop("run_" + _player.facing);
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			MovePlayer(dtMs);
			
			
			if (GameInput.action1Hit) {
				_player.SetState(Player.ST_FIRE);
				return;
			}
			
			if (GameInput.action2Hit)
			{
				_player.tryToThrowGrenade();
				return;
			}
			
			if (!GameInput.left && !GameInput.right && !GameInput.up && !GameInput.down) {
				_player.SetState(Player.ST_STOP);
				return;
			}
		}
		
		private function MovePlayer(dtMs:int):void {
			
			var diagonal : Boolean = (GameInput.left || GameInput.right) && (GameInput.up || GameInput.down);
			if (GameInput.right) {
				_player.speedX	= diagonal?_speedHorizontalDiagonal:_speedHorizontal;
			}
			else if (GameInput.left) {
				_player.speedX	= diagonal?-_speedHorizontalDiagonal:-_speedHorizontal;
			} else {
				_player.speedX	= 0;
			}
			if (GameInput.up) {
				_player.speedZ	= diagonal?-_speedVerticalDiagonal:-_speedVertical;
			} else if (GameInput.down) {
				_player.speedZ	= diagonal?_speedVerticalDiagonal:_speedVertical;
			} else {
				_player.speedZ	= 0;
			}
			
			var dX	: Number	= _player.speedX * dtMs;
			//var dY	: Number	= _player.speedY * dtMs;
			var dZ	: Number	= _player.speedZ * dtMs;
			
			_player.MoveX(dX);
			_player.MoveZ(dZ);
			
			updateView();
		}
		
		/**
		 * 
		 * @param	side: Player position relative to obstacle
		 */
		override public function againstObstacle(playerPositionRelativeToObstacle:String, obstacle:Obstacle):void {
			super.againstObstacle(playerPositionRelativeToObstacle, obstacle);
			//Debugger.Log("playerPositionRelativeToObstacle: " + playerPositionRelativeToObstacle);
			switch(playerPositionRelativeToObstacle)
			{
				case CollisionManager.LEFT:
					//Debugger.Log("GameInput.right: " + GameInput.right);
					if (GameInput.right) _player.SetState(Player.ST_HIDE, [CollisionManager.LEFT, obstacle]);
					break;
				case CollisionManager.RIGHT:
					if (GameInput.left) _player.SetState(Player.ST_HIDE, [CollisionManager.RIGHT, obstacle]);
					break;
				case CollisionManager.BACK:
					if (GameInput.down) _player.SetState(Player.ST_HIDE, [CollisionManager.BACK, obstacle]);
					break;
				case CollisionManager.FRONT:
					if (GameInput.up) _player.SetState(Player.ST_HIDE, [CollisionManager.FRONT, obstacle]);
					break;
			}
		}
		
		override public function PassedLevelLimitLeft(limitValue:Number) : void {
			super.PassedLevelLimitLeft(limitValue);
			_player.TeleportToX(limitValue);
		}
		
		override public function PassedLevelLimitRight(limitValue:Number) : void {
			super.PassedLevelLimitRight(limitValue);
			_player.TeleportToX(limitValue);
		}
		
		override public function PassedLevelLimitBack(limitValue:Number) : void {
			super.PassedLevelLimitBack(limitValue);
			_player.TeleportToZ(limitValue);
		}
		
		override public function PassedLevelLimitFront(limitValue:Number) : void {
			super.PassedLevelLimitFront(limitValue);
			_player.TeleportToZ(limitValue);
		}
		
	}

}
