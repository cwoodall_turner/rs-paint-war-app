package players 
{
import communication.database.DBManager;

import enemies.EnemiesManager;
import enemies.Enemy;

import obstacles.Bullseye;
import obstacles.Obstacle;
import obstacles.ObstaclesManager;

import shots.Shot;
import shots.ShotsManager;

import ui.HUDManager;

import utils.MovieClipUtils;

import view.LayerFactory;

/**
	 * ...
	 * @author ...
	 */
	public class PlayerState_Fire extends PlayerState
	{	
		protected var _nextFire:Boolean	= false;
		protected var _nextGrenade:Boolean	= false;
		
		protected var _fireHeightRangeLength	: Number	= 0;
		protected var _fireHeightRangeWidth		: Number	= 0;
		
		protected var _forceLowShot	: Boolean = false;
		protected var _lowShot		: Boolean = false;
		
		public function PlayerState_Fire(player:Player) 
		{
			super(player);
			_forceLowShot	= (Config.getValue("force_low_shot") == "true");
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			/* FIRE */
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= true;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function Enter():void {
			super.Enter();
			
			_player.speedX = _player.speedZ = _player.speedY	= 0;
			_nextFire	= false;
			_nextGrenade	= false;
			_lowShot	= false;
			
			
			// Check if there's a "lower" enemy in sight
			var fireHeightSuffix	: String	= "";
			// TMP
			if (_forceLowShot || _player.heightForFire >= 2)
			{
				var facingStr	: String	= (_player.IsFacingLeft || _player.IsFacingRight)?"_facing_side":"_facing_front";
				var numDistanceShots	: int	= DBManager.instance.user.getVar("shots_distance");
				var distanceStr	: String	= (numDistanceShots > 0)?"_long_shots":"";
				_fireHeightRangeLength	= Config.getValue(_player.configPrefix + "range_to_check_fire_low_length_px" + facingStr + distanceStr);
				_fireHeightRangeWidth	= Config.getValue(_player.configPrefix + "range_to_check_fire_low_width_px" + facingStr + distanceStr);
				var leftX	: Number = 0;
				var rightX	: Number = 0;
				var backZ	: Number = 0;
				var frontZ	: Number = 0;
				if (_player.IsFacingFront)
				{
					leftX	= _player.x - _fireHeightRangeWidth * .5;
					rightX	= leftX + _fireHeightRangeWidth;
					backZ	= _player.z;
					frontZ	= backZ + _fireHeightRangeLength;
				}
				else if (_player.IsFacingBack)
				{
					leftX	= _player.x - _fireHeightRangeWidth * .5;
					rightX	= leftX + _fireHeightRangeWidth;
					backZ	= _player.z - _fireHeightRangeLength;
					frontZ	= backZ + _fireHeightRangeLength;
				}
				else if (_player.IsFacingRight)
				{
					leftX	= _player.x;
					rightX	= leftX + _fireHeightRangeLength;
					backZ	= _player.z - _fireHeightRangeWidth * .5;
					frontZ	= backZ + _fireHeightRangeWidth;
				}
				else
				{
					leftX	= _player.x - _fireHeightRangeLength;
					rightX	= leftX + _fireHeightRangeLength;
					backZ	= _player.z - _fireHeightRangeWidth * .5;
					frontZ	= backZ + _fireHeightRangeWidth;
				}
				var areaBoundaries	: Boundaries	= new Boundaries(leftX, rightX, Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY, backZ, frontZ);
				// Chequear si hay algún enemigo enano cerca
				if (Config.debug_range_to_check_fire_low)
				{
					AreaDebugger.instance.drawAreaBoundaries(areaBoundaries, LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_FRONT).graphics, 0, true);
				}
				for each(var enemy:Enemy in EnemiesManager.instance.theEnemies)
				{
					//Debugger.Log("enemy.heightForFire: " + enemy.heightForFire);
					if (_forceLowShot || (enemy.canBeHit && enemy.heightForFire == 1 && enemy.teamId != _player.teamId))
					{
						if (_forceLowShot || CollisionManager.instance.isInArea(enemy, areaBoundaries))
						{
							_lowShot	= true;
							fireHeightSuffix = "_low";
							
							// TODO: Chequear que no haya obstáculo entre el enemigo y el que dispara
						}
						
					}
				}
				// Si no hay enemigo enano cerca, chequear si no hay un blanco a pintar cerca
				if (!_lowShot)
				{
					for each(var bullseye:Obstacle in ObstaclesManager.instance.theObstacles)
					{
						if (bullseye.IsCurrentState(GameObject.ST_READY) || !(bullseye is Bullseye)) continue;
						if ((bullseye as Bullseye).paintedByTeamId == _player.teamId) continue;
						if (CollisionManager.instance.isInArea(bullseye, areaBoundaries))
						{
							_lowShot	= true;
							fireHeightSuffix = "_low";
						}
					}
				}
			}
			
			_player.ViewGoToAndStop("fire_" + _player.facing + fireHeightSuffix);
		}
		
		override public function Leave():void {
			super.Leave();
			
			GameManager.Instance.updatePlayerWeaponAsset();
		}
		
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			//Debugger.Log("_player.shotRef: " + _player.shotRef);
			if (_player.shotRef) {
				var shotSpecials	: uint	= 0;
				var numDistanceShots	: int	= DBManager.instance.user.getVar("shots_distance");
				if (numDistanceShots > 0)
				{
					shotSpecials += Shot.SPECIAL_DISTANCE;
					DBManager.instance.user.setVar("shots_distance", numDistanceShots - 1);
				}
				var numPowerShots	: int	= DBManager.instance.user.getVar("shots_power");
				if (numPowerShots > 0)
				{
					shotSpecials += Shot.SPECIAL_POWER;
					DBManager.instance.user.setVar("shots_power", numPowerShots - 1);
				}
				Debugger.Log("_lowShot: " + _lowShot);
				if(_lowShot) shotSpecials += Shot.SPECIAL_LOW;
				Debugger.Log("shotSpecials: " + shotSpecials);
				ShotsManager.instance.AddShot(_player, Targets.ENEMY + Targets.OBSTACLE, shotSpecials);
				//Sfx.PlaySound("Shot_" + _player.characterId);
			}
			
			
			// Fire again before animation end
			if (_player.innerMc && (_player.innerMc.currentLabel == "can_press_next_key" || _player.innerMc.currentLabel == "can_attack_again")) {
				if(GameInput.action1Hit)	_nextFire = true;
				if (GameInput.action2Hit)
				{
					if (_player.hasGrenades)	_nextGrenade = true;
					else HUDManager.Instance.onTryToThrowEmptyGrenade();
				}
			}
			
			if (_player.innerMc && _player.innerMc.currentLabel == "can_hit_again") {
				if (_nextFire) {
					_player.currentState.ReEnter();
					return;
				}
				if (_nextGrenade && _player.hasGrenades) {
					_player.SetState(Player.ST_GRENADE);
					return;
				}
			}
			
			if (MovieClipUtils.AnimationEnded(_player.innerMc))
			{
				_player.SetState(Player.ST_IDLE);
			}
		}
	}

}
