package players 
{
/**
	 * ...
	 * @author ...
	 */
	public class PlayerState_Idle extends PlayerState
	{
		
		public function PlayerState_Idle(player:Player) 
		{
			super(player);
			_onReEnterPlayInnerMcFromStart	= false;
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= true;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function Enter():void {
			super.Enter();
			
			_player.ViewGoToAndStop("idle_" + _player.facing);
			_player.speedX = _player.speedY = _player.speedZ	= 0;
			
			// TEST: If has to walk go NOW
			//if (ActionInputMapper.keyDown(ActionInputMapper.RIGHT, _player.number) || ActionInputMapper.keyDown(ActionInputMapper.LEFT, _player.number) || ActionInputMapper.keyDown(ActionInputMapper.UP, _player.number) || ActionInputMapper.keyDown(ActionInputMapper.DOWN, _player.number)) {
				//_player.SetState(Player.ST_WALK);
			//}
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			if (GameInput.left || GameInput.right || GameInput.up || GameInput.down) {
				_player.SetState(Player.ST_RUN);
				return;
			}
			
			if (GameInput.action1Hit) {
				_player.SetState(Player.ST_FIRE);
				return;
			}
			
			if (GameInput.action2Hit)
			{
				_player.tryToThrowGrenade();
				return;
			}
		}
		
	}

}
