package players 
{
import obstacles.Obstacle;

/**
	 * ...
	 * @author ...
	 */
	public class PlayerState  extends GameObjectState
	{
		
		protected var _player	: Player	= null;
		
		public function PlayerState(player:Player) 
		{
			super(player);
			_player	= player;
		}
		
		override public function Destroy():void {
			super.Destroy();
			_player	= null;
		}
		
		public function againstObstacle(playerPositionRelativeToObstacle:String, obstacle:Obstacle):void { }
		
	}

}
