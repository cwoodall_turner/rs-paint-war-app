package players 
{
import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class PlayerState_Killed extends PlayerState {
		
		public function PlayerState_Killed(player:Player) 
		{
			super(player);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= false;
			_gObj.canGrabPowerups	= false;
		}
		
		override public function Enter():void {
			super.Enter();
			
			_player.speedX = _player.speedZ = _player.speedY	= 0;
			_player.ViewGoToAndStop("killed_" + _player.facing);
		}
		
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (MovieClipUtils.AnimationEnded(_player.innerMc))
			{
				//_player.SetState(Player.ST_IDLE);
				GameManager.Instance.OnPlayerKilledAnimationEnd(_player);
			}
		}
	}

}
