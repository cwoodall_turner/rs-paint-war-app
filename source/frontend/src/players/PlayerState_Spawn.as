package players 
{
import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class PlayerState_Spawn extends PlayerState {
		
		public function PlayerState_Spawn(player:Player) 
		{
			super(player);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= false;
			_gObj.canGrabPowerups	= false;
		}
		
		override public function Enter():void {
			super.Enter();
			
			_player.speedX = _player.speedZ = _player.speedY	= 0;
			_player.ViewGoToAndStop("spawn");
			
			Sfx.PlaySound("PlayerSpawn");
		}
		
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (MovieClipUtils.AnimationEnded(_player.innerMc))
			{
				_player.SetState(Player.ST_IDLE);
			}
		}
	}

}
