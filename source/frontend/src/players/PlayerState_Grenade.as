package players 
{
import communication.database.DBManager;

import flash.display.MovieClip;

import shots.ShotsManager;

import ui.HUDManager;

import utils.MathUtils;
import utils.MovieClipUtils;

import view.LayerFactory;

/**
	 * ...
	 * @author ...
	 */
	public class PlayerState_Grenade extends PlayerState
	{	
		protected var _nextFire:Boolean	= false;
		protected var _nextGrenade:Boolean	= false;
		
		protected var _chargeController	: ChargeController	= null;
		protected var _chargePercentage : Number	= 0;
		
		protected var _grenadeHitMark	: MovieClip	= null;
		
		protected var _charging	: Boolean = false;
		
		public function PlayerState_Grenade(player:Player) 
		{
			super(player);
			
			if (Config.show_grenade_hit_area)
			{
				_grenadeHitMark	= ExternalLoader.Instance.GetSingleton("GrenadeHitMarkAsset", "hud.swf") as MovieClip;
				LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_BACK).addChild(_grenadeHitMark);
				_grenadeHitMark.visible = false;
			}
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			/* FIRE Grenade */
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= true;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function Enter():void {
			super.Enter();
			
			_player.speedX = _player.speedZ = _player.speedY	= 0;
			_nextFire	= false;
			_nextGrenade	= false;
			
			_player.ViewGoToAndStop("grenade_" + _player.facing);
			
			_chargeController	= new ChargeController(_player.configPrefix + "grenade");
			_chargeController.start();
			
			_charging	= true;
		}
		
		override public function Leave():void {
			super.Leave();
			if (_chargeController)
			{
				//_chargeController.onValueSelected();
				//_chargePercentage	= _chargeController.getChargePercentage();
				_chargeController.destroy();
				_chargeController	= null;
			}
			if (_grenadeHitMark != null)
			{
				_grenadeHitMark.visible = false;
			}
		}
		
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			if (_chargeController) {
				_chargeController.update(dtMs);
				_chargePercentage	= _chargeController.getChargePercentage();
			}
			
			if (_player.innerMc && !GameInput.action2Down)
			{
				_player.innerMc.canContinue = true;
				_player.innerMc.play();
				_charging	= false;
			}
			
			/*
			if (_charging)
			{
				// Mientras está cargando, puede apuntar a otros lados
				if (GameInput.right) {
					_player.FaceRight();
				}
				else if (GameInput.left) {
					_player.FaceLeft();
				}
				else if (GameInput.up) {
					_player.FaceBack();
				} else if (GameInput.down) {
					_player.FaceFront();
				}
			}
			*/
			
			//Debugger.Log("_chargeController.getChargePercentage(): " + _chargeController.getChargePercentage());
			//Debugger.Log("_player.shotRef: " + _player.shotRef);
			if (_player.shotRef) {
				_chargeController.onValueSelected();
				_chargePercentage	= _chargeController.getChargePercentage();
				Debugger.Log("chargePercentage: " + _chargePercentage);
				ShotsManager.instance.AddGrenade(_player, Targets.ENEMY + Targets.OBSTACLE, _chargePercentage);
				
				
				// CODE: INFINITE_GRENADES
				if (CheatsManager.isActive(CheatsManager.INFINITE_GRENADES))
				{
					var maxGrenades: int = Config.getValue("powerup_grenades_max_value");
					DBManager.instance.user.setVar("grenades", maxGrenades);
				}
				else
				{
					var numGrenades	: int	= DBManager.instance.user.getVar("grenades");
					if (numGrenades > 0)
					{
						DBManager.instance.user.setVar("grenades", numGrenades - 1);
					}
				}
			}
			
			
			// Shoot again before animation end
			if (_player.innerMc && (_player.innerMc.currentLabel == "can_press_next_key" || _player.innerMc.currentLabel == "can_attack_again")) {
				if(GameInput.action1Hit)	_nextFire = true;
				if (GameInput.action2Hit)
				{
					if (_player.hasGrenades)	_nextGrenade = true;
					else HUDManager.Instance.onTryToThrowEmptyGrenade();
				}
			}
			
			if (_player.innerMc && _player.innerMc.currentLabel == "can_hit_again") {
				if (_nextFire) {
					_player.currentState.ReEnter();
					return;
				}
				if (_nextGrenade && _player.hasGrenades) {
					_player.SetState(Player.ST_GRENADE);
					return;
				}
			}
			
			if (MovieClipUtils.AnimationEnded(_player.innerMc))
			{
				_player.SetState(Player.ST_IDLE);
				return;
			}
			
			if (Config.show_grenade_hit_area)
			{
				debugGrenadeStrength();
			}
		}
		
		private function debugGrenadeStrength():void 
		{
			if (_grenadeHitMark == null) return;
			
			var speedHorizontal 	: Number	= 0;
			var speedVertical		: Number	= 0;
			var initSpeedUp			: Number	= 0;
			var gravityGoingUp		: Number	= 0;
			var gravityGoingDown	: Number	= 0;
			
			var timeGoingUp		: Number = 0;
			var timeGoingDown	: Number = 0;
			var maxHeight		: Number = 0;
			
			var chargePercentage:Number = _chargePercentage;
			var shooterShotPrefix : String = _player.configPrefix + "grenade_";
			var minValue : Number = Config.getValue(shooterShotPrefix + "speed_horizontal_range_min");
			var maxValue : Number = Config.getValue(shooterShotPrefix + "speed_horizontal_range_max");
			//Debugger.Log("Speed horizontal: [" + minValue + "," + maxValue + "] (" + chargePercentage + ") -> " + MathUtils.linearInterpolationY(minValue, maxValue, 0, 100, chargePercentage));
			speedHorizontal	= MathUtils.linearInterpolationY(minValue, maxValue, 0, 100, chargePercentage);
			
			minValue = Config.getValue(shooterShotPrefix + "speed_vertical_range_min");
			maxValue = Config.getValue(shooterShotPrefix + "speed_vertical_range_max");
			//Debugger.Log("Speed vertical: [" + minValue + "," + maxValue + "] (" + chargePercentage + ") -> " + MathUtils.linearInterpolationY(minValue, maxValue, 0, 100, chargePercentage));
			speedVertical	= MathUtils.linearInterpolationY(minValue, maxValue, 0, 100, chargePercentage);
			
			minValue = Config.getValue(shooterShotPrefix + "initial_speed_up_range_min");
			maxValue = Config.getValue(shooterShotPrefix + "initial_speed_up_range_max");
			//Debugger.Log("Speed Y: [" + minValue + "," + maxValue + "] (" + chargePercentage + ") -> " + MathUtils.linearInterpolationY(minValue, maxValue, 0, 100, chargePercentage));
			initSpeedUp	= MathUtils.linearInterpolationY(minValue, maxValue, 0, 100, chargePercentage) * -1;
			
			
			gravityGoingUp = Config.getValue(shooterShotPrefix + "gravity_going_up");
			gravityGoingDown = Config.getValue(shooterShotPrefix + "gravity_going_down");
			
			
			var initY	: Number	= Config.floor_y - Config.getValue(shooterShotPrefix + "init_height_for_debug");
			maxHeight = initY;
			if (initSpeedUp < 0)
			{
				// Calcular tiempo subiendo
				// SpeedYFinal = speedYInicial + gravity * dT;
				timeGoingUp = (0 - initSpeedUp) / gravityGoingUp;
				//Debugger.Log("timeGoingUp: " + timeGoingUp);
				
				maxHeight = initY + initSpeedUp * timeGoingUp + .5 * gravityGoingUp * timeGoingUp * timeGoingUp;
			}
			
			// Calcular tiempo bajando
			var dY : Number = Config.floor_y - maxHeight;
			timeGoingDown = Math.sqrt(dY * 2 / gravityGoingDown);
			//Debugger.Log("dY: " + dY);
			//Debugger.Log("timeGoingDown: " + timeGoingDown);
			
			var totalTimeMs : Number = timeGoingUp + timeGoingDown;
			//Debugger.Log("totalTimeMs: " + totalTimeMs);
			
			
			var dX : Number = 0;
			var dZ : Number = 0;
			
			if (_player.IsFacingRight)
			{
				dX = speedHorizontal * totalTimeMs;
			}
			else if (_player.IsFacingLeft)
			{
				dX = -speedHorizontal * totalTimeMs;
			}
			else if (_player.IsFacingFront)
			{
				dZ = speedVertical * totalTimeMs;
			}
			else
			{
				dZ = -speedVertical * totalTimeMs;
			}
			
			//var screenPos	: Point = Camera.Instance.ConvertGamePosToScreenPos(new GameAreaPoint(_player.x + dX, Config.floor_y, _player.z + dZ));
			_grenadeHitMark.x = _player.x + dX;
			_grenadeHitMark.y = _player.z + dZ + Config.floor_y;
			_grenadeHitMark.visible = true;
		}
	}

}
