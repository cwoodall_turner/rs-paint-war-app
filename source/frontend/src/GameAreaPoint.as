package  
{
import flash.geom.Vector3D;

/**
	 * ...
	 * @author ...
	 */
	public class GameAreaPoint extends Vector3D 
	{
		
		public function GameAreaPoint($x:Number = 0, $y:Number = 0, $z:Number = 0, $w:Number = 0)
		{
			super($x, $y, $z, $w);
		}
		
		override public function toString():String {
			return "[" + x.toFixed(2) + ", " + y.toFixed(2) + ", " + z.toFixed(2) + "]";
		}
		
		public function clonar(): GameAreaPoint {
			return new GameAreaPoint(x, y, z, w);
		}
	}
	
}