package powerups 
{
/**
	 * ...
	 * @author ...
	 */
	public class PowerupState_Enter extends PowerupState
	{
		public function PowerupState_Enter(powerup:Powerup) 
		{
			super(powerup);
		}
		
		override public function Enter():void {
			super.Enter();
			_powerup.ViewGoToAndStop("enter");
			_powerup.canBeGrabbed = false;
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (_powerup.innerMc && _powerup.innerMc.currentFrame == _powerup.innerMc.totalFrames) {
				_powerup.SetState(GameObject.ST_IDLE);
			}
		}
		
	}

}
