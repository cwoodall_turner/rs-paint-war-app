package powerups
{
import flash.display.MovieClip;
import flash.geom.Point;
import flash.utils.getQualifiedClassName;

import utils.MovieClipUtils;

import view.Camera;
import view.GameObjectToScreenMapper;

/**
	 * ...
	 * @author ...
	 */
	public class PowerupsManager
	{
		
		private static var _instance:PowerupsManager = null;
		
		private var _powerups	:Vector.<Powerup>		= null;
		
		public function PowerupsManager()
		{
			_instance = this;
			_powerups	= new Vector.<Powerup>();
		}
		
		public function PrepareFromSwf():void {
			var levelAsset	: MovieClip	= GameManager.Instance.currentLevel.levelAsset;
			
			var powerupContainer : MovieClip	= levelAsset.getChildByName("powerups_mc") as MovieClip;
			if (powerupContainer == null) return;
			var i : int = powerupContainer.numChildren;
			while (i--)
			{
				var powerpRef : MovieClip	= powerupContainer.getChildAt(i) as MovieClip;
				addPowerupFromSwf(powerpRef, MovieClipUtils.GetRelativePos(powerpRef, levelAsset));
			}
		}
		
		private function addPowerupFromSwf(powerupRef: MovieClip, screenPos:Point):void 
		{
			var id:String = getQualifiedClassName(powerupRef).split("_").reverse()[0];
			Debugger.Log("ADDING POWERUP FROM SWF: " + id);
			var newPowerup	: Powerup	= GetNextAvailablePowerup(id);
			if (!newPowerup) {
				Debugger.Log("No powerup available. Creating new powerup...");
				newPowerup	= AddNewPowerup(id);
			} else {
				Debugger.Log("Reusing Powerup " + newPowerup);
			}
			
			
			var newPowerupPos	: GameAreaPoint = Camera.Instance.ConvertScreenPosToGamePos(screenPos, Config.floor_y);
			newPowerup.TeleportTo(newPowerupPos.x, newPowerupPos.y, newPowerupPos.z);
			newPowerup.SetState(Powerup.ST_IDLE);
			newPowerup.RecalculateHitBoundaries();
		}
		
		public function AddPowerup(id:String, ref:GameObject):void
		{
			Debugger.Log("ADDING POWERUP " + id);
			var newPowerup	: Powerup	= GetNextAvailablePowerup(id);
			if (!newPowerup) {
				Debugger.Log("No powerup available. Creating new powerup...");
				newPowerup	= AddNewPowerup(id);
			} else {
				Debugger.Log("Reusing Powerup " + newPowerup);
			}
			newPowerup.SetNewPos(ref.x, ref.y, ref.z);
		}
		
		private function AddNewPowerup(id:String):Powerup {
			var powerupAsset	: GameObjectView	= null;
			var newPowerup		: Powerup	= null;
			switch(id) {
				case Powerup.ID_DOG_TAG:
					powerupAsset	= ExternalLoader.Instance.GetNewInstance("DogTagAsset", "items.swf") as GameObjectView;
					newPowerup		= new DogTag(powerupAsset, null, Powerup.ID_DOG_TAG);
					break;
				case Powerup.ID_FLAG:
					powerupAsset	= ExternalLoader.Instance.GetNewInstance("FlagAsset", "items.swf") as GameObjectView;
					newPowerup		= new Flag(powerupAsset, null, Powerup.ID_FLAG);
					break;
				default:
					Debugger.Log("WARNING - Trying to add inexistent powerup (" + id + ")");
					break;
			}
			_powerups.push(newPowerup);
			GameObjectToScreenMapper.instance.AddGameObject(newPowerup);
			return newPowerup;
		}
		
		private function GetNextAvailablePowerup(id:String):Powerup 
		{
			var len : int = _powerups.length;
			for (var i : int = 0; i < len; i++) {
				if (_powerups[i].id == id && _powerups[i].ready) return _powerups[i];
			}
			return null;
		}
		
		public function Update(dtMs:int):void
		{
			//Debugger.Log("Powerups manager update");
			if (!_powerups) return;
			for each (var powerup:Powerup in _powerups) {
				powerup.Update(dtMs);
			}
		}
		
		public static function get instance():PowerupsManager
		{
			return _instance;
		}
		
		
		public function get thePowerups():Vector.<Powerup> 
		{
			return _powerups;
		}
		
		public function Reset():void {
			for each (var powerup : Powerup in _powerups) {
				GameObjectToScreenMapper.instance.RemoveGameObject(powerup);
				powerup.Destroy();
			}
			_powerups = new Vector.<Powerup>();
		}
		
		public function getNearestPowerup(gObj:GameObject, id:String = ""):Powerup 
		{
			var shortestDistance	: Number	= Number.MAX_VALUE;
			var nearestPowerup		: Powerup	= null;
			
			for each(var powerup:Powerup in _powerups)
			{
				if (id != "" && powerup.id != id) continue;
				var distance : Number	= (powerup.x - gObj.x) * (powerup.x - gObj.x) + (powerup.z - gObj.z) * (powerup.z - gObj.z);
				if (distance < shortestDistance)
				{
					shortestDistance = distance;
					nearestPowerup = powerup;
				}
			}
			return nearestPowerup;
		}
	
	}

}