package powerups 
{
/**
	 * ...
	 * @author ...
	 */
	public class PowerupState_Grabbed extends PowerupState
	{
		public function PowerupState_Grabbed(powerup:Powerup) 
		{
			super(powerup);
		}
		
		override public function Enter():void {
			super.Enter();
			_powerup.ViewGoToAndStop("grabbed");
			_powerup.canBeGrabbed = false;
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (_powerup.innerMc && _powerup.innerMc.currentFrame == _powerup.innerMc.totalFrames) {
				_powerup.SetState(Powerup.ST_READY);
			}
		}
		
	}

}
