package powerups 
{
import flash.display.MovieClip;

/**
	 * ...
	 * @author ...
	 */
	public class Powerup extends GameObject
	{
		public static const ST_ENTER	: String	= "enter";
		public static const ST_IDLE		: String	= "idle";
		public static const ST_GRABBED	: String	= "grabbed";
		public static const ST_READY	: String	= "ready";
		
		public static const ID_DOG_TAG			: String	= "dogtag";
		public static const ID_FLAG				: String	= "Flag";
		
		protected var _canBeGrabbed	: Boolean	= false;
		
		public function Powerup(view:GameObjectView, shadow:MovieClip = null, $id:String="powerup")
		{
			super(view, shadow);
			_id = $id;
			
			addGameObjectStates();
			
			SetState(ST_READY);
		}
		
		public function addGameObjectStates():void { }
		
		public function get ready():Boolean {
			return IsCurrentState(ST_READY);
		}
		
		
		public function SetNewPos(newX:Number, newY:Number, newZ:Number):void {
			
			Debugger.Log("POWERUP SET NEW POS @" + "(" + newX.toFixed(2) + "," + newY.toFixed(2) + "," + newZ.toFixed(2) + ")" + this);
			TeleportTo(newX, newY, newZ);
			SetState(ST_ENTER);
		}
		
		public function GrabbedBy(gObj:GameObject):void { }
		
		public function canBeGrabbedBy(player:GameObject):Boolean {
			return true;
		}
		
		public function get canBeGrabbed():Boolean 
		{
			return _canBeGrabbed;
		}
		
		public function set canBeGrabbed(value:Boolean):void 
		{
			_canBeGrabbed = value;
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			_currentState.Update(dtMs);
		}
		
		override public function Reset():void {
			super.Reset();
			SetState(ST_READY);
		}
		
		
	}

}