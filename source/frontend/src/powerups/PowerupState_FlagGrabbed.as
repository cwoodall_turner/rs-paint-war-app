package powerups 
{
/**
	 * ...
	 * @author ...
	 */
	public class PowerupState_FlagGrabbed extends PowerupState
	{
		protected var _teamGrabbedId	: String	= "";
		public function PowerupState_FlagGrabbed(powerup:Powerup) 
		{
			super(powerup);
			_canReEnterSameState = true;
		}
		
		
		override public function EnterWithArgs(args:Array):void {
			super.EnterWithArgs(args);
			if (args != null && args.length > 0)	_teamGrabbedId	= args[0];
			else {
				Debugger.Log("WARNING! - Flag grabbed: enter state without args");
				_teamGrabbedId	= "1";
			}
			Enter();
		}
		
		override public function Enter():void {
			super.Enter();
			_powerup.ViewGoToAndStop("grabbed_team" + _teamGrabbedId);
			_powerup.canBeGrabbed	= true;
			_gObj.canBeHit	= true;
		}
		
	}

}
