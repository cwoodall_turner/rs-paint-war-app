package powerups 
{
import flash.display.MovieClip;
import flash.utils.Dictionary;

/**
	 * ...
	 * @author ...
	 */
	public class DogTag extends Powerup
	{
		
		public function DogTag(view:GameObjectView, shadow:MovieClip = null, $id:String = "dogtag") 
		{
			super(view, shadow, $id);
		}
		
		override public function addGameObjectStates():void {
			
			_states = new Dictionary();
			_states[ST_ENTER]	= new PowerupState_Enter(this);
			_states[ST_IDLE]	= new PowerupState_Idle(this);
			_states[ST_GRABBED]	= new PowerupState_Grabbed(this);
			_states[ST_READY]	= new PowerupState_Ready(this);
		}
		
		override public function GrabbedBy(gObj:GameObject):void {
			super.GrabbedBy(gObj);
			SetState(ST_GRABBED);
		}
		
		
	}

}