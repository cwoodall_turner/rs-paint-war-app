package powerups 
{
/**
	 * ...
	 * @author ...
	 */
	public class PowerupState extends GameObjectState
	{
		
		protected var _powerup		: Powerup	= null;
		
		public function PowerupState(powerup:Powerup) 
		{
			super(powerup);
			_powerup				= powerup;
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= false;
		}
		
	}

}
