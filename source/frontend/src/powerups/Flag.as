package powerups 
{
import flash.display.MovieClip;
import flash.utils.Dictionary;

/**
	 * ...
	 * @author ...
	 */
	public class Flag extends Powerup implements Capturable
	{
		protected var _grabbedByTeamId	: String	= "";
		
		protected var _timeLeftToClearMs	: int	= 0;
		protected var _timeToClearMs		: int	= 0;
		
		public function Flag(view:GameObjectView, shadow:MovieClip = null, $id:String = "flag") 
		{
			super(view, shadow, $id);
			_grabbedByTeamId	= null;
			_timeToClearMs		= Config.getValue("time_to_clear_flag_ms");
		}
		
		override public function addGameObjectStates():void {
			
			_states = new Dictionary();
			_states[ST_ENTER]	= new PowerupState_Enter(this);
			_states[ST_IDLE]	= new PowerupState_Idle(this);
			_states[ST_GRABBED]	= new PowerupState_FlagGrabbed(this);
			_states[ST_READY]	= new PowerupState_Ready(this);
		}
		
		override public function GrabbedBy(gObj:GameObject):void {
			super.GrabbedBy(gObj);
			Debugger.Log("_grabbedByTeamId: " + _grabbedByTeamId);
			Debugger.Log("gObj.teamId: " + gObj.teamId);
			if (_grabbedByTeamId != gObj.teamId)
			{
				_grabbedByTeamId	= gObj.teamId;
				SetState(ST_GRABBED, [gObj.teamId]);
				
				GameManager.Instance.currentLevel.onFlagGrabbed(this, gObj);
				
				_timeLeftToClearMs	= _timeToClearMs;
				Debugger.Log("_timeLeftToClearMs: " + _timeLeftToClearMs);
			}
		}
		
		override public function Update(dtMs:int):void
		{
			super.Update(dtMs);
			if (_timeLeftToClearMs > 0)
			{
				_timeLeftToClearMs -= dtMs;
				if (_timeLeftToClearMs <= 0)
				{
					resetGrabbedBy();
				}
			}
		}
		
		public function get grabbedByTeamId():String 
		{
			return _grabbedByTeamId;
		}
		
		public function set grabbedByTeamId(value:String):void 
		{
			_grabbedByTeamId = value;
		}
		
		public function get timeLeftToClearMs():int 
		{
			return _timeLeftToClearMs;
		}
		
		public function get timeToClearMs():int 
		{
			return _timeToClearMs;
		}
		
		public function get gObj():GameObject
		{
			return this;
		}
		
		public function resetGrabbedBy():void
		{
			GameManager.Instance.currentLevel.onFlagGrabbedTimeEnd(this);
			_grabbedByTeamId = null;
			SetState(ST_IDLE);
		}
		
		override public function canBeGrabbedBy(gObj:GameObject):Boolean {
			return (_grabbedByTeamId != gObj.teamId);
		}
		
		
	}

}