package powerups 
{
/**
	 * ...
	 * @author ...
	 */
	public class PowerupState_Ready extends PowerupState
	{
		public function PowerupState_Ready(powerup:Powerup) 
		{
			super(powerup);
		}
		
		override public function Enter():void {
			super.Enter();
			_powerup.speedX = _powerup.speedZ = _powerup.speedY	= 0;
			_powerup.visible	= false;
			_powerup.canBeGrabbed	= false;
			_powerup.TeleportToX( -1000);
			_gObj.View.gotoAndStop(1);
		}
		
		override public function Leave():void {
			super.Leave();
			_powerup.visible	= true;
		}
		
		override public function Update(dtMs:int):void {
			//super.Update(dtMs);
		}
		
	}

}
