package powerups 
{
/**
	 * ...
	 * @author ...
	 */
	public class PowerupState_Idle extends PowerupState
	{
		public function PowerupState_Idle(powerup:Powerup) 
		{
			super(powerup);
		}
		
		override public function Enter():void {
			super.Enter();
			_powerup.ViewGoToAndStop("idle");
			Debugger.Log("SETTING _canBeGrabbed true");
			_powerup.canBeGrabbed	= true;
			_gObj.canBeHit	= true;
		}
		
	}

}
