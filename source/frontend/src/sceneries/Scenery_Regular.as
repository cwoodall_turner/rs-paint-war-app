package sceneries 
{
import flash.display.DisplayObjectContainer;
import flash.display.MovieClip;
import flash.display.Sprite;

import scroll.ScrollableScreenElement;

import view.Camera;
import view.LayerFactory;

/**
	 * ...
	 * @author ...
	 */
	public class Scenery_Regular extends Scenery
	{
		
		public function Scenery_Regular(sceneryName:String) 
		{
			super(sceneryName);
			_type	= Scenery.REGULAR;
		}
		
		override protected function AddSceneryLayers():void 
		{
			super.AddSceneryLayers();
			
			var fullSceneryMc	: MovieClip	= ExternalLoader.Instance.GetNewInstance("SceneryAsset", "scenery" + _sceneryName.toString() + ".swf") as MovieClip;
			var i : int = fullSceneryMc.numChildren;
			while (i--) {
				var sceneryElementContainer	: Sprite	= new Sprite();
				var sceneryElement			: MovieClip	= fullSceneryMc.getChildAt(0) as MovieClip;
				sceneryElementContainer.addChild(sceneryElement);
				var sceneryLayer	: DisplayObjectContainer	= LayerFactory.Instance.GetLayer(sceneryElement.name.split("_")[0]);
				sceneryLayer.addChild(sceneryElementContainer);
				var depthHorizontal	: Number	= ConfigLoader.Instance.Config["scenery_" + _sceneryName.toString() + "_" + sceneryElement.name + "_depth_horizontal"];
				var depthVertical	: Number	= ConfigLoader.Instance.Config["scenery_" + _sceneryName.toString() + "_" + sceneryElement.name + "_depth_vertical"];
				var loopHorizontal	: Boolean	= (ConfigLoader.Instance.Config["scenery_" + _sceneryName.toString() + "_" + sceneryElement.name + "_loop_horizontal"] == "true");
				Debugger.Log("SCENERY LAYER " + sceneryElement.name + " - Depth Horiz: " + depthHorizontal + "  --  Depth Vert: " + depthVertical + "  --  Loop Horiz: " + loopHorizontal);
				Camera.Instance.AddScrollableLayer(
					new ScrollableScreenElement(sceneryElement.name, sceneryElementContainer, depthHorizontal, depthVertical, loopHorizontal)
				);
			}
		}
		
	}

}
