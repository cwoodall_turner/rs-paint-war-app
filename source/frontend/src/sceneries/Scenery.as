package sceneries 
{
import flash.events.EventDispatcher;

import scroll.ScrollableScreenElement;

import view.Camera;
import view.LayerFactory;

/**
	 * ...
	 * @author ...
	 */
	public class Scenery extends EventDispatcher
	{
		public static const REGULAR		: String	= "regular";
		
		protected var 	_type			: String	= "";
		protected var	_sceneryName	: String	= "1";
		
		protected var _hasScroll	: Boolean	= true;
		
		public function Scenery(sceneryName:String) 
		{
			_sceneryName	= sceneryName;
		}
		
		public function Prepare():void {
			AddSceneryLayers();
		}
		
		public function Update(dtMs:int):void { }
		public function Destroy():void { }
		
		protected function AddSceneryLayers():void {
			Debugger.Log("--- ADDING LAYER " + LayerFactory.Instance.GetLayer(LayerFactory.GAME_COMPLEX) + " (" + LayerFactory.GAME_COMPLEX + ")");
			Camera.Instance.AddScrollableLayer(
				new ScrollableScreenElement(LayerFactory.GAME_COMPLEX, LayerFactory.Instance.GetLayer(LayerFactory.GAME_COMPLEX), 1, 1, false)
			);
		}
		
		public function get name():String 
		{
			return _sceneryName;
		}
		
		public function get type():String 
		{
			return _type;
		}
		
		public function get hasScroll():Boolean 
		{
			return _hasScroll;
		}
		public function Reset():void { }
		
	}

}
