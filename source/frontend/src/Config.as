﻿package {
import flash.utils.Dictionary;

import utils.MathUtils;

/**
	 * ...
	 * @author ahodes
	 */
	public class Config {
		
		public static const swf_path_sounds		: String	= "sounds.swf";
		public static const swf_path_fx			: String	= "fx.swf";
		
		public static const music_id_silence	: String	= "MusicSilence";
		public static const sfx_id_hit			: String	= "Hit";
		
		public static var language	: String	= "";
		
		public static var online	: Boolean	= false;
		
		public static var shared_object_string	: String	= "sarasa";
		
		public static var game_width	: Number	= 600;
		public static var game_height	: Number	= 400;
		
		public static var snd_wght_pct			: Number	= 3;
		public static var external_swf_wght_pct	: Number	= 95;
		public static var master_volume			: Number	= 1;
		
		public static var level_number			: int	= 1;
		public static var num_levels			: int	= 4;
		public static var dificultad_inicial	: String	= "a";
		
		static public var current_level_music_id	: String	= "MusicInGame4";
		
		static public var reset_achievements_each_session	: Boolean	= false;
		
		// Sounds & Musics
		static public var global_sfx_ids	: Array	= null;
		static public var global_music_ids	: Array	= null;
		
		// DEBUG
		static public var debug_attack_area				: Boolean	= false;
		static public var debug							: Boolean	= false;
		static public var debug_range_to_check_fire_low	: Boolean	= false;
		
		// FLOOR
		public static var floor_y			: Number	= 300;
		
		// GameObjects To Debug
		private static var gObjectsToDebug	: Dictionary	= null;
		
		// ENEMIES
		public static var default_destination_tolerance_x	: Number	= 5;
		public static var default_destination_tolerance_y	: Number	= 5;
		public static var default_destination_tolerance_z	: Number	= 5;
		
		public static var guest_cn_id	: String		= "guest";
		public static var is_guest		: Boolean	= false;
		public static var user_name		: String	= "invitado";
		
		// BADGES
		public static var use_tracking	: Boolean	= true;
		public static var use_badges	: Boolean	= true;
		public static var test_badges	: Boolean	= true;

        public static var   app_id : String = "regshowpaintwar";
		

		
		
		public static var characted_id	: String = "mordecai";
		
		static public var show_grenade_hit_area	: Boolean	= false;
		
		static public var paint_obstacles : Boolean = true;
		

		
		// PATHFINDING / SURROUNDING
		static public var use_pathfinding_to_avoid_obstacles : Boolean = true;
		static public var surround_obstacles : Boolean = true;
		static public var debug_pathfinding	: Boolean	= true;
		static public var ranking_num_players : int = 5;

        public static var allow_guests: Boolean = false;
        public static var show_ranking: Boolean = false;
        public static var allow_cnla_credits: Boolean = false;

		public function Config() {	}
		
		public static function Initialize():void {
			
			if (language == "")	language	= getValue("language");
			
			online	= (getValue("online") == "true");
			
			shared_object_string	= getValue("shared_object_string");
			
			game_width	= getValue("game_width");
			game_height	= getValue("game_height");
			

			
			// ENEMIES
			default_destination_tolerance_x	= getValue("default_destination_tolerance_x");
			default_destination_tolerance_y	= getValue("default_destination_tolerance_y");
			default_destination_tolerance_z	= getValue("default_destination_tolerance_z");
			master_volume	= getValue("master_volume");
			
			num_levels	= getValue("num_levels");
			
			guest_cn_id	= getValue("guest_cn_id");
			
			characted_id	= getValue("default_characted_id");
			
			ranking_num_players	= getValue("ranking_num_players");
			
			// Sounds & Musics
			global_sfx_ids		= String(getValue("global_sfx_ids")).split(",");
			global_music_ids	= String(getValue("global_music_ids")).split(",");
			
			// DEBUG
			debug_attack_area	= (getValue("debug_attack_area") == "true");
			debug				= (getValue("debug") == "true");
			show_grenade_hit_area			= (getValue("show_grenade_hit_area") == "true");
			debug_range_to_check_fire_low	= (getValue("debug_range_to_check_fire_low") == "true");
			
			// PATHFINDING
			use_pathfinding_to_avoid_obstacles	= (getValue("use_pathfinding_to_avoid_obstacles") == "true");
			surround_obstacles	= (getValue("surround_obstacles") == "true");
			debug_pathfinding	= (getValue("debug_pathfinding") == "true");
			
			// PAINT FXs
			paint_obstacles	= (getValue("paint_obstacles") == "true");
			
			// BADGES
			use_tracking	= (ConfigLoader.Instance.Config["use_tracking"] == "true");
			use_badges	= (ConfigLoader.Instance.Config["use_badges"] == "true");
			test_badges	= (ConfigLoader.Instance.Config["test_badges"] == "true");

            allow_guests	= (ConfigLoader.Instance.Config["allow_guests"] == "true");
            show_ranking	= (ConfigLoader.Instance.Config["show_ranking"] == "true");
            allow_cnla_credits	= (ConfigLoader.Instance.Config["allow_cnla_credits"] == "true");

            app_id	= getValue("app_id");

			external_swf_wght_pct	= getValue("external_swf_wght_pct");
			if (ConfigLoader.Instance.Config["game_objects_to_debug"] != "") {
				gObjectsToDebug	= new Dictionary();
				var gObjectsToDebugArray	: Array	= String(ConfigLoader.Instance.Config["game_objects_to_debug"]).split(",");
				for each (var gObjId	: String in gObjectsToDebugArray) {
					gObjectsToDebug[gObjId]	= true;
				}
			}
			level_number			= getValue("default_level_nbr");
		}
		
		public static function UpdateValues(levelNumber:int = 1,levelDifficulty:String	= "c"):void {
			
			current_level_music_id	= getValue("music_id", levelNumber, levelDifficulty);
		}
		
		public static function GetInterpolatedValue(refValuePairsCfgName:String, ref:Number, difficulty:String = ""):Number {
			var prevRef		: Number = Number.NEGATIVE_INFINITY;
			var prevValue	: Number = 0;
			var refValuePairsStr	: String	= Config.getValueDifficultySuffix(refValuePairsCfgName, difficulty);
			var refValuePairsArr	: Array		= refValuePairsStr.split(";");
			var refValuePairsLen	: int		= refValuePairsArr.length;
			
			//Debugger.Log("GetInterpolatedValue [" + refValuePairsCfgName + "] ---  " + ref);
			
			for (var refValuePairsIdx	: int = 0; refValuePairsIdx < refValuePairsLen; refValuePairsIdx++) {
				var nextRefValuePairStr	: String	= refValuePairsArr[refValuePairsIdx];
				var nextRefValuePairArr	: Array		= nextRefValuePairStr.split(",");
				var nextRef		: Number	= nextRefValuePairArr[0];
				var nextValue	: Number	= nextRefValuePairArr[1];
				//Debugger.Log("  -- " + refValuePairsIdx + " --> " + StringUtils.ToCoord(prevRef, nextRef));
				if (prevRef <= ref && ref <= nextRef) {
					if (refValuePairsIdx == 0) {
						// Lower than 1st ref
						//Debugger.Log("    ---   Lower than 1st ref!  --> "  + nextValue);
						return nextValue;
					} else {
						// Middle Factor
						//Debugger.Log("    ---   linearInterpolationY!");
						return MathUtils.linearInterpolationY(prevValue,nextValue,prevRef,nextRef,ref);
					}
				} else if (refValuePairsIdx == refValuePairsLen - 1) {
					// Higher than last ref
					//Debugger.Log("    ---   Higher than last ref!  --> "  + nextValue);
					return nextValue;
				}
				prevRef		= nextRef;
				prevValue	= nextValue;
			}
			return prevValue;
		}
		
		public static function getValue(id:String, level:int = -999, difficulty:String = ""):*{
			
			//Debugger.Log("GET VALUE " + id + " [" + level + difficulty + "]");
			var key : String 		= "";
			var d 	: Dictionary	= ConfigLoader.Instance.Config;
			
			// Level specific
			if (level != -999) {
				// Level + difficulty
				key = id + "_level_" + level.toString() + difficulty;
				//Debugger.Log("Looking for specific (" + key + ")");
				if(d[key] != undefined){
					return d[key];
				}
				// Level
				key = id + "_level_" + level.toString();
				//Debugger.Log("Looking for specific (" + key + ")");
				if(d[key] != undefined){
					return d[key];
				}
			}
			
			// Global
			//Debugger.Log("Looking for specific (" + key + ")");
			return d[id];
		}
		
		public static function getValueSpecialSuffix(id:String, suffix:String = ""):*{
			
			//Debugger.Log("GET VALUE W/SUFFIX " + id + " [" + suffix + "]");
			var key : String 		= "";
			var d 	: Dictionary	= ConfigLoader.Instance.Config;
			
			// Difficulty specific
			if (suffix != "" && suffix != "por-defecto") {
				key = id + "--" + suffix;
				trace("Looking for specific (" + key + ")");
				if(d[key] != undefined){
					return d[key];
				}
			}
			
			// Global
			//trace("Looking for specific (" + id + ")");
			return d[id];
		}
		
		public static function getValueDifficultySuffix(id:String, difficulty:String = ""):*{
			
			//Debugger.Log("GET VALUE DIFFICULTY SUFFIX " + id + " [" + difficulty + "]");
			var key : String 		= "";
			var d 	: Dictionary	= ConfigLoader.Instance.Config;
			
			// Difficulty specific
			if (difficulty != "") {
				key = id + "--" + difficulty;
				//trace("Looking for specific (" + key + ")");
				if(d[key] != undefined){
					return d[key];
				}
			}
			
			// Global
			//trace("Looking for specific (" + id + ")");
			return d[id];
		}
		
		static public function debugThisGameObject(gameObjectId:String):Boolean 
		{
			return (debug && (!gObjectsToDebug || gObjectsToDebug[gameObjectId]));
		}
		
		static public function valueInList(valueStr:String, id:String, separador:String = ","):Boolean 
		{
			var possibleValuesStr	: String	= ConfigLoader.Instance.Config[id];
			var possibleValuesArr	: Array		= possibleValuesStr.split(separador);
			for each (var possibleValue : String in possibleValuesArr) {
				if (valueStr == possibleValue) return true;
			}
			return false;
		}
		
	}

}