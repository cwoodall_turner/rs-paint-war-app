package  
{
import communication.database.DBManager;
import communication.database.DBTeam;

import txt.TextLoader;

import utils.MathUtils;

public class Captain
	{	
		public function Boundaries() { }
		
		private static var _lastLevelPlayed			: int = 0;
		private static var _lastLevelObjectiveValue	: int = 0;
		private static var _lastLevelCoinsEarned	: int = 0;
		private static var _lastLevelScore			: int = 0;
		
		private static var _textsSaid	: Vector.<String> = new Vector.<String>();
		
		private static var _pos1TeamId	: String = "";
		private static var _pos2TeamId	: String = "";
		private static var _pos3TeamId	: String = "";
		
		private static var _lastWeekPos1TeamId	: String = "";
		private static var _lastWeekPos2TeamId	: String = "";
		private static var _lastWeekPos3TeamId	: String = "";
		
		private static var _lastDayResultsSeenStr	: String = "";
		
		private static var _positionsPerLevel:Vector.<Vector.<DBTeam>>	= null;
		
		
		public static function get possibleLines():Vector.<String>
		{
			var possibleLines : Vector.<String> = new Vector.<String>();
			
			// DEFAULT LINES
			var numDefaultLines : int = TextLoader.Instance.Texts["captain_num_default_texts"];
			for (var i : int = 1; i <= numDefaultLines; i++)
			{
				possibleLines.push("default_" + i.toString());
			}
			
			// POWERUPS
			var powerups:Array = ["grenades", "shots_distance", "shots_power", "extra_life_slots"];
			for each(var powerupId:String in powerups)
			{
				var powerupQty:int = DBManager.instance.user.getVar(powerupId);
				if (powerupQty == 0)
				{
					possibleLines.push("no_" + powerupId);
				}
			}
			
			// LAST LEVEL
			if (_lastLevelPlayed > 0)
			{
				// SCORE
				var highScore	: int	= TextLoader.Instance.Texts["captain_check_high_score_level_" + _lastLevelPlayed.toString()];
				var lowScore	: int	= TextLoader.Instance.Texts["captain_check_low_score_level_" + _lastLevelPlayed.toString()];
				if (_lastLevelScore >= highScore)
				{
					possibleLines.push("high_score_level_" + _lastLevelPlayed.toString());
				}
				else if (_lastLevelScore <= highScore)
				{
					possibleLines.push("low_score_level_" + _lastLevelPlayed.toString());
				}
				// COINS
				var highCoins	: int	= TextLoader.Instance.Texts["captain_check_high_coins_level_" + _lastLevelPlayed.toString()];
				if (_lastLevelCoinsEarned>= highCoins)
				{
					possibleLines.push("high_coins");
				}
			}
			
			// TEAM POSITION
			if (_pos1TeamId == DBManager.instance.user.teamId)
			{
				// 1st
				possibleLines.push("winning");
			}
			else
			{
				// 2nd OR 3rd
				possibleLines.push("losing_first_team_" + _pos1TeamId);
			}
			
			// LEVEL SCORES
			if (_positionsPerLevel != null)
			{
				for (var i : int = 0; i < _positionsPerLevel.length; i++)
				{
					var levelNbr : int = i + 1;
					if (_positionsPerLevel[i][0].teamId != DBManager.instance.user.teamId)
					{
						possibleLines.push("losing_level_" + levelNbr.toString());
					}
				}
			}
			
			return possibleLines;
		}
		
		public static function get mandatoryLines():Vector.<String>
		{
			var mandatoryLines : Vector.<String> = new Vector.<String>();
			
			// LAST WEEK RESULTS
			// If last week results were just shown, say something about it
			if (_lastWeekPos1TeamId != "")
			{
				if (_lastWeekPos1TeamId == DBManager.instance.user.teamId)
				{
					mandatoryLines.push("last_week_won");
					
					var numDefaultLines : int = 0;
					numDefaultLines	= TextLoader.Instance.Texts["captain_last_week_won_num_default_texts"];
					for (var i : int = 1; i <= numDefaultLines; i++)
					{
						mandatoryLines.push("last_week_won_default_" + i.toString());
					}
				}
				else
				{
					mandatoryLines.push("last_week_lost");
					
					var numDefaultLines : int = 0;
					numDefaultLines	= TextLoader.Instance.Texts["captain_last_week_lost_num_default_texts"];
					for (var i : int = 1; i <= numDefaultLines; i++)
					{
						mandatoryLines.push("last_week_lost_default_" + i.toString());
					}
				}
				_lastWeekPos1TeamId = _lastWeekPos2TeamId = _lastWeekPos3TeamId = "";
				
				return mandatoryLines;
			}
			
			// LONG TIME NO SEE
			Debugger.Log("saidText(long_time_no_see): " + saidText("long_time_no_see"));
			if (!saidText("long_time_no_see"))
			{
				var todayStr:String = DBManager.instance.game.getVar("today");
				Debugger.Log("lastDayResultsSeenStr: " + _lastDayResultsSeenStr);
				Debugger.Log("todayStr: " + todayStr);
				var today : Date = new Date(todayStr.split("-")[0], todayStr.split("-")[1], todayStr.split("-")[2]);
				var lastDayResultsSeen : Date = new Date(_lastDayResultsSeenStr.split("-")[0], _lastDayResultsSeenStr.split("-")[1], _lastDayResultsSeenStr.split("-")[2]);
				
				const msInADay : Number = 1000 * 60 * 60 * 24;
				var msDifference : Number = today.getTime() - lastDayResultsSeen.getTime();
				var daysDifference : int = Math.round(msDifference / msInADay);
				Debugger.Log("today: " + today.toDateString());
				Debugger.Log("lastDayResultsSeen: " + lastDayResultsSeen.toDateString());
				Debugger.Log("msDifference: " + msDifference);
				Debugger.Log("msInADay: " + msInADay);
				Debugger.Log("daysDifference: " + daysDifference);
				if (daysDifference > 1)
				{
					mandatoryLines.push("long_time_no_see");
				}
			}
			
			// MUSCLEMAN
			if (canBuyMuscleman && !saidText("buy_muscleman"))
			{
				mandatoryLines.push("buy_muscleman");
			}
			
			// LAST DAY
			var weekday : int = DBManager.instance.game.getVar("weekday");
			if (weekday == 7 && !saidText("last_day"))
			{
				mandatoryLines.push("last_day");
			}
			
			return mandatoryLines;
		}
		
		public static function getText(lineId:String, teamId:String):String
		{
			lineId = "captain_" + lineId;
			var rawLine : String = TextLoader.Instance.Texts[lineId + "_team_" + teamId];
			if (rawLine == null || rawLine == "")
			{
				Debugger.Log("WARNING! Couldn't find Captain text [" + lineId + "] for team " + teamId);
				rawLine	= TextLoader.Instance.Texts[lineId];
			}
			if (rawLine == null || rawLine == "")
			{
				Debugger.Log("WARNING! Couldn't find Captain text [" + lineId + "]");
				var numDefaultLines : int = TextLoader.Instance.Texts["captain_num_default_texts"];
				var defaultNbr : int = MathUtils.random(1, numDefaultLines, true);
				rawLine = TextLoader.Instance.Texts["captain_default_" + defaultNbr.toString()];
			}
			if (rawLine == null || rawLine == "")
			{
				Debugger.Log("WARNING! Couldn't find Captain text [captain_default]");
				rawLine = "";
			}
			
			var lineAfterReplacingSpecialStrings : String = replaceSpecialStrings(rawLine);
			var textLabels 		: Vector.<String> = getTextLabels(lineAfterReplacingSpecialStrings);
			
			// Replace labels
			var lineAfterReplacingLabels : String = lineAfterReplacingSpecialStrings;
			for each(var textLabel:String in textLabels)
			{
				Debugger.Log("TEXTLABEL: " + textLabel);
				while (lineAfterReplacingLabels.indexOf(textLabel) >= 0)
				{
					lineAfterReplacingLabels = lineAfterReplacingLabels.replace("[" + textLabel + "]", TextLoader.Instance.Texts[textLabel]);
				}
			}
			
			return lineAfterReplacingLabels;
		}
		
		static private function replaceSpecialStrings(s:String):String 
		{
			// {team}
			while (s.indexOf("{team}") >= 0)
			{
				s	= s.replace("{team}", DBManager.instance.user.teamId);
			}
			return s;
		}
		
		static private function getTextLabels(rawLine:String):Vector.<String>
		{
			var textLabels : Vector.<String> = new Vector.<String>();
			var possibleLabelsStr : String = TextLoader.Instance.Texts["text_labels"];
			var possibleLabelsArr : Array	= possibleLabelsStr.split(",");
			for each(var possibleLabel:String in possibleLabelsArr)
			{
				if (rawLine.indexOf(possibleLabel) >= 0)
				{
					textLabels.push(possibleLabel);
				}
			}
			
			return textLabels;
		}
		
		
		static public function sayText(textId:String):void
		{
			Debugger.Log("CAPTAIN sayText: " + textId);
			resetLevelStats();
			_textsSaid.push(textId);
		}
		static public function sayNothing():void
		{
			resetLevelStats();
		}
		
		static private function saidText(textId:String):Boolean 
		{
			for each(var textSaid:String in _textsSaid)
			{
				Debugger.Log("_textsSaid... " + textSaid);
				if (textId == textSaid)
				{
					return true;
				}
			}
			return false;
		}
		
		static private function resetLevelStats():void 
		{
			_lastLevelPlayed = _lastLevelObjectiveValue = _lastLevelCoinsEarned = _lastLevelScore = 0;
		}
		
		
		static public function updateAfterLevelFinish():void
		{
			_lastLevelPlayed 			= GameManager.Instance.currentLevel.number;
			_lastLevelObjectiveValue	= GameManager.Instance.currentLevel.objectiveValue;
			_lastLevelCoinsEarned		= GameManager.Instance.currentLevel.coinsGrabbed + GameManager.Instance.currentLevel.getCoinsPerObjective();
			_lastLevelScore				= GameManager.Instance.currentLevel.getScore();
		}
		
		static public function updateTeamsPosition(pos1Team:DBTeam, pos2Team:DBTeam, pos3Team:DBTeam):void 
		{
			_pos1TeamId = pos1Team.teamId;
			_pos2TeamId = pos2Team.teamId;
			_pos3TeamId = pos3Team.teamId;
		}
		
		static public function updateLastWeekTeamsPosition(pos1Team:DBTeam, pos2Team:DBTeam, pos3Team:DBTeam):void 
		{
			_lastWeekPos1TeamId = pos1Team.teamId;
			_lastWeekPos2TeamId = pos2Team.teamId;
			_lastWeekPos3TeamId = pos3Team.teamId;
		}
		
		static public function updateOnDayComplete():void 
		{
			_textsSaid	= new Vector.<String>();
		}
		
		static public function updateOnMapPrepare():void 
		{
			if (_lastDayResultsSeenStr == "")
			{
				_lastDayResultsSeenStr	= DBManager.instance.user.getVar("last_day_results_seen");
			}
		}
		
		static public function updateLevelsScores(positionsPerLevel:Vector.<Vector.<DBTeam>>):void 
		{
			_positionsPerLevel	= positionsPerLevel;
		}
		
		
		
		
		// CHECKS
		private static function get canBuyMuscleman():Boolean
		{
			if ((Config.getValue("muscleman_available") == "true") && (DBManager.instance.user.getVar("muscleman") == "0"))
			{
				var cost : int = Config.getValue("muscleman_cost");
				if (DBManager.instance.user.money >= cost)
				{
					return true;
				}
			}
			return false;
		}
	}
}