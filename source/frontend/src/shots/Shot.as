package shots 
{
import flash.display.MovieClip;

/**
	 * ...
	 * @author ...
	 */
	public class Shot extends GameObject
	{
		public static const ST_MOVE		: String	= "move";
		public static const ST_HIT		: String	= "hit";
		
		public static const ID_REGULAR_SHOT	: String	= "shot";
		public static const ID_GRENADE		: String	= "grenade";
		
		public static const SPECIAL_NONE		: uint	= 0;
		public static const SPECIAL_POWER		: uint	= 1;
		public static const SPECIAL_DISTANCE	: uint	= 2;
		public static const SPECIAL_LOW			: uint	= 4;
		
		protected var _shooter	: GameObject	= null;
		protected var _possibleTargets	: uint	= 0;
		
		protected var _chargePercentage	: Number	= 0;
		
		protected var _specials	: uint	= 0;
		
		
		public function Shot(view:GameObjectView, shadow:MovieClip = null, $id:String="shot")
		{
			super(view, shadow, $id);
			_removeFromCanvasOnReset	= false;
			
			addGameObjectStates();
			
			SetState(ST_READY);
		}
		
		public function addGameObjectStates():void { }
		
		override public function Update(dtMs:int):void {
			//Debugger.Log("SHOT UPDATE");
			super.Update(dtMs);
			_currentState.Update(dtMs);
		}
		
		override public function Reset():void {
			super.Reset();
			SetState(ST_READY);
		}
		
		
		public function SetNewPos(newX:Number, newY:Number, newZ:Number, copyShooterFacing:Boolean = true):void {
			
			//Debugger.Log("SHOT SET NEW POS @" + "(" + newX.toFixed(2) + "," + newY.toFixed(2) + "," + newZ.toFixed(2) + ")  -  " + this);
			
			if (copyShooterFacing && shooter != null)
			{
				if (shooter.IsFacingRight) FaceRight();
				else if (shooter.IsFacingLeft) FaceLeft();
				else if (shooter.IsFacingFront) FaceFront();
				else if (shooter.IsFacingBack) FaceBack();
			}
			
			TeleportTo(newX, newY, newZ);
			SetState(ST_MOVE);
		}
		
		public function get ready():Boolean {
			return IsCurrentState(ST_READY);
		}
		
		public function HitPlayer(player:GameObject):void {
			//SetState(ST_READY);
			SetState(ST_HIT, [player]);
		}
		public function HitEnemy(enemy:GameObject):void {
			//SetState(ST_READY);
			SetState(ST_HIT, [enemy]);
		}
		public function HitObstacle(obstacle:GameObject):void {
			//SetState(ST_READY);
			SetState(ST_HIT);
		}
		
		public function resetChargePercentage():void 
		{
			_chargePercentage = -1;
		}
		
		public function setChargePercentage(value:Number):void
		{
			_chargePercentage = value;
		}
		
		public function get possibleTargets():uint 
		{
			return _possibleTargets;
		}
		
		public function set possibleTargets(value:uint):void 
		{
			_possibleTargets = value;
		}
		
		public function get shooter():GameObject 
		{
			return _shooter;
		}
		
		public function set shooter(value:GameObject):void 
		{
			_shooter = value;
			teamId = _shooter.teamId;
		}
		
		public function get chargePercentage():Number 
		{
			return _chargePercentage;
		}
		
		public function get hasChargePercentage():Boolean 
		{
			return (_chargePercentage >= 0);
		}
		
		public function get specials():uint 
		{
			return _specials;
		}
		
		public function set specials(value:uint):void 
		{
			_specials = value;
		}
		
	}

}