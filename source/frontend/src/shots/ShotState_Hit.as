package shots 
{
import enemies.Enemy;

import obstacles.Obstacle;

import players.Player;

import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class ShotState_Hit extends ShotState
	{
		private var _hitTarget		: String	= "";
		private var _hitDirection	: String	= "";
		
		public function ShotState_Hit(shot:Shot) 
		{
			_checkScreenLimits	= false;
			super(shot);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit = Targets.NONE;
			_gObj.canBeHit	= false;
		}
		
		override public function EnterWithArgs(args:Array):void {
			super.EnterWithArgs(args);
			if (args != null && args.length > 0)
			{
				var target:* = args[0];
				if (target is Obstacle) _hitTarget = "obstacle";
				else if (target is Enemy || target is Player) _hitTarget = "character";
				else _hitTarget = "floor";
				
				// TMP
				if (target is GameObject && _shot.z <= (target as GameObject).z)
				{
					_shot.TeleportToZ((target as GameObject).z + 1);
				}
			}
			if (args != null && args.length > 1)
			{
				_hitDirection = args[1];
			} else
			{
				_hitDirection = CollisionManager.UP;
			}
			Enter();
		}
		
		override public function Enter():void {
			super.Enter();
			
			_shot.speedX = _shot.speedY = _shot.speedZ = 0;
			
			var frameLbl : String	= "hit_" + _shot.facing + "_" + _hitTarget;
			//if (_hitTarget == "obstacle" && _hitDirection == CollisionManager.UP)
			//{
				//frameLbl	= "hit_front_floor";
			//}
			Debugger.Log("SHOT HIT " + _hitTarget + " FROM " + _hitDirection + " - (" + frameLbl + ")");
			_shot.ViewGoToAndStop(frameLbl);
			_shot.View.visible = true;
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			if (MovieClipUtils.AnimationEnded(_shot.innerMc))
			{
				_shot.SetState(GameObject.ST_READY);
			}
		}
	}

}
