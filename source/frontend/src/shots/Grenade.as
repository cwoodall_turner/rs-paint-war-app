package shots 
{
import flash.display.MovieClip;
import flash.utils.Dictionary;

import players.Player;

/**
	 * ...
	 * @author ...
	 */
	public class Grenade extends Shot
	{	
		/* Badges */
		private var _enemiesToKillForBadge	: int	= 0;
		
		public function Grenade(view:GameObjectView, shadow:MovieClip = null, $id:String="shot")
		{
			super(view, shadow, $id);
			_configPrefix = Shot.ID_GRENADE + "_";
		}
		
		override public function addGameObjectStates():void {
			
			_states = new Dictionary();
			_states[ST_MOVE]	= new ShotState_GrenadeMove(this);
			_states[ST_HIT]		= new ShotState_Explode(this);
			_states[ST_READY]	= new ShotState_Ready(this);
		}
		
		override public function SetNewPos(newX:Number, newY:Number, newZ:Number, copyShooterFacing:Boolean = true):void {
			
			super.SetNewPos(newX, newY, newZ, copyShooterFacing);
			
			if (shooter is Player)
			{
				_enemiesToKillForBadge	= Config.getValue("badge_grenade_combo_num_enemies");
			}
			else
			{
				_enemiesToKillForBadge = 0;
			}
		}
		
		public function onEnemyKilled():void 
		{
			if (_enemiesToKillForBadge > 0)
			{
				_enemiesToKillForBadge--;
				if (_enemiesToKillForBadge == 0)
				{
					MainApp.instance.EarnBadge("grenade_combo");
				}
			}
		}
	}

}