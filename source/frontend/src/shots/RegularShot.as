package shots 
{
import flash.display.MovieClip;
import flash.utils.Dictionary;

/**
	 * ...
	 * @author ...
	 */
	public class RegularShot extends Shot
	{
		
		public function RegularShot(view:GameObjectView, shadow:MovieClip = null, $id:String="shot")
		{
			super(view, shadow, $id);
			_configPrefix = Shot.ID_REGULAR_SHOT + "_";
		}
		
		override public function addGameObjectStates():void {
			
			_states = new Dictionary();
			_states[ST_MOVE]	= new ShotState_Move(this);
			_states[ST_HIT]		= new ShotState_Hit(this);
			_states[ST_READY]	= new ShotState_Ready(this);
		}
		
	}

}