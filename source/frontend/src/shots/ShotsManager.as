package shots
{
import enemies.Enemy;

import flash.display.MovieClip;

import players.Player;

import utils.MovieClipUtils;

import view.Camera;
import view.GameObjectToScreenMapper;

/**
	 * ...
	 * @author ...
	 */
	public class ShotsManager
	{
		
		private static var _instance:ShotsManager = null;
		
		private var _shots	:Vector.<Shot>		= null;
		private var _lastAttackSeq	: int = 0;
		
		public function ShotsManager()
		{
			_instance = this;
			_shots	= new Vector.<Shot>();
		}
		
		public function AddShot(shooter:GameObject, possibleTargets:uint, shotSpecials:uint = 0):void {
			
			//Debugger.Log("ADDING SHOT... (" + shooter.id + ")");
			
			var shotRefAsset	: MovieClip	= null;
			
			//TMP: Usar interface Shooter
			if (shooter is Player) shotRefAsset	= (shooter as Player).shotRef;
			else if (shooter is Enemy) shotRefAsset	= (shooter as Enemy).shotRef;
			
			if (!shotRefAsset) {
				Debugger.Log("Warning: Trying to add Shot but no ref found for " + shooter);
				return;
			}
			var shotZPos	: Number	= shooter.z;
			if (shooter.IsFacingBack) shotZPos -= 1;
			else shotZPos += 1;
			
			var newPosition	: GameAreaPoint	= Camera.Instance.ConvertScreenPosToGamePosWithFixedZ(MovieClipUtils.ScreenPosition(shotRefAsset), shotZPos); 
			var newPosX	: Number	= newPosition.x;
			var newPosY	: Number	= newPosition.y;
			var newPosZ	: Number	= newPosition.z;
			MovieClipUtils.Remove(shotRefAsset);
			
			//var shooterFinalId	: String	= shooter.id;
			var teamId	: String	= shooter.teamId;
			
			var shotId	: String	= Shot.ID_REGULAR_SHOT + "-" + teamId;
			var newShot	: Shot	= GetNextAvailableShot(shotId);
			if (!newShot) {
				Debugger.Log("No shot available. Creating new shot...");
				var shotAsset	: GameObjectView	= ExternalLoader.Instance.GetNewInstance("Shot" + teamId + "Asset", "shots.swf") as GameObjectView;
				var shadowAsset	: MovieClip	= ExternalLoader.Instance.GetNewInstance("ShotShadowAsset", "shots.swf") as MovieClip;
				newShot = new RegularShot(shotAsset, shadowAsset, Shot.ID_REGULAR_SHOT);
				_shots.push(newShot);
				GameObjectToScreenMapper.instance.AddGameObject(newShot);
			} else {
				Debugger.Log("Reusing Shot " + newShot);
			}
			
			//Debugger.LogStack();
			newShot.specials	= shotSpecials;
			newShot.resetChargePercentage();
			newShot.possibleTargets = possibleTargets;
			newShot.shooter = shooter;
			newShot.SetNewPos(newPosX, newPosY, newPosZ);
			
			Sfx.PlaySound("Shot");
		}
		
		public function AddGrenade(shooter:GameObject, possibleTargets:uint, chargePercentage:Number = -1):void {
			
			//Debugger.Log("ADDING SHOT... (" + shooter.id + ")");
			
			var shotRefAsset	: MovieClip	= null;
			
			//TMP: Usar interface Shooter
			if (shooter is Player) shotRefAsset	= (shooter as Player).shotRef;
			else if (shooter is Enemy) shotRefAsset	= (shooter as Enemy).shotRef;
			
			if (!shotRefAsset) {
				Debugger.Log("Warning: Trying to add Shot but no ref found for " + shooter);
				return;
			}
			var shotZPos	: Number	= shooter.z;
			if (shooter.IsFacingBack) shotZPos -= 1;
			else shotZPos += 1;
			
			var newPosition	: GameAreaPoint	= Camera.Instance.ConvertScreenPosToGamePosWithFixedZ(MovieClipUtils.ScreenPosition(shotRefAsset), shotZPos);
			var newPosX	: Number	= newPosition.x;
			var newPosY	: Number	= newPosition.y;
			var newPosZ	: Number	= newPosition.z;
			MovieClipUtils.Remove(shotRefAsset);
			
			//var shooterFinalId	: String	= shooter.id;
			var teamId	: String	= shooter.teamId;
			
			var grenadeId	: String	= Shot.ID_GRENADE + "-" + teamId;
			var newShot	: Shot	= GetNextAvailableShot(grenadeId);
			if (!newShot) {
				Debugger.Log("No shot available. Creating new shot...");
				var shotAsset	: GameObjectView	= ExternalLoader.Instance.GetNewInstance("Grenade" + teamId + "Asset", "shots.swf") as GameObjectView; // TMP
				var shadowAsset	: MovieClip	= ExternalLoader.Instance.GetNewInstance("GrenadeShadowAsset", "shots.swf") as MovieClip; // TMP
				newShot = new Grenade(shotAsset, shadowAsset, grenadeId);
				_shots.push(newShot);
				GameObjectToScreenMapper.instance.AddGameObject(newShot);
			} else {
				Debugger.Log("Reusing Grenade " + newShot);
			}
			//Debugger.LogStack();
			if (chargePercentage >= 0)	newShot.setChargePercentage(chargePercentage);
			else newShot.resetChargePercentage();
			newShot.possibleTargets = possibleTargets;
			newShot.shooter = shooter;
			newShot.SetNewPos(newPosX, newPosY, newPosZ);
			
			Sfx.PlaySound("Grenade");
		}
		
		
		
		private function GetNextAvailableShot(id:String):Shot 
		{
			var len : int = _shots.length;
			for (var i : int = 0; i < len; i++) {
				if (_shots[i].id == id && _shots[i].ready) return _shots[i];
			}
			return null;
		}
		
		public function Update(dtMs:int):void
		{
			//Debugger.Log("Shots manager update");
			var i:int = _shots.length;
			while (i--)
			{
				//Debugger.Log("Shot " + i + " update");
				_shots[i].Update(dtMs);
			}
		}
		
		public static function get instance():ShotsManager
		{
			return _instance;
		}
		
		
		public function get theShots():Vector.<Shot> 
		{
			return _shots;
		}
		
		public function get lastAttackSeq():int 
		{
			return _lastAttackSeq;
		}
		
		public function set lastAttackSeq(value:int):void 
		{
			_lastAttackSeq = value;
		}
		
		public function Reset():void {
			for each (var shot : Shot in _shots) {
				shot.Reset();
				GameObjectToScreenMapper.instance.RemoveGameObject(shot);
				shot.Destroy();
			}
			_shots = new Vector.<Shot>();
			_lastAttackSeq = 0;
		}
	
	}

}