package shots 
{
/**
	 * ...
	 * @author ...
	 */
	public class ShotState_Ready extends ShotState
	{
		protected var _speedX				: Number	=.4;
		protected var _speedZ				: Number	= 0;
		
		public function ShotState_Ready(shot:Shot) 
		{
			super(shot);
			_onReEnterPlayInnerMcFromStart	= false;
			_checkScreenLimits	= false;
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= false;
		}
		
		override public function Enter():void {
			super.Enter();
			_checkScreenLimits	= false;
			_shot.ViewGoToAndStop("ready");
			_shot.View.visible = false;
			_shot.TeleportToX( -1000);
		}
		
	}

}
