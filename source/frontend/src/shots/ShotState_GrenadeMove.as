package shots 
{
import fx.FxManager;

import utils.MathUtils;

/**
	 * ...
	 * @author ...
	 */
	public class ShotState_GrenadeMove extends ShotState
	{
		protected var _speedHorizontal	: Number	= .1;
		protected var _speedVertical	: Number	= 0;
		protected var _maxSpeedDown		: Number	= 0;
		protected var _goingUp			: Boolean	= true;
		protected var _gravity			: Number	= 0;
		
		public function ShotState_GrenadeMove(shot:Shot) 
		{
			super(shot);
			_onReEnterPlayInnerMcFromStart	= false;
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit = Targets.NONE;
			_gObj.canBeHit	= false;
		}
		
		override public function Enter():void {
			super.Enter();
			//var shooterFinalId	: String	= _shot.shooter.characterId;
			//Debugger.Log("_shot.configPrefix: " + _shot.configPrefix);
			
			_shot.speedX	= _shot.speedY = _shot.speedZ = 0;
			
			var shooterShotPrefix : String = _shot.shooter.configPrefix + _shot.configPrefix;
			if (_shot.hasChargePercentage)
			{
				var minValue : Number = Config.getValue(shooterShotPrefix + "speed_horizontal_range_min");
				var maxValue : Number = Config.getValue(shooterShotPrefix + "speed_horizontal_range_max");
				_speedHorizontal	= MathUtils.linearInterpolationY(minValue, maxValue, 0, 100, _shot.chargePercentage);
				
				minValue = Config.getValue(shooterShotPrefix + "speed_vertical_range_min");
				maxValue = Config.getValue(shooterShotPrefix + "speed_vertical_range_max");
				_speedVertical	= MathUtils.linearInterpolationY(minValue, maxValue, 0, 100, _shot.chargePercentage);
				
				minValue = Config.getValue(shooterShotPrefix + "initial_speed_up_range_min");
				maxValue = Config.getValue(shooterShotPrefix + "initial_speed_up_range_max");
				_shot.speedY	= MathUtils.linearInterpolationY(minValue, maxValue, 0, 100, _shot.chargePercentage) * -1;
			}
			else
			{
				_speedHorizontal	= Config.getValue(shooterShotPrefix + "speed_horizontal");
				_speedVertical		= Config.getValue(shooterShotPrefix + "speed_vertical");
				_shot.speedY	= -Config.getValue(shooterShotPrefix + "initial_speed_up");
			}
			
			_shot.attackPower	= Config.getValue(shooterShotPrefix + "power");
			
			
			if (_shot.IsFacingRight) _shot.speedX = _speedHorizontal;
			else if (_shot.IsFacingLeft) _shot.speedX = -_speedHorizontal;
			else if (_shot.IsFacingFront) _shot.speedZ = _speedVertical;
			else	_shot.speedZ = -_speedVertical;
			
			_maxSpeedDown	= Config.getValue(shooterShotPrefix + "max_speed_down");
			if (_shot.speedY < 0)
			{
				_goingUp = true;
				_gravity	= Config.getValue(shooterShotPrefix + "gravity_going_up");
			} else {
				_goingUp = false;
				_gravity	= Config.getValue(shooterShotPrefix + "gravity_going_down");
			}
			
			_shot.ViewGoToAndStop("move_" + _shot.facing);
			_shot.visible	= true;
			
		}
		
		override public function Leave():void {
			super.Leave();
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (_shot.speedY > 0 && _goingUp)
			{
				_goingUp = false;
				_gravity	= Config.getValue(_shot.shooter.configPrefix + _shot.configPrefix + "gravity_going_down");
			}
			
			_shot.speedY += _gravity * dtMs;
			if (_shot.speedY > _maxSpeedDown) _shot.speedY = _maxSpeedDown;
			
			var dX	: Number	= _shot.speedX * dtMs;
			var dY	: Number	= _shot.speedY * dtMs;
			var dZ	: Number	= _shot.speedZ * dtMs;
			
			_shot.MoveX(dX);
			_shot.MoveY(dY);
			_shot.MoveZ(dZ);
			
			if (_shot.y >= Config.floor_y)
			{
				_shot.TeleportToY(Config.floor_y);
				_shot.SetState(Shot.ST_HIT, ["floor", CollisionManager.UP]);
				Sfx.PlaySound(Shot.ID_GRENADE + "HitFloor");
				FxManager.instance.shotHitFloor(_shot);
			}
		}
		
	}

}
