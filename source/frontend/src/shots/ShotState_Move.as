package shots 
{
import fx.FxManager;

/**
	 * ...
	 * @author ...
	 */
	public class ShotState_Move extends ShotState
	{
		protected var _speedHorizontal	: Number	= .1;
		protected var _speedVertical	: Number	= 0;
		protected var _maxSpeedDown		: Number	= 0;
		protected var _goingUp			: Boolean	= true;
		protected var _gravity			: Number	= 0;
		
		public function ShotState_Move(shot:Shot) 
		{
			super(shot);
			_onReEnterPlayInnerMcFromStart	= false;
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit = _shot.possibleTargets;
			_gObj.canBeHit	= false;
		}
		
		override public function Enter():void {
			super.Enter();
			//var shooterFinalId	: String	= _shot.shooter.characterId;
			Debugger.Log("_shot.configPrefix: " + _shot.configPrefix);
			
			var shooterShotPrefix 	: String = _shot.shooter.configPrefix + _shot.configPrefix;
			
			var specialDistanceSuffix		: String	= "";
			if (_shot.specials & Shot.SPECIAL_DISTANCE) specialDistanceSuffix	= "_special_distance";
			if (_shot.specials & Shot.SPECIAL_LOW) specialDistanceSuffix 		+= "_special_low";
			
			var specialPowerSuffix	: String	= "";
			if (_shot.specials & Shot.SPECIAL_POWER) specialPowerSuffix = "_special_power";
			
			_speedHorizontal	= Config.getValue(shooterShotPrefix + "speed_horizontal" + specialDistanceSuffix);
			_speedVertical		= Config.getValue(shooterShotPrefix + "speed_vertical" + specialDistanceSuffix);
			_shot.attackPower	= Config.getValue(shooterShotPrefix + "power" + specialPowerSuffix);
			_maxSpeedDown		= Config.getValue(shooterShotPrefix + "max_speed_down" + specialDistanceSuffix);
			_shot.speedY		= -Config.getValue(shooterShotPrefix + "initial_speed_up" + specialDistanceSuffix);
			
			_shot.speedX	= _shot.speedZ = 0;
			if (_shot.IsFacingRight) _shot.speedX = _speedHorizontal;
			else if (_shot.IsFacingLeft) _shot.speedX = -_speedHorizontal;
			else if (_shot.IsFacingFront) _shot.speedZ = _speedVertical;
			else	_shot.speedZ = -_speedVertical;
			
			if (_shot.speedY < 0)
			{
				_goingUp = true;
				_gravity	= Config.getValue(shooterShotPrefix + "gravity_going_up" + specialDistanceSuffix);
			} else {
				_goingUp = false;
				_gravity	= Config.getValue(shooterShotPrefix + "gravity_going_down" + specialDistanceSuffix);
			}
			
			var frameLbl	: String	= "move_" + _shot.facing;
			if (_shot.specials & Shot.SPECIAL_POWER) frameLbl += "_powerful";
			Debugger.Log("frameLbl: " + frameLbl);
			_shot.ViewGoToAndStop(frameLbl);
			_shot.visible	= true;
			
			_gObj.canHit = _shot.possibleTargets;
			
		}
		
		override public function Leave():void {
			super.Leave();
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (_shot.speedY > 0 && _goingUp)
			{
				_goingUp = false;
				var specialDistanceSuffix		: String	= "";
				if (_shot.specials & Shot.SPECIAL_DISTANCE) specialDistanceSuffix = "_special_distance";
				_gravity	= Config.getValue(_shot.shooter.configPrefix + _shot.configPrefix + "gravity_going_down" + specialDistanceSuffix);
			}
			
			_shot.speedY += _gravity * dtMs;
			if (_shot.speedY > _maxSpeedDown) _shot.speedY = _maxSpeedDown;
			
			var dX	: Number	= _shot.speedX * dtMs;
			var dY	: Number	= _shot.speedY * dtMs;
			var dZ	: Number	= _shot.speedZ * dtMs;
			
			_shot.MoveX(dX);
			_shot.MoveY(dY);
			_shot.MoveZ(dZ);
			
			if (_shot.y >= Config.floor_y)
			{
				_shot.TeleportToY(Config.floor_y);
				_shot.SetState(Shot.ST_HIT, ["floor", CollisionManager.UP]);
				Sfx.PlaySound(Shot.ID_REGULAR_SHOT + "HitFloor");
				FxManager.instance.shotHitFloor(_shot);
			}
		}
		
	}

}
