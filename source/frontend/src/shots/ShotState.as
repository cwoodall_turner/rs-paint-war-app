package shots 
{
	/**
	 * ...
	 * @author ...
	 */
	public class ShotState extends GameObjectState
	{
		
		protected var _shot	: Shot	= null;
		
		
		public function ShotState(shot:Shot) 
		{
			super(shot);
			_shot	= shot;
		}
		
		override public function Enter():void
		{
			super.Enter();
			if (_shot.canHit)
			{
				ShotsManager.instance.lastAttackSeq++;
				_shot.attackSeq = ShotsManager.instance.lastAttackSeq;
			}
		}
		
	}

}
