	/**
* ...
* @author Alfredo Hodes
* @version 0.1
*/

package  {
import com.reintroducing.sound.SoundManager;

import flash.display.MovieClip;
import flash.media.Sound;
import flash.media.SoundMixer;
import flash.media.SoundTransform;
import flash.utils.Dictionary;

public class Sfx {
		
		
		// Music & sfx Ids
		private static var	_sfxIds		: Vector.<String>	= new Vector.<String>();
		private static var	_musicIds	: Vector.<String>	= new Vector.<String>();
		private static var _currentMusicPlaying	: String	= "";
		
		// Volumes
		private static var _volumes	: Dictionary	= new Dictionary(true);
		
		private static var _masterVolume	: Number	= 1;
		private static var _muted			:Boolean	= false;
		
		public static function get Muted() : Boolean	{	return	_muted;	}
		public static function get Volumes() : Dictionary	{	return	_volumes;	}
		
		private static var _soundManager : SoundManager = null;
		
		
		
		public function Sfx() {
			
		}
		
		
		public static function Init():void {
			_soundManager = SoundManager.getInstance();
			
			// Load Master Volume
			Volume =  Config.master_volume;
		}
		
		
		public static function AddSound(id:String, sound:Sound, isMusic:Boolean	= false):void {
			//Debugger.Log("<--SFX-->  Add sound: " + id);
			if (SoundExists(id)) {
				Debugger.Log("WARNING! TRYING TO ADD ALREADY ADDED SOUND (" + id + "). Skipping...");
				return;
			}
			_soundManager.AddSound(id, sound);
			_volumes[id]	= GetVolumeFromConfig(id);
			if (isMusic) _musicIds.push(id);
			else _sfxIds.push(id);
		}
		
		static public function SoundExists(id:String):Boolean {
			return (_sfxIds.indexOf(id) >= 0 || _musicIds.indexOf(id) >= 0);
		}
		
		
		/**
		 * Play a sound in loop
		 *
		 * @param sound's class identifier in the library
		 *
		 * @param If false, this same sound (id) cannot be played again until it stops
		 * 
		 * @param A number between 0 and n for this sound volume (1 = 100%)
		 * 
		 * @param A number between -1 and 1 for its panning ( 0 is the center )
		 * 
		 */
		public static function PlaySoundLoop(id :String, allowOverlapping :Boolean = true, gain :Number = -1, panning :Number = 0,startTime:Number = 0) :void {
			
			//Debugger.Log("<--SFX-->  PlaySoundLoop: [" + id + "]");
			if (allowOverlapping || !IsPlaying(id)) {
				if (gain == -1) {
					gain = _volumes[id];
				}
				//Debugger.Log("<--SFX-->  Loop con vol: " + gain);
				_soundManager.playSound(id, gain * Volume, startTime, int.MAX_VALUE, panning);
			} else {
				//Debugger.Log("<--SFX-->  No permite overlapping");
			}
		}
		
		
		
		/**
		 * Play a sound once and then stop it once it's finished.
		 *
		 * @param sound's class identifier in the library
		 *
		 * @param If false, this same sound (id) cannot be played again until it stops (currently not working :P  )
		 * 
		 * @param A number between 0 and n for this sound volume (1 = 100%)
		 * 
		 * @param A number between -1 and 1 for its panning ( 0 is the center )
		 * 
		 * @param Amount of loops to play
		 * 
		 */
		public static function PlaySound(id :String, allowOverlapping :Boolean = true, gain :Number = -1, panning :Number = 0, numLoops : int = 0, startTime:Number = 0, onComplete:Function = null) :void {
			
			//Debugger.Log("<--SFX-->  PlaySound: [" + id + "]");
			//Debugger.LogStack();
			if (allowOverlapping || !IsPlaying(id)) {
				if (gain == -1) {
					gain = _volumes[id];
				}
				//Debugger.Log("<--SFX-->      Snd [" + id + "] con vol: " + gain);
				////Debugger.Log("<--SFX-->  Play Sound: " + id + " @ " + (gain * Volume).toString());
				_soundManager.playSound(id, gain * Volume, startTime, numLoops, panning, onComplete);
			} else {
				//Debugger.Log("<--SFX-->  No permite overlapping");
			}
		}
		
		/**
		 * If it's loading, it returns a number between 0 and 1
		 * (bytesLoaded / bytesTotal). If not loading, it returns 1, so you should
		 * use it in conjuction with IsLoading()
		 * 
		 * @param sound's class identifier in the library
		 * 
		 * @return true or false.
		 * 
		 * @see #IsPlaying()
		 * 
		 */ 
		public static function IsPlaying(id : String):Boolean {
			//Debugger.Log("<--SFX-->  Is playing id: " + id);
			return _soundManager.IsPlaying(id);
		}
		
		/**
		 * Pauses a sound if it's playing
		 *
		 * @param sound's class identifier in the library
		 * 
		 */
		public static function PauseSound(id :String,numSoundChannelsToPause:int = 999999):void {
			
			_soundManager.pauseSound(id,null,numSoundChannelsToPause);
		}
		
		/**
		 * Stop a sound playback immediately
		 *
		 * @param sound's class identifier in the library
		 * 
		 */
		public static function StopSound(id :String,numSoundChannelsToStop:int = 999999):void {
			//Debugger.Log("<--SFX-->  stopSound: [" + id + "]");
			//Debugger.LogStack();
			_soundManager.stopSound(id,null,numSoundChannelsToStop);
			if(id == _currentMusicPlaying)
				_currentMusicPlaying = null;
		}
		
		
		/**
		 * SetGainOverCurrent : Sets a new gain, in relation to the current sound volume.
		 * 
		 * @param sound's class identifier in the library
		 * @param a number between 0 and n for this sound volume (% of the current volume)
		 * 
		 */ 
		public static function SetGainOverCurrent(id :String, gainRatio :Number) :void {
			//Debugger.Log("<--SFX-->  setSoundVolume [" + id + "]: " + gainRatio);
			_soundManager.setSoundVolume(id, gainRatio * Volume);
		}
		
		
		/**
		 * 
		 * Get Master Volume ( 0 to 1 ). By default master volume is set to 1.
		 * 
		 * @return Master Volume ( 0 to 1 )
		 * 
		 * @see #set Volume()
		 * 
		 */ 
		public static function get Volume():Number {
			
			return _masterVolume;
		}
		
		
		/**
		 * 
		 * Set Master Volume ( 0 to 1 ). By default master volume is set to 1.
		 * 
		 * @param 0 to 1
		 * 
		 * @see #get Volume()
		 * 
		 */ 
		public static function set Volume(n : Number) :void {
			
			_masterVolume	= n;
		}
		
		/**
		 * 
		 * Get Volume of SFX sound(XML)( 0 to 1 ).
		 * 
		 * @param sound's class identifier in the library
		 * @return Voumen ( 0 to 1 ) to XML; 
		 * 
		 */ 
		public static function GetVolumeFromConfig(musicId:String) :Number {
			
			var gain	: Number = Config.getValue("Volume_" + musicId);
			if (isNaN(gain)) gain = 1;
			
			return gain;
		}
		
		public static function ToggleMute(mute:Boolean):void {
			
			Debugger.Log("SFX - TOGGLE MUTE (" + mute + ")");
			Debugger.LogStack();
			var myTransform:SoundTransform = SoundMixer.soundTransform;
			_muted = mute;
			myTransform.volume =  _muted ? 0 : Config.master_volume;
			
			SoundMixer.soundTransform = myTransform;
		}
		
		public static function Pause():void {
			
			_soundManager.pauseAllSounds();
		}
		
		
		public static function Resume():void {
			
			_soundManager.resumeAllPausedSounds();
		}
		
		
		public static function PlayMusic(musicId:String, fadeSeconds:Number = 0,startTime:Number = 0):void {
			
			Debugger.Log("<--SFX-->  musicId: " + musicId + " - while playing " + _currentMusicPlaying);
			if (musicId	== _currentMusicPlaying) return;
			var mId	: String	= "";
			if (fadeSeconds > 0) {
				/* Con fade */
				PlaySoundLoop(musicId, true, 0,0,startTime);
				Sfx.FadeSound(musicId, GetVolumeFromConfig(musicId) * Volume, fadeSeconds);
				for each (mId in _musicIds) {
					if (mId != musicId && IsPlaying(mId)) {
						var sndAParar	: String	= mId;
						Sfx.FadeSound(mId, 0, fadeSeconds, -1, function():void { Sfx.StopSound(sndAParar) } );
					}
				}
			} else {
				/* Sin fade */
				PlaySoundLoop(musicId,true,-1,0,startTime);
				for each (mId in _musicIds) {
					if (mId != musicId && IsPlaying(mId)) StopSound(mId);
				}
			}
			_currentMusicPlaying	= musicId;
		}
		
		
		/**
		 * Fades the sound to the specified volume over the specified amount of time.
		 * 
		 * @param soundId The string identifier of the sound
		 * @param toVolume The target volume to fade to, between 0 and 1 (default: 0)
		 * @param $fadeLength The time to fade over, in seconds (default: 1)
		 * @param fromVolume The origin volume to fade from, between 0 and 1 (default: currentVolume)
		 * 
		 * @return void
		 */
		public static function FadeSound(soundId:String, toVolume:Number = 0, fadeLength:Number = 1, fromGain:Number = -1, onFadeComplete:Function = null) : void {
			
			//Debugger.Log("<--SFX-->  FadeSound [" + soundId + "]: De " + fromGain + " a " + toVolume + " en " + fadeLength + " s");
			if (!fromGain || fromGain != -1) _soundManager.setSoundVolume(soundId, Volume * fromGain);
			_soundManager.fadeSound(soundId, toVolume, fadeLength, onFadeComplete);
		}
		
		
		
		public static function StopAllSounds():void {
			
			// Stop all sfxs
			for each (var sfxId : String in _sfxIds) {
				StopSound(sfxId);
			}
			// Stop all musics
			for each (var musicId : String in _musicIds) {
				StopSound(musicId);
			}
		}
		
		
		public static function Destroy() : void {
			
			StopAllSounds();
			_soundManager.stopAllSounds();
			_soundManager.removeAllSounds();
		}
		
		
		public static function NextMusic():void {
			
			var musicIdToPlay		: String	= null;
			var currentMusicId		: String	= null;
			
			for each (var musicId : String in _musicIds) {
				if (musicIdToPlay == null) musicIdToPlay = musicId;
				if (IsPlaying(musicId)) currentMusicId = musicId;
				else if (currentMusicId != null) musicIdToPlay = musicId;
			}
			
			PlayMusic(musicIdToPlay);
		}
		
		
		public static function PrevMusic():void {
			
			var musics			: Array = new Array();
			var cycleIdx		: int	= 0;
			var currentMusicIdx	: int	= 0;
			var musicIdToPlay	: String	= null;
			
			for each (var musicId : String in _musicIds) {
				musics.push(musicId);
				if (IsPlaying(musicId)) currentMusicIdx = cycleIdx;
				cycleIdx++;
			}
			
			var musicToPlayIdx	: int	= (currentMusicIdx > 0)?currentMusicIdx - 1:musics.length - 1;
			musicIdToPlay = musics[musicToPlayIdx] as String;
			PlayMusic(musicIdToPlay);
		}
		
		
		public static function GetSoundPosition(soundId:String):Number {
			return	_soundManager.getSoundPosition(soundId);
		}
		
		
		public static function GetSoundDuration(soundId:String):Number {
			return	_soundManager.getSoundDuration(soundId);
		}
		
		
		public static function PlayMCSound(soundId:String, mc:MovieClip):void {
			// TMP
			PlaySound(soundId);
		}
		
	}
}
