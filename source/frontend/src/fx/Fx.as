package fx 
{
import flash.display.MovieClip;

/**
	 * ...
	 * @author ...
	 */
	public class Fx
	{
		
		public static const ID_REGULAR_SHOT_SPLASH_FLOOR	: String	= "RegularShotSplashFloor";
		public static const ID_GRENADE_SPLASH_FLOOR			: String	= "GrenadeSplashFloor";
		
		protected var _id : String 	= "";
		
		public function Fx($id:String, algo:Object = null) 
		{
			_id = $id;
		}
		
		public function showAt(x:Number, y:Number, z:Number, frameLbl:String = "show"):void { }
		
		public function update(dtMs:int):void { }
		
		public function get id():String
		{
			return _id;
		}
		
		public function get View():MovieClip {
			return null;
		}
		
		
		public function get ready():Boolean { return false; }
	}
}
