package fx 
{
import flash.display.MovieClip;

/**
	 * ...
	 * @author ...
	 */
	public class FxGObj extends Fx
	{
		protected var _gObj : InternalFxGObj	= null;
		
		public function FxGObj(fxAsset:GameObjectView, $shadow:MovieClip = null, $id:String = "fx") 
		{
			super($id);
			_gObj	= new InternalFxGObj(fxAsset, $shadow, $id);
		}
		
		//override public function get ready():Boolean {
			//return _gObj.ready;
		//}
		
		override public function get ready():Boolean {
			return (_gObj.View.currentFrameLabel == "ready");
		}
		
		override public function showAt(showX:Number, showY:Number, showZ:Number, frameLbl:String = "show"):void {
			
			//Debugger.Log("SHOWING FX @" + StringUtils.ToCoord(showX, showY));
			_gObj.View.gotoAndStop("ready");
			_gObj.View.gotoAndStop(frameLbl);
			_gObj.TeleportTo(showX, showY, showZ);
			_gObj.View.visible = true;
		}
		
		override public function update(dtMs:int):void {
			if (!ready)
			{
				var innerMc : MovieClip = _gObj.View.getChildByName("inner_mc") as MovieClip;
				if (innerMc == null)
				{
					Debugger.Log("WARNING! Fx not able to get inner_mc (" + _gObj + ")");
				}
				else
				{
					if (innerMc.currentFrame == innerMc.totalFrames)
					{
						_gObj.View.gotoAndStop("ready");
						_gObj.View.visible = false;
					}
				}
			}
			return;
		}
		
		override public function get View():MovieClip {
			return _gObj.View;
		}
		
		public function get gObj():InternalFxGObj 
		{
			return _gObj;
		}
		
		
	}
}
