package fx
{
import flash.display.DisplayObjectContainer;
import flash.display.MovieClip;
import flash.geom.Point;
import flash.utils.getQualifiedClassName;

import shots.Shot;

import utils.MathUtils;
import utils.MovieClipUtils;

import view.Camera;
import view.GameObjectToScreenMapper;
import view.LayerFactory;

/**
	 * ...
	 * @author ...
	 */
	public class FxManager
	{
		
		private static var _instance:FxManager = null;
		
		private var _fxs	: Vector.<Fx>	= null;
		
		
		public function FxManager()
		{
			_instance = this;
			_fxs	= new Vector.<Fx>();
			
			FxGobjAsset.addEventListener(FxGobjAsset.NEW_FX, showGObjFx);
		}
		
		
		public function initialize():void {
		}
		
		public function prepareLevel(levelNumber:int):void {
			
		}
		
		public function update(dtMs:int):void
		{
			for each(var $fx:Fx in _fxs)
			{
				$fx.update(dtMs);
			}
		}
		
		
		public static function get instance():FxManager
		{
			return _instance;
		}
		
		public function reset():void {
			
		}
		
		
		public function shotHitFloor(shot:Shot)
		{
			Debugger.Log("shotHitFloor (" + shot.id + ")");
			var id : String	= (shot.id == Shot.ID_REGULAR_SHOT)?Fx.ID_REGULAR_SHOT_SPLASH_FLOOR:Fx.ID_GRENADE_SPLASH_FLOOR;
			// "RegularShotSplashFloorAsset" - "GrenadeSplashFloorAsset"
			var newFx	: Fx	= GetNextAvailableFx(id);
			if (!newFx) {
				Debugger.Log("No fx available. Creating new fx...");
				newFx	= AddNewFxAnim(id);
			} else {
				Debugger.Log("Reusing Fx " + newFx);
			}
			
			var splatNbr : int = MathUtils.random(1, 3, true);
			newFx.showAt(shot.x, shot.y, shot.z, "team" + shot.teamId + "_" + splatNbr.toString());
		}
		
		private function showGObjFx(fxEvent:FxEvent):void 
		{
			//Debugger.Log("__FX__ Showing FX "  + fxEvent + "  --  " + fxEvent.fxAsset + " ( " + fxEvent.fxAsset.name + ")");
			var fxAsset	: FxGobjAsset	= fxEvent.fxAsset;
			fxAsset.stop();
			var parent	: DisplayObjectContainer	= fxAsset.parent;
			var rotation		: Number = fxAsset.rotation;
			var scaleFactorX	: Number = fxAsset.scaleX;
			var scaleFactorY	: Number = fxAsset.scaleY;
			//Debugger.Log("parent: " + parent);
			while (parent) {
				//Debugger.Log("parent.scaleX: " + parent.scaleX);
				scaleFactorX	*= parent.scaleX;
				scaleFactorY	*= parent.scaleY;
				rotation		+= parent.rotation;
				parent = parent.parent;
			}
			var fxScaleX	: Number	= fxAsset.scaleX;
			var fxScaleY	: Number	= fxAsset.scaleY;
			
			// TODO: Por ahora, sólo en el piso
			//var newPosition	: Point	= MovieClipUtils.GetRelativePos(fxAsset, _gameCanvas);
			var fxYPos	: Number	= Config.floor_y;
			//Debugger.Log("MovieClipUtils.ScreenPosition(fxAsset): " + MovieClipUtils.ScreenPosition(fxAsset));
			var newScreenPosition	: Point	= MovieClipUtils.ScreenPosition(fxAsset);
			var newPosition	: GameAreaPoint	= Camera.Instance.ConvertScreenPosToGamePos(newScreenPosition, fxYPos);
			var newScreenPosX	: Number	= newScreenPosition.x;
			var newScreenPosY	: Number	= newScreenPosition.y;
			var newPosX	: Number	= newPosition.x;
			var newPosY	: Number	= newPosition.y;
			var newPosZ	: Number	= newPosition.z;
			
			MovieClipUtils.Remove(fxAsset);
			
			var id : String = "FXGOBJ-" + getQualifiedClassName(fxAsset);
			//Debugger.Log("id: " + id);
			var fxToShow	: FxGObj	= GetNextAvailableFx(id) as FxGObj;
			if (!fxToShow) {
				Debugger.Log("No fx available. Creating new fx...");
				fxToShow	= AddNewGObjFx(fxAsset, null, id);
			} else {
				//Debugger.Log("Reusing Fx " + fxToShow);
			}
			
			// Place & Show Fx
			if (!fxToShow) {
				Debugger.Log("WARNING! - No Fx To Show [" + getQualifiedClassName(fxAsset) + "]");
				return;
			}
			//Debugger.Log("Fx New Pos: " + newPosition);
			//Debugger.Log("Fx New Local Pos: " + StringUtils.ToCoord(newPosition.x, newPosition.y) + " - scaleFactorX: " + scaleFactorX  + " - scaleFactorY: " + scaleFactorY + " -- fxScaleX: " + fxScaleX + " - fxScaleY: " + fxScaleY + " Floor Y: " + Config.floor_y);
			fxToShow.View.scaleX	= fxScaleX * scaleFactorX;
			fxToShow.View.scaleY	= fxScaleY * scaleFactorY;
			fxToShow.View.rotation	= rotation;
			//if (scaleFactor < 0 && fxToShow.View.scaleX > 0) fxToShow.View.scaleX *= -1;
			//else if (scaleFactor > 0 && fxToShow.View.scaleX < 0) fxToShow.View.scaleX *= -1;
			
			// Para corregir que se muestra primero en el último frame anterior
			fxToShow.View.x = newScreenPosX;
			fxToShow.View.y = newScreenPosY - 10000;
			fxToShow.showAt(newPosX, newPosY, newPosZ);
		}
		
		
		
		private function GetNextAvailableFx(id:String):Fx 
		{
			var len : int = _fxs.length;
			for (var i : int = 0; i < len; i++) {
				if (_fxs[i].id == id && _fxs[i].ready) return _fxs[i];
			}
			return null;
		}
		
		private function AddNewFxAnim(id:String):Fx {
			Debugger.Log("AddNewFxAnim: " + id);
			var newFx	: Fx	= null;
			switch(id) {
				case Fx.ID_REGULAR_SHOT_SPLASH_FLOOR:
					newFx	= new Fx_FloorPaint(
												ExternalLoader.Instance.GetNewInstance("RegularShotSplashFloorAsset", "fx.swf") as InternalFxAnimation,
												id
											);
					LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_BACK).addChild(newFx.View);
					break;
				case Fx.ID_GRENADE_SPLASH_FLOOR:
					newFx	= new Fx_FloorPaint(
												ExternalLoader.Instance.GetNewInstance("GrenadeSplashFloorAsset", "fx.swf") as InternalFxAnimation,
												id
											);
					LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_BACK).addChild(newFx.View);
					break;
				default:
					Debugger.Log("WARNING - Trying to add inexistent powerup (" + id + ")");
					break;
			}
			_fxs.push(newFx);
			return newFx;
		}
		
		private function AddNewGObjFx(fxAsset:FxGobjAsset, shadow:MovieClip, id:String):FxGObj 
		{
			Debugger.Log("AddNewGObjFx: " + id);
			var newFx : FxGObj = new FxGObj(fxAsset, shadow, id);
			GameObjectToScreenMapper.instance.AddGameObject(newFx.gObj);
			_fxs.push(newFx);
			return newFx;
		}
	
	}

}