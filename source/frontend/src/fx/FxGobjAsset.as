package fx 
{
import flash.display.Sprite;

/**
	 * ...
	 * @author ...
	 */
	public class FxGobjAsset extends GameObjectView
	{
		public static const NEW_FX	: String 	= "newFx";
		
		private static var _eventDispatcher	: Sprite	= new Sprite();
		//private static var _fxEvent	: FxEvent	= new FxEvent(NEW_FX);
		private static var _fxEvent	: FxEvent	= null;
		
		public function FxGobjAsset() 
		{
			super();
			dispatchFxEvent(this);
		}
		
		private static function dispatchFxEvent(fxAsset:FxGobjAsset):void {
			_fxEvent	= new FxEvent(NEW_FX);
			_fxEvent.fxAsset	= fxAsset;
			_eventDispatcher.dispatchEvent(_fxEvent);
		}
		
		public static function addEventListener(type:String, listener:Function, useCapture:Boolean=false, priority:int=0, useWeakReference:Boolean=false):void {
			_eventDispatcher.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}
		
		public static function removeEventListener(type:String, listener:Function, useCapture:Boolean = false) : void {
			_eventDispatcher.removeEventListener (type, listener, useCapture);
		}
		
	}
}
