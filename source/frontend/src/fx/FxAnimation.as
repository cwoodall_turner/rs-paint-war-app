package fx 
{
import flash.display.MovieClip;

import utils.StringUtils;

/**
	 * ...
	 * @author ...
	 */
	public class FxAnimation extends Fx
	{
		protected var _fxAnimation	: InternalFxAnimation = null;
		
		public function FxAnimation($fxAnimationMc:InternalFxAnimation, $id:String = "fx") 
		{
			super($id);
			_fxAnimation	= $fxAnimationMc;
			_fxAnimation.gotoAndStop("ready");
			_fxAnimation.visible = false;
		}
		
		override public function showAt(x:Number, y:Number, z:Number, frameLbl:String = "show"):void 
		{
			Debugger.Log("showAt: " + StringUtils.ToCoord3D(x, y, z));
			_fxAnimation.x	= x;
			_fxAnimation.y	= y + z;
			_fxAnimation.gotoAndStop("ready");
			_fxAnimation.gotoAndStop(frameLbl);
			Debugger.Log("Playing animation " + this + " @" + StringUtils.ToCoord(_fxAnimation.x, _fxAnimation.y) + " - [" + frameLbl + "]");
			_fxAnimation.visible = true;
		}
		
		override public function update(dtMs:int):void {
			if (!ready)
			{
				var innerMc : MovieClip = _fxAnimation.getChildByName("inner_mc") as MovieClip;
				if (innerMc == null)
				{
					Debugger.Log("WARNING! Fx not able to get inner_mc (" + _fxAnimation + ")");
				}
				else
				{
					if (innerMc.currentFrame == innerMc.totalFrames)
					{
						_fxAnimation.gotoAndStop("ready");
						_fxAnimation.visible = false;
					}
				}
			}
			return;
		}
		
		override public function get ready():Boolean {
			return (_fxAnimation.currentFrameLabel == "ready");
		}
		
		override public function get View():MovieClip {
			return _fxAnimation;
		}
	}
}