package fx 
{
import flash.display.MovieClip;

/**
	 * ...
	 * @author alfred
	 */
	public class InternalFxGObj extends GameObject
	{
		
		public function InternalFxGObj(fxAsset:GameObjectView, $shadow:MovieClip = null, $id:String = "fx") 
		{
			super(fxAsset, $shadow, $id);
			_removeFromCanvasOnReset	= false;
		}
		
		public function get ready():Boolean {
			if (!_innerMc) {
				_innerMc = _view.getChildByName("inner_mc") as MovieClip;
			}
			return (_innerMc && _innerMc.currentFrame == _innerMc.totalFrames);
		}
		
		public function Show(showX:Number, showY:Number, showZ:Number):void {
			
			//Debugger.Log("SHOWING FX @" + StringUtils.ToCoord(showX, showY));
			if (!_innerMc) {
				_innerMc = _view.getChildByName("inner_mc") as MovieClip;
			}
			if (!_innerMc) {
				Debugger.Log("WARNING - No inner mc @ FX");
			}
			TeleportTo(showX, showY, showZ);
			
			// TODO: Ver esto
			//_view.x = showX;
			//_view.y = showY;
			_innerMc.gotoAndPlay(1);
			_innerMc.visible = true;
			//Debugger.Log("_innerMc.alpha: " + _innerMc.alpha);
			//Debugger.Log("_innerMc.visible: " +  _innerMc.visible);
			//_view.addEventListener(Event.ENTER_FRAME, DebuggerUpdate);
		}
		
		//private function DebuggerUpdate(e:Event):void 
		//{
			//Debugger.Log("_view @ " + _view.currentFrame);
		//}
		
		override public function Update(dtMs:int):void {
			// Completely override GameObject Update
			Debugger.Log("_view @ " + _view.currentFrame);
			return;
		}
		
	}

}