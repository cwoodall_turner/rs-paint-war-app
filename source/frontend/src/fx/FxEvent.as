package fx 
{
import flash.events.Event;

/**
	 * ...
	 * @author ...
	 */
	public class FxEvent extends Event
	{
		private var _fxAsset	: FxGobjAsset	= null;
		
		public function FxEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public function get fxAsset():FxGobjAsset 
		{
			return _fxAsset;
		}
		
		public function set fxAsset(value:FxGobjAsset):void 
		{
			_fxAsset = value;
		}
		
	}
}
