package enemies 
{
	/**
	 * ...
	 * @author ...
	 */
	public class EnemyState_Run extends EnemyState
	{	
		protected var _speedHorizontal			: Number	= 1;
		protected var _speedHorizontalDiagonal	: Number	= 1;
		protected var _speedVertical			: Number	= 1;
		protected var _speedVerticalDiagonal	: Number	= 1;

		
		protected var _gotToDestinationBackOrFront	: Boolean	= false;
		protected var _gotToDestinationLeftOrRight	: Boolean	= false;
		
		// SURROUNDING
		protected var _surrounding						: Boolean	= false;
		protected var _destinationDirectionWhenBlocked	: uint	= 0;
		
		public function EnemyState_Run(enemy:Enemy) 
		{
			super(enemy);
			_speedHorizontal			= Config.getValue(_enemy.configPrefix + "run-speed-horizontal");
			_speedVertical				= Config.getValue(_enemy.configPrefix + "run-speed-vertical");
			_speedHorizontalDiagonal	= Config.getValue(_enemy.configPrefix + "run-speed-horizontal-diagonal");
			_speedVerticalDiagonal		= Config.getValue(_enemy.configPrefix + "run-speed-vertical-diagonal");
			_onReEnterPlayInnerMcFromStart	= false;
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit = Targets.NONE;
			_gObj.canBeHit	= true;
			_gObj.canGrabPowerups	= true;
		}
		
		
		override public function Enter():void {
			super.Enter();
			
			_gotToDestinationBackOrFront	= _gotToDestinationLeftOrRight	= false;
			
			// Force stand on floor
			_gObj.TeleportToY(Config.floor_y);
			
			
			MoveEnemy(33); // TMP
		}
		
		override public function Leave():void {
			super.Leave();
			_enemy.ResetDestination();
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			MoveEnemy(dtMs);
		}
		
		private function MoveEnemy(dtMs:int):void {
			
			if (_enemy.destinationActive) {
				var xToDest	: Number	= _enemy.destination.x - _enemy.x;
				var zToDest	: Number	= _enemy.destination.z - _enemy.z;
				
				var goingRight	: Boolean	= false;
				var goingLeft	: Boolean	= false;
				var goingBack	: Boolean	= false;
				var goingFront	: Boolean	= false;
				
				if	(xToDest > 0  && xToDest > Config.default_destination_tolerance_x) {
					goingRight = true;
				} else if (xToDest < 0  && xToDest < -Config.default_destination_tolerance_x) {
					goingLeft = true;
				} else {
					_gotToDestinationLeftOrRight = true;
				}
				
				if	(zToDest > 0  && zToDest > Config.default_destination_tolerance_z) {
					goingFront = true;
				} else if (zToDest < 0  && zToDest < -Config.default_destination_tolerance_z) {
					goingBack = true;
				} else {
					_gotToDestinationBackOrFront = true;
				}
				
				// Check if already in destination
				if (!goingRight && !goingLeft && !goingBack && !goingFront) {
					_enemy.GotToDestination();
					//_enemy.SetState(GameObject.ST_IDLE);
					return;
				}
				
				
				if (Config.surround_obstacles)
				{
					if (_surrounding)
					{
						// Si está rodeando obstáculos, que siga haciéndolo hasta que dé la vuelta.
						if (surroundingComplete)
						{
							// Dio la vuelta, deja de rodear
							Debugger.Log("surrounding complete");
							_surrounding = false;
						}
						else
						{
							if (_enemy.blockedDirections != CollisionManager.BLOCK_NONE)
							{
								// Está rodeando y está en contacto con obstáculos. Hacer que se mueva para rodearlos (sentido antihorario)
								goingRight	= (_enemy.blockedDirections & CollisionManager.BLOCK_BACK);
								goingFront	= (_enemy.blockedDirections & CollisionManager.BLOCK_RIGHT);
								goingLeft	= (_enemy.blockedDirections & CollisionManager.BLOCK_FRONT);
								goingBack	= (_enemy.blockedDirections & CollisionManager.BLOCK_LEFT);
							}
							else
							{
								// Está rodeando, no toca ningún obstáculo pero no terminó de dar la vuelta -> va en la dirección que lo bloqueaba en el ciclo anterior
								goingRight	= (_enemy.prevBlockedDirections & CollisionManager.BLOCK_RIGHT);
								goingFront	= (_enemy.prevBlockedDirections & CollisionManager.BLOCK_FRONT);
								goingLeft	= (_enemy.prevBlockedDirections & CollisionManager.BLOCK_LEFT);
								goingBack	= (_enemy.prevBlockedDirections & CollisionManager.BLOCK_BACK);
							}
							Debugger.Log("BlockedDirs: " + _enemy.blockedDirections + "  - PrevBlockedDirs: " + _enemy.prevBlockedDirections + "  - Moving... Right: " + goingRight + "  - Front: " + goingFront + "  - Left: " + goingLeft + "  - Back: " + goingBack);
						}
					}
					else
					{
						if (_enemy.blockedDirections != CollisionManager.BLOCK_NONE)
						{
							// No está rodeando y colisiona con obstáculos -> Si está completamente bloqueado empezar a rodear
							var canMove : Boolean = (		(goingRight && !(_enemy.blockedDirections & CollisionManager.BLOCK_RIGHT))
														||	(goingLeft && !(_enemy.blockedDirections & CollisionManager.BLOCK_LEFT))
														||	(goingBack && !(_enemy.blockedDirections & CollisionManager.BLOCK_BACK))
														||	(goingFront && !(_enemy.blockedDirections & CollisionManager.BLOCK_FRONT)));
							Debugger.Log("canMove: " + canMove);
							if (!canMove)
							{
								// Está completamente bloqueado -> Comenzar a rodear
								_destinationDirectionWhenBlocked = CollisionManager.BLOCK_NONE;
								if (goingRight) _destinationDirectionWhenBlocked += CollisionManager.BLOCK_RIGHT;
								if (goingLeft) _destinationDirectionWhenBlocked += CollisionManager.BLOCK_LEFT;
								if (goingBack) _destinationDirectionWhenBlocked += CollisionManager.BLOCK_BACK;
								if (goingFront) _destinationDirectionWhenBlocked += CollisionManager.BLOCK_FRONT;
								_surrounding = true;
								Debugger.Log("_destinationDirectionWhenBlocked: " + _destinationDirectionWhenBlocked);
							}
						}
					}
				}
							//if (goingRight && (_enemy.blockedDirections & CollisionManager.BLOCK_RIGHT))
							//{
								//Debugger.Log("BLOCK RIGHT!");
								//
							//} else if (goingLeft && (_enemy.blockedDirections & CollisionManager.BLOCK_LEFT))
							//{
								//Debugger.Log("BLOCK LEFT!");
							//}
							//if (goingBack && (_enemy.blockedDirections & CollisionManager.BLOCK_BACK))
							//{
								//Debugger.Log("BLOCK BACK!");
							//} else if (goingFront && (_enemy.blockedDirections & CollisionManager.BLOCK_FRONT))
							//{
								//Debugger.Log("BLOCK FRONT!");
							//}
						//}
					//}
					//
					//
					//if (goingRight && (_enemy.blockedDirections & CollisionManager.BLOCK_RIGHT))
					//{
						//Debugger.Log("BLOCK RIGHT!");
					//} else if (goingLeft && (_enemy.blockedDirections & CollisionManager.BLOCK_LEFT))
					//{
						//Debugger.Log("BLOCK LEFT!");
					//}
					//if (goingBack && (_enemy.blockedDirections & CollisionManager.BLOCK_BACK))
					//{
						//Debugger.Log("BLOCK BACK!");
					//} else if (goingFront && (_enemy.blockedDirections & CollisionManager.BLOCK_FRONT))
					//{
						//Debugger.Log("BLOCK FRONT!");
					//}
				//}
				
				var diagonal : Boolean = (goingRight || goingLeft) && (goingFront || goingBack) /*&& (!_enemy.isAgainstSectorLimits)*/;
				
				var dX	: Number	= 0;
				var dZ	: Number	= 0;
				if (goingRight) {
					dX	= diagonal?_speedHorizontalDiagonal * dtMs:_speedHorizontal * dtMs;
					if (!_surrounding && _enemy.x + dX > _enemy.destination.x) _enemy.TeleportToX(_enemy.destination.x);
					else _enemy.MoveX(dX);
				} else if (goingLeft) {
					dX	= diagonal?-_speedHorizontalDiagonal * dtMs:-_speedHorizontal * dtMs;
					if (!_surrounding && _enemy.x + dX < _enemy.destination.x) _enemy.TeleportToX(_enemy.destination.x);
					else _enemy.MoveX(dX);
				}
				if (goingBack) {
					dZ	= diagonal?-_speedVerticalDiagonal * dtMs:-_speedVertical * dtMs;
					if (!_surrounding && _enemy.z + dZ < _enemy.destination.z) _enemy.TeleportToZ(_enemy.destination.z);
					else _enemy.MoveZ(dZ);
				} else if (goingFront) {
					dZ	= diagonal?_speedVerticalDiagonal * dtMs:_speedVertical * dtMs;
					if (!_surrounding && _enemy.z + dZ > _enemy.destination.z) _enemy.TeleportToZ(_enemy.destination.z);
					else _enemy.MoveZ(dZ);
				}
				
				
				// CHECK ENEMY BLOCKED
				if (_enemy.blockedDirections != CollisionManager.BLOCK_NONE)
				{
					// No está rodeando y colisiona con obstáculos -> Si está completamente bloqueado empezar a rodear
					var canMove : Boolean = (		(goingRight && !(_enemy.blockedDirections & CollisionManager.BLOCK_RIGHT))
												||	(goingLeft && !(_enemy.blockedDirections & CollisionManager.BLOCK_LEFT))
												||	(goingBack && !(_enemy.blockedDirections & CollisionManager.BLOCK_BACK))
												||	(goingFront && !(_enemy.blockedDirections & CollisionManager.BLOCK_FRONT)));
					if (!canMove)
					{
						// Está completamente bloqueado -> Comenzar a rodear
						_destinationDirectionWhenBlocked = CollisionManager.BLOCK_NONE;
						if (goingRight) _destinationDirectionWhenBlocked += CollisionManager.BLOCK_RIGHT;
						if (goingLeft) _destinationDirectionWhenBlocked += CollisionManager.BLOCK_LEFT;
						if (goingBack) _destinationDirectionWhenBlocked += CollisionManager.BLOCK_BACK;
						if (goingFront) _destinationDirectionWhenBlocked += CollisionManager.BLOCK_FRONT;
						
						Debugger.Log("ENEMY CAN'T MOVE!!!");
						_enemy.currentBehavior.onBlocked(_destinationDirectionWhenBlocked);
					}
				}
			} else {
				// No Destination
				//Debugger.Log("DE WALK A IDLE");
				//_enemy.SetState(GameObject.ST_IDLE);
			}
			
			updateView(goingRight, goingLeft, goingBack, goingFront);
		}
		
		private function get surroundingComplete():Boolean
		{
			Debugger.Log("_enemy.blockedDirections: " + _enemy.blockedDirections);
			if (_enemy.blockedDirections != CollisionManager.BLOCK_NONE) return false;
			
			// Si el obst. estaba a la izquierda, que siga hasta que no toque ningún obst. y el último bloqueo sea del frente.
			// Si el obst. estaba a abajo, que siga hasta que no toque ningún obst. y el último bloqueo sea de la derecha.
			// Si el obst. estaba a la derecha, que siga hasta que no toque ningún obst. y el último bloqueo sea de arriba.
			// Si el obst. estaba a arriba, que siga hasta que no toque ningún obst. y el último bloqueo sea de la izquierda.
			if (_destinationDirectionWhenBlocked & CollisionManager.BLOCK_LEFT && _enemy.prevBlockedDirections & CollisionManager.BLOCK_FRONT)
			{
				return true;
			}
			if (_destinationDirectionWhenBlocked & CollisionManager.BLOCK_FRONT && _enemy.prevBlockedDirections & CollisionManager.BLOCK_RIGHT)
			{
				return true;
			}
			if (_destinationDirectionWhenBlocked & CollisionManager.BLOCK_RIGHT && _enemy.prevBlockedDirections & CollisionManager.BLOCK_BACK)
			{
				return true;
			}
			if (_destinationDirectionWhenBlocked & CollisionManager.BLOCK_BACK && _enemy.prevBlockedDirections & CollisionManager.BLOCK_LEFT)
			{
				return true;
			}
			
			return false;
		}
		
		private function updateView(goingRight:Boolean, goingLeft:Boolean, goingBack:Boolean, goingFront:Boolean):void
		{
			if (goingRight) {
				//Debugger.Log("goingRight: " + goingRight);
				_enemy.FaceRight();
			} else if (goingLeft) {
				//Debugger.Log("goingLeft: " + goingLeft);
				_enemy.FaceLeft();
			} else if (goingBack) {
				//Debugger.Log("goingBack: " + goingBack);
				_enemy.FaceBack();
			} else if (goingFront) {
				//Debugger.Log("goingFront: " + goingFront);
				_enemy.FaceFront();
			}
			
			updateViewFacing();
		}
		
		override public function updateViewFacing():void 
		{
			super.updateViewFacing();
			_enemy.ViewGoToAndStop("run_" + _enemy.facing);
		}
		
		override public function PassedLevelLimitLeft(limitValue:Number) : void {
			super.PassedLevelLimitLeft(limitValue);
			_enemy.TeleportToX(limitValue);
			PassedLevelLimitLeftOrRight();
		}
		
		override public function PassedLevelLimitRight(limitValue:Number) : void {
			super.PassedLevelLimitRight(limitValue);
			_enemy.TeleportToX(limitValue);
			PassedLevelLimitLeftOrRight();
		}
		
		override public function PassedLevelLimitBack(limitValue:Number) : void {
			super.PassedLevelLimitBack(limitValue);
			_enemy.TeleportToZ(limitValue);
			PassedLevelLimitBackOrFront();
		}
		
		override public function PassedLevelLimitFront(limitValue:Number) : void {
			super.PassedLevelLimitFront(limitValue);
			_enemy.TeleportToZ(limitValue);
			PassedLevelLimitBackOrFront();
		}
		
		public function PassedLevelLimitBackOrFront() : void {
			// If above or below limits, pretend it got to destination if it already got to the other limits
			_gotToDestinationBackOrFront	= true;
			if(_gotToDestinationLeftOrRight)	_enemy.GotToDestination();
		}
		
		public function PassedLevelLimitLeftOrRight() : void {
			// If left or right limits, pretend it got to destination if it already got to the other limits
			_gotToDestinationLeftOrRight	= true;
			if(_gotToDestinationBackOrFront)	_enemy.GotToDestination();
		}
		
	}

}
