package enemies 
{
	/**
	 * ...
	 * @author ...
	 */
	public class EnemyState_Ready extends EnemyState
	{
		
		public function EnemyState_Ready(enemy:Enemy) 
		{
			super(enemy);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canBeHit = false;
			_gObj.canHit = Targets.NONE;
			_gObj.canGrabPowerups	= false;
		}
		
		override public function Enter():void {
			super.Enter();
			_enemy.visible	= false;
			//_enemy.TeleportToX( -1000);
			_enemy.ViewGoToAndStop("ready");
			
			_enemy.RemoveBehavior();
		}
		
		override public function Leave():void {
			super.Leave();
			_enemy.visible	= true;
		}
		
	}

}
