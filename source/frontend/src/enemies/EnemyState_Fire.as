package enemies 
{
import obstacles.Bullseye;
import obstacles.Obstacle;
import obstacles.ObstaclesManager;

import shots.Shot;
import shots.ShotsManager;

import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class EnemyState_Fire extends EnemyState
	{	
		
		protected var _fireHeightRangeLength	: Number	= 0;
		protected var _fireHeightRangeWidth		: Number	= 0;
		
		protected var _lowShot		: Boolean = false;
		
		public function EnemyState_Fire(enemy:Enemy) 
		{
			super(enemy);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			/* FIRE */
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= true;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function Enter():void {
			super.Enter();
			
			_enemy.speedX = _enemy.speedZ = _enemy.speedY	= 0;
			
			_lowShot	= false;
			
			
			// Check if there's a "lower" enemy in sight
			var fireHeightSuffix	: String	= "";
			// TMP
			if (_enemy.heightForFire >= 2)
			{
				var facingStr	: String	= (_enemy.IsFacingLeft || _enemy.IsFacingRight)?"_facing_side":"_facing_front";
				_fireHeightRangeLength	= Config.getValue(_enemy.configPrefix + "range_to_check_fire_low_length_px" + facingStr);
				_fireHeightRangeWidth	= Config.getValue(_enemy.configPrefix + "range_to_check_fire_low_width_px" + facingStr);
				var leftX	: Number = 0;
				var rightX	: Number = 0;
				var backZ	: Number = 0;
				var frontZ	: Number = 0;
				if (_enemy.IsFacingFront)
				{
					leftX	= _enemy.x - _fireHeightRangeWidth * .5;
					rightX	= leftX + _fireHeightRangeWidth;
					backZ	= _enemy.z;
					frontZ	= backZ + _fireHeightRangeLength;
				}
				else if (_enemy.IsFacingBack)
				{
					leftX	= _enemy.x - _fireHeightRangeWidth * .5;
					rightX	= leftX + _fireHeightRangeWidth;
					backZ	= _enemy.z - _fireHeightRangeLength;
					frontZ	= backZ + _fireHeightRangeLength;
				}
				else if (_enemy.IsFacingRight)
				{
					leftX	= _enemy.x;
					rightX	= leftX + _fireHeightRangeLength;
					backZ	= _enemy.z - _fireHeightRangeWidth * .5;
					frontZ	= backZ + _fireHeightRangeWidth;
				}
				else
				{
					leftX	= _enemy.x - _fireHeightRangeLength;
					rightX	= leftX + _fireHeightRangeLength;
					backZ	= _enemy.z - _fireHeightRangeWidth * .5;
					frontZ	= backZ + _fireHeightRangeWidth;
				}
				var areaBoundaries	: Boundaries	= new Boundaries(leftX, rightX, Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY, backZ, frontZ);
				// Chequear si hay algún enemigo enano cerca
				for each(var participant:GameObject in GameManager.Instance.participants)
				{
					//Debugger.Log("enemy.heightForFire: " + enemy.heightForFire);
					//AreaDebugger.instance.drawAreaBoundaries(areaBoundaries, LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_FRONT).graphics, 0, true);
					if (participant.canBeHit && participant.heightForFire == 1 && participant.teamId != _enemy.teamId)
					{
						if (CollisionManager.instance.isInArea(participant, areaBoundaries))
						{
							_lowShot	= true;
							fireHeightSuffix = "_low";
							
							// TODO: Chequear que no haya obstáculo entre el enemigo y el que dispara
						}
						
					}
				}
				// Si no hay enemigo enano cerca, chequear si no hay un blanco a pintar cerca
				if (!_lowShot)
				{
					for each(var bullseye:Obstacle in ObstaclesManager.instance.theObstacles)
					{
						if (bullseye.IsCurrentState(GameObject.ST_READY) || !(bullseye is Bullseye)) continue;
						if ((bullseye as Bullseye).paintedByTeamId == _enemy.teamId) continue;
						if (CollisionManager.instance.isInArea(bullseye, areaBoundaries))
						{
							_lowShot	= true;
							fireHeightSuffix = "_low";
						}
					}
				}
			}
			_enemy.ViewGoToAndStop("fire_" + _enemy.facing + fireHeightSuffix);
		}
		
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			if (_enemy.shotRef) {
				var shotSpecials	: uint	= 0;
				if(_lowShot) shotSpecials += Shot.SPECIAL_LOW;
				ShotsManager.instance.AddShot(_enemy, Targets.ENEMY + Targets.PLAYER + Targets.OBSTACLE, shotSpecials);
			}
			
			if (MovieClipUtils.AnimationEnded(_enemy.innerMc))
			{
				_enemy.SetState(Enemy.ST_IDLE);
				_enemy.ShotFireComplete();
			}
		}
	}

}
