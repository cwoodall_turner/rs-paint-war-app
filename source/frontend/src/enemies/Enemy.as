package enemies 
{
import behaviors.GameObjectWithBehavior;

import flash.display.MovieClip;
import flash.events.Event;

import obstacles.Obstacle;

import shots.Shot;

import specialassets.SpecialAsset;

/**
	 * ...
	 * @author ...
	 */
	public class Enemy extends GameObjectWithBehavior {
		
		public static const ID_ENEMY_MORDECAI	: String	= "enemyMordecai";
		public static const ID_ENEMY_RIGBY		: String	= "enemyRigby";
		public static const ID_ENEMY_MUSCLEMAN	: String	= "enemyMuscleman";
		public static const ID_ENEMY_GUY		: String	= "enemyGuy";
		
		public static const ST_IDLE					: String	= "idle";
		public static const ST_RUN					: String	= "run";
		public static const ST_FIRE					: String	= "fire";
		public static const ST_GRENADE				: String	= "grenade";
		public static const ST_HIDE					: String	= "hide";
		public static const ST_HIT					: String	= "hit";
		public static const ST_KILLED				: String	= "killed";
		public static const ST_STOP					: String	= "stop";
		public static const ST_SPAWN				: String	= "spawn";
		
		
		public static const FIRE_FINISH_COMPLETE		: String	= "fire_complete";
		public static const GRENADE_FINISH_COMPLETE		: String	= "grenade_complete";
		
		protected var _fireShotCompleteEvt		: Event	= null;
		protected var _grenadeShotCompleteEvt	: Event	= null;
		
		public function Enemy(view:GameObjectView, shadow:MovieClip = null, $id:String="1") 
		{
			super(view, shadow, $id);
			_targetId	= Targets.ENEMY;
			
			ST_MOVE_DEFAULT	= ST_RUN;
			
			_removeFromCanvasOnReset	= false;
			
			
			UpdateVars();
			
			_fireShotCompleteEvt		= new Event(FIRE_FINISH_COMPLETE);
			_grenadeShotCompleteEvt		= new Event(GRENADE_FINISH_COMPLETE);
			// TMP
			//View.addEventListener(MouseEvent.CLICK, InfoLog);
		}
		
		//private function InfoLog(e:MouseEvent):void 
		//{
			//Debugger.Log("CLICKED ON " + this);
		//}
		
		override public function Reset():void {
			super.Reset();
			SetState(ST_READY);
		}
		
		public function UpdateVars():void 
		{
			_maxEnergy		= Config.getValue(configPrefix + "max_energy");
			_heightForFire	= Config.getValue(configPrefix + "height_for_fire");
			
			for each (var state:EnemyState in _states) {
				state.UpdateVars();
			}
			
			_energy	= _maxEnergy;
			Debugger.Log("Enemy Max Energy: " + _maxEnergy);
		}
		
		override public function Update(dtMs:int):void {
			//Debugger.Log("Player Update. Player: " + this);
			super.Update(dtMs);
			_currentState.Update(dtMs);
			
			if (killed) return;
		}
		/*
		public function HitPlayer(player:GameObject):void {
			if (_currentState)	(_currentState as EnemyState).EnemyHitPlayer();
			if (_currentCombo)	_currentCombo.HitTarget();
		}
		
		public function HitByPlayer(thePlayer:GameObject):void {
			lastHitByAttackSeq	= thePlayer.attackSeq;
			_lastHitBy			= thePlayer;
			energy -= thePlayer.attackPower * Config.player_damage_dealt_factor;
			
			if (!killed && _currentBehavior.target != thePlayer) {
				// Attack the player that just hit him
				_currentBehavior.SetTarget(thePlayer);
			}
			_consecutiveShotsHit	= 0;
		}
		*/
		
		
		//public function toString():String {
			//return "[CurSt: " + _currentState + " - CanHit: " + canHit + " - CanBeHit: " + canBeHit + "]";
		//}
		
		public function AttackComplete():void {
			
		}
		
		
		public function ShotFireComplete():void {
			dispatchEvent(_fireShotCompleteEvt);
		}
		
		
		public function ShotGrenadeComplete():void {
			dispatchEvent(_grenadeShotCompleteEvt);
		}
		
		public function againstObstacle(positionRelativeToObstacle:String, obstacle:Obstacle):void 
		{
			currentBehavior.againstObstacle(positionRelativeToObstacle, obstacle);
		}
		
		override public function FaceRight():void {
			super.FaceRight();
			_currentState.updateViewFacing();
		}
		
		override public function FaceLeft():void {
			super.FaceLeft();
			_currentState.updateViewFacing();
		}
		
		override public function FaceFront():void {
			super.FaceFront();
			_currentState.updateViewFacing();
		}
		
		override public function FaceBack():void {
			super.FaceBack();
			_currentState.updateViewFacing();
		}
		
		
		/*
		public function HitByShot(shot:Shot):void {
			_lastHitBy			= shot.shooter;
			Debugger.Log("shot: " + shot);
			Debugger.Log("shot.attackPower: " + shot.attackPower);
			energy -= shot.attackPower * Config.player_damage_dealt_factor;
		}
		*/
		
/* ========================================================*/
/* ============= GAME OBJECTS HITS/COLLISIONS =============*/
/* ========================================================*/
		
		public function HitByShot(shot:Shot):void {
			_lastHitBy			= shot;
			_lastHitByAttackSeq	= shot.attackSeq;
			Debugger.Log("ENEMY LOST " + shot.attackPower + " HP");
			//energy -= shot.attackPower * Config.player_damage_dealt_factor;
			energy -= shot.attackPower;
		}
		
		
		
/* =============================================*/
/* ============= SETTERS & GETTERS =============*/
/* =============================================*/
		
		public function get shotRef():MovieClip {
			if (!_innerMc) _innerMc = _view.getChildByName("inner_mc") as MovieClip;
			return _innerMc.getChildByName("shotRef_mc") as MovieClip;
		}
		
		override public function set teamId(value:String):void
		{
			super.teamId = value;
			SpecialAsset.setSpecialState(View, "specialassets::Vincha", teamId);
		}
		
	}

}
