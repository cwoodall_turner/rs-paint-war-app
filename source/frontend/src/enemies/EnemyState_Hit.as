package enemies 
{
import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class EnemyState_Hit extends EnemyState
	{
		
		public function EnemyState_Hit(enemy:Enemy) 
		{
			super(enemy);
			_canReEnterSameState	= true;
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canBeHit	= true;
			_gObj.canHit = Targets.NONE;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function Enter():void {
			super.Enter();
			_enemy.speedX = _enemy.speedZ = _enemy.speedY	= 0;
			_enemy.currentBehavior.OnHit();
			_enemy.ViewGoToAndStop("hit_" + _enemy.facing);
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (MovieClipUtils.AnimationEnded(_enemy.innerMc))
			{
				Debugger.Log("DE HIT A IDLE");
				_enemy.SetState(Enemy.ST_IDLE);
			}
		}
		
		override public function ReEnter():void {
			_gObj.ResetInnerMc();
			super.ReEnter();
		}
		
	}

}
