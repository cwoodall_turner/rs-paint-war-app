package enemies 
{
import flash.display.MovieClip;

/**
	 * ...
	 * @author ...
	 */
	public class EnemyState extends GameObjectState
	{
		
		protected var _enemy	: Enemy	= null;
		
		public function EnemyState(enemy:Enemy) 
		{
			super(enemy);
			_enemy	= enemy;
		}
		
		public function UpdateVars():void { }
		
		
		public function EnemyHitPlayer():void { }
		
		override public function Enter():void {
			super.Enter();
			_enemy.OnStateEnter(this);
			//Debugger.Log("Enemy State Enter " + this);
		}
		
		override public function Leave():void {
			super.Leave();
			_enemy.OnStateLeave(this);
			//Debugger.Log("Enemy State Leave " + this);
		}
		
		public function SetNewView(newView:MovieClip, newShadow:MovieClip = null):void {	}
		
	}

}
