package enemies 
{
import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class EnemyState_Spawn extends EnemyState
	{
		
		public function EnemyState_Spawn(enemy:Enemy) 
		{
			super(enemy);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canBeHit = false;
			_gObj.canHit = Targets.NONE;
			_gObj.canGrabPowerups	= false;
		}
		
		override public function Enter():void {
			super.Enter();
			
			_enemy.speedX = _enemy.speedZ = _enemy.speedY	= 0;
			_enemy.ViewGoToAndStop("spawn");
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			if (MovieClipUtils.AnimationEnded(_enemy.innerMc))
			{
				_enemy.SetState(Enemy.ST_IDLE);
			}
		}
		
	}

}
