package enemies
{
import behaviors.BehaviorsManager;
import behaviors.Path;

import flash.display.MovieClip;
import flash.geom.Point;
import flash.utils.Dictionary;
import flash.utils.getQualifiedClassName;

import specialassets.SpecialAsset;

import utils.MathUtils;
import utils.MovieClipUtils;
import utils.VectorUtils;

import view.Camera;
import view.GameObjectToScreenMapper;

/**
	 * ...
	 * @author ...
	 */
	public class EnemiesManager
	{
		
		private static var _instance:EnemiesManager = null;
		
		private var _enemies:Vector.<Enemy> = null;
		private var _paths	: Dictionary	= null;
		
		private var _possibleBehaviors	: Vector.<String>	= null;
		private var _possibleBehaviorsNextIdx	: int = 0;
		
		private var _possibleGuyFaces			: Vector.<String>	= null;
		private var _possibleGuyFacesNextIdx	: int = 0;
		
		public function EnemiesManager()
		{
			_instance = this;
			_enemies = new Vector.<Enemy>();
		}
		
		
		public function PrepareFromSwf():void {
			var levelAsset	: MovieClip	= GameManager.Instance.currentLevel.levelAsset;
			_paths	= new Dictionary();
			var pathsContainer : MovieClip	= levelAsset.getChildByName("paths_mc") as MovieClip;
			var i : int = pathsContainer.numChildren;
			while (i--)
			{
				var pathRef : MovieClip	= pathsContainer.getChildAt(i) as MovieClip;
				
				var possibleTeamsStr:String	= pathRef.name.split("_").shift();
				var possibleTeamsArr: Array = possibleTeamsStr.split("");
				var possibleTeamsVect : Vector.<String> = new Vector.<String>();
				for each(var possibleTeamRef:String in possibleTeamsArr)
				{
					possibleTeamsVect.push(GameManager.Instance.currentLevel.getTeamIdFromRef(possibleTeamRef));
				}
				
				var newPath	: Path	= new Path(possibleTeamsVect, pathRef.name);
				var j:int = pathRef.numChildren;
				if (j == 0)
				{
					Debugger.Log("WARNING! EMPTY PATH - Level " + GameManager.Instance.currentLevel.number + "-" + GameManager.Instance.currentLevel.config + " (" + getQualifiedClassName(pathRef) + ")");
					continue;
				}
				var tmpNodesArr	: Array = new Array();
				while (j--)
				{
					var pathNodeRef : MovieClip	= pathRef.getChildAt(j) as MovieClip;
					var nodeOrder	: int		= int(getQualifiedClassName(pathNodeRef).split("_").pop());
					var tmpNodeObj	: Object	= new Object();
					tmpNodeObj["node"] = Camera.Instance.ConvertScreenPosToGamePos(MovieClipUtils.GetRelativePos(pathNodeRef, levelAsset), Config.floor_y).clonar();
					tmpNodeObj["order"] = nodeOrder;
					tmpNodesArr.push(tmpNodeObj);
				}
				j = tmpNodesArr.length;
				tmpNodesArr.sortOn("order", Array.NUMERIC);
				while (j--)
				{
					newPath.unshift(tmpNodesArr[j]["node"])
					
					//if (Config.debug_pathfinding)
					//{
						//var pathNodeTmp:GameAreaPoint = tmpNodesArr[j]["node"];
						//var canvas : Graphics = LayerFactory.Instance.GetLayer(LayerFactory.DEBUG_CURRENT_PATH).graphics;
						//var leftX	: Number = pathNodeTmp.x - 2;
						//var rightX	: Number = pathNodeTmp.x + 2;
						//var backZ	: Number = pathNodeTmp.z - 2;
						//var frontZ	: Number = pathNodeTmp.z + 2;
						//canvas.lineStyle(.5, 0xffffff, .5);
						//canvas.beginFill(0xffffff, .5);
						//canvas.drawRect(leftX, Config.floor_y + backZ, rightX - leftX, frontZ - backZ);
						//if (j > 0)
						//{
							//var nextNode:GameAreaPoint = tmpNodesArr[j - 1]["node"];
							//canvas.moveTo(pathNodeTmp.x, Config.floor_y + pathNodeTmp.z);
							//canvas.lineTo(nextNode.x, Config.floor_y + nextNode.z);
						//}
						//canvas.endFill();
					//}
				}
				tmpNodesArr	= null;
				var pathName	: String	= pathRef.name.split("_").pop();
				//Debugger.Log("pathName: " + pathName);
				_paths[pathName] = newPath;
			}
			
			_possibleBehaviors	= new Vector.<String>();
			var possibleBehaviorsStr	: String	= Config.getValue("level_" + GameManager.Instance.currentLevel.id + "_possible_behaviors");
			if (possibleBehaviorsStr != null && possibleBehaviorsStr != "")
			{
				Debugger.Log("possibleBehaviorsStr: " + possibleBehaviorsStr);
				var possibleBehaviorsArr = possibleBehaviorsStr.split(",");
				for each (var possibleBehaviorStr:String in possibleBehaviorsArr)
				{
					Debugger.Log("possibleBehaviorStr: " + possibleBehaviorStr);
					_possibleBehaviors.push(possibleBehaviorStr);
				}
			}
			else
			{
				_possibleBehaviors.push("por-defecto");
			}
			_possibleBehaviorsNextIdx = 0;
			
			VectorUtils.shuffle(_possibleBehaviors as Vector.<*>);
			
			
			_possibleGuyFaces	= new Vector.<String>();
			var possibleGuyFacesStr	: String	= Config.getValue("possible_guy_faces");
			// CODE: BALD_ENEMIES
			if (CheatsManager.isActive(CheatsManager.BALD_ENEMIES))
			{
				possibleGuyFacesStr	= Config.getValue("code-bald_enemies-possible_guy_faces");
			}
			
			if (possibleGuyFacesStr != null && possibleGuyFacesStr != "")
			{
				var possibleGuyFacesArr = possibleGuyFacesStr.split(",");
				for each (var possibleGuyFacestr:String in possibleGuyFacesArr)
				{
					_possibleGuyFaces.push(possibleGuyFacestr);
				}
			}
			else
			{
				_possibleGuyFaces.push("por-defecto");
			}
			_possibleGuyFacesNextIdx = 0;
			
			
			// CODE: PLAY_WITH_GUY (Do not repeat faces)
			if (CheatsManager.isActive(CheatsManager.PLAY_WITH_GUY))
			{
				var playerFaceId : String = SpecialAsset.getSpecialState(GameManager.Instance.activePlayer.View, "specialassets::Cara");
				Debugger.Log("playerFaceId: " + playerFaceId);
				for each(var possibleGuyFace : String in _possibleGuyFaces)
				{
					if (possibleGuyFace == playerFaceId)
					{
						Debugger.Log("REMOVING ENEMY FACE " + possibleGuyFace);
						_possibleGuyFaces.splice(_possibleGuyFaces.indexOf(possibleGuyFace), 1);
					}
				}
			}
			
			VectorUtils.shuffle(_possibleGuyFaces as Vector.<*>);
		}
		
		public function addEnemy(id:String, $teamId:String):Enemy
		{
			Debugger.Log("addEnemy(" + id + "," + $teamId + ")");
			var theEnemy	: Enemy	= null;
			// Try to reuse
			for each (var enemy:Enemy in _enemies) {
				if (enemy.id == id && enemy.IsCurrentState(GameObject.ST_READY)) {
					theEnemy	= enemy;
					continue;
				}
			}
			if (!theEnemy) {
				theEnemy	= createNewEnemy(id);
				Debugger.Log("New enemy: " + theEnemy);
				if (!theEnemy) {
					Debugger.Log("ERROR - Enemigo no creado");
					return null;
				}
			} else {
				Debugger.Log("Reusing enemy " + theEnemy);
			}
			
			// TMP: Set team
			theEnemy.teamId = $teamId;
			
			// Set enemy initial variables
			theEnemy.UpdateVars();
			theEnemy.energy 		= theEnemy.maxEnergy;
			theEnemy.visible		= true;
			
			
			// BEHAVIOR
			// ========
			//var behaviorStr	: String	= "por-defecto";
			//var possibleBehaviorsStr	: String	= Config.getValue("level_" + GameManager.Instance.currentLevel.id + "_possible_behaviors");
			//if (possibleBehaviorsStr != null && possibleBehaviorsStr != "")
			//{
				//var possibleBehaviorsArr = possibleBehaviorsStr.split(",");
				//behaviorStr	= possibleBehaviorsArr[int(MathUtils.random(0, possibleBehaviorsArr.length - 1))];
			//}
			Debugger.Log("_possibleBehaviorsNextIdx: " + _possibleBehaviorsNextIdx);
			Debugger.Log("_possibleBehaviors[_possibleBehaviorsNextIdx]: " + _possibleBehaviors[_possibleBehaviorsNextIdx]);
			BehaviorsManager.instance.SetBehavior(theEnemy, _possibleBehaviors[_possibleBehaviorsNextIdx]);
			_possibleBehaviorsNextIdx++;
			if (_possibleBehaviorsNextIdx >= _possibleBehaviors.length)
			{
				_possibleBehaviorsNextIdx = 0;
			}
			
			theEnemy.SetState(GameObject.ST_IDLE);
			
			return theEnemy;
		}
		
		private function createNewEnemy(id:String, addToCollection:Boolean = true):Enemy {
			var newEnemy	: Enemy	= null;
			
			switch (id)
			{
				case Enemy.ID_ENEMY_MORDECAI:
					newEnemy = new Enemy1(ExternalLoader.Instance.GetSingleton("PlayerAsset", "mordecai.swf") as GameObjectView, ExternalLoader.Instance.GetSingleton("PlayerShadowAsset", "mordecai.swf") as MovieClip, id);
					break;
				case Enemy.ID_ENEMY_RIGBY:
					newEnemy = new Enemy1(ExternalLoader.Instance.GetSingleton("PlayerAsset", "rigby.swf") as GameObjectView, ExternalLoader.Instance.GetSingleton("PlayerShadowAsset", "rigby.swf") as MovieClip, id);
					break;
				case Enemy.ID_ENEMY_MUSCLEMAN:
					newEnemy = new Enemy1(ExternalLoader.Instance.GetSingleton("PlayerAsset", "muscleman.swf") as GameObjectView, ExternalLoader.Instance.GetSingleton("PlayerShadowAsset", "muscleman.swf") as MovieClip, id);
					break;
				case Enemy.ID_ENEMY_GUY:
					newEnemy = new Enemy1(ExternalLoader.Instance.GetNewInstance("PlayerAsset", "guy.swf") as GameObjectView, ExternalLoader.Instance.GetNewInstance("PlayerShadowAsset", "guy.swf") as MovieClip, id);
					setNextPossibleFace(newEnemy);
					break;
				default: 
					Debugger.Log("WARNING!!! - Trying to Add inexistent enemy");
					return null;
			}
			
			if (addToCollection) {
				_enemies.push(newEnemy);
				GameObjectToScreenMapper.instance.AddGameObject(newEnemy);
			}
			return newEnemy;
		}
		
		private function setNextPossibleFace(newEnemy:Enemy):void 
		{
			SpecialAsset.setSpecialState(newEnemy.View, "specialassets::Cara", _possibleGuyFaces[_possibleGuyFacesNextIdx]);
			_possibleGuyFacesNextIdx++;
			if (_possibleGuyFacesNextIdx >= _possibleGuyFaces.length)
			{
				_possibleGuyFacesNextIdx = 0;
			}
		}
		
		public function Reset():void {
			
			var i:int = _enemies.length;
			while (i--)
			{
				_enemies[i].Reset();
				GameObjectToScreenMapper.instance.RemoveGameObject(_enemies[i]);
				_enemies[i].Destroy();
			}
			_enemies	= new Vector.<Enemy>();
		}
		
		public function Update(dtMs:int):void
		{
			var i:int = _enemies.length;
			while (i--)
			{
				_enemies[i].Update(dtMs);
			}
		}
		
		public static function get instance():EnemiesManager
		{
			return _instance;
		}
		
		public function get theEnemies():Vector.<Enemy>
		{
			return _enemies;
		}
		
		public function get aliveEnemy():Enemy
		{
			for each(var theEnemy:Enemy in _enemies) {
				if (!theEnemy.killed && !theEnemy.IsCurrentState(GameObject.ST_READY)) {
					return theEnemy;
				}
			}
			return null;
		}
		
		public function get enemyOnScreen():Enemy
		{
			for each(var theEnemy:Enemy in _enemies) {
				if (theEnemy.visible && !theEnemy.IsCurrentState(GameObject.ST_READY)) {
					var enemyScreenPos	: Point	= Camera.Instance.ConvertGamePosToScreenPos(new GameAreaPoint(theEnemy.x, theEnemy.y, theEnemy.z));
					if (enemyScreenPos.x > 0 && enemyScreenPos.x < Config.game_width && enemyScreenPos.y > 0 && enemyScreenPos.y < Config.game_height) {
						return theEnemy;
					}
				}
			}
			return null;
		}
		
		public function get numAliveEnemies() : int {
			var qty : int = 0;
			for each(var theEnemy:Enemy in _enemies) {
				if (!theEnemy.IsCurrentState(GameObject.ST_READY) && !theEnemy.killed) {
					qty++;
				}
			}
			return qty;
		}
		
		public function getPath(pathName:String):Path 
		{
			if (_paths != null && _paths[pathName] != undefined) return _paths[pathName];
			return null;
		}
		
		public function getRandomPath(gObj:GameObject):Path  
		{
			Debugger.Log("_paths: " + _paths);
			Debugger.Log("_paths.length: " + _paths.length);
			if (_paths == null) return null;
			
			var numPaths : int = 0;
			for each(var path:Path in _paths)
			{
				numPaths++;
			}
			
			if (numPaths == 0) return null;
			
			var pathIdx : int = MathUtils.random(0, _paths.length - 1, true);
			for each(var path:Path in _paths)
			{
				if (pathIdx == 0) return path as Path;
				pathIdx--;
			}
			
			return null;
		}
		
		public function getFarthestPath(gObj:GameObject, forceFreePath:Boolean = false):Path  
		{
			var longestDistance	: Number					= Number.NEGATIVE_INFINITY;
			var farthestPath			: Path	= null;
			for each(var path:Path in _paths)
			{
				var distance : Number	= getDistanceToPath(gObj, path);
				if (distance > longestDistance && (!forceFreePath || !isPathBusy(path)))
				{
					longestDistance = distance;
					farthestPath = path;
				}
			}
			if (farthestPath == null && forceFreePath) farthestPath = getFarthestPath(gObj, false);
			return farthestPath;
		}
		
		public function getNearestPath(gObj:GameObject, forceFreePath:Boolean = false):Path  
		{
			var shortestDistance	: Number					= Number.MAX_VALUE;
			var nearestPath			: Path	= null;
			for each(var path:Path in _paths)
			{
				var distance : Number	= getDistanceToPath(gObj, path);
				if (distance < shortestDistance && (!forceFreePath || !isPathBusy(path)))
				{
					shortestDistance = distance;
					nearestPath = path;
				}
			}
			if (nearestPath == null && forceFreePath) nearestPath = getNearestPath(gObj, false);
			return nearestPath;
		}
		
		private function isPathBusy(path:Path):Boolean
		{
			return (path.followedBy != null);
		}
		
		private function getDistanceToPath(gObj:GameObject, path:Path):Number 
		{
			var shortestDistance:Number	= Number.MAX_VALUE;
			for each(var node:GameAreaPoint in path.nodes)
			{
				var distance : Number = (node.x - gObj.x) * (node.x - gObj.x) + (node.z - gObj.z) * (node.z - gObj.z);
				if (distance < shortestDistance) shortestDistance = distance;
			}
			return shortestDistance;
		}
	
	}

}