package enemies 
{
	/**
	 * ...
	 * @author ...
	 */
	public class EnemyState_Idle extends EnemyState
	{
		
		public function EnemyState_Idle(enemy:Enemy) 
		{
			super(enemy);
			_canReEnterSameState = true;
			_onReEnterPlayInnerMcFromStart	= false;
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit = Targets.NONE;
			_gObj.canBeHit	= true;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function Enter():void {
			super.Enter();
			_enemy.speedX = _enemy.speedZ = _enemy.speedY	= 0;
			
			// Force stand on floor
			_gObj.TeleportToY(Config.floor_y);
			
			updateViewFacing();
		}
		
		override public function updateViewFacing():void 
		{
			super.updateViewFacing();
			_enemy.ViewGoToAndStop("idle_" + _enemy.facing);
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			//_enemy.FaceTarget();
			
			if (_enemy.destinationActive) {
				_enemy.SetState(_enemy.ST_MOVE_DEFAULT);
			}
		}
		
	}

}
