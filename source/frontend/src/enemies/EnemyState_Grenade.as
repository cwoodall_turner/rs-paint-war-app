package enemies 
{
import shots.ShotsManager;

import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class EnemyState_Grenade extends EnemyState
	{	
		public function EnemyState_Grenade(enemy:Enemy) 
		{
			super(enemy);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			/* FIRE */
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= true;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function Enter():void {
			super.Enter();
			
			_enemy.speedX = _enemy.speedZ = _enemy.speedY	= 0;
			
			_enemy.ViewGoToAndStop("grenade_" + _enemy.facing);
		}
		
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			if (_enemy.innerMc)
			{
				_enemy.innerMc.canContinue = true;
			}
			
			if (_enemy.shotRef) {
				ShotsManager.instance.AddGrenade(_enemy, Targets.ENEMY + Targets.PLAYER + Targets.OBSTACLE);
			}
			
			if (MovieClipUtils.AnimationEnded(_enemy.innerMc))
			{
				_enemy.SetState(Enemy.ST_IDLE);
				_enemy.ShotGrenadeComplete();
			}
		}
	}

}
