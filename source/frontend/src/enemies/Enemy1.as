package enemies
{
import flash.display.MovieClip;

/**
	 * ...
	 * @author ...
	 */
	public class Enemy1 extends Enemy
	{
		
		public function Enemy1(view:GameObjectView, shadow:MovieClip = null, $id:String="1")
		{	
			super(view, shadow, $id);
			
			
			_states[ST_SPAWN] 	= new EnemyState_Spawn(this);
			_states[ST_IDLE] 	= new EnemyState_Idle(this);
			_states[ST_HIT]		= new EnemyState_Hit(this);
			_states[ST_RUN] 	= new EnemyState_Run(this);
			_states[ST_HIDE] 	= new EnemyState_Hide(this);
			_states[ST_FIRE] 	= new EnemyState_Fire(this);
			_states[ST_GRENADE] = new EnemyState_Grenade(this);
			_states[ST_KILLED]	= new EnemyState_Killed(this);
			_states[ST_READY] 	= new EnemyState_Ready(this);
			
			// Permitted external transitions
			(_states[ST_READY] as EnemyState).AddPossibleNextStatesExternal(ST_IDLE, ST_RUN);
			(_states[ST_IDLE] as EnemyState).AddPossibleNextStatesExternal(ST_HIT, ST_KILLED, ST_RUN, ST_FIRE, ST_GRENADE, ST_HIDE);
			(_states[ST_HIT] as EnemyState).AddPossibleNextStatesExternal(ST_IDLE, ST_RUN, ST_HIT, ST_KILLED, ST_FIRE, ST_GRENADE, ST_HIDE);
			(_states[ST_KILLED] as EnemyState).AddPossibleNextStatesExternal(ST_READY);
			(_states[ST_RUN] as EnemyState).AddPossibleNextStatesExternal(ST_HIT, ST_KILLED, ST_IDLE, ST_FIRE, ST_GRENADE, ST_HIDE);
			(_states[ST_HIDE] as EnemyState).AddPossibleNextStatesExternal(ST_HIT, ST_KILLED, ST_IDLE, ST_FIRE, ST_GRENADE, ST_RUN);
			(_states[ST_FIRE] as EnemyState).AddPossibleNextStatesExternal(ST_HIT, ST_KILLED, ST_RUN, ST_IDLE, ST_HIDE, ST_GRENADE);
			(_states[ST_GRENADE] as EnemyState).AddPossibleNextStatesExternal(ST_HIT, ST_KILLED, ST_RUN, ST_IDLE, ST_HIDE, ST_FIRE);
			
			SetStateExternal(ST_READY);
		}
	
	}

}
