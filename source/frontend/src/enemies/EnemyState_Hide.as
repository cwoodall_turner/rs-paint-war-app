package enemies 
{
import obstacles.Obstacle;

/**
	 * ...
	 * @author ...
	 */
	public class EnemyState_Hide extends EnemyState
	{	
		protected var _enemyPositionRelativeToObstacle	: String	= "right";
		protected var _obstacle							: Obstacle	= null;
		
		public function EnemyState_Hide(enemy:Enemy) 
		{
			super(enemy);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canHit 	= Targets.NONE;
			_gObj.canBeHit	= true;
			_gObj.canGrabPowerups	= true;
		}
		
		override public function EnterWithArgs(args:Array):void {
			super.EnterWithArgs(args);
			if (args != null && args.length > 0)	_enemyPositionRelativeToObstacle = args[0];
			else {
				Debugger.Log("WARNING! - Enemy hide: enter state without args");
				_enemyPositionRelativeToObstacle	= CollisionManager.LEFT;
			}
			
			_obstacle = null;
			if (args != null && args.length > 1)	_obstacle = args[1] as Obstacle;
			
			Enter();
		}
		
		override public function Enter():void {
			super.Enter();
			
			_enemy.speedX = _enemy.speedZ = _enemy.speedY	= 0;
			
			switch(_enemyPositionRelativeToObstacle)
			{
				case CollisionManager.LEFT:
					_enemy.FaceRight();
					if (_obstacle != null && Config.getValue("obstacle_" + _obstacle.configPrefix + "force_hide_center_depth") == "true") _enemy.TeleportToZ(_obstacle.z + 1);
					break;
				case CollisionManager.RIGHT:
					_enemy.FaceLeft();
					if (_obstacle != null && Config.getValue("obstacle_" + _obstacle.configPrefix + "force_hide_center_depth") == "true") _enemy.TeleportToZ(_obstacle.z + 1);
					break;
				case CollisionManager.BACK:
					_enemy.FaceFront();
					if (_obstacle != null && Config.getValue("obstacle_" + _obstacle.configPrefix + "force_hide_center_width") == "true") _enemy.TeleportToX(_obstacle.x);
					break;
				case CollisionManager.FRONT:
					_enemy.FaceBack();
					if (_obstacle != null && Config.getValue("obstacle_" + _obstacle.configPrefix + "force_hide_center_width") == "true") _enemy.TeleportToX(_obstacle.x);
					break;
			}
			_enemy.ViewGoToAndStop("hide_" + _enemy.facing);
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			if (_enemy.destinationActive) {
				_enemy.SetState(_enemy.ST_MOVE_DEFAULT);
			}
		}
		
	}

}
