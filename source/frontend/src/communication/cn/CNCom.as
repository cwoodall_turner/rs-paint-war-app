package communication.cn 
{
import com.adobe.serialization.json.JSON;
import com.turner.caf.business.SimpleResponder;

import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IEventDispatcher;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.external.ExternalInterface;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLVariables;
import flash.utils.Dictionary;

/**
	 * ...
	 * @author alfred
	 */
	public class CNCom
	{
		private static var _instance	: CNCom	= null;
		
		private const GET_LOGGED_USER_COMPLETE	: String	= "get_logged_user_complete";

        private var MS_API : String = "https://audience.cartoonnetwork.com/cn/msapi/";

		private var _user:CNUser;
		
		private var _currentResponder	: SimpleResponder	= null;

        private var _loader:URLLoader = null;


        private var _TEGid : String = "";
        private var _authid : String = "";
        private var _authpass : String = "";

        private var _displayName : String = "";

		public function CNCom() 
		{
			
		}
		
		static public function get instance():CNCom 
		{
			if (_instance == null) _instance = new CNCom();
			return _instance;
		}
		
		public function initialize(responder:SimpleResponder):void
		{
			_currentResponder	= responder;
			
			Debugger.Log("CNCom Initialize");

            // Get logged user
            getLoggedUser();
		}
		
		public function doLogin(username:String, password:String, responder:SimpleResponder):void
		{
			_currentResponder	= responder;

            prepareLoader();
            var url:String = MS_API + "login";
            var request:URLRequest = new URLRequest(url);
            Debugger.Log("url: " + url);
            Debugger.Log("user: " + username);
            var variables:URLVariables = new URLVariables();

            variables.loginId = username;
            variables.password = password;
            variables.appid = Config.app_id;
            request.data = variables;

            _loader.addEventListener(Event.COMPLETE, onLoginComplete);
            try {
                _loader.load(request);
            } catch (error:Error) {
                Debugger.Log("Unable to load requested document.");
            }
		}


        private function onLoginComplete(event:Event):void {

            var loader:URLLoader = URLLoader(event.target);

            // Crear usuario
            var response : Object = com.adobe.serialization.json.JSON.decode(loader.data);
            if(response.success)
            {
                Debugger.Log("SUCCESS");

                _TEGid = response.tid;
                _authid = response.authid;
                _authpass = response.authpass;
                var username	: String = response.authorities.USERNAME;

                Debugger.Log("tid: " + _TEGid);
                Debugger.Log("authid: " + _authid);
                Debugger.Log("authpass: " + _authpass);
                Debugger.Log("username: " + username);

                _user = new CNUser(_TEGid, _displayName);
                Config.user_name = _user.userName;
            }
            else
            {
                Debugger.Log("OOPS");
                var errors:Array = response.errors;
                if(errors.length >= 1)
                {
                    var errorId :String = errors[0].error;
                    var errorMsg :String = errors[0].message;
                    Debugger.Log("errorId: " + errorId);
                    Debugger.Log("errorMsg: " + errorMsg);
                }

                _user = null;
            }

            if (_currentResponder != null && _currentResponder.result != null)
            {
                _currentResponder.result(_user);
                _currentResponder = null;
            }
        }
		
		public function reGetLoggedUser(responder:SimpleResponder):void
		{
			Debugger.Log("reGetLoggedUser");
			_currentResponder = responder;
			getLoggedUser();
		}
		
		
		public function getLoggedUser():CNUser
		{
			Debugger.Log("getLoggedUser");

            // Get cookies
            try {
                //this will hold the data returned from javascript
                var browserCookieString:String = ExternalInterface.call("function(){return document.cookie}");
                Debugger.Log("browserCookieString: " + browserCookieString);
                if(browserCookieString == null) browserCookieString = "";
                // Remove blanks
                browserCookieString = browserCookieString.replace(/;\s/g, ";");

                var cookies : Dictionary = new Dictionary();
                var pairsArr : Array = browserCookieString.split(";");
                for each(var pairStr:String in pairsArr)
                {
                    var pairArr : Array = pairStr.split("=");
                    var key : String = pairArr[0];
                    var value : String = pairArr[1];

                    Debugger.Log("cookies[" + key + "]: " + value);
                    cookies[key] = value;
                }


                if(cookies["TEGid"] != undefined)    _TEGid = cookies["TEGid"];
                if(cookies["authid"] != undefined)    _authid = cookies["authid"];
                if(cookies["authpass"] != undefined)    _authpass = cookies["authpass"];
                if(cookies["dname"] != undefined)    _displayName = cookies["dname"];

                Debugger.Log("TEGid: " + _TEGid);
                Debugger.Log("authid: " + _authid);
                Debugger.Log("authpass: " + _authpass);
                Debugger.Log("displayName: " + _displayName);
            } catch (error : SecurityError) {
                Debugger.Log("SecurityError:" + error.message);
            } catch (error : Error) {
                Debugger.Log("Error:" + error.message);
            }

            if(_TEGid == "" || _authid == "" || _authpass == "")
            {
                _user = null;

                if (_currentResponder != null && _currentResponder.result != null)
                {
                    _currentResponder.result(_user);
                    _currentResponder = null;
                }
            }
            else
            {
                _user = new CNUser(_TEGid, _displayName);
                Config.user_name = _user.userName;
            }

            if (_currentResponder != null && _currentResponder.result != null)
            {
                _currentResponder.result(_user);
                _currentResponder = null;
            }

            return _user;
		}

		protected function onFault(data:Object):void
		{
            Debugger.Log("Fault - " + data);
			if (_currentResponder != null && _currentResponder.fault != null)
			{
				_currentResponder.fault(data);
			}
		}
		
		public function get isLogged():Boolean
		{
			Debugger.Log("user: " + user);
			return (user != null);
		}
		
		
		public function get user():CNUser
		{
			return _user;
		}

        private function prepareLoader():void
        {
            _loader = new URLLoader();
            configureListeners(_loader);
        }
        private function configureListeners(dispatcher:IEventDispatcher):void {
            dispatcher.addEventListener(Event.OPEN, openHandler);
            dispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            dispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
            dispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
            dispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
        }

        private function openHandler(event:Event):void {
            Debugger.Log("openHandler: " + event);
        }

        private function progressHandler(event:ProgressEvent):void {
            Debugger.Log("progressHandler loaded:" + event.bytesLoaded + " total: " + event.bytesTotal);
        }

        private function securityErrorHandler(event:SecurityErrorEvent):void {
            Debugger.Log("securityErrorHandler: " + event);
        }

        private function httpStatusHandler(event:HTTPStatusEvent):void {
            Debugger.Log("httpStatusHandler: " + event);
        }

        private function ioErrorHandler(event:IOErrorEvent):void {
            Debugger.Log("ioErrorHandler: " + event);
        }
	}

}