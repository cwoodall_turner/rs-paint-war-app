package communication.cn 
{
/**
	 * ...
	 * @author alfred
	 */
	public class CNUser 
	{
        private var _userName   : String = "";
        private var _id         : String = "";

		public function CNUser($id:String,  $username:String)
		{
            _id         = $id;
            _userName   = $username;
		}

        public function get userName():String
        {
            return _userName;
        }

        public function get id():String {
            return _id;
        }
}

}