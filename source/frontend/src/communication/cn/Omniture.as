package communication.cn 
{
import flash.external.ExternalInterface;

/**
	 * ...
	 * @author alfred
	 */
	public class Omniture 
	{
		
		public function Omniture() 
		{
			
		}
		
		public static function trackView():void
		{
			if (Config.use_tracking) {
				Debugger.Log("OMNITURE! - Calling trackView");
			try {
					ExternalInterface.call( Config.getValue("track_view_function"),// Track Function
											ConfigLoader.Instance.Config["track_gameName"],		// Game Name
											Config.getValue("track_view_type"),				// Play Type
											null				// userData
											);
			} catch (e:*) {
				Debugger.Log("Error: " + e);
				trace("Error: " + e);
			}
				Debugger.Log("DESPUES");
			}
		}
		
		public static function trackPlay():void
		{
			if (Config.use_tracking) {
				Debugger.Log("OMNITURE! - Calling trackPlay");
			try {
					ExternalInterface.call( Config.getValue("track_play_function"),// Track Function
											ConfigLoader.Instance.Config["track_gameName"],		// Game Name
											Config.getValue("track_play_type"),				// Play Type
											null				// userData
											);
			} catch (e:*) {
				Debugger.Log("Error: " + e);
				trace("Error: " + e);
			}
				Debugger.Log("DESPUES");
			}
		}
		
		static public function trackCode(code:String):void 
		{
			if (Config.use_tracking) {
				Debugger.Log("OMNITURE! - Calling trackCode(" + code + ")");
			try {
					ExternalInterface.call( Config.getValue("track_code_function"),	// Track Functon
											Config.getValue("track_gameName"),		// Game Name
											Config.getValue("track_code_source"),	// Code Source
											code.toLowerCase(),						// Code
											null									// userData
											);
			} catch (e:*) {
				Debugger.Log("Error: " + e);
				trace("Error: " + e);
			}
				Debugger.Log("DESPUES");
			}
			
		}
		
		static public function trackMilestone(milestoneId:String, milestoneValue:String):void 
		{
			if (Config.use_tracking) {
				var milestoneName : String = Config.getValue("track_milestone_" + milestoneId + "_name");
				Debugger.Log("OMNITURE! - Calling trackMilestone(" + milestoneName + ", " + milestoneValue + ")");
			try {
					ExternalInterface.call( Config.getValue("track_milestone_function"),	// Track Functon
											Config.getValue("track_gameName"),		// Game Name
											milestoneName,	// Milestone Name
											milestoneValue,	// Milestone value
											null			// userData
											);
			} catch (e:*) {
				Debugger.Log("Error: " + e);
				trace("Error: " + e);
			}
				Debugger.Log("DESPUES");
			}
			
		}
		
	}

}