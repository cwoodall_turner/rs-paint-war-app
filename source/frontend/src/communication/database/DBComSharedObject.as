package communication.database 
{
import com.turner.caf.business.SimpleResponder;

import flash.net.SharedObject;

import utils.MathUtils;

/**
	 * ...
	 * @author alfred
	 */
	public class DBComSharedObject extends DBCom
	{
		
		public function DBComSharedObject() 
		{
			super();
			Debugger.Log("new DBComSharedObject");
		}
		
		override public function initialize(responder:SimpleResponder):void
		{
			super.initialize(responder);
			
			if (Config.getValue("reset_shared_objects") == "true")
			{
				eraseData();
			}
			
			responder.result(null);
		}
		
		/* GET USER INFO */
		override public function getUserInfo(cnId:String, responder:SimpleResponder):void
		{
			super.getUserInfo(cnId, responder);
			
			var shared : SharedObject	= SharedObject.getLocal(Config.shared_object_string);
			Debugger.Log("getUserInfo -  shared.data.userinfo: " + shared.data.userinfo);
			if (shared.data.userinfo != undefined)
			{
				var userinfo : Object = shared.data.userinfo;
				_user	= new DBUser(Config.guest_cn_id);
				for (var varName:String in userinfo){
					Debugger.Log("userinfo[" + varName + "]: " + userinfo[varName]);
					_user.setVar(varName, userinfo[varName]);
				}
			}
			else
			{
				_user = null;
			}
			shared.flush();
			shared.close();
			
			if (_currentResponder) _currentResponder.result(_user);
		}
		
		
		/* SAVE USER INFO */
		override public function saveUserInfo(cnId:String, responder:SimpleResponder):void
		{
			Debugger.Log("saveUserInfo");
			super.saveUserInfo(cnId, responder);
			if (_user == null)
			{
				Debugger.Log("User == null");
				if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault("Null user");
				return;
			}
			
			var shared : SharedObject	= SharedObject.getLocal(Config.shared_object_string);
			shared.data.userinfo = new Object();
			for (var varName:String in _user.vars){
				Debugger.Log("user data[" + varName + "]: " + _user.vars[varName]);
				shared.data.userinfo[varName] = _user.vars[varName];
			}
			shared.flush();
			shared.close();
			
			if (_currentResponder) _currentResponder.result(_user);
		}
		
		
		
		/* CREATE NEW USER */
		override public function createNewUser(cnId:String, name: String, responder:SimpleResponder):void
		{
			Debugger.Log("createNewUser: " + cnId);
			super.createNewUser(cnId, name, responder);
			
			var newUserInfo : Object = new Object();
			var newUserInitVarsStr		: String	= Config.getValue("new_user_init_vars");
			var newUserInitVarsArr		: Array		= newUserInitVarsStr.split(",");
			for each (var newUserVarStr:String in newUserInitVarsArr)
			{
				var newUserVarArr : Array = newUserVarStr.split(":");
				var newUserVarName 	: String = newUserVarArr[0];
				var newUserVarValue : String = newUserVarArr[1];
				Debugger.Log("New user data[" + newUserVarName + "]: " + newUserVarValue);
				newUserInfo[newUserVarName] = newUserVarValue;
			}
			
			// TMP
			var teamInt : int = Config.getValue("default_guest_team");
			if (isNaN(teamInt) || teamInt < 1 || teamInt > 3)
			{
				teamInt = MathUtils.random(1, 3, true);
			}
			newUserInfo["team_id"] = teamInt.toString();
			
			newUserInfo["name"] = name;
			
			
			var shared : SharedObject	= SharedObject.getLocal(Config.shared_object_string);
			shared.data.userinfo = new Object();
			for (var varName:String in newUserInfo){
				Debugger.Log("user data[" + varName + "]: " + newUserInfo[varName]);
				shared.data.userinfo[varName] = newUserInfo[varName];
			}
			shared.flush();
			shared.close();
			
			if (_currentResponder) _currentResponder.result(_user);
		}
		
		
		public static function eraseData():void
		{
			Debugger.Log("DBComSharedObject - eraseData");
			var shared : SharedObject	= SharedObject.getLocal(Config.shared_object_string);
			delete shared.data.userinfo;
			shared.flush();
			shared.close();
		}
		
		static public function getLocalData():Object 
		{
			var shared : SharedObject	= SharedObject.getLocal(Config.shared_object_string);
			Debugger.Log("getLocalData -  shared.data.userinfo: " + shared.data.userinfo);
			var userInfo : Object = null;
			if (shared.data.userinfo != undefined)
			{
				userInfo = shared.data.userinfo;
			}
			
			shared.flush();
			shared.close();
			
			return userInfo;
		}
	}
}
