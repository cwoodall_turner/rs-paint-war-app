package communication.database 
{
import flash.utils.Dictionary;

/**
	 * ...
	 * @author alfred
	 */
	public class DBUser 
	{
		private var _cnId	: String	= "";
		
		private var _vars	: Dictionary	= null;
		
		public function DBUser($cnId:String)
		{
			_cnId	= $cnId;
			_vars	= new Dictionary();
		}
		
		public function get cnId():String
		{
			return _cnId;
		}
		
		public function set cnId(value:String):void
		{
			_cnId = value;
		}
		
		public function getVar(key:String):*
		{
			if (_vars && _vars[key] != undefined) return _vars[key];
			return null;
		}
		
		public function setVar(key:String, value:*):void
		{
			if (_vars == null) _vars = new Dictionary();
			_vars[key] = value;
		}
		
		public function setScoreLevel(levelNbr:int, score:int):void 
		{
			setVar("score_level_" + levelNbr.toString(), score);
			
			// Update general score (level 0)
			var totalScore : int = 0;
			for (var i: int = 1; i <= 4; i++)
			{
				totalScore += getScoreLevel(i);
			}
			setVar("score_level_0", totalScore);
		}
		
		public function getScoreLevel(levelNbr:int):int
		{
			return int(getVar("score_level_" + levelNbr.toString()));
		}
		
		public function get money():int 
		{
			return int(getVar("money"));
		}
		
		public function set money(value:int):void 
		{
			if (value < 0)
			{
				Debugger.Log("Warning: trying to set negative money (" + value + ")");
				value = 0;
			}
			setVar("money", value);
		}
		
		public function get teamId():String 
		{
			return getVar("team_id");
		}
		
		public function get vars():Dictionary 
		{
			return _vars;
		}
		
		public function get rank():int 
		{
			return int(getVar("rank"));
		}
		
		public function set rank(value:int):void 
		{
			
			setVar("rank", value);
		}
		
	}

}