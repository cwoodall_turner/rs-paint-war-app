package communication.database 
{
import com.adobe.crypto.MD5;
import com.turner.caf.business.SimpleResponder;

import flash.net.NetConnection;
import flash.net.Responder;
import flash.utils.Dictionary;

import utils.MathUtils;

/**
	 * ...
	 * @author alfred
	 */
	public class DBComPhp extends DBCom
	{
		
		private var _netConnection	:NetConnection;
		private var _phpPath		: String	= "";
		private var _amfClass		: String	= "";
		
		public function DBComPhp() 
		{
			super();
			Debugger.Log("new DBComPhp");
		}
		
		override public function initialize(responder:SimpleResponder):void
		{
			super.initialize(responder);
			
			_phpPath	= Config.getValue("db_php_path");
			_amfClass	= Config.getValue("amf_class");
			
			_netConnection	= new NetConnection();
			
			responder.result(null);
		}
		
		/* GET USER INFO */
		override public function getUserInfo(cnId:String, responder:SimpleResponder):void
		{
			super.getUserInfo(cnId, responder);
			
			//if(loaderInfo.url.indexOf("file://") == 0){
				//log("Please access this example through a server, and not through the file system, as the gateway is set as a relative url. Alternatively you can change the url in the class.");
			//}
			
			_netConnection.connect(_phpPath);
			_netConnection.call(_amfClass + "/getUserInfo", new Responder(onGetUserInfoComplete, onGetUserInfoError), cnId.toString());
		}
		
		private function onGetUserInfoComplete(result:Object)
		{
			Debugger.Log("onGetUserInfoComplete");
			Debugger.Log("info: " + result.info);
			Debugger.Log("result.data: " + result.data);
			
			if (result.data == null || result.data == undefined || result.data == "")
			{
				Debugger.Log("NO USER");
			} else {
				_user	= new DBUser(_tmpCNId);
				for (var varName in result.data){
					Debugger.Log("data[" + varName + "]: " + result.data[varName]);
					_user.setVar(varName, result.data[varName]);
				}
			}
			
			if (_currentResponder) _currentResponder.result(_user);
		}
		
		private function onGetUserInfoError(info:Object)
		{
			Debugger.Log("onGetUserInfoError Error: " + info);
			
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
		
		
		/* SAVE USER INFO */
		override public function saveUserInfo(cnId:String, responder:SimpleResponder):void
		{
			Debugger.Log("saveUserInfo(" + cnId + ")");
			super.saveUserInfo(cnId, responder);
			if (_user == null)
			{
				Debugger.Log("User == null");
				if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault("Null user");
				return;
			}
			
			var userInfo : Object = new Object();
			_user.setVar("name", Config.user_name);
			for (var varName in _user.vars){
				Debugger.Log("user data[" + varName + "]: " + _user.vars[varName]);
				userInfo[varName] = _user.vars[varName];
			}
			if (userInfo["id"] != undefined) delete userInfo["id"];
			if (userInfo["rank"] != undefined) delete userInfo["rank"];
			
			//if(false && loaderInfo.url.indexOf("file://") == 0){
				//Debugger.Log("Please access this example through a server, and not through the file system, as the gateway is set as a relative url. Alternatively you can change the url in the class.");
			//}else{
			
			var hashStr : String	= (userInfo['grenades']).toString() + (userInfo['shots_power']).toString() + (userInfo['shots_distance']).toString() + (userInfo['extra_life_slots']).toString() + (userInfo['money']).toString() + (userInfo['score_level_1']).toString() + (userInfo['score_level_2']).toString() + (userInfo['score_level_3']).toString() + (userInfo['score_level_4']).toString() + (userInfo['score_level_0']).toString();
			var hash : String = MD5.hash(cnId.toString() + hashStr + "queremos a los chili peppers");
			
				_netConnection = new NetConnection();
				_netConnection.connect(_phpPath);
				_netConnection.call(_amfClass + "/setUserInfo", new Responder(setUserInfoComplete, onSetUserInfoError), cnId.toString(), userInfo, hash);
			//}
		}
		
		private function setUserInfoComplete(result:Object):void{
			Debugger.Log("setUserInfoComplete");
			Debugger.Log("info: " + result.info);
			
			if (_currentResponder) _currentResponder.result(_user);
		}
		
		private function onSetUserInfoError(info:Object)
		{
			Debugger.Log("onSetUserInfoError Error: " + info);
			
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
		
		
		/* CREATE NEW USER */
		override public function createNewUser(cnId:String, name: String, responder:SimpleResponder):void
		{
			Debugger.Log("createNewUser: " + cnId + " - " + name);
			super.createNewUser(cnId, name, responder);
			
			// If there is local data, use it. Otherwise, create new user from scratch.
			var newUserInfo : Object = DBComSharedObject.getLocalData();
			if (newUserInfo != null)
			{
				// Use local data
				Debugger.Log("USE LOCAL DATA");
				for (var varName in newUserInfo) {
					Debugger.Log("LOCALDATA[" + varName + "]: " + newUserInfo[varName]);
				}
			}
			else
			{
				newUserInfo = new Object();
				// Create from scratch
				Debugger.Log("CREATE FROM SCRATCH");
				var newUserInitVarsStr		: String	= Config.getValue("new_user_init_vars");
				var newUserInitVarsArr		: Array		= newUserInitVarsStr.split(",");
				for each (var newUserVarStr:String in newUserInitVarsArr)
				{
					var newUserVarArr : Array = newUserVarStr.split(":");
					var newUserVarName 	: String = newUserVarArr[0];
					var newUserVarValue : String = newUserVarArr[1];
					Debugger.Log("New user data[" + newUserVarName + "]: " + newUserVarValue);
					newUserInfo[newUserVarName] = newUserVarValue;
				}
				
				// TMP
				var teamInt : int = MathUtils.random(1, 3, true);
				newUserInfo["team_id"] = teamInt.toString();
				
			}
			
			newUserInfo["name"] = name;
			newUserInfo["cn_id"] = cnId;
			
			

			
			//if(false && loaderInfo.url.indexOf("file://") == 0){
				//Debugger.Log("Please access this example through a server, and not through the file system, as the gateway is set as a relative url. Alternatively you can change the url in the class.");
			//}else{
			
			var hash : String = MD5.hash(cnId.toString() + "queremos a los chili peppers");
			
				_netConnection = new NetConnection();
				_netConnection.connect(_phpPath);
				_netConnection.call(_amfClass + "/createNewUser", new Responder(createNewUserComplete, onCreateNewUserError), newUserInfo, hash);
			//}
		}
		
		private function createNewUserComplete(result:Object):void{
			Debugger.Log("createNewUserComplete");
			Debugger.Log("info: " + result.info);
			
			if (_currentResponder) _currentResponder.result(_user);
		}
		
		private function onCreateNewUserError(info:Object)
		{
			Debugger.Log("onCreateNewUserError Error: " + info);
			
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
		
		
		
		/* GET TEAMS INFO */
		override public function getTeamsInfo(responder:SimpleResponder):void 
		{
			super.getTeamsInfo(responder);
			
			//if(loaderInfo.url.indexOf("file://") == 0){
				//log("Please access this example through a server, and not through the file system, as the gateway is set as a relative url. Alternatively you can change the url in the class.");
			//}
			
			_netConnection.connect(_phpPath);
			_netConnection.call(_amfClass + "/getTeamsInfo", new Responder(onGetTeamsInfoComplete, ongetTeamsInfoError));
		}
		
		private function onGetTeamsInfoComplete(result:Object)
		{
			Debugger.Log("onGetTeamsInfoComplete");
			Debugger.Log("info: " + result.info);
			
			if (result.data == null || result.data == undefined || result.data == "")
			{
				Debugger.Log("NO TEAMS");
			} else {
				_teams = new Dictionary();
				
				for (var varName in result.data) {
					
					var teamData : Object = result.data[varName];
					Debugger.Log("teamData: " + teamData);
					Debugger.Log("teamData[id]: " + teamData["id"]);
					if (teamData["id"] != "1" && teamData["id"] != "2" && teamData["id"] != "3")
					{
						continue;
					}
					
					
					var teamId : String = teamData["id"];
					Debugger.Log("Getting team... " + teamId);
					delete(teamData["id"]);
					
					var newTeam : DBTeam = new DBTeam(teamId);
					for(var teamDataVarName in teamData)
					{
						Debugger.Log("teamDataVarName[" + teamDataVarName + "]: " + teamData[teamDataVarName]);
						newTeam.setVar(teamDataVarName, teamData[teamDataVarName]);
					}
					_teams[teamId] = newTeam;
				}
				
			}
			
			if (_currentResponder) _currentResponder.result(_teams);
		}
		
		private function ongetTeamsInfoError(info:Object)
		{
			Debugger.Log("ongetTeamsInfoError Error: " + info);
			
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
		
		
		
		/* GET LAST WEEK RESULTS */
		override public function getLastWeekResults(responder:SimpleResponder):void 
		{
			super.getLastWeekResults(responder);
			
			//if(loaderInfo.url.indexOf("file://") == 0){
				//log("Please access this example through a server, and not through the file system, as the gateway is set as a relative url. Alternatively you can change the url in the class.");
			//}
			
			_netConnection.connect(_phpPath);
			_netConnection.call(_amfClass + "/getLastWeekResults", new Responder(getLastWeekResultsComplete, getLastWeekResultsError));
		}
		
		private function getLastWeekResultsComplete(result:Object)
		{
			Debugger.Log("getLastWeekResultsComplete");
			Debugger.Log("info: " + result.info);
			
			var team	: DBTeam = null;
			var i : int = 0;
			
			// RESET PREV RESULTS
			for each(team in _teams)
			{
				for (i = 1; i <= Config.num_levels; i++)
				{
					team.setVar("last_week_score_level_" + i.toString(), 0);
				}
				
			}
			
			
			if (result.data == null || result.data == undefined || result.data == "")
			{
				Debugger.Log("NO TEAMS");
			} else {
				
				for (var varName in result.data) {
					
					var teamData : Object = result.data[varName];
					Debugger.Log("teamData: " + teamData);
					
					var teamId : String = teamData["id"];
					Debugger.Log("teamData[id]: " + teamData["id"]);
					if (teamData["id"] != "1" && teamData["id"] != "2" && teamData["id"] != "3")
					{
						continue;
					}
					
					team	= getTeam(teamId);
					Debugger.Log("Getting team... " + teamId);
					delete(teamData["id"]);
					
					for (i = 1; i <= Config.num_levels; i++)
					{
						var lastWeekScore	: int = teamData["last_week_score_level_" + i.toString()];
						team.setVar("last_week_score_level_" + i.toString(), lastWeekScore);
						Debugger.Log("lastWeekScore(" + i.toString() + "): " + lastWeekScore);
					}
				}
				
			}
			
			if (_currentResponder) _currentResponder.result(_teams);
		}
		
		private function getLastWeekResultsError(info:Object)
		{
			Debugger.Log("getLastWeekResultsError Error: " + info);
			
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
		
		
		
		/* GET LAST DAY RESULTS */
		override public function getLastDayResults(responder:SimpleResponder):void 
		{
			super.getLastDayResults(responder);
			Debugger.Log("DBComPhp - getLastDayResults - Responder: " + responder);
			//if(loaderInfo.url.indexOf("file://") == 0){
				//log("Please access this example through a server, and not through the file system, as the gateway is set as a relative url. Alternatively you can change the url in the class.");
			//}
			
			_netConnection.connect(_phpPath);
			_netConnection.call(_amfClass + "/getLastDayResults", new Responder(getLastDayResultsComplete, getLastDayResultsError));
		}
		
		private function getLastDayResultsComplete(result:Object)
		{
			Debugger.Log("DBComPhp - getLastDayResultsComplete");
			Debugger.Log("info: " + result.info);
			Debugger.Log("result.data: " + result.data);
			Debugger.Log("DBComPhp - _currentResponder: " + _currentResponder);
			
			var team	: DBTeam = null;
			var i : int = 0;
			
			// RESET PREV RESULTS
			for each(team in _teams)
			{
				for (i = 1; i <= Config.num_levels; i++)
				{
					team.setVar("last_day_score_level_" + i.toString(), 0);
				}
				
			}
			
			if (result.data == null || result.data == undefined || result.data == "")
			{
				Debugger.Log("NO TEAMS");
			} else {
				
				for (var varName in result.data) {
					
					var teamData : Object = result.data[varName];
					Debugger.Log("teamData: " + teamData);
					
					var teamId : String = teamData["id"];
					Debugger.Log("teamData[id]: " + teamData["id"]);
					if (teamData["id"] != "1" && teamData["id"] != "2" && teamData["id"] != "3")
					{
						continue;
					}
					
					team	= getTeam(teamId);
					Debugger.Log("Getting team... " + teamId);
					delete(teamData["id"]);
					
					for (i = 1; i <= Config.num_levels; i++)
					{
						var lastDayScore	: int = teamData["last_day_score_level_" + i.toString()];
						team.setVar("last_day_score_level_" + i.toString(), lastDayScore);
						Debugger.Log("lastDayScore(" + i.toString() + "): " + lastDayScore);
					}
				}
				
			}
			
			Debugger.Log("_currentResponder: " + _currentResponder);
			Debugger.Log("_currentResponder.result: " + _currentResponder.result);
			if (_currentResponder) _currentResponder.result(_teams);
		}
		
		private function getLastDayResultsError(info:Object)
		{
			Debugger.Log("getLastDayResultsError Error: " + info);
			
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
		
		
		
		
		
		/* ADD MONEY TO USER */
		override public function addMoneyToUser(cnId:String, deltaMoney:int, responder:SimpleResponder):void
		{
			super.addMoneyToUser(cnId, deltaMoney, responder);
			Debugger.Log("addMoneyToUser(" + cnId + " -> +" + deltaMoney + " money");
			
			var hash : String = MD5.hash(cnId.toString() + deltaMoney.toString() + "queremos a los chili peppers");
			
			_netConnection = new NetConnection();
			_netConnection.connect(_phpPath);
			_netConnection.call(_amfClass + "/addMoneyToUser", new Responder(addMoneyToUserComplete, addMoneyToUserError), cnId, deltaMoney, hash);
		}
		
		private function addMoneyToUserComplete(data:Object):void 
		{
			Debugger.Log("addMoneyToUserComplete: " + data);
			if (_currentResponder != null && _currentResponder.result != null) _currentResponder.result(data);
		}
		
		private function addMoneyToUserError(info:Object):void 
		{
			Debugger.Log("addMoneyToUserError: " + info);
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
		
		
		/* SAVE TEAM INFO */
		override public function saveTeamInfo(teamId:String, responder:SimpleResponder):void 
		{
			Debugger.Log("saveTeamInfo");
			super.saveTeamInfo(teamId, responder);
			if (_teams == null || _teams[teamId] == undefined)
			{
				Debugger.Log("Teams null or team undefined");
				if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault("Teams null or team undefined");
				return;
			}
			
			var team : DBTeam = _teams[teamId] as DBTeam;
			var teamInfo : Object = new Object();
			
			for (var varName in team.vars){
				Debugger.Log("team data[" + varName + "]: " + team.vars[varName]);
				teamInfo[varName] = team.vars[varName];
			}
			
			//if(false && loaderInfo.url.indexOf("file://") == 0){
				//Debugger.Log("Please access this example through a server, and not through the file system, as the gateway is set as a relative url. Alternatively you can change the url in the class.");
			//}else{
				_netConnection = new NetConnection();
				_netConnection.connect(_phpPath);
				_netConnection.call(_amfClass + "/setTeamInfo", new Responder(setTeamInfoComplete, onSetTeamInfoError), teamId.toString(), teamInfo);
			//}
		}
		
		private function setTeamInfoComplete(result:Object):void{
			Debugger.Log("setTeamInfoComplete");
			Debugger.Log("info: " + result.info);
			
			if (_currentResponder) _currentResponder.result(_teams[_tmpTeamId]);
		}
		
		private function onSetTeamInfoError(info:Object)
		{
			Debugger.Log("onSetTeamInfoError Error: " + info);
			
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
		
		
		/* ADD TEAM SCORE */
		override public function addTeamScore(teamId:String, levelNbr:int, deltaScore:int, responder:SimpleResponder):void 
		{
			Debugger.Log("addTeamScore(" + teamId + " -> +" + deltaScore + " points");
			super.addTeamScore(teamId, levelNbr, deltaScore, responder);
			
			var hash : String = MD5.hash(teamId.toString() + levelNbr.toString() + deltaScore.toString() + "queremos a los chili peppers");
			
			
			_netConnection = new NetConnection();
			_netConnection.connect(_phpPath);
			_netConnection.call(_amfClass + "/addTeamScore", new Responder(addTeamScoreComplete, addTeamScoreError), teamId, levelNbr, deltaScore, hash);
		}
		
		private function addTeamScoreComplete(result:Object):void{
			Debugger.Log("addTeamScoreComplete");
			Debugger.Log("info: " + result.info);
			
			if (_currentResponder) _currentResponder.result(_teams[_tmpTeamId]);
		}
		
		private function addTeamScoreError(info:Object)
		{
			Debugger.Log("addTeamScoreError Error: " + info);
			
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
		
		
		
		
		
		/* GET RANKING */
		override public function getRankingLevel(cnId:String, levelNbr:int, numRowsEnExtremos:int, responder:SimpleResponder):void
		{
			super.getRankingLevel(cnId, levelNbr, numRowsEnExtremos, responder);
			
			Debugger.Log("getRankingLevel(" + levelNbr + ")");
			//if(loaderInfo.url.indexOf("file://") == 0){
				//log("Please access this example through a server, and not through the file system, as the gateway is set as a relative url. Alternatively you can change the url in the class.");
			//}
			
			_netConnection.connect(_phpPath);
			_netConnection.call(_amfClass + "/getRankingLevel", new Responder(onGetRankingLevelComplete, onGetRankingLevelError), cnId, levelNbr, numRowsEnExtremos);
		}
		
		function onGetRankingLevelComplete(result:Object):void {
			
			Debugger.Log("onGetRankingLevelComplete: " + result);
			var rankingPlayers	: Vector.<Object> = new Vector.<Object>();
			for (var varName in result.data) {
				var playerData : Object = result.data[varName];
				rankingPlayers.push(playerData);
			}
			if (_currentResponder) _currentResponder.result(rankingPlayers);
		}	
		
		
		
		private function onGetRankingLevelError(info:Object)
		{
			Debugger.Log("onGetRankingLevelError Error: " + info);
			
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
		
		
		
		
		
		
		/* GET GAME INFO */
		override public function getGameInfo(responder:SimpleResponder):void 
		{
			super.getGameInfo(responder);
			
			//if(loaderInfo.url.indexOf("file://") == 0){
				//log("Please access this example through a server, and not through the file system, as the gateway is set as a relative url. Alternatively you can change the url in the class.");
			//}
			
			_netConnection.connect(_phpPath);
			_netConnection.call(_amfClass + "/getGameInfo", new Responder(onGetGameInfoComplete, onGetGameInfoError));
		}
		
		private function onGetGameInfoComplete(result:Object)
		{
			Debugger.Log("onGetGameInfoComplete");
			Debugger.Log("info: " + result.info);
			
			if (result.data == null || result.data == undefined || result.data == "")
			{
				Debugger.Log("NO GAME");
			} else {
				_game	= new DBGame();
				for (var varName in result.data){
					Debugger.Log("data[" + varName + "]: " + result.data[varName]);
					_game.setVar(varName, result.data[varName]);
				}
			}
			
			if (_currentResponder) _currentResponder.result(_game);
		}
		
		private function onGetGameInfoError(info:Object)
		{
			Debugger.Log("onGetGameInfoError Error: " + info);
			
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
		
		
		/* SAVE GAME INFO */
		override public function saveGameInfo(responder:SimpleResponder):void 
		{
			Debugger.Log("saveGameInfo");
			super.saveGameInfo(responder);
			if (_game == null)
			{
				Debugger.Log("Game == null");
				if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault("Null game");
				return;
			}
			
			var gameInfo : Object = new Object();
			
			for (var varName in _game.vars){
				Debugger.Log("game data[" + varName + "]: " + _game.vars[varName]);
				gameInfo[varName] = _game.vars[varName];
			}
			
			//if(false && loaderInfo.url.indexOf("file://") == 0){
				//Debugger.Log("Please access this example through a server, and not through the file system, as the gateway is set as a relative url. Alternatively you can change the url in the class.");
			//}else{
				_netConnection = new NetConnection();
				_netConnection.connect(_phpPath);
				_netConnection.call(_amfClass + "/setGameInfo", new Responder(setGameInfoComplete, onSetGameInfoError), DBGame.ID, gameInfo);
			//}
		}
		
		private function setGameInfoComplete(result:Object):void{
			Debugger.Log("setGameInfoComplete");
			Debugger.Log("info: " + result.info);
			
			if (_currentResponder) _currentResponder.result(_game);
		}
		
		private function onSetGameInfoError(info:Object)
		{
			Debugger.Log("onSetGameInfoError Error: " + info);
			
			if (_currentResponder != null && _currentResponder.fault != null) _currentResponder.fault(info);
		}
	}
}
