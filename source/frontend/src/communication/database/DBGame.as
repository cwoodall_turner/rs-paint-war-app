package communication.database 
{
import flash.utils.Dictionary;

/**
	 * ...
	 * @author alfred
	 */
	public class DBGame 
	{
		public static const ID	: String	= "1";
		
		private var _vars	: Dictionary	= null;
		
		public function DBGame() 
		{
			_vars	= new Dictionary();
		}
		
		public function getVar(key:String):*
		{
			if (_vars && _vars[key] != undefined) return _vars[key];
			return null;
		}
		
		public function setVar(key:String, value:*):void
		{
			if (_vars == null) _vars = new Dictionary();
			_vars[key] = value;
		}
		
		public function get vars():Dictionary 
		{
			return _vars;
		}
		
	}

}