package communication.database 
{
import flash.utils.Dictionary;

/**
	 * ...
	 * @author DBTeam
	 */
	public class DBTeam 
	{
		private var _teamId	: String	= "0";
		
		private var _vars	: Dictionary	= null;
		
		private var _sortValue : int = 0;
		
		public function DBTeam($teamId:String) 
		{
			_teamId	= $teamId;
			_vars	= new Dictionary();
		}
		
		public function get teamId():String 
		{
			return _teamId;
		}
		
		public function set teamId(value:String):void 
		{
			_teamId = value;
		}
		
		public function getVar(key:String):*
		{
			if (_vars && _vars[key] != undefined) return _vars[key];
			return null;
		}
		
		public function setVar(key:String, value:*):void
		{
			if (_vars == null) _vars = new Dictionary();
			_vars[key] = value;
		}
		
		public function getTotalScore():int 
		{
			var totalScore : int = 0;
			var numLevels : int = Config.num_levels;
			for (var i : int = 1; i <= numLevels; i++) {
				var levelScore : int = getVar("score_level_" + i.toString());
				Debugger.Log("Level " + i + " score: " + levelScore);
				totalScore += levelScore;
			}
			Debugger.Log("* PRE RETURN totalScore: " + totalScore);
			return totalScore;
		}
		
		public function getLevelScore(levelNbr:int):int 
		{
			return int(getVar("score_level_" + levelNbr.toString()));
		}
		
		public function getLastDayLevelScore(levelNbr:int):int 
		{
			return int(getVar("last_day_score_level_" + levelNbr.toString()));
		}
		
		public function getLastWeekLevelScore(levelNbr:int):int 
		{
			return int(getVar("last_week_score_level_" + levelNbr.toString()));
		}
		
		public function get vars():Dictionary 
		{
			return _vars;
		}
		
		public function get sortValue():int 
		{
			return _sortValue;
		}
		
		public function set sortValue(value:int):void 
		{
			_sortValue = value;
		}
		
	}

}