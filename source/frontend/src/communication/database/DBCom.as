package communication.database 
{
import com.turner.caf.business.SimpleResponder;

import flash.utils.Dictionary;

/**
	 * ...
	 * @author alfred
	 */
	public class DBCom
	{
		//protected static var _instance	: DBCom	= null;
		
		protected const ACCESS_KEY	: String	= "AKIAJJV7SBZFN7LSOM3A";
		protected const SECRET_KEY	: String	= "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";
		
		protected var _user:DBUser;
		protected var _teams:Dictionary;
		protected var _game:DBGame;
		
		protected var _currentResponder	: SimpleResponder	= null;
		
		protected var _tmpCNId		: String	= "";
		protected var _tmpName		: String	= "guest";
		protected var _tmpTeamId	: String	= "0";
		protected var _tmpLevelNbr	: int = 1;
		
		
		public function DBCom() {
			//_instance = this;
		}
		
		//static public function get instance():DBCom 
		//{
			//return _instance;
		//}
		
		public function initialize(responder:SimpleResponder):void { }
		
		public function getUserInfo(cnId:String, responder:SimpleResponder):void
		{
			_tmpCNId	= cnId;
			_currentResponder	= responder;
		}
		
		public function saveUserInfo(cnId:String, responder:SimpleResponder):void
		{
			_tmpCNId	= cnId;
			_currentResponder	= responder;
		}
		
		
		public function createNewUser(cnId:String, name: String, responder:SimpleResponder):void
		{
			_tmpCNId	= cnId;
			_tmpName	= name;
			_currentResponder	= responder;
		}
		
		
		public function get user():DBUser
		{
			return _user;
		}
		
		
		
		public function getTeamsInfo(responder:SimpleResponder):void
		{
			_currentResponder	= responder;
		}
		
		public function addTeamScore(teamId:String, levelNbr:int, deltaScore:int, responder:SimpleResponder):void 
		{
			_tmpTeamId	= teamId;
			_currentResponder	= responder;
		}
		
		public function saveTeamInfo(teamId:String, responder:SimpleResponder):void 
		{
			_tmpTeamId	= teamId;
			_currentResponder	= responder;
		}
		
		public function getTeam(teamId:String):DBTeam
		{
			if (_teams != null && _teams[teamId] != undefined) return _teams[teamId] as DBTeam;
			return null;
		}
		
		
		
		public function getGameInfo(responder:SimpleResponder):void
		{
			_currentResponder	= responder;
		}
		
		public function saveGameInfo(responder:SimpleResponder):void 
		{
			_currentResponder	= responder;
		}
		
		public function getRankingLevel(cnId:String, levelNbr:int, numRowsEnExtremos:int, responder:SimpleResponder):void
		{
			_tmpLevelNbr		= levelNbr;
			_currentResponder = responder;
		}
		
		public function addMoneyToUser(cnId:String, deltaMoney:int, responder:SimpleResponder):void
		{
			_tmpCNId = cnId;
			_currentResponder = responder;
		}
		
		public function getLastWeekResults(responder:SimpleResponder):void 
		{
			_currentResponder = responder;
		}
		
		public function getLastDayResults(responder:SimpleResponder):void 
		{
			_currentResponder = responder;
		}
		
		public function get game():DBGame
		{
			return _game;
		}
		
		public function get teams():Dictionary 
		{
			return _teams;
		}
	}
}