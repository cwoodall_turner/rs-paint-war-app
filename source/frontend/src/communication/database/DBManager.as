package communication.database 
{
import com.turner.caf.business.SimpleResponder;

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.utils.Dictionary;
import flash.utils.getTimer;

/**
	 * ...
	 * @author alfred
	 */
	public class DBManager extends EventDispatcher
	{
		public static const COMPLETE	: String	= "complete";
		
		private static var _instance : DBManager = null;
		
		private var _isNewDBUser				: Boolean	= false;
		private var _userCreationAttemptsLeft	: int	    = 2;
		private var _tmpCnId					: String    = "";
		
		private var _responder	: SimpleResponder = null;
		
		private var _userDBCom : DBCom = null;
		private var _gameDBCom : DBCom = null;
		private var _teamDBCom : DBCom = null;
		
		public function DBManager() 
		{
		}
		
		static public function get instance():DBManager 
		{
			if (_instance == null) _instance = new DBManager();
			return _instance;
		}
		
		public function get isNewUser():Boolean 
		{
			return _isNewDBUser;
		}
		
		public function get user():DBUser
		{
			return _userDBCom.user;
		}
		
		public function getTeam(teamId:String):DBTeam
		{
			return _teamDBCom.getTeam(teamId);
		}
		
		public function get teams():Dictionary 
		{
			return _teamDBCom.teams;
		}
		
		public function get game():DBGame
		{
			return _gameDBCom.game;
		}
		
		private function get userDBCom():DBCom 
		{
			return _userDBCom;
		}
		
		private function get gameDBCom():DBCom 
		{
			return _gameDBCom;
		}
		
		private function get teamDBCom():DBCom 
		{
			return _teamDBCom;
		}
		
		private function get responder():SimpleResponder 
		{
			return _responder;
		}
		
		private function set responder(value:SimpleResponder):void 
		{
			Debugger.Log("SET RESPONDER: " + value);
			if (_responder != null)
			{
				Debugger.Log("WARNING! DBManager - Overriding responder!");
			}
			_responder = value;
			Debugger.LogStack();
		}
		
		public function isReady():Boolean
		{
			return (_responder == null);
		}
		
		private function onGeneralOk(data:Object):void 
		{
			if (_responder != null && _responder.result != null) _responder.result(data);
			resetResponder();
			dispatchEvent(new Event(COMPLETE));
		}
		
		private function onGeneralError(info:Object):void 
		{
			if (_responder != null && _responder.fault != null) _responder.fault(info);
			resetResponder();
			dispatchEvent(new Event(COMPLETE));
		}
		
		private function resetResponder():void 
		{
			Debugger.Log("RESET RESPONDER");
			_responder	= null;
			Debugger.LogStack();
		}
		
		
		public function getDBData(cnId:String, attemptNumbers:int, $responder:SimpleResponder):void
		{
			_tmpCnId	= cnId; 
			_userCreationAttemptsLeft	= attemptNumbers;
			responder = $responder;
			_isNewDBUser	= false;
			
			if (cnId == Config.guest_cn_id && !(_userDBCom is DBComSharedObject))
			{
				Debugger.Log("GUEST! Using DBComSharedObject...");
				_userDBCom = new DBComSharedObject();
				_userDBCom.initialize(new SimpleResponder(onDBComInitOk, onDBComInitError));
			}
			else if (cnId != Config.guest_cn_id && !(_userDBCom is DBComPhp))
			{
				Debugger.Log("USER! Using DBComPhp...");
				_userDBCom = new DBComPhp();
				_userDBCom.initialize(new SimpleResponder(onDBComInitOk, onDBComInitError));
			}
			else
			{
				getUserInfo();
			}
		}
		
		private function onDBComInitError(info:Object):void 
		{
			onGeneralError(info);
		}
		
		private function onDBComInitOk(data:Object):void 
		{
			getUserInfo();
		}
		
		
		public function initialize($responder:SimpleResponder):void 
		{
			responder = $responder;
			
			_userDBCom = _gameDBCom = _teamDBCom = new DBComPhp();
			_userDBCom.initialize(new SimpleResponder(onInitializeOk, onInitializeError));
		}
		
		private function onInitializeOk(data:Object):void 
		{
			onGeneralOk(data);
		}
		
		
		private function onInitializeError(info:Object):void 
		{
			onGeneralOk(info);
		}
		
		
		public function getRankingLevel(cnId:String, levelNbr:int, numRowsEnExtremos:int, $responder:SimpleResponder):void
		{
			responder = $responder;
			_userDBCom.getRankingLevel(cnId, levelNbr, numRowsEnExtremos, new SimpleResponder(onGetRankingLevelOk, onGetRankingLevelError));
		}
		
		private function onGetRankingLevelOk(data:Object):void 
		{
			onGeneralOk(data);
		}
		
		private function onGetRankingLevelError(info:Object):void 
		{
			onGeneralError(info);
		}
		
		public function getLastWeekResults($responder:SimpleResponder):void 
		{
			responder = $responder;
			teamDBCom.getLastWeekResults(new SimpleResponder(getLastWeekResultsOk, getLastWeekResultsError));
		}
		
		private function getLastWeekResultsOk(data:Object):void 
		{
			onGeneralOk(data);
		}
		
		private function getLastWeekResultsError(info:Object):void 
		{
			onGeneralError(info);
		}
		
		public function getLastDayResults($responder:SimpleResponder):void 
		{
			responder = $responder;
			teamDBCom.getLastDayResults(new SimpleResponder(getLastDayResultsOk, getLastDayResultsError));
		}
		
		private function getLastDayResultsOk(data:Object):void 
		{
			Debugger.Log("DBManager - getLastDayResultsOk: " + getLastDayResultsOk);
			onGeneralOk(data);
		}
		
		private function getLastDayResultsError(info:Object):void 
		{
			onGeneralError(info);
		}
		
		
		
		private function getUserInfo():void 
		{
			userDBCom.getUserInfo(_tmpCnId, new SimpleResponder(onGetUserInfoComplete, onGetUserInfoError));
		}
		
		private function onGetUserInfoComplete(data:Object):void 
		{
			Debugger.Log("onGetUserInfoComplete (" + data + ")");
			
			if (data == null)
			{
				Debugger.Log("Inexistent DB user. Creating new...");
				_isNewDBUser	= true;
				if (_userCreationAttemptsLeft > 0)
				{
					createNewDBUser(_tmpCnId);
				} else {
					Debugger.Log("Inexistent DB user. Alrady tried to create new. Oops!");
					onGetDbUserCompleteError("Inexistent DB user. Alrady tried to create new. Oops!");
				}
			}
			else
			{
				onGetDbUserCompleteOk();
			}
		}
		
		private function createNewDBUser(id:String):void
		{
			_userCreationAttemptsLeft--;
			Debugger.Log("createNewDBUser: " + id + " - _userCreationAttemptsLeft: " + _userCreationAttemptsLeft);
			userDBCom.createNewUser(id, Config.user_name, new SimpleResponder(onCreateDBUserComplete, onCreateDBUserError));
		}
		
		private function onCreateDBUserComplete(data:Object):void 
		{
			Debugger.Log("onCreateDBUserComplete");
			userDBCom.getUserInfo(_tmpCnId, new SimpleResponder(onGetUserInfoComplete, onGetUserInfoError));
		}
		
		private function onCreateDBUserError(info:Object):void 
		{
			Debugger.Log("onCreateDBUserError - Error creating new DB user: " + info);				
			onGetDBDataError("onCreateDBUserError - Error creating new DB user: " + info);
		}
		
		private function onGetUserInfoError(info:Object):void 
		{
			Debugger.Log("onGetUserInfoError: " + info);						
			onGetDBDataError("onGetUserInfoError: " + info);
		}
		
		private function onGetDbUserCompleteError(info:Object):void 
		{
			Debugger.Log("onGetDbUserCompleteError: " + info);
			onGetDBDataError("onGetDbUserCompleteError: " + info);
		}
		
		
		
		private function onGetDbUserCompleteOk():void 
		{
			getTeamsInfo();
		}
		
		private function getTeamsInfo():void 
		{
			teamDBCom.getTeamsInfo(new SimpleResponder(onGetTeamsInfoComplete, onGetTeamsInfoError));
		}
		
		private function onGetTeamsInfoError(info:Object):void 
		{
			Debugger.Log("onGetTeamsInfoError: " + info);						
			onGetDBDataError("onGetTeamsInfoError: " + info);
		}
		
		private function onGetTeamsInfoComplete(data:Object):void 
		{
			Debugger.Log("onGetTeamsInfoComplete (" + data + ")");
			
			if (data == null)
			{
				Debugger.Log("ERROR - No teams data.");
				onGetDBDataError("No teams data.");
			}
			else
			{
				onGetTeamsInfoCompleteOk();
			}
		}
		
		private function onGetTeamsInfoCompleteOk():void 
		{
			getGameInfo();
		}
		
		
		
		private function getGameInfo():void 
		{
			gameDBCom.getGameInfo(new SimpleResponder(onGetGameInfoComplete, onGetGameInfoError));
		}
		
		private function onGetGameInfoError(info:Object):void 
		{
			Debugger.Log("onGetGameInfoError: " + info);						
			onGetDBDataError("onGetGameInfoError: " + info);
		}
		
		private function onGetGameInfoComplete(data:Object):void 
		{
			Debugger.Log("onGetGameInfoComplete (" + data + ")");
			
			if (data == null)
			{
				Debugger.Log("ERROR - No game data.");
				onGetDBDataError("No game data.");
			}
			else
			{
				gameDBCom.game.setVar("game_time_at_time_of_day_ms", getTimer());
				onGeneralOk(data);
			}
		}
		
		
		
		
		public function saveUserData($responder:SimpleResponder):void 
		{
			responder = $responder;
			userDBCom.saveUserInfo(userDBCom.user.cnId, new SimpleResponder(onSaveUserDataOk, onSaveUserDataError));
		}
		
		private function onSaveUserDataOk(data:Object):void 
		{
			onGeneralOk(data);
		}
		
		private function onSaveUserDataError(info:Object):void 
		{
			onGeneralError(info);
		}
		
		
		
		
		
		public function addMoneyToUser(cnId:String, deltaMoney:int, $responder:SimpleResponder):void
		{
			Debugger.Log("DBM - addMoneyToUser");
			responder = $responder;
			userDBCom.addMoneyToUser(cnId, deltaMoney, new SimpleResponder(addMoneyToUserComplete, addMoneyToUserError));
		}
		
		private function addMoneyToUserComplete(data:Object):void 
		{
			Debugger.Log("addMoneyToUserComplete: " + data);
			onGeneralOk(data);
		}
		
		private function addMoneyToUserError(info:Object):void 
		{
			Debugger.Log("addMoneyToUserError: " + info);
			onGeneralError(info);
		}
		
		
		
		private var _tmpTeamId			: String 	= "";
		private var _tmpDeltaTeamScore	: int	= 0;
		private var _tmpLevelNbr		: int	= 0;
		public function saveAfterLevel(cnId:String, teamId:String, levelNbr:int, deltaScore:int, $responder:SimpleResponder):void
		{
			responder = $responder;
			_tmpTeamId 			= teamId;
			_tmpDeltaTeamScore 	= deltaScore;
			_tmpLevelNbr 		= levelNbr;
			
			Debugger.Log("DBM - saveAfterLevel");
			Debugger.Log("cnId: " + cnId);
			Debugger.Log("teamId: " + teamId);
			Debugger.Log("levelNbr: " + levelNbr);
			Debugger.Log("deltaScore: " + deltaScore);
			userDBCom.saveUserInfo(cnId, new SimpleResponder(onSaveAfterLevelUserComplete, onSaveAfterLevelError));
		}
		
		private function onSaveAfterLevelUserComplete(data:Object):void 
		{
			Debugger.Log("DBM - onSaveAfterLevelUserComplete");
			teamDBCom.addTeamScore(_tmpTeamId, _tmpLevelNbr, _tmpDeltaTeamScore, new SimpleResponder(onSaveAfterLevelTeamComplete, onSaveAfterLevelError));
		}
		
		private function onSaveAfterLevelTeamComplete(data:Object):void 
		{
			Debugger.Log("DBM - onSaveAfterLevelTeamComplete");
			teamDBCom.getTeamsInfo(new SimpleResponder(onSaveAfterLevelTeamInfoComplete, onSaveAfterLevelError));
		}
		
		private function onSaveAfterLevelTeamInfoComplete(data:Object):void 
		{
			onGeneralOk(data);
		}
		
		
		
		
		private function onSaveAfterLevelError(info:Object):void 
		{
			Debugger.Log("DBM - onSaveAfterLevelError");
			onGeneralError(info);
		}
		
		
		
		
		public function updateGameInfoAfterDayEnd($responder:SimpleResponder):void 
		{
			responder = $responder;
			gameDBCom.getGameInfo(new SimpleResponder(onUpdateGameInfoComplete, onUpdateGameInfoError));
		}
		
		private function onUpdateGameInfoComplete(data:Object):void 
		{
			Debugger.Log("onUpdateGameInfoComplete (" + data + ")");
			onGeneralOk(data);
		}
		
		private function onUpdateGameInfoError(info:Object):void 
		{
			Debugger.Log("onUpdateGameInfoError (" + info + ")");
			onGeneralError(info);
		}
		
		
		
		private function onGetDBDataError(info:Object):void 
		{
			onGeneralError(info);
		}
	}

}