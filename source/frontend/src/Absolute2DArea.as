﻿package {
	/**
	 * ...
	 * @author ahodes
	 */
	public class Absolute2DArea {
		
		protected var _leftX	: Number	= 0;
		protected var _rightX	: Number	= 0;
		protected var _upY		: Number	= 0;
		protected var _downY	: Number	= 0;
		
		public function Absolute2DArea($leftX:Number, $rightX:Number, $upY:Number, $downY:Number) {
			SetLimits($leftX, $rightX, $upY, $downY);
		}
		
		public function SetLimits($leftX:Number, $rightX:Number, $upY:Number, $downY:Number): void {
			_leftX	= $leftX;
			_rightX = $rightX;
			_upY	= $upY;
			_downY 	= $downY;
		}
		
		public function get leftX():Number 
		{
			return _leftX;
		}
		
		public function get rightX():Number 
		{
			return _rightX;
		}
		
		public function get width() : Number {
			return rightX - leftX;
		}
		
		public function get upY():Number 
		{
			return _upY;
		}
		
		public function get downY():Number 
		{
			return _downY;
		}
		
		public function get height() : Number {
			return _downY - _upY;
		}
		
		public function toString():String {
			return "[leftX: " + leftX.toFixed(1) + ", rightX: " + rightX.toFixed(1) + ", upY: " + upY.toFixed(1) + ", downY: " + downY.toFixed(1) + "]";
		}
		
		
		public function Move(dX:Number, dY:Number):void {
			_leftX	+= dX;
			_rightX	+= dX;
			_upY	+= dY;
			_downY 	+= dY;
		}
		
		
		public function SetLimitsLeftX($leftX:Number):void {
			_leftX	= $leftX;
		}
		
	}

}