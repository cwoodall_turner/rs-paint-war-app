﻿package {
import behaviors.BehaviorsLoader;

import com.turner.caf.business.SimpleResponder;
import com.turner.dmtla.cnla.corelib.vo.CosmosActionLogVO;

import communication.cn.CNCom;
import communication.cn.Omniture;
import communication.database.DBComSharedObject;
import communication.database.DBManager;

import flash.display.DisplayObjectContainer;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.media.Sound;
import flash.text.TextField;
import flash.ui.ContextMenu;
import flash.ui.ContextMenuItem;

import players.Player;

import txt.TextLoader;

import ui.UIFactory;
import ui.UIPreloaderTransition;
import ui.UITransition;
import ui.UIWidget;

import utils.MovieClipUtils;
import utils.StringUtils;

import view.LayerFactory;

public class MainApp extends MovieClip {
		
		public static var instance	: MainApp	= null;

		/* Preloader */
		private var _preloader	: MovieClip = null;

		// Game Flow
		private var _onTransitionFadeInOver		: Function	= null;
		private var _onTransitionFadeOutBegin	: Function	= null;
		private var _onTransitionFadeOutOver	: Function	= null;
		private var _onSpacebarPressed			: Function	= null;
		private var _testingWidgets				: Boolean	= false;
		private var _initRule					: Rule		= null;

		// News
		private var _lastWeekResultsShown	: Boolean	= false;
		private var _lastDayResultsShown	: Boolean	= false;

		// Debugging
		private var _debuggingStr	: String	= null;

		public function MainApp() {

			_debuggingStr							= (loaderInfo.parameters.debugging as String) == null?"":(loaderInfo.parameters.debugging as String);
			var lang			 		: String	= (loaderInfo.parameters.lang as String) == null?"":(loaderInfo.parameters.lang as String);

			Config.language	= lang;

			/* NOTE: Comment these lines to set Debugging via flashvars (Ref:[Debug01])*/
			/* [Debug01] */
            _debuggingStr			= "true";
			/* /[Debug01] */

			Debugger.Initialize(_debuggingStr == "true");

			Debugger.Log("BEGIN");
			GameContext.BasePath	= (loaderInfo.parameters.basePath as String) == null?"":(loaderInfo.parameters.basePath as String);
			instance = this;

			_initRule	= new Rule(["loads_complete", "cncom_complete", "dbcom_complete"], [preInitialize]);
			StartPreloader();
		}

		/*
		 * Starts the Preloader
		 * */
		private function StartPreloader() : void {
			Debugger.Log("StartPreloader");

			_preloader 		= this.getChildByName("preloader_mc") as MovieClip;
            Debugger.Log("_preloader: " + _preloader);
            
            if(_preloader != null)
            {
                onPreloaderAdded();
            }
            else
            {
                this.addEventListener(Event.ENTER_FRAME, getPreloader);
            }
		}

        private function getPreloader(event:Event):void {

            _preloader 		= this.getChildByName("preloader_mc") as MovieClip;
            Debugger.Log("_preloader: " + _preloader);
            if(_preloader != null)
            {
                onPreloaderAdded();
            }
        }

        private function onPreloaderAdded():void {
            this.removeEventListener(Event.ENTER_FRAME, getPreloader);

            _preloader.gotoAndStop(1);
            (_preloader.getChildByName("progress_mc") as MovieClip).gotoAndStop(1);

            // Load config
            ConfigLoader.Instance.addEventListener(ConfigLoader.LOAD_COMPLETE, OnConfigLoadComplete);
            ConfigLoader.Instance.LoadConfig(GameContext.BasePath + "config/config.xml");
        }


		private function OnConfigLoadComplete(e:Event):void {

			ConfigLoader.Instance.removeEventListener(ConfigLoader.LOAD_COMPLETE, OnConfigLoadComplete);
			Config.Initialize();


			// If debugging is set via flashVars, use it. Otherwise, use config setting.
			if (_debuggingStr == "true")
			{
				Config.debug = true;
			}
			else if (_debuggingStr == "false")
			{
				Config.debug = false;
			}
			else
			{

				Debugger.debugging = Config.debug;
			}


			TextLoader.Instance.addEventListener(TextLoader.LOAD_COMPLETE, OnTxtLoadComplete);
			TextLoader.Instance.LoadTexts(GameContext.BasePath + "texts/texts-" + Config.language + ".xml");

			// Initialize CNCom in parallel
			MainApp.instance.Log("CNCom - PRE Initialize");
			CNCom.instance.initialize(new SimpleResponder(onCNComInitComplete, onCNComInitError));


			// Initialize DBCom in parallel
			DBManager.instance.initialize(new SimpleResponder(onDBManagerInitComplete, onDBManagerInitError));
		}

		private function onCNComInitComplete(data:Object):void
		{
			_initRule.setConditionTrue("cncom_complete");
		}

		private function onCNComInitError(info:Object):void
		{
			Debugger.Log("onCNComInitError");
		}

		private function onDBManagerInitComplete(data:Object):void
		{
			_initRule.setConditionTrue("dbcom_complete");
		}

		private function onDBManagerInitError(info:Object):void
		{
			Debugger.Log("onDBManagerInitError");
		}

		private function OnTxtLoadComplete(e:Event):void {
			TextLoader.Instance.removeEventListener(TextLoader.LOAD_COMPLETE, OnTxtLoadComplete);
			CustomizeContextMenu();

			// Load Behaviors
			BehaviorsLoader.Instance.addEventListener(BehaviorsLoader.LOAD_COMPLETE, OnBehaviorsLoadComplete, false, 0, true);
			BehaviorsLoader.Instance.Load(GameContext.BasePath + "config/behaviors.xml");
		}

		private function OnBehaviorsLoadComplete(e:Event):void {
			BehaviorsLoader.Instance.removeEventListener(BehaviorsLoader.LOAD_COMPLETE, OnBehaviorsLoadComplete);

			addEventListener(Event.ENTER_FRAME, Preloader);
		}


		private function CustomizeContextMenu() : void {
			Debugger.Log("CustomizeContextMenu");
			var contextMenu:ContextMenu = new ContextMenu();
			contextMenu.hideBuiltInItems();
			var contextMenuItems	: Array	= String(TextLoader.Instance.Texts["ContextMenuItems"]).split(",");
			if (contextMenuItems == null) return;

			var menuTitle	: ContextMenuItem	= null;
			var i			: int				= contextMenuItems.length;
			while (--i >= 0) {
				menuTitle	= new ContextMenuItem(contextMenuItems[i], true);
				contextMenu.customItems.unshift(menuTitle);
			}
		}


		private function Preloader(event:Event) :void {
			var bytestotal = this.root.loaderInfo.bytesTotal;
			var bytesloaded = this.root.loaderInfo.bytesLoaded;

			var percentageLoaded 	: int	= Math.floor(bytesloaded / bytestotal * (100 - Config.external_swf_wght_pct));
			if (percentageLoaded == 0) percentageLoaded = 1;

			var progressTxt : TextField	= (_preloader.getChildByName("progress_mc") as MovieClip).getChildByName("progress_txt") as TextField;
			progressTxt.text = (StringUtils.IntToStr(percentageLoaded, 2));

			Debugger.Log("Preloader: " + bytestotal + "/" + bytesloaded);
			(_preloader.getChildByName("progress_mc") as MovieClip).gotoAndStop(percentageLoaded);

			if (bytesloaded >= bytestotal) {
				removeEventListener(Event.ENTER_FRAME, Preloader);
				ExternalLoader.Instance.Load(["sounds.swf", "splash.swf", "hud.swf", "map.swf", "transitions.swf", "characterselect.swf", "shop.swf", "ranking.swf", "weekresults.swf", "dayresults.swf", "daycomplete.swf"]);
				addEventListener(Event.ENTER_FRAME, OnExternalLoadProgress);
			}
		}

		private function OnExternalLoadProgress(e:Event):void {
			var perc :int = (100 - Config.external_swf_wght_pct) + Math.floor(ExternalLoader.Instance.GetLoadedPercent() * Config.external_swf_wght_pct / 100);
			if (perc > 99) perc = 99;
			var progressTxt : TextField	= (_preloader.getChildByName("progress_mc") as MovieClip).getChildByName("progress_txt") as TextField;
			progressTxt.text = (StringUtils.IntToStr(perc, 2));

			Debugger.Log("ExternalLoader.Instance.GetLoadedPercent(): " + ExternalLoader.Instance.GetLoadedPercent());
			(_preloader.getChildByName("progress_mc") as MovieClip).gotoAndStop(perc);

			if (ExternalLoader.Instance.loadComplete) {
				removeEventListener(Event.ENTER_FRAME, OnExternalLoadProgress);

				/* NOTE: If animations are to be cached, this is the place to do it (Ref: [Anim01])*/
				/* [Anim01] *
				AnimationCache.Instance.addEventListener(AnimationCache.CACHE_COMPLETE, OnCacheComplete);
				AnimationCache.Instance.cacheAnimations(["ExplossionAsset","SmokeAsset","ShootAsset"],[ExternalLoader.Instance.GetFxAsset,ExternalLoader.Instance.GetFxAsset,ExternalLoader.Instance.GetFxAsset],SimpleAnimation);
				**/
				OnCacheComplete(new Event("DummyEvent"));
				/* /[Anim01] */
			}
		}

		private function OnCacheComplete(e:Event):void {
			/* [Anim01] *
			AnimationCache.Instance.removeEventListener(AnimationCache.CACHE_COMPLETE,OnCacheComplete);
			Debugger.Log("======== OnCacheComplete ========");
			* /[Anim01] */

			_initRule.setConditionTrue("loads_complete");
		}

		private function preInitialize():void {
			if (Config.getValue("login_at_start") == "true")
			{
				CNCom.instance.doLogin(Config.getValue("cn_default_user"), Config.getValue("cn_default_password"), new SimpleResponder(onFirstLoginComplete, onFirstLoginError));
			}
			else
			{
				Initialize();
			}
		}

		private function onFirstLoginError(info:Object):void
		{
			Debugger.Log("PRE LOGIN ERROR");
			Initialize();
		}

		private function onFirstLoginComplete(data:Object):void
		{
			Debugger.Log("PRE LOGIN COMPLETE");
			Debugger.Log("2. CNCom.instance.isLogged: " + CNCom.instance.isLogged);
			CNCom.instance.reGetLoggedUser(new SimpleResponder(onCNInitializeLoggedInComplete, onCNInitializeLoggedInError));
		}

		private function onCNInitializeLoggedInComplete(data:Object):void
		{
			Debugger.Log("onCNInitializeLoggedInComplete");
			Initialize();
		}

		private function onCNInitializeLoggedInError(info:Object):void
		{
			Debugger.Log("onCNInitializeLoggedInError");
			Initialize();
		}

		private function Initialize():void {
			Debugger.Log("INITIALIZE!!!");

			var progressTxt : TextField	= (_preloader.getChildByName("progress_mc") as MovieClip).getChildByName("progress_txt") as TextField;
			progressTxt.text = "100";


			// Add sounds & music
			Sfx.Init();
			AddGlobalSoundsAndMusic();
			Sfx.PlayMusic("MusicSplash", 1);

			GameContext.Initialize(this, Config.game_width, Config.game_height, Debugger.debugging, Config.language);

			// Layer Factory
			/* Note: To create a complex layer (ie: A layer called 'COMPLEX_GAME' containing 'BACKGROUND', 'GAME_LAYER' and 'FOREGROUND':
			 *
			 * [LayerFactory.COMPLEX_GAME,LayerFactory.BACKGROUND,LayerFactory.GAME_LAYER, LayerFactory.FOREGROUND]
			 *
			 * */
			LayerFactory.Instance.Initialize(DisplayObjectContainer(this),
				//[LayerFactory.BACKGROUND2,
				//LayerFactory.BACKGROUND1,
				[LayerFactory.BACKGROUND,
				LayerFactory.SPECIALS,
				[LayerFactory.GAME_COMPLEX,
				LayerFactory.DEBUG_PATH_MAP,
				LayerFactory.DEBUG_CURRENT_PATH,
				LayerFactory.DEBUG_INIT_PATH,
				LayerFactory.FX_GAME_BACK,
				LayerFactory.GAME_SHADOWS,
				LayerFactory.GAME,
				LayerFactory.FX_GAME_FRONT],
				LayerFactory.FOREGROUND,
				LayerFactory.HUD,
				LayerFactory.UI]);

			// CANVAS DEBUGGER
			//new AreaDebugger(LayerFactory.Instance.GetLayer((LayerFactory.FX_GAME_BACK).addChild(new Sprite()) as Sprite).graphics);
			new AreaDebugger((LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_BACK).addChild(new Sprite()) as Sprite).graphics);

			// UIFactory
			UIFactory.Instance.Initialize(LayerFactory.Instance.GetLayer(LayerFactory.UI));

			GameManager.Instance.Initialize();

			// Fade Out Preloader and show Splash screen
			addChild(_preloader); // Pace it on top of the display list
			_preloader.addEventListener("AnimationEnd", OnPreloaderAnimationEnd);
			_preloader.gotoAndPlay("FadeOut");

			// Use keyboard to control flow (not using Input class so we don't depend on an EnterFrame Event)
			//GameContext.GlobalStage.addEventListener(KeyboardEvent.KEY_DOWN, OnKeyDown);

			// Shared Object Reset?
			if (Config.getValue("reset-shared-object") == "true") {
				DBComSharedObject.eraseData();
			}

			if (Config.test_badges) {
				GameContext.GlobalStage.addEventListener(KeyboardEvent.KEY_DOWN, OnKeyDown);
			}

			Omniture.trackView();
		}

		private function OnPreloaderAnimationEnd(e:Event):void
		{
			_preloader.addEventListener("AnimationEnd", OnPreloaderAnimationEnd);

			if (Config.getValue("quick_start") == "true")
			{
				OnGameEvent("PlayClicked");
			} else if (Config.getValue("skip_menu_intro") == "true") {
				UIFactory.Instance.MainMenu.ShowFadedIn();
			} else {
				UIFactory.Instance.MainMenu.FadeIn();
			}

			MovieClipUtils.Remove(_preloader);
			_preloader = null;
		}

		private function AddGlobalSoundsAndMusic():void {
			for each(var sfxId : String  in Config.global_sfx_ids) {
				Sfx.AddSound(sfxId, ExternalLoader.Instance.GetNewInstance(sfxId, Config.swf_path_sounds) as Sound);
			}
			for each(var musicId : String  in Config.global_music_ids) {
				Sfx.AddSound(musicId, ExternalLoader.Instance.GetNewInstance(musicId, Config.swf_path_sounds) as Sound, true);
			}
		}


		/*
		 * Use SPACEBAR to control flow
		 * @param	e
		 */
		private function OnKeyDown(e:KeyboardEvent):void {

			//Debugger.Log("e.keyCode: " +e.keyCode);
			if (_onSpacebarPressed != null && e.keyCode == 32) {
				_onSpacebarPressed();
			}


			if (Config.test_badges) {
				Debugger.Log("e.keyCode: " + e.keyCode);
				switch (e.keyCode) {
					case 49: // "1"
						EarnBadge("enemies_killed");
						break;
					case 50: // "2"
						EarnBadge("grenade_combo");
						break;
					case 51: // "3"
						EarnBadge("all_items");
						break;
					case 52: // "4"
						EarnBadge("muscleman");
						break;
					case 53: // "5"
						EarnBadge("unpaintable");
						break;
					case 54: // "6"
						EarnBadge("week_champion");
						break;
				}
			}
		}

		public function EarnBadge(badgeObjective:String):void
		{
			if (!Config.use_badges) return;

//			var gameName	: String = ConfigLoader.Instance.Config["badges_game_name"];
//			var badgeName	: String = ConfigLoader.Instance.Config["badge_name_" + badgeObjective];
//
//			var action : String = gameName + "_" + badgeName;
//			Debugger.Log("EARN BADGE " + badgeObjective + " - Action: " + action);
//			trace("EARN BADGE " + badgeObjective + " - Action: " + action);
//			try {
//				var result: Boolean = cnlaGames.notifyAction(action);
//			} catch (e:*) {
//				Debugger.Log("Error: " + e);
//				trace("Error: " + e);
//			}
//
//			Debugger.Log("result: " + result);
//			trace("result: " + result);
		}



		/**
		 * Plays the Transition widget and sets the _onTransitionFadeInOver and _onTransitionFadeOutOver arrays of functions.
		 *
		}*/
		public function PlayTransition(transitionWidget:UITransition, onTransitionFadeInOver:Function = null, onTransitionFadeOutBegin:Function = null, onTransitionFadeOutOver:Function = null, showFadedIn:Boolean = false, htmlText:String = "", htmlText2:String = "", silenceMusic:Boolean = false) :void {

			_onTransitionFadeInOver		= onTransitionFadeInOver;
			_onTransitionFadeOutBegin	= onTransitionFadeOutBegin;
			_onTransitionFadeOutOver	= onTransitionFadeOutOver;

			transitionWidget.HtmlText 	= htmlText;
			transitionWidget.HtmlText2 	= htmlText2;

			Debugger.Log("***** TRANSITION - htmlText: " + htmlText + " - " + "htmlText2: " + htmlText2 + "**********");
			if (silenceMusic)
			{
				Debugger.Log("silenceMusic: " + silenceMusic);
				Debugger.LogStack();
				Sfx.PlayMusic(Config.music_id_silence, 1);
			}

			if (showFadedIn) transitionWidget.ShowFadedIn();
			else transitionWidget.FadeIn();
		}


		/****************************************************************\
		 * 																*
		 * NOTE: A Level has the following stages:						*
		 * 1) Load 		-> Load external assets							*
		 * 2) Prepare 	-> Create instances, initialize objects, etc	*
		 * 3) Start 	-> Begin main loop								*
		 * 																*
		 * **************************************************************/

		/* Separate loads */
		private var _preloaderTransition	: UIPreloaderTransition	= null;
		private var _levelLoadedNbr			: int			= 0;
		private var _levelLoadedDifficulty	: String		= "a";
		private var _checkpointIdx			: int			= 0;

		private function LoadPrepareAndStartLevel(levelNbr:int, levelDifficulty:String, preloaderTransition:UIPreloaderTransition, widgetToHide:UIWidget = null, checkpointIdx:int = -1, isNewGame:Boolean = false):void
		{
			_levelLoadedNbr			= levelNbr;
			_levelLoadedDifficulty	= levelDifficulty;
			_checkpointIdx			= checkpointIdx;
			var swfsToLoad	: Array	= new Array();
			var swfsSizes	: Array	= new Array();

			/* Load Level assets once */
			var sceneryName : String = Config.getValue("scenery_name",levelNbr);
			AddToLoadQueueIfNotLoaded("scenery" + sceneryName + ".swf", swfsToLoad, swfsSizes);
			AddToLoadQueueIfNotLoaded("level" + levelNbr.toString() + ".swf", swfsToLoad, swfsSizes);

			AddToLoadQueueIfNotLoaded("exit.swf", swfsToLoad, swfsSizes);
			AddToLoadQueueIfNotLoaded("fx.swf", swfsToLoad, swfsSizes);
			AddToLoadQueueIfNotLoaded("mordecai.swf", swfsToLoad, swfsSizes);
			AddToLoadQueueIfNotLoaded("rigby.swf", swfsToLoad, swfsSizes);
			AddToLoadQueueIfNotLoaded("items.swf", swfsToLoad, swfsSizes);
			AddToLoadQueueIfNotLoaded("muscleman.swf", swfsToLoad, swfsSizes);
			AddToLoadQueueIfNotLoaded("guy.swf", swfsToLoad, swfsSizes);
			AddToLoadQueueIfNotLoaded("debug_assets.swf", swfsToLoad, swfsSizes);
			AddToLoadQueueIfNotLoaded("obstacles.swf", swfsToLoad, swfsSizes);
			AddToLoadQueueIfNotLoaded("shots.swf", swfsToLoad, swfsSizes);
			AddToLoadQueueIfNotLoaded("levelfinish.swf", swfsToLoad, swfsSizes);

			var swfsToLoadStr	: String	= Config.getValue("swfs_to_load", levelNbr);
			if (swfsToLoadStr != "") {
				var swfsToLoadArr	: Array		= swfsToLoadStr.split(",");
				for each(var swfToLoadPath	: String in swfsToLoadArr) {
					AddToLoadQueueIfNotLoaded(swfToLoadPath, swfsToLoad, swfsSizes);
				}
			}


			// Play preloader transition
			_preloaderTransition	= preloaderTransition;
			var onPreloaderTransitionFadeInOver	: Function	= null;
			if (widgetToHide)
				onPreloaderTransitionFadeInOver	= widgetToHide.Hide;

			_preloaderTransition.PrepareToLoadLevel(_levelLoadedNbr, isNewGame);
			//PlayTransition(_preloaderTransition, onPreloaderTransitionFadeInOver, null, GameManager.Instance.StartCurrentLevel, false, "", "", false);
			PlayTransition(_preloaderTransition, onPreloaderTransitionFadeInOver, null, GameManager.Instance.initializeCurrentLevel, false, "", "", false);

			Debugger.Log("swfsToLoad: " + swfsToLoad);
			Debugger.Log("swfsSizes: " + swfsSizes);
			if (swfsToLoad.length == 0) {
				OnLevelLoaded(new Event("Dummy"));
			} else {
				ExternalLoader.Instance.addEventListener(ExternalLoader.LOAD_COMPLETE, OnLevelLoaded);
				ExternalLoader.Instance.addEventListener(ExternalLoader.LOAD_CANCELED, OnLevelLoadCanceled);
				ExternalLoader.Instance.Load(swfsToLoad, swfsSizes);
			}

		}

		private function AddToLoadQueueIfNotLoaded(swfPath:String, swfsToLoad:Array, sizesArray:Array):void
		{
			if (!ExternalLoader.Instance.IsLoaded(swfPath)) {
				swfsToLoad.push(swfPath);
				sizesArray.push(Config.getValue("size_" + swfPath));
			}
		}

		private function OnLevelLoaded(e:Event):void {
			Debugger.Log("OnLevelLoaded!");
			ExternalLoader.Instance.removeEventListener(ExternalLoader.LOAD_COMPLETE, OnLevelLoaded);
			ExternalLoader.Instance.removeEventListener(ExternalLoader.LOAD_CANCELED, OnLevelLoadCanceled);
			UIFactory.Instance.allAssetsLoaded();
			Debugger.Log("_preloaderTransition.LoadComplete");
			_preloaderTransition.LoadComplete();
			GameManager.Instance.PrepareLevel(_levelLoadedNbr, _levelLoadedDifficulty, _checkpointIdx);
		}

		private function OnLevelLoadCanceled(e:Event):void {
			ExternalLoader.Instance.removeEventListener(ExternalLoader.LOAD_COMPLETE, OnLevelLoaded);
			ExternalLoader.Instance.removeEventListener(ExternalLoader.LOAD_CANCELED, OnLevelLoadCanceled);
			PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.LevelLoadingTransition.Hide, UIFactory.Instance.MainMenu.ShowFadedIn);

		}

		private function StartCurrentLevel():void {
			GameManager.Instance.StartCurrentLevel();
		}


		// FLOW CONTROL
		private var _rankingContext	: String	= "";

		/**
		 * Emulates an event handler
		 *
		 * @param	event
		 */
		public function OnGameEvent(eventName	: String) :void {

			Debugger.Log("Game Event:  [" + eventName + "]");
			if (_testingWidgets) {
				Debugger.Log("(testing widgets is on)");
				return;
			}

			switch(eventName) {

				// GAME FLOW
				// =========
				case "PlayClicked":
					PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.MainMenu.Hide, UIFactory.Instance.Map.FadeIn );
					CheatsManager.onPlayClicked();
					// OMNITURE
					Omniture.trackPlay();
					break;
				case "PlayAsGuestClicked":
					PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.MainMenu.Hide, UIFactory.Instance.Map.FadeIn );
					CheatsManager.onPlayClicked();
					// OMNITURE
					Omniture.trackPlay();
					Omniture.trackMilestone("login_mode", "guest");
					break;
				case "LevelSelected":
					// CODE: PLAY_WITH_GUY
					if (CheatsManager.isActive(CheatsManager.PLAY_WITH_GUY))
					{
						Config.characted_id = Player.ID_GUY;
						LoadPrepareAndStartLevel(Config.level_number, Config.dificultad_inicial, UIFactory.Instance.LevelLoadingTransition, UIFactory.Instance.Map, -1);
					}
					else
					{
						PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.Map.Hide, UIFactory.Instance.CharacterSelect.ShowFadedIn );
					}
					break;
				case "ShopBtnClicked":
					PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.Map.Hide, UIFactory.Instance.Shop.ShowFadedIn );
					break;
				case "RankingBtnClicked":
					_rankingContext	= "map";
					UIFactory.Instance.Ranking.FadeIn();
					break;
				case "OnCharacterSelectContinue":
					LoadPrepareAndStartLevel(Config.level_number, Config.dificultad_inicial, UIFactory.Instance.LevelLoadingTransition, UIFactory.Instance.CharacterSelect, -1);
					break;
				case "OnCharacterSelectBack":
					PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.CharacterSelect.Hide, UIFactory.Instance.Map.FadeIn );
					break;
				case "LoadingBackClicked":
					PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.LevelLoadingTransition.Hide, UIFactory.Instance.Map.FadeIn, GameManager.Instance.DestroyCurrentLevel);
					break;
				case "OnShopBack":
					PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.Shop.Hide, UIFactory.Instance.Map.FadeIn );
					break;
				case "OnShopOk":
					PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.Shop.Hide, UIFactory.Instance.Map.FadeIn );
					break;
				case "LevelFinish":
					PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.LevelFinish.FadeIn, GameManager.Instance.updateDataAndDestroyLevel  );
					break;
				case "OnLevelFinishOk":
					_rankingContext = "level_finish";
					if (Config.is_guest || !Config.show_ranking)
					{
						PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.LevelFinish.Hide, UIFactory.Instance.Shop.ShowFadedIn );
					}
					else
					{
						UIFactory.Instance.Ranking.FadeIn();
					}
					break;

				case "OnRankingBack":
					switch(_rankingContext) {
						case "map":
							UIFactory.Instance.Ranking.FadeOut();
							break;
						case "level_finish":
							PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.Shop.ShowFadedIn, UIFactory.Instance.Ranking.Hide, UIFactory.Instance.LevelFinish.Hide );
							break;
					}
					break;
				case "OnRankingFadeOutComplete":
					switch(_rankingContext) {
						case "map":
							UIFactory.Instance.Map.ShowFadedIn();
							UIFactory.Instance.Map.OnRankingFadeOutOver();
							break;
						case "level_finish":
							break;
					}
					break;

				case "OnWeekResultsContinue":
					UIFactory.Instance.WeekResults.FadeOut();
					UIFactory.Instance.Map.ShowFadedIn();
					break;

				case "OnWeekResultsFadeOutOver":
					UIFactory.Instance.Map.OnWeekResultsFadeOutOver();
					break;

				case "OnDayResultsContinue":
					UIFactory.Instance.DayResults.FadeOut();
					UIFactory.Instance.Map.ShowFadedIn();
					break;

				case "OnDayResultsFadeOutOver":
					UIFactory.Instance.Map.OnDayResultsFadeOutOver();
					break;

				case "DayComplete":
					//PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.Map.Hide, UIFactory.Instance.DayComplete.ShowFadedIn);
					UIFactory.Instance.DayComplete.FadeIn();
					_lastWeekResultsShown	= _lastDayResultsShown	= false;
					break;
				case "DayCompleteContinue":
					UIFactory.Instance.DayComplete.FadeOut();
					//if (hasToShowLastWeekScores)
					//{
						//PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.DayComplete.Hide, UIFactory.Instance.WeekResults.ShowFadedIn );
					//}
					//else if (hasToShowLastDayScores)
					//{
						//PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.DayComplete.Hide, UIFactory.Instance.DayResults.ShowFadedIn );
					//}
					//else
					//{
						//PlayTransition(UIFactory.Instance.SimpleTransition, UIFactory.Instance.DayComplete.Hide, UIFactory.Instance.Map.FadeIn );
					//}
					break;
				case "DayCompleteFadeOutOver":
					UIFactory.Instance.Map.OnDayCompleteFadeOutOver();
					break;



				// HUD Btns
				case "OnPauseBtnClick":
					GameManager.Instance.TogglePause(!GameManager.Instance.paused, true);
					break;
				case "OnExitBtnClick":
					GameManager.Instance.TogglePause(true, true, "exit");
					UIFactory.Instance.Exit.FadeIn();
					break;
				case "ExitPopUpFadeInOver":
					break;
				case "ExitNoBtnClick":
					UIFactory.Instance.Exit.FadeOut();
					GameManager.Instance.TogglePause(false);
					break;
				case "ExitYesBtnClick":
					PlayTransition(UIFactory.Instance.SimpleTransition, GameManager.Instance.DestroyCurrentLevel, UIFactory.Instance.Map.FadeIn, UIFactory.Instance.Exit.Hide);
					break;

				// Transitions
				case "SimpleTransitionFadeInOver":
					if (_onTransitionFadeInOver != null) _onTransitionFadeInOver();
					_onTransitionFadeInOver = null;
					break;
				case "SimpleTransitionFadeOutBegin":
					if (_onTransitionFadeOutBegin != null) _onTransitionFadeOutBegin();
					_onTransitionFadeOutBegin = null;
					break;
				case "SimpleTransitionFadeOutOver":
					if (_onTransitionFadeOutOver != null) _onTransitionFadeOutOver();
					_onTransitionFadeOutOver = null;
					break;
				case "LevelLoadingTransitionFadeInOver":
					if (_onTransitionFadeInOver != null) _onTransitionFadeInOver();
					_onTransitionFadeInOver = null;
					break;
				case "LevelLoadingTransitionFadeOutBegin":
					if (_onTransitionFadeOutBegin != null) _onTransitionFadeOutBegin();
					_onTransitionFadeOutBegin = null;
					break;
				case "PretLevelTransitionFadeOutOver":
					if (_onTransitionFadeOutOver != null) _onTransitionFadeOutOver();
					_onTransitionFadeOutOver = null;
					break;
				case "PostLevelTransitionFadeInOver":
					if (_onTransitionFadeInOver != null) _onTransitionFadeInOver();
					_onTransitionFadeInOver = null;
					break;
				case "PostLevelTransitionFadeOutBegin":
					if (_onTransitionFadeOutBegin != null) _onTransitionFadeOutBegin();
					_onTransitionFadeOutBegin = null;
					break;
				case "PostLevelTransitionFadeOutOver":
					if (_onTransitionFadeOutOver != null) _onTransitionFadeOutOver();
					_onTransitionFadeOutOver = null;
					break;
			}
		}

		public function get hasToShowLastWeekScores():Boolean
		{
			if (Config.getValue("force_show_last_week_scores") == "true")
			{
				if (_lastWeekResultsShown) return false;
				_lastWeekResultsShown = true;
				return true;
			}

			var lastWeekResultsSeen	: int = DBManager.instance.user.getVar("last_week_results_seen");
			Debugger.Log("lastWeekResultsSeen: " + lastWeekResultsSeen);

			var currentWeek:int = DBManager.instance.game.getVar("week_number");
			Debugger.Log("currentWeek: " + currentWeek);

			var firstWeek	: int	= Config.getValue("first_week");
			Debugger.Log("firstWeek: " + firstWeek);

			return (lastWeekResultsSeen != currentWeek && currentWeek > firstWeek);
		}

		public function get hasToShowLastDayScores():Boolean
		{
			if (Config.getValue("force_show_last_day_scores") == "true")
			{
				if (_lastWeekResultsShown) return false;
				_lastWeekResultsShown = true;
				return true;
			}

			// Do not show yesterday results if it's the 1st day of the week
			var weekDay	: int = DBManager.instance.game.getVar("weekday");
			if (weekDay == 1) return false;


			var lastDayResultsSeen	: String = DBManager.instance.user.getVar("last_day_results_seen");
			Debugger.Log("lastDayResultsSeen: " + lastDayResultsSeen);

			var today:String = DBManager.instance.game.getVar("today");
			Debugger.Log("today: " + today);

			return (lastDayResultsSeen != today);
		}

		public function Log(o:Object):void
		{
			(getChildByName("log_txt") as TextField).text = "->" + o.toString();
		}

		/* NOTE: To blur splash (Ref:[Blur01]) */
		/* [Blur01] **
		private function BlurSplash():void {
			if (_splashBlurred) return;
			Camera.Instance.CaptureBackground(null, UIFactory.Instance.MainMenu.blurredSplashParent);
			Camera.Instance.BlurBitmap(.5);
			_splashBlurred	= true;
			UIFactory.Instance.MainMenu.blurredSplashParent.visible = true;
		}

		private function UnBlurSplash():void {
			if (!_splashBlurred) return;
			Camera.Instance.UnBlurBitmap(2);
			_splashBlurred	= false;
		}
		** /[Blur01] **/
	}
}
