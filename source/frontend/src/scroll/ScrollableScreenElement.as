﻿﻿/**
* ...
* @author Default
* @version 0.1
*/

package scroll {

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.MovieClip;
import flash.geom.Rectangle;

import utils.MovieClipUtils;

import view.LayerFactory;

public class ScrollableScreenElement {
		
		
		public static const LOOP_HORIZONTAL	: String	= "LoopHorizontal";
		public static const LOOP_VERTICAL	: String	= "LoopVertical";
		
		protected var _asset			: DisplayObject	= null;
		protected var _depthHorizontal	: Number;
		protected var _depthVertical	: Number;
		protected var _loopHorizontal	: Boolean;
		protected var _loopVertical		: Boolean;
		
		protected var _loopWidth	: Number;
		protected var _loopHeight	: Number;
		
		protected var _frozen	: Boolean;
		
		protected var _id	: String	= "";
		
		protected var _camera	: Rectangle	= null;
		public function get camera():Rectangle { return _camera; }
		
		protected var _leftCameraY	: Number	= 0;
		protected var _rightCameraY	: Number	= 500;
		protected var _leftCameraX	: Number	= 0;
		protected var _rightCameraX	: Number	= 850;
		
		
		public function ScrollableScreenElement(id:String, asset:DisplayObjectContainer, depthHorizontal:Number, depthVertical: Number,
												loopHorizontal:Boolean, loopVertical:Boolean = false, loopOverlappingHorizontal:Number = 0,
												loopOverlappingVertical:Number = 0, cacheAsBitmap:Boolean = true) {
			
			_id					= id;
			_asset 				= asset;
			_depthHorizontal	= depthHorizontal;
			_depthVertical		= depthVertical;
			_loopHorizontal		= loopHorizontal;
			_loopVertical		= loopVertical;
			
			_frozen	= false;
			
			var innerAsset	: MovieClip	= asset.getChildByName(id) as MovieClip;
			var leftCamera	: MovieClip	= null;
			var rightCamera	: MovieClip	= null;
			if(innerAsset){
				leftCamera	= innerAsset.getChildByName("leftCamera_mc") as MovieClip;
				rightCamera	= innerAsset.getChildByName("rightCamera_mc") as MovieClip;
			}
			//Debugger.Log("innerAsset: " + innerAsset + " -- leftCamera: " + leftCamera + "  -- rightCamera: " + rightCamera );
			if (leftCamera && rightCamera) {
				_leftCameraX	= leftCamera.x + innerAsset.x;
				//Debugger.Log("rightCamera.x: " + rightCamera.x);
				//Debugger.Log("asset.x: " + asset.x);
				//Debugger.Log("asset: " + asset);
				//Debugger.Log("innerAsset: " + innerAsset);
				_rightCameraX	= rightCamera.x + innerAsset.x;
				_leftCameraY	= leftCamera.y + innerAsset.y;
				_rightCameraY	= rightCamera.y + innerAsset.y;
				//Debugger.Log("leftCamera.y: " + leftCamera.y);
				//Debugger.Log("asset.y: " + asset.y);
				//Debugger.Log("innerAsset.y: " + innerAsset.y);
				//Debugger.Log("_leftCameraY: " + _leftCameraY);
				_loopHeight	= (_asset.height - loopOverlappingVertical) / 2;
				leftCamera.visible = false;
				rightCamera.visible = false;
			} else {
				_leftCameraX	= 0;
				_leftCameraY	= 0;
				_rightCameraX	= (_asset.width - loopOverlappingHorizontal) / 2;
				_rightCameraY	= (_asset.height - loopOverlappingHorizontal) - Config.game_height;
				_loopHeight	= (_asset.height - loopOverlappingVertical) / 2;
			}
			
			_camera	= new Rectangle(_leftCameraX, _leftCameraY, Config.game_width, Config.game_height);
			asset.scrollRect	= _camera;
			asset.cacheAsBitmap	= cacheAsBitmap;
			
			asset.mouseChildren = false;
			asset.mouseEnabled	= false;
		}
		
		
		public function Reset() : void {
			// TMP
			if (_camera != null){
				_camera.x = _leftCameraX;
				_camera.y = _leftCameraY;
			}
			if (_asset != null) {
				_asset.scrollRect = _camera;
			}
		}
		
		public function Destroy() : void {
			// TMP
			if (_camera != null){
				_camera.x = _leftCameraX;
				_camera.y = _leftCameraY;
			}
			if (_asset != null)
				_asset.scrollRect = _camera;
				
			// TMP!!!
			if (_asset.name != LayerFactory.GAME_COMPLEX) {
				//Debugger.Log("REMOVING LAYER: " + _asset.name);
				MovieClipUtils.Remove(_asset);
			}
			_asset	= null;
		}
		
		
		
		public function	Freeze() : void {
			
			_frozen	= true;
		}
		
		
		
		public function	Unfreeze() : void {
			
			_frozen	= false;
		}
		
		public function SetGameLevelPos(gameLevelX:Number, gameLevelY:Number):void {
			if (_frozen) return;
			_camera.x	= _leftCameraX + gameLevelX	* _depthHorizontal;
			_camera.y	= _leftCameraY + gameLevelY	* _depthVertical;
			
			// Check Horizontal loop
			if (_loopHorizontal) {
				//Debugger.Log("_camera.x: " + _camera.x + " -- _rightCameraX: " + _rightCameraX);
				while (_camera.x > _rightCameraX) {
					_camera.x -= (_rightCameraX - _leftCameraX);
					//Debugger.Log("NEW camera.x: " + _camera.x);
				}
				while (_camera.x < _leftCameraX) _camera.x += (_rightCameraX - _leftCameraX);
			}
			// Check Vertical loop
			if (_loopVertical) {
				//Debugger.Log("_camera.x: " + _camera.x + " -- _rightCameraX: " + _rightCameraX);
				while (_camera.y > _rightCameraY) {
					_camera.y -= (_rightCameraY - _leftCameraY);
					//Debugger.Log("NEW camera.x: " + _camera.x);
				}
				while (_camera.y < _leftCameraY) _camera.y += (_rightCameraY - _leftCameraY);
			}
			
			// Para usar valores enteros
			//_camera.x	= Math.round(_camera.x);
			//_camera.y	= Math.round(_camera.y);
			
			_asset.scrollRect	= _camera;
		}
		
		public function get Asset():DisplayObject { return _asset; }
		
		public function get Id():String	{ return _id; }
	}
	
}
