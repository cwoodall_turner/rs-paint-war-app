﻿package {
	import flash.utils.Dictionary;
	
	public class Rule {
		
		protected var _callbacks	: Vector.<Function>	= null;
		protected var _conditions	: Dictionary	= null;
		
		public function Rule ($conditions:Array = null, $callbacks:Array = null) {
			
			_conditions	= new Dictionary();
			if ($conditions != null)
			{
				for each(var condition : String in $conditions)
				{
					addCondition(condition);
				}
			}
			
			_callbacks	= new Vector.<Function>();
			if ($callbacks != null)
			{
				for each(var callback : Function in $callbacks)
				{
					addCallback(callback);
				}
			}
		}
		
		public function addCondition(condition:String, initValue:Boolean = false):void 
		{
			_conditions[condition] = initValue;
		}
		
		public function addCallback(callback:Function):void 
		{
			_callbacks.push(callback);
		}
		
		public function setConditionTrue(condition:String):void
		{
			_conditions[condition] = true;
			Debugger.Log("RULE! - setConditionTrue: " + condition + " (complete: " + isComplete);
			if (isComplete)
			{
				executeCallbacks();
			}
		}
		
		private function executeCallbacks():void 
		{
			for each (var callback:Function in _callbacks) {
				callback.call(null);
			}
		}
		
		public function get isComplete():Boolean
		{
			for each (var condition:Boolean in _conditions) {
				if (condition == false)
					return false;
			}
			return true;
		}
		
		public function destroy():void
		{
			_callbacks	= null;
			_conditions	= null;
		}

	}
	
}
