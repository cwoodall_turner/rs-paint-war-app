﻿package txt {

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.utils.Dictionary;

public class TextLoader extends EventDispatcher {
		
		/***** Public Constants ******/
		public static const LOAD_COMPLETE	: String = "LoadComplete";
		
		private	static var _instance	: TextLoader	= null;
		
		/***** Private variables *****/
		private var _loader	: URLLoader		= null;
		private var _texts	: Dictionary	= null;
		
		
		/**
		* Singleton implementation.
		*/
		public static function get Instance() :TextLoader {
		
			if(!(TextLoader._instance is TextLoader))
				TextLoader._instance = new TextLoader(new SingletonBlocker());
				
			return _instance;
		}
		
		/**
		* Constructor
		*/
		public function TextLoader(singletonBlocker:SingletonBlocker) {
		
			if (singletonBlocker == null) return;
			
			_texts	= new Dictionary();
		}
		
		
		/**
		 * Texts Dictionary
		 * 
		 */
		public function get Texts() : Dictionary	{	return	_texts;	}
		
		
		
		public function LoadTexts(path :String) :void {
			
			_loader	= new URLLoader();
			_loader.addEventListener(Event.COMPLETE, OnXmlLoaded, false, 0 , true);
			_loader.load(new URLRequest(path));
		}
		
		private function OnXmlLoaded(e:Event):void {
			_loader.removeEventListener(Event.COMPLETE, OnXmlLoaded);
			ParseTexts(_texts, new XML(e.target.data));
			dispatchEvent(new Event(LOAD_COMPLETE));
		}
		
		private function ParseTexts(texts:Dictionary, xml:XML):void {
			
			for each(var term:XML in xml.term) {
				texts[term.@key.toString()] = term.toString();
			}
		}
		
	}
	
}

internal class SingletonBlocker { }
