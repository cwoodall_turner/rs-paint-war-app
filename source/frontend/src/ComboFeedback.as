﻿package {
import flash.display.MovieClip;
import flash.text.TextField;

import utils.TextFieldUtils;

/**
	 * ...
	 * @author ahodes
	 */
	public class ComboFeedback extends MovieClip {
		
		private var _textField : TextField	= null;
		
		public function ComboFeedback() {
			super();
			_textField = ((getChildByName("textContainer_mc") as MovieClip).getChildByName("textContainerInside_mc") as MovieClip).getChildByName("text_txt") as TextField;
		}
		public function set htmlText	(textStr: String) : void {
			TextFieldUtils.SetHtmlTextPreservingFormat(_textField, textStr);
		}
	}
}
