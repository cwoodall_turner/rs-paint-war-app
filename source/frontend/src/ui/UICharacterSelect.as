﻿package ui 
{
import buttons.RollOutBtn;
import buttons.SelectableRollOutBtn;

import com.turner.caf.business.SimpleResponder;

import communication.database.DBManager;

import flash.display.MovieClip;
import flash.events.MouseEvent;
import flash.text.TextField;

import players.Player;

import txt.TextLoader;

import utils.TextFieldUtils;

public class UICharacterSelect  extends UIWidget
	{
		protected var _container	: MovieClip		= null;
		
		public function UICharacterSelect(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
			addSoundsToButtons();
		}
		
		private function addSoundsToButtons():void 
		{
			RollOutBtn(View.getChildByName("sound_btn")).OverSoundId		= "BtnOver";
			RollOutBtn(View.getChildByName("mute_btn")).OverSoundId		= "BtnOver";
			
			
			for (var i : int = 1; i <= 3; i++)
			{
				var characterBtn	: SelectableRollOutBtn = View.getChildByName("character" + i.toString() + "_btn") as SelectableRollOutBtn;
				characterBtn.clickSoundId	= "CharacterSelect";
				characterBtn.OverSoundId	= "BtnOver";
			}
			
			(View.getChildByName("back_btn") as RollOutBtn).clickSoundId	= "BtnNoClick";
			(View.getChildByName("back_btn") as RollOutBtn).OverSoundId		= "BtnOver";
			
			var musclemanBuyMc	: MovieClip = View.getChildByName("buyMuscleman_mc") as MovieClip;
			(musclemanBuyMc.getChildByName("buy_btn") as RollOutBtn).clickSoundId	= "BtnPerillaClick";
			(musclemanBuyMc.getChildByName("buy_btn") as RollOutBtn).OverSoundId	= "BtnPerillaOver";
			
			var confirmMc	: MovieClip	= View.getChildByName("buyConfirm_mc") as MovieClip;
			(confirmMc.getChildByName("back_btn") as RollOutBtn).clickSoundId	= "BtnNoClick";
			(confirmMc.getChildByName("back_btn") as RollOutBtn).OverSoundId	= "BtnOver";
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).clickSoundId		= "BuyItemOk";
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).OverSoundId		= "BtnOver";
		}
		
		
		
		private function prepare():void 
		{
			//  -> a level
			var titleMc : MovieClip	= View.getChildByName("title_mc") as MovieClip;
			titleMc.gotoAndStop(Config.level_number);
			
			var titleTxt: TextField = (titleMc.getChildByName("title_mc") as MovieClip).getChildByName("level_title") as TextField;
			var infoTitleStr	: String	= TextLoader.Instance.Texts["cs_level_title"];
			infoTitleStr = infoTitleStr.replace("[X]", Config.level_number.toString());
			
			selectCharacter(0); // SELECT NONE
			AddListenersToButtons();
			
			RollOutBtn(View.getChildByName("sound_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnSndButtonClick);
			RollOutBtn(View.getChildByName("mute_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnMuteButtonClick);
			updateSndButtonsVisibility();
			
			// MUSCLEMAN
			if (Config.getValue("muscleman_available") == "true")
			{
				if (DBManager.instance.user.getVar("muscleman") == "1")
				{
					hideBuyMuscleman();
					enableMuscleman();
				}
				else
				{
					showBuyMuscleman();
					disableMuscleman();
				}
			}
			else
			{
				hideBuyMuscleman();
				disableMuscleman();
			}
		}
		
		private function AddListenersToButtons():void 
		{
			for (var i : int = 1; i <= 3; i++)
			{
				var characterBtn	: SelectableRollOutBtn = View.getChildByName("character" + i.toString() + "_btn") as SelectableRollOutBtn;
				characterBtn.HitArea.addEventListener(MouseEvent.CLICK, onCharacterSelected);
			}
			
			(View.getChildByName("continue_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onContinueBtnClicked);
			(View.getChildByName("back_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onBackBtnClicked);
		}
		
		private function RemoveListenersFromButtons():void 
		{
			for (var i : int = 1; i <= 3; i++)
			{
				var characterBtn	: SelectableRollOutBtn = View.getChildByName("character" + i.toString() + "_btn") as SelectableRollOutBtn;
				characterBtn.HitArea.removeEventListener(MouseEvent.CLICK, onCharacterSelected);
			}
			
			(View.getChildByName("continue_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onContinueBtnClicked);
			(View.getChildByName("back_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onBackBtnClicked);
		}
		
		private function onCharacterSelected(e:MouseEvent):void 
		{
			var characterBtn : SelectableRollOutBtn = (e.currentTarget is SelectableRollOutBtn)?(e.currentTarget as SelectableRollOutBtn):((e.currentTarget as MovieClip).parent as SelectableRollOutBtn);
			Debugger.Log("characterBtn: " + characterBtn);
			var characterNbr : int = int(characterBtn.name.substr(9, 1));
			Debugger.Log("characterNbr: " + characterNbr);
			selectCharacter(characterNbr);
		}
		
		private function selectCharacter(characterNbr:int):void 
		{
			for (var i : int = 1; i <= 3; i++)
			{
				var characterBtn	: SelectableRollOutBtn = View.getChildByName("character" + i.toString() + "_btn") as SelectableRollOutBtn;
				characterBtn.selected = (characterNbr == i);
				Config.characted_id = getCharacterId(characterNbr);
			}
			
			if (characterNbr > 0)
			{
				//showContinueBtn(characterNbr);
				onContinueBtnClicked();
			}
			else
			{
				hideContinueBtn();
			}
			
			if (characterNbr == 3)
			{
				MainApp.instance.EarnBadge("muscleman");
			}
		}
		
		private function getCharacterId(characterNbr:int):String 
		{
			switch(characterNbr)
			{
				case 1: return Player.ID_MORDECAI; break;
				case 2: return Player.ID_RIGBY; break;
				case 3: return Player.ID_MUSCLEMAN; break;
			}
			return Config.getValue("default_characted_id");
		}
		
		private function hideContinueBtn():void 
		{
			Debugger.Log("hideContinueBtn");
			View.getChildByName("continue_btn").visible = false;
		}
		
		private function showContinueBtn(characterNbr:int):void 
		{
			Debugger.Log("showContinueBtn");
			View.getChildByName("continue_btn").visible = true;
		}
		
		
		private function onContinueBtnClicked(e:MouseEvent = null):void 
		{
			RemoveListenersFromButtons();
			MainApp.instance.OnGameEvent("OnCharacterSelectContinue");
		}
		
		
		private function onBackBtnClicked(e:MouseEvent):void 
		{
			RemoveListenersFromButtons();
			MainApp.instance.OnGameEvent("OnCharacterSelectBack");
		}
		
		
		private function OnMuteButtonClick(e:MouseEvent):void {
			Sfx.ToggleMute(false);
			updateSndButtonsVisibility();
		}
		
		private function OnSndButtonClick(e:MouseEvent):void {
			Sfx.ToggleMute(true);
			updateSndButtonsVisibility();
		}
		
		private function updateSndButtonsVisibility():void
		{
			RollOutBtn(_view.getChildByName("mute_btn")).visible = Sfx.Muted;
			RollOutBtn(_view.getChildByName("sound_btn")).visible = !Sfx.Muted;
		}
		
		
		
		
		// =========
		// MUSCLEMAN
		// =========
		
		private function showBuyMuscleman():void 
		{
			var musclemanBuyMc	: MovieClip = View.getChildByName("buyMuscleman_mc") as MovieClip;
			musclemanBuyMc.visible = true;
			
			// Money
			TextFieldUtils.SetTextPreservingFormat(
				(musclemanBuyMc.getChildByName("money_mc") as MovieClip).getChildByName("money_txt") as TextField,
				DBManager.instance.user.money.toString());
				
			// Cost
			var cost : int = Config.getValue("muscleman_cost");
			TextFieldUtils.SetTextPreservingFormat(
				(musclemanBuyMc.getChildByName("cost_mc") as MovieClip).getChildByName("cost_txt") as TextField,
				cost.toString());
			
			// Buy Button
			if (DBManager.instance.user.money >= cost)
			{
				(musclemanBuyMc.getChildByName("buy_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onBuyMusclemanClick);
				(musclemanBuyMc.getChildByName("buy_btn") as RollOutBtn).Enable();
			}
			else
			{
				(musclemanBuyMc.getChildByName("buy_btn") as RollOutBtn).Disable();
			}
		}
		
		private function onBuyMusclemanClick(e:MouseEvent):void 
		{
			showBuyConfirmPopup();
		}
		
		private function hideBuyMuscleman():void 
		{
			var musclemanBuyMc	: MovieClip = View.getChildByName("buyMuscleman_mc") as MovieClip;
			musclemanBuyMc.visible = false;
		}
		
		private function showBuyConfirmPopup():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("buyConfirm_mc") as MovieClip;
			confirmMc.gotoAndPlay("enter");
			confirmMc.visible = true;
			addBuyConfirmButtonsListeners();
		}
		
		private function addBuyConfirmButtonsListeners():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("buyConfirm_mc") as MovieClip;
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onConfirmBuyOkBtn);
			(confirmMc.getChildByName("back_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onConfirmBuyBackBtn);
			//Debugger.Log("OK BTN: " + (confirmMc.getChildByName("ok_btn") as RollOutBtn));
		}
		
		private function removeBuyConfirmButtonsListeners():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("buyConfirm_mc") as MovieClip;
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onConfirmBuyOkBtn);
			(confirmMc.getChildByName("back_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onConfirmBuyBackBtn);
		}
		
		private function onConfirmBuyOkBtn(e:MouseEvent):void 
		{
			removeBuyConfirmButtonsListeners();
			exitBuyConfirmPopup();
			
			var cost : int = Config.getValue("muscleman_cost");
			
			DBManager.instance.user.money -= cost;
			
			var musclemanBuyMc	: MovieClip = View.getChildByName("buyMuscleman_mc") as MovieClip;
			TextFieldUtils.SetTextPreservingFormat(
				(musclemanBuyMc.getChildByName("money_mc") as MovieClip).getChildByName("money_txt") as TextField,
				DBManager.instance.user.money.toString());
			
			DBManager.instance.user.setVar("muscleman", "1");
			
			hideBuyMuscleman();
			enableMuscleman();
			
			DBManager.instance.saveUserData(new SimpleResponder(onSaveUserDataComplete, onSaveUserDataError));
			
			MainApp.instance.EarnBadge("muscleman");
		}
		
		private function onConfirmBuyBackBtn(e:MouseEvent):void 
		{
			removeBuyConfirmButtonsListeners();
			exitBuyConfirmPopup();
		}
		
		
		private function exitBuyConfirmPopup():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("buyConfirm_mc") as MovieClip;
			confirmMc.gotoAndPlay("exit");
		}
		
		private function enableMuscleman():void 
		{
			(View.getChildByName("character3_btn") as SelectableRollOutBtn).Enable();
		}
		
		private function disableMuscleman():void 
		{
			(View.getChildByName("character3_btn") as SelectableRollOutBtn).Disable();
		}
		
		
		private function onSaveUserDataComplete(data:Object):void 
		{
			Debugger.Log("onSaveUserDataComplete: " + data);
		}
		private function onSaveUserDataError(info:Object):void 
		{
			Debugger.Log("onSaveUserDataError: " + info);
		}
		
		
		
		
		
		
		/**
		 * Shows the MC Faded In
		 */
		public override function ShowFadedIn() :void {
			super.ShowFadedIn();
			prepare();
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public override function FadeIn() :void {
			super.FadeIn();
			prepare();
		}
		
	}
	
}
