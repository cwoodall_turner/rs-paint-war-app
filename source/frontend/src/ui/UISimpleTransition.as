﻿package ui {
import flash.display.MovieClip;
import flash.events.Event;
import flash.text.TextField;

import utils.TextFieldUtils;

public class UISimpleTransition  extends UITransition
	{
		
		public function UISimpleTransition(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
		}
		
		override public function set Text(text:String):void {
			
			var textContainer	: MovieClip	= View.getChildByName("textContainer_mc") as MovieClip;
			if (textContainer == null) return;
			
			var txtFld	: TextField	= textContainer.getChildByName("text_txt") as TextField;
			if (txtFld == null) return;
			
			txtFld.text	= text;
			/*
			var i : int = View.numChildren;
			while (i--) {
				var child : DisplayObject	= View.getChildAt(i);
				if (child.name.substr(0, 9) == "translate") (child as MovieClip).gotoAndStop(GameContext.Lang);
			}*/
		}
		
		override public function set HtmlText(htmlText:String):void {
			
			var textContainer	: MovieClip	= View.getChildByName("textContainer_mc") as MovieClip;
			if (textContainer == null) return;
			
			var txtFld	: TextField	= textContainer.getChildByName("text_txt") as TextField;
			if (txtFld == null) return;
			
			TextFieldUtils.SetHtmlTextPreservingFormat(txtFld, htmlText);
			/*
			var i : int = View.numChildren;
			while (i--) {
				var child : DisplayObject	= View.getChildAt(i);
				if (child.name.substr(0, 9) == "translate") (child as MovieClip).gotoAndStop(GameContext.Lang);
			}*/
		}
		
		
		/**
		 * Called when the transition screen finishes fading in.
		 */
		public override function OnFadeInOver(e:Event) :void {
			
			super.OnFadeInOver(e);
			MainApp.instance.OnGameEvent("SimpleTransitionFadeInOver");
		}
		
		
		/**
		 * Called when the transition screen starts fading out.
		 */
		public override function OnFadeOut(e:Event) :void {
			
			super.OnFadeOut(e);
			MainApp.instance.OnGameEvent("SimpleTransitionFadeOutBegin");
		}
		
		
		
		/**
		 * Called when the transition screen finishes fading out.
		 */
		public override function OnFadeOutOver(e:Event) :void {
			
			super.OnFadeOutOver(e);
			MainApp.instance.OnGameEvent("SimpleTransitionFadeOutOver");
		}
	}
	
}
