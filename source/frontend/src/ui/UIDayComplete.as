﻿package ui
{

import buttons.RollOutBtn;

import com.turner.caf.business.SimpleResponder;

import communication.database.DBManager;

import flash.display.MovieClip;
import flash.events.Event;
import flash.events.MouseEvent;

public class UIDayComplete  extends UIWidget
	{

		private var _weekComplete : Boolean = false;
		
		public function UIDayComplete(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
		}
		
		
		
		private function prepare():void 
		{
			
			// SHOW DAY/WEEK
			_weekComplete	= (int(DBManager.instance.game.getVar("weekday")) == 7);
			if (_weekComplete)
			{
				(View.getChildByName("info_mc") as MovieClip).gotoAndStop("week");
			} else 
			{
				(View.getChildByName("info_mc") as MovieClip).gotoAndStop("day");
			}
			AddListenersToButtons();
			
			updateGameInfo();
			showLoadingMc();
			
			// TMP
			View.alpha = 0;
			
			Captain.updateOnDayComplete();
		}
		
		private function AddListenersToButtons():void 
		{
			(View.getChildByName("continue_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onContinueBtnClicked);
		}
		
		private function RemoveListenersFromButtons():void 
		{
			(View.getChildByName("continue_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onContinueBtnClicked);
		}
		
		private function onContinueBtnClicked(e:MouseEvent = null):void 
		{
			RemoveListenersFromButtons();
			MainApp.instance.OnGameEvent("DayCompleteContinue");
		}
		
		private function showLoadingMc():void 
		{
			View.getChildByName("loading_mc").visible = true;
			View.getChildByName("continue_btn").visible = false;
		}
		
		private function hideLoadingMc():void 
		{
			View.getChildByName("loading_mc").visible = false;
			View.getChildByName("continue_btn").visible = true;
		}
		
		
		private function updateGameInfo():void 
		{
			DBManager.instance.updateGameInfoAfterDayEnd(new SimpleResponder(onUpdateGameInfoComplete, onUpdateGameInfoError));
		}
		
		
		private function onUpdateGameInfoError(info:Object):void 
		{
			Debugger.Log("onUpdateGameInfoError: " + info);
			hideLoadingMc();
			onContinueBtnClicked();
		}
		
		private function onUpdateGameInfoComplete(data:Object):void 
		{
			Debugger.Log("onUpdateGameInfoComplete: " + data);
			hideLoadingMc();
			onContinueBtnClicked();
		}
		
		
		
		/**
		 * Shows the MC Faded In
		 */
		public override function ShowFadedIn() :void {
			super.ShowFadedIn();
			prepare();
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public override function FadeIn() :void {
			super.FadeIn();
			prepare();
		}
		
		
		public override function OnFadeOutOver(e:Event) :void {
			super.OnFadeOutOver(e);
			MainApp.instance.OnGameEvent("DayCompleteFadeOutOver");
		}
		
	}
	
}
