﻿package ui {
import buttons.RollOutBtn;

import communication.database.DBManager;

import flash.display.MovieClip;
import flash.events.MouseEvent;
import flash.text.TextField;

import utils.TextFieldUtils;

public class UILevelFinish  extends UIWidget
	{
		private var _dbManagerReady : Boolean = false;
		
		public function UILevelFinish(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
			addSoundsToButtons();
		}
		
		private function addSoundsToButtons():void 
		{
			RollOutBtn(View.getChildByName("sound_btn")).OverSoundId		= "BtnOver";
			RollOutBtn(View.getChildByName("mute_btn")).OverSoundId		= "BtnOver";
			
			(View.getChildByName("ok_btn") as RollOutBtn).clickSoundId	= "BtnOkClick";
			(View.getChildByName("ok_btn") as RollOutBtn).OverSoundId		= "BtnOver";
		}
		
		
		
		private function prepare():void 
		{
			Debugger.Log("LEVEL FINISH prepare");
			
			_dbManagerReady = false;
			showInfo();
			AddListenersToButtons();
			
			Sfx.PlayMusic("MusicLevelFinish", 1);
			
			RollOutBtn(View.getChildByName("sound_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnSndButtonClick);
			RollOutBtn(View.getChildByName("mute_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnMuteButtonClick);
			updateSndButtonsVisibility();
		}
		
		private function showInfo():void 
		{
			var levelNbr	: int	= Config.level_number;
			
			// MONEY
			TextFieldUtils.SetTextPreservingFormat(
				(View.getChildByName("money_mc") as MovieClip).getChildByName("money_txt") as TextField,
				DBManager.instance.user.money.toString());
				
			// OBJECTIVE
			var objectiveMc : MovieClip = View.getChildByName("objectivesTxt_mc") as MovieClip;
			objectiveMc.gotoAndStop(levelNbr);
			var objectiveValue	: int = GameManager.Instance.currentLevel.objectiveValue;
			TextFieldUtils.SetTextPreservingFormat(objectiveMc.getChildByName("value_txt") as TextField, objectiveValue.toString());
			
			// LEVEL ICON
			(View.getChildByName("image_mc") as MovieClip).gotoAndStop(levelNbr);
			
			// SCORE
			TextFieldUtils.SetTextPreservingFormat(
				(View.getChildByName("score_mc") as MovieClip).getChildByName("score_txt") as TextField,
				GameManager.Instance.currentLevel.getScore().toString());
			
			// COINS
			var totalCoins : int = GameManager.Instance.currentLevel.coinsGrabbed + GameManager.Instance.currentLevel.getCoinsPerObjective();
			Debugger.Log("totalCoins: " + totalCoins);
			var coinsWonMc	: MovieClip	= View.getChildByName("coinsWon_mc") as MovieClip;
			TextFieldUtils.SetHtmlTextPreservingFormat((coinsWonMc.getChildByName("coinsWonTxt_mc") as MovieClip).getChildByName("coinsWon_txt") as TextField, totalCoins.toString());
			
			// CHARACTER
			var characterMc : MovieClip = View.getChildByName("character_mc") as MovieClip;
			if (characterMc != null)
			{
				characterMc.gotoAndStop(Config.characted_id);
			}
		}
		
		private function AddListenersToButtons():void 
		{
			(View.getChildByName("ok_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onOkBtnClicked);
		}
		
		private function RemoveListenersFromButtons():void 
		{
			(View.getChildByName("ok_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onOkBtnClicked);
		}
		
		private function onOkBtnClicked(e:MouseEvent):void 
		{
			RemoveListenersFromButtons();
			showCoinsWonPopup();
		}
		
		private function showCoinsWonPopup():void 
		{
			Debugger.Log("showCoinsWonPopup");
			var coinsWonMc	: MovieClip	= View.getChildByName("coinsWon_mc") as MovieClip;
			coinsWonMc.gotoAndPlay("enter");
			coinsWonMc.visible = true;
			
			addCoinsWonButtonListeners();
		}
		
		private function addCoinsWonButtonListeners():void 
		{
			var coinsWonMc	: MovieClip	= View.getChildByName("coinsWon_mc") as MovieClip;
			(coinsWonMc.getChildByName("ok_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onCoinsWonOkBtn);
		}
		
		private function removeCoinsWonButtonListeners():void 
		{
			var coinsWonMc	: MovieClip	= View.getChildByName("coinsWon_mc") as MovieClip;
			(coinsWonMc.getChildByName("ok_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onCoinsWonOkBtn);
		}
		
		private function onCoinsWonOkBtn(e:MouseEvent):void 
		{
			removeCoinsWonButtonListeners();
			exitCoinsWonPopup();
			MainApp.instance.OnGameEvent("OnLevelFinishOk");
		}
		
		private function exitCoinsWonPopup():void 
		{
			var coinsWonMc	: MovieClip	= View.getChildByName("coinsWon_mc") as MovieClip;
			coinsWonMc.gotoAndPlay("exit");
		}
		
		
		private function OnMuteButtonClick(e:MouseEvent):void {
			Sfx.ToggleMute(false);
			updateSndButtonsVisibility();
		}
		
		private function OnSndButtonClick(e:MouseEvent):void {
			Sfx.ToggleMute(true);
			updateSndButtonsVisibility();
		}
		
		private function updateSndButtonsVisibility():void
		{
			RollOutBtn(_view.getChildByName("mute_btn")).visible = Sfx.Muted;
			RollOutBtn(_view.getChildByName("sound_btn")).visible = !Sfx.Muted;
		}
		
		
		
		/**
		 * Shows the MC Faded In
		 */
		public override function ShowFadedIn() :void {
			super.ShowFadedIn();
			prepare();
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public override function FadeIn() :void {
			super.FadeIn();
			prepare();
		}
	}
	
}
