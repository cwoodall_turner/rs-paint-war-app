﻿package ui 
{
import buttons.RollOutBtn;

import com.greensock.TweenMax;
import com.greensock.easing.Linear;
import com.turner.caf.business.SimpleResponder;

import communication.database.DBManager;
import communication.database.DBTeam;

import flash.display.MovieClip;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;

import txt.TextLoader;

import utils.TextFieldUtils;

public class UIDayResults  extends UIWidget
	{
		protected var _firstTime : Boolean = true;
		protected var _bottomNews1StartingX	: Number = 0;
		protected var _bottomNewsSecondsPerPixelWidth	: Number = 0;
		
		protected var _bottomNewsTweens	: Vector.<TweenMax> = null;
		
		public function UIDayResults(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
			_firstTime	= true;
			addSoundsToButtons();
		}
		
		private function addSoundsToButtons():void 
		{
			(View.getChildByName("continue_btn") as RollOutBtn).clickSoundId	= "BtnOkClick";
			(View.getChildByName("continue_btn") as RollOutBtn).OverSoundId		= "BtnOver";
		}
		
		
		
		private function prepare():void 
		{
			Debugger.Log("DAY RESULTS prepare");
			Debugger.LogStack();
			
			showLoadingMc();
			DBManager.instance.getLastDayResults(new SimpleResponder(getLastDayResultsComplete, getLastDayResultsError));
			AddListenersToButtons();
			
			_bottomNewsSecondsPerPixelWidth	= Config.getValue("bottom_news_seconds_per_pixel_width");
		}
		
		private function showLoadingMc():void 
		{
			View.getChildByName("loading_mc").visible = true;
			View.getChildByName("info_mc").visible = false;
		}
		
		private function hideLoadingMc():void 
		{
			View.getChildByName("loading_mc").visible = false;
			View.getChildByName("info_mc").visible = true;
		}
		
		
		
		
		
		private function getLastDayResultsError(info:Object):void 
		{
			Debugger.Log("getLastDayResultsError: " + info);
		}
		
		private function getLastDayResultsComplete(data:Object):void 
		{
			Debugger.Log("UIDayResults - getLastDayResultsComplete: " + data);
			for (var levelNbr : int = 1; levelNbr <= Config.num_levels; levelNbr++)
			{
				// INFO MC
				var levelInfoMc	: MovieClip	= (View.getChildByName("info_mc") as MovieClip).getChildByName("level" + levelNbr.toString() + "_mc") as MovieClip;
				// TITLE
				var infoTitleTxt: TextField = (levelInfoMc.getChildByName("title_mc") as MovieClip).getChildByName("level_info_title") as TextField;
				var infoTitleStr	: String	= TextLoader.Instance.Texts["level_info_title"];
				infoTitleStr = infoTitleStr.replace("[X]", levelNbr.toString());
				TextFieldUtils.SetHtmlTextPreservingFormat(infoTitleTxt, infoTitleStr);
				
				// TEAM SCORES
				var levelTotalScore : int = 0;
				var teams : Array = new Array();
				for each (var team:DBTeam in DBManager.instance.teams)
				{
					var lastDayLevelScore : int = 0;
					if (Config.getValue("force_show_last_day_scores") == "true")
					{
						lastDayLevelScore = Config.getValue("previous_score_level_" + levelNbr.toString() + "_team_" + team.teamId);
						Debugger.Log("FORCING previous_score_level_" + levelNbr.toString() + "_team_" + team.teamId + ": " + lastDayLevelScore);
					}
					else
					{
						lastDayLevelScore	= team.getLastDayLevelScore(levelNbr);
					}
					team.sortValue = lastDayLevelScore;
					teams.push(team);
					levelTotalScore += team.sortValue;
				}
				teams.sort(compareTeams, Array.DESCENDING);
				for (var teamPosIdx: int = teams.length - 1; teamPosIdx >= 0; teamPosIdx--)
				{
					var t:DBTeam	= teams[teamPosIdx] as DBTeam;
					var levelScore : int = t.sortValue;
					var scoreMc		: MovieClip = levelInfoMc.getChildByName("score" + (teamPosIdx + 1).toString() + "_mc") as MovieClip;
					(scoreMc.getChildByName("team_mc") as MovieClip).gotoAndStop(t.teamId);
					// SCORE PERCENTAGE
					var scorePertentage : int = Math.ceil(100 * (levelScore / levelTotalScore) + 1);
					(scoreMc.getChildByName("percentageBar_mc") as MovieClip).gotoAndStop(scorePertentage);
					((scoreMc.getChildByName("percentageBar_mc") as MovieClip).getChildByName("progressBarInside_mc") as MovieClip).gotoAndStop(t.teamId);
					Debugger.Log("Score level " + levelNbr + " team " + t.teamId + ": " + levelScore + " / " + levelTotalScore + " (" + scorePertentage + " %)");
					// SCORE NUMBER
					var scoreTxt	: TextField = scoreMc.getChildByName("score_txt") as TextField;
					var scoreStr	: String = "x " + levelScore.toString();
					TextFieldUtils.SetTextPreservingFormat(scoreTxt, scoreStr);
					// LEVEL ICON
					(scoreMc.getChildByName("image_mc") as MovieClip).gotoAndStop(levelNbr);
					
					var infoPerTeamMc : MovieClip = View.getChildByName("infoPerTeam_mc") as MovieClip;
					var scoreMc : MovieClip	= infoPerTeamMc.getChildByName("score_team_" + t.teamId + "_level_" + levelNbr.toString()) as MovieClip;
					scoreMc.gotoAndStop(teamPosIdx + 1);
					TextFieldUtils.SetTextPreservingFormat(scoreMc.getChildByName("score_txt") as TextField, levelScore.toString());
				}
				
			}
			
			
			// NEWS ABAJO
			var bottomNewsStr : String = TextLoader.Instance.Texts["bottom_news_day_winner_team_" + (teams[0] as DBTeam).teamId + "_loser_team_" + (teams[teams.length - 1] as DBTeam).teamId];
			_bottomNewsTweens	= new Vector.<TweenMax>();
			for (var i : int = 1; i <= 2; i++)
			{
				var bottomNewsMc	: MovieClip	= (View.getChildByName("bottomNews_mc") as MovieClip).getChildByName("text" + i.toString()+ "_mc") as MovieClip;
				Debugger.Log("bottomNewsMc: " + bottomNewsMc);
				Debugger.Log("bottomNewsStr: " + bottomNewsStr);
				
				var bottomNewsTxt : TextField	= bottomNewsMc.getChildByName("text_txt") as TextField;
				bottomNewsTxt.width = 3000;
				TextFieldUtils.SetHtmlTextPreservingFormat(bottomNewsTxt, bottomNewsStr);
				var width	: Number = bottomNewsTxt.textWidth;
				if (_firstTime)
				{
					_firstTime = false;
					_bottomNews1StartingX	= bottomNewsMc.x;
				}
				var startingX	: Number	= _bottomNews1StartingX + (width + 30) * (i - 1);
				var finishX		: Number	= startingX - width - 30;
				var timeSeconds	: Number	= width * _bottomNewsSecondsPerPixelWidth;
				Debugger.Log("_bottomNewsSecondsPerPixelWidth: " + _bottomNewsSecondsPerPixelWidth);
				Debugger.Log("startingX: " + startingX);
				Debugger.Log("width: " + width);
				Debugger.Log("timeSeconds: " + timeSeconds);
				bottomNewsMc.x = startingX;
				var tween : TweenMax	= TweenMax.to(bottomNewsMc, timeSeconds, { x:finishX, repeat:999, ease:Linear.easeNone } );
				_bottomNewsTweens.push(tween);
			}
			
			updateUserLastDayResultsSeen();
			hideLoadingMc();
			
		}
		
		private function updateUserLastDayResultsSeen():void 
		{
			var today:String	= DBManager.instance.game.getVar("today");
			Debugger.Log("today: " + today);
			DBManager.instance.user.setVar("last_day_results_seen", today);
		}
		
		/**
		 * Custom team ranking sort function
		 */
		private function compareTeams( t1:DBTeam, t2:DBTeam ):int
		{
			return (t1.sortValue - t2.sortValue);
		}
		
		
		
		private function AddListenersToButtons():void 
		{
			(View.getChildByName("continue_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onContinueBtnClicked);
		}
		
		private function RemoveListenersFromButtons():void 
		{
			(View.getChildByName("continue_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onContinueBtnClicked);
		}
		
		
		
		private function onContinueBtnClicked(e:MouseEvent):void 
		{
			RemoveListenersFromButtons();
			MainApp.instance.OnGameEvent("OnDayResultsContinue");
			
			Sfx.PlaySound("TVExit");
		}
		
		
		public override function OnFadeOutOver(e:Event) :void {
			super.OnFadeOutOver(e);
			MainApp.instance.OnGameEvent("OnDayResultsFadeOutOver");
			
			if (_bottomNewsTweens != null)
			{
				for each (var tween : TweenMax in _bottomNewsTweens)
				{
					tween.kill();
					tween = null;
				}
			}
			_bottomNewsTweens = null;
		}
		
		
		/**
		 * Shows the MC Faded In
		 */
		public override function ShowFadedIn() :void {
			super.ShowFadedIn();
			prepare();
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public override function FadeIn() :void {
			super.FadeIn();
			prepare();
			
			Sfx.PlaySound("TVEnter");
		}
		
	}
	
}
