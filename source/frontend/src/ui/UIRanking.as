﻿package ui 
{
import buttons.RollOutBtn;
import buttons.SelectableRollOutBtn;

import com.turner.caf.business.SimpleResponder;

import communication.cn.CNCom;
import communication.database.DBManager;

import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;

import utils.TextFieldUtils;

public class UIRanking  extends UIWidget
	{
		protected var _currentLevelNbr	: int		= 1;
		protected var _avatarImg		: Sprite	= null;
		
		public function UIRanking(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
			addSoundsToButtons();
		}
		
		private function addSoundsToButtons():void 
		{
			(View.getChildByName("back_btn") as RollOutBtn).clickSoundId	= "BtnOkClick";
			(View.getChildByName("back_btn") as RollOutBtn).OverSoundId		= "BtnOver";
			
			for (var i : int = 0; i <= Config.num_levels; i++)
			{
				var levelBtn	: SelectableRollOutBtn = View.getChildByName("level" + i.toString() + "_btn") as SelectableRollOutBtn;
				levelBtn.clickSoundId	= "BtnOkClick";
				levelBtn.OverSoundId	= "BtnOver";
			}
		}
		
		
		
		private function prepare():void 
		{
			Debugger.Log("RANKING prepare");
			
			if (_avatarImg == null)
			{
				//_avatarImg = CNCom.instance.user.cloneImage();
			}
			
			selectLevel(0);
			AddListenersToButtons();
		}
		
		private function AddListenersToButtons():void 
		{
			AddListenersToLevelButtons();
			(View.getChildByName("back_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onBackBtnClicked);
		}
		
		private function AddListenersToLevelButtons():void 
		{
			for (var i : int = 0; i <= Config.num_levels; i++)
			{
				var levelBtn	: SelectableRollOutBtn = View.getChildByName("level" + i.toString() + "_btn") as SelectableRollOutBtn;
				if (i != _currentLevelNbr)
				{
					levelBtn.HitArea.addEventListener(MouseEvent.CLICK, onLevelSelected);
					levelBtn.Enable();
				}
				else
				{
					levelBtn.HitArea.removeEventListener(MouseEvent.CLICK, onLevelSelected);
				}
			}
		}
		
		private function RemoveListenersFromLevelButtons():void 
		{
			for (var i : int = 0; i <= Config.num_levels; i++)
			{
				var levelBtn	: SelectableRollOutBtn = View.getChildByName("level" + i.toString() + "_btn") as SelectableRollOutBtn;
				levelBtn.HitArea.removeEventListener(MouseEvent.CLICK, onLevelSelected);
			}
		}
		
		private function RemoveListenersFromButtons():void 
		{
			RemoveListenersFromLevelButtons();
			(View.getChildByName("back_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onBackBtnClicked);
		}
		
		
		private function onLevelSelected(e:MouseEvent):void 
		{
			RemoveListenersFromLevelButtons();
			var levelBtn : SelectableRollOutBtn = (e.currentTarget is SelectableRollOutBtn)?(e.currentTarget as SelectableRollOutBtn):((e.currentTarget as MovieClip).parent as SelectableRollOutBtn);
			Debugger.Log("levelBtn: " + levelBtn);
			var levelNbr : int = int(levelBtn.name.substr(5, 1));
			Debugger.Log("levelNbr: " + levelNbr);
			selectLevel(levelNbr);
		}
		
		
		private function selectLevel(levelNbr:int):void 
		{
			hidePositionMarker();
			_currentLevelNbr	= levelNbr;
			for (var i : int = 0; i <= Config.num_levels; i++)
			{
				var levelBtn	: SelectableRollOutBtn = View.getChildByName("level" + i.toString() + "_btn") as SelectableRollOutBtn;
				levelBtn.selected = (levelNbr == i);
			}
			
			showLoadingMc();
			
			if (_currentLevelNbr == 0)
			{
				(View.getChildByName("levelImage_mc") as MovieClip).gotoAndStop("global");
			}
			else
			{
				(View.getChildByName("levelImage_mc") as MovieClip).gotoAndStop(_currentLevelNbr);
			}
			//DBManager.instance.getRankingLevel(CNCom.instance.user.id, levelNbr, Config.ranking_num_players - 1, new SimpleResponder(onGetRankingLevelComplete, onGetRankingLevelError));
		}
		
		private function hidePositionMarker():void 
		{
			View.getChildByName("positionMarker_mc").visible = false;
		}
		
		private function showLoadingMc():void 
		{
			View.getChildByName("loading_mc").visible = true;
			View.getChildByName("info_mc").visible = false;
		}
		
		private function hideLoadingMc():void 
		{
			View.getChildByName("loading_mc").visible = false;
			View.getChildByName("info_mc").visible = true;
		}
		
		private function onGetRankingLevelError(info:Object):void 
		{
			Debugger.Log("onGetRankingLevelError: " + info);
			AddListenersToLevelButtons();
		}
		
		private function onGetRankingLevelComplete(data:Object):void 
		{
			Debugger.Log("onGetRankingLevelComplete: " + data);
			
			var playersList : Vector.<Object> = data as Vector.<Object>;
			var i : int = playersList.length;
			var playerRank 	: int = 0;
			var playerIdx	: int = 0;
			// Look for player in ranking
			while (i-- && playerRank == 0)
			{
				if (playersList[i]["rank"] != undefined)
				{
					playerIdx	= i;
					playerRank	= playersList[i]["rank"];
				}
			}
			
			// Populate table
			var firstIdxToShow : int = playerIdx + Math.floor(Config.ranking_num_players / 2);
			if (firstIdxToShow >= playersList.length) firstIdxToShow = playersList.length - 1;
			
			i = firstIdxToShow + 1;
			var nextRow : int = 1;
			while (i-- && nextRow <= Config.ranking_num_players)
			{	
				var positionMarkerRow : MovieClip = (View.getChildByName("positionMarker_mc") as MovieClip).getChildByName("row" + nextRow.toString() + "_mc") as MovieClip;
				var rowMc : MovieClip = (View.getChildByName("info_mc") as MovieClip).getChildByName("row" + nextRow.toString() + "_mc") as MovieClip;
				if (i == playerIdx)
				{
					rowMc.gotoAndStop("player");
					positionMarkerRow.gotoAndStop("player");
					
					(positionMarkerRow.getChildByName("avatar_mc") as MovieClip).addChild(_avatarImg);
					(positionMarkerRow.getChildByName("avatar_mc") as MovieClip).gotoAndStop("loggedin");
					
					var scale:Number = Config.getValue("ranking_avatar_scale");
					_avatarImg.scaleX = _avatarImg.scaleY = scale;
					_avatarImg.x = Config.getValue("ranking_avatar_x");
					_avatarImg.y = Config.getValue("ranking_avatar_y");
					
					// TMP
					playersList[i]["name"]	= Config.user_name;
				}
				else
				{
					rowMc.gotoAndStop("other");
					positionMarkerRow.gotoAndStop("other");
				}
				
				var rank 	: int = playerRank - (i - playerIdx);
				TextFieldUtils.SetTextPreservingFormat(rowMc.getChildByName("rank_txt") as TextField, rank.toString());
				
				var name	: String	= 	playersList[i]["name"];
				TextFieldUtils.SetTextPreservingFormat(rowMc.getChildByName("name_txt") as TextField, name);
				
				var teamId	: String	= 	playersList[i]["team_id"];
				(rowMc.getChildByName("team_mc") as MovieClip).gotoAndStop(teamId);
				
				var score	: int = playersList[i]["score_level_" + _currentLevelNbr.toString()];
				TextFieldUtils.SetTextPreservingFormat(rowMc.getChildByName("score_txt") as TextField, score.toString());
				
				nextRow++;
			}
			
			hideLoadingMc();
			
			View.getChildByName("positionMarker_mc").visible = true;
			
			AddListenersToLevelButtons();
			
			// If general ranking with updated info, update player info
			if (_currentLevelNbr == 0)
			{
				DBManager.instance.user.rank = playerRank;
			}
		}
		
		
		private function onBackBtnClicked(e:MouseEvent):void 
		{
			RemoveListenersFromButtons();
			MainApp.instance.OnGameEvent("OnRankingBack");
		}
		
		
		
		/**
		 * Shows the MC Faded In
		 */
		public override function ShowFadedIn() :void {
			super.ShowFadedIn();
			prepare();
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public override function FadeIn() :void {
			super.FadeIn();
			prepare();
		}
		
		/**
		 * Public method to handle the state of the widget once
		 * the fading out has completed (might be overridden)
		 */
		override public function OnFadeOutOver(e:Event) :void
		{
			super.OnFadeOutOver(e);
			MainApp.instance.OnGameEvent("OnRankingFadeOutComplete");
		}
		
	}
	
}
