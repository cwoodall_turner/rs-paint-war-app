﻿package ui {
import buttons.RollOutBtn;

import flash.display.MovieClip;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;

import utils.StringUtils;
import utils.TextFieldUtils;

public class UIPreloaderTransition  extends UISimpleTransition
	{
		private var _canFadeOut	: Boolean	= false;
		protected var _levelNumber	: int		= 1;
		protected var _isNewGame	: Boolean	= false;
		
		public function UIPreloaderTransition(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
			addSoundsToButtons();
		}
		
		private function addSoundsToButtons():void 
		{
			(View.getChildByName("back_btn") as RollOutBtn).clickSoundId	= "BtnNoClick";
			(View.getChildByName("back_btn") as RollOutBtn).OverSoundId		= "BtnOver";
			(View.getChildByName("continue_btn") as RollOutBtn).clickSoundId	= "BtnOkClick";
			(View.getChildByName("continue_btn") as RollOutBtn).OverSoundId		= "BtnOver";
		}
		
		public function PrepareToLoadLevel(levelNumber:int, isNewGame:Boolean):void {
			_levelNumber	= levelNumber;
			_isNewGame		= isNewGame;
			
			HideContinueBtn();
			
			GameContext.GlobalStage.addEventListener(Event.ENTER_FRAME, UpdateProgress);
			
			(View.getChildByName("back_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, OnBackBtnClicked);
		}
		
		private function UpdateProgress(e:Event):void 
		{
			var progressTxt	: TextField	= (View.getChildByName("preloader_mc") as MovieClip).getChildByName("progress_txt") as TextField;
			var progress : Number	= ExternalLoader.Instance.GetLoadedPercent();
			
			Debugger.Log("progress: " + progress);
			(View.getChildByName("preloader_mc") as MovieClip).gotoAndStop(Math.ceil(progress));
			TextFieldUtils.SetTextPreservingFormat(progressTxt, StringUtils.IntToStr(Math.ceil(progress), 2));
		}
		
		private function HideContinueBtn():void 
		{
			(View.getChildByName("continue_btn") as RollOutBtn).visible = false;
			View.getChildByName("preloader_mc").visible = true;
		}
		
		private function showContinueBtn():void 
		{
			(View.getChildByName("continue_btn") as RollOutBtn).visible = true;
			View.getChildByName("preloader_mc").visible = false;
			
			GameContext.GlobalStage.removeEventListener(Event.ENTER_FRAME, UpdateProgress);
		}
		
		/**
		 * Called when the transition screen finishes fading in.
		 */
		public override function OnFadeInOver(e:Event) :void {
			
			super.OnFadeInOver(e);
			MainApp.instance.OnGameEvent("SimpleTransitionFadeInOver");
			
			//if (_canFadeOut) OnContinueBtnClicked();
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public override function FadeIn() :void {
			LoadIncomplete();
			super.FadeIn();
		}
		
		public function LoadIncomplete():void {
			_canFadeOut	= false;
		}
		
		public function LoadComplete():void {
			showContinueBtn();
			(View.getChildByName("continue_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, OnContinueBtnClicked);
			
			_canFadeOut	= true;
			//Play();
		}
		
		private function OnContinueBtnClicked(e:MouseEvent = null):void 
		{
			(View.getChildByName("continue_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, OnContinueBtnClicked);
			(View.getChildByName("back_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, OnBackBtnClicked);
			FadeOut();
		}
		
		private function OnBackBtnClicked(e:MouseEvent):void 
		{
			(View.getChildByName("continue_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, OnContinueBtnClicked);
			(View.getChildByName("back_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, OnBackBtnClicked);
			GameContext.GlobalStage.removeEventListener(Event.ENTER_FRAME, UpdateProgress);
			
			ExternalLoader.Instance.CancelCurrentLoads();
			MainApp.instance.OnGameEvent("LoadingBackClicked");
		}
	}
	
}
