﻿package ui 
{
import buttons.RollOutBtn;

import flash.display.MovieClip;
import flash.events.Event;
import flash.events.MouseEvent;

public class UIExit  extends UIWidget
	{
		protected var _container	: MovieClip		= null;
		
		public function UIExit(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
			addSoundsToButtons();
		}
		
		private function addSoundsToButtons():void 
		{
			var popupMc	: MovieClip	= View.getChildByName("popup_mc") as MovieClip;
			RollOutBtn(popupMc.getChildByName("no_btn")).clickSoundId	= "BtnNoClick";
			RollOutBtn(popupMc.getChildByName("no_btn")).OverSoundId		= "BtnOver";
			RollOutBtn(popupMc.getChildByName("yes_btn")).clickSoundId	= "BtnOkClick";
			RollOutBtn(popupMc.getChildByName("yes_btn")).OverSoundId		= "BtnOver";
		}
		
		
		
		/**
		 * Called when the exit screen finishes fading in.
		 */
		public override function OnFadeInOver(e:Event) :void {
			
			super.OnFadeInOver(e);
			MainApp.instance.OnGameEvent("ExitFadeInOver");
			AddButtonsEventListeners();
		}
		
		private function AddButtonsEventListeners():void {
			var popupMc	: MovieClip	= View.getChildByName("popup_mc") as MovieClip;
			RollOutBtn(popupMc.getChildByName("no_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnNoBtnClick);
			RollOutBtn(popupMc.getChildByName("yes_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnYesBtnClick);
		}
		
		private function RemoveButtonsEventListeners():void {
			var popupMc	: MovieClip	= View.getChildByName("popup_mc") as MovieClip;
			RollOutBtn(popupMc.getChildByName("no_btn")).HitArea.removeEventListener(MouseEvent.CLICK, OnNoBtnClick);
			RollOutBtn(popupMc.getChildByName("yes_btn")).HitArea.removeEventListener(MouseEvent.CLICK, OnYesBtnClick);
		}
		
		
		private function OnNoBtnClick(e:MouseEvent):void {
			MainApp.instance.OnGameEvent("ExitNoBtnClick");
			RemoveButtonsEventListeners();
		}
		
		
		private function OnYesBtnClick(e:MouseEvent):void {
			MainApp.instance.OnGameEvent("ExitYesBtnClick");
			RemoveButtonsEventListeners();
		}
		
		
		
		
		/**
		 * Called when the exit screen finishes fading out.
		 */
		public override function OnFadeOutOver(e:Event) :void {
			
			super.OnFadeOutOver(e);
			MainApp.instance.OnGameEvent("ExitFadeOutOver");
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public override function FadeIn() :void {
			super.FadeIn();
			
			/** [Camera01] *
			HUDManager.Instance.Hide(true);
			Camera.Instance.CaptureBackground();
			HUDManager.Instance.Show(true);
			Camera.Instance.BlurBitmap();
			HUDManager.Instance.Hide();
			** /[Camera01] */
		}
		
		/**
		 * Starts playing the MC from the end label.
		 */
		public override function FadeOut() :void {
			super.FadeOut();
			/** [Camera01] *
			Camera.Instance.UnBlurBitmap(.5);
			** /[Camera01] */
			HUDManager.Instance.Show();
		}
		
	}
	
}
