﻿package ui {

import flash.display.MovieClip;
import flash.events.*;

public class UIWidget extends EventDispatcher{
		/*** Public constants ***/
		
		public static const FADE_IN :String 		= "OnFadeIn";
		public static const FADE_IN_OVER :String 	= "OnFadeInOver";
		public static const FADE_OUT :String 		= "OnFadeOut";
		public static const FADE_OUT_OVER :String 	= "OnFadeOutOver";	
		public static const FORTH :String 			= "OnForth";	
		public static const BACK :String 			= "OnBack";	
		public static const ACTION :String 			= "OnAction";
		public static const SECOND_ACTION :String	= "OnSecondAction";
	
		/*** Private variables ***/
		
		protected var _view :MovieClip 			= null;
		protected var _depth :int				= -1;
		protected var _hasFadeIn :Boolean 		= false;
		protected var _hasFadeOut :Boolean 		= false;
		
		/*** Setter / Getters ***/
		
		public function get View() :MovieClip { return _view; }
		
		public function get Depth() :int { return _depth; }
		
		public function get HasFadeIn() :Boolean { return _hasFadeIn; }
		public function get HasFadeOut() :Boolean { return _hasFadeOut; }
		
		/*** Methods ***/
		
		/**
		 * Constructor.
		 */
		public function UIWidget(asset :MovieClip, depth :int = -1) {
			_view = asset;
			_depth = depth;
			
			Validate();
			
			_view.addEventListener(UIWidget.FADE_IN, OnFadeIn);
			_view.addEventListener(UIWidget.FADE_IN_OVER, OnFadeInOver);
			_view.addEventListener(UIWidget.FADE_OUT, OnFadeOut);
			_view.addEventListener(UIWidget.FADE_OUT_OVER, OnFadeOutOver);
			_view.addEventListener(UIWidget.FORTH, OnForth);
			_view.addEventListener(UIWidget.BACK, OnBack);
			_view.addEventListener(UIWidget.ACTION, OnAction);
			_view.addEventListener(UIWidget.SECOND_ACTION, OnSecondAction);
			
			Init();
		}
		
		/** 
		 * Initializes the widget (might be overridden). 
		 */
		public function Init() :void { Validate(); }
		
		/**
		 * Plays the view
		 */
		public function Play() :void{ _view.play();	}
		
		/**
		 * Stops the view
		 */
		public function Stop() :void{ _view.stop();	}
		
		/**
		 *  Hides the widget.
		 */
		public function Hide() :void { _view.visible = false; }
		
		/**
		 * Shows the widget.
		 */
		public function Show() :void { _view.visible = true; }
		
		/**
		 * Shows the widget completely faded in
		 */
		public function get CompletelyFadedIn() :Boolean {
			return (View.currentLabel == "FadedIn");
		}
		/**
		 * Shows the widget completely faded in
		 */
		public function ShowFadedIn() :void {
			
			if (_view.currentLabel != "FadedIn") {
				_view.gotoAndStop("FadedIn");
			} else {
				OnFadeInOver(new Event("Dummy"));
			}
			_view.visible	= true;
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public function FadeIn() :void {
			_view.visible	= true;
			if (_hasFadeIn) {
				_view.gotoAndPlay("Start");
			}
			else{
				_view.gotoAndStop(1);
				OnFadeIn(null);
			}
		}
		
		/**
		 * Public method to handle the state of the widget once
		 * the fading in has started (might be overridden)
		 */
		public function OnFadeIn(e:Event) :void{ dispatchEvent(e); }
		
		/**
		 * Public method to handle the state of the widget once
		 * the fading in has completed (might be overridden)
		 */
		public function OnFadeInOver(e:Event) :void{ dispatchEvent(e); }	
		
		/**
		 * Starts playing the MC from the end label.
		 */
		public function FadeOut() :void {
			if(_hasFadeOut)
				_view.gotoAndPlay("End");
			else{
				_view.gotoAndStop(_view.totalFrames);
				OnFadeOut(null);
			}
		}		
		
		/**
		 * Public method to handle the state of the widget once
		 * the fading out has completed (might be overridden)
		 */
		public function OnFadeOut(e:Event) :void{ dispatchEvent(e);	}	
		
		/**
		 * Public method to handle the state of the widget once
		 * the fading out has completed (might be overridden)
		 */
		public function OnFadeOutOver(e:Event) :void{ dispatchEvent(e);	
			_view.visible	= false;}		
		
		/**
		 * Public method to handle the state of the widget once
		 * an action has been produced (might be overridden)
		 */
		public function OnAction(e:Event) :void{ dispatchEvent(e); }		

		/**
		 * Public method to handle the state of the widget once
		 * an action has been produced (might be overridden)
		 */
		public function OnSecondAction(e:Event) :void{ dispatchEvent(e); }

		/**
		 * Public method to handle the state of the widget once
		 * a forth navigation has been requested (might be overridden)
		 */
		public function OnForth(e:Event) :void{ dispatchEvent(e); }	
		
		/**
		 * Public method to handle the state of the widget once
		 * a back navigation has been requested (might be overridden)
		 */
		public function OnBack(e:Event) :void{ dispatchEvent(e); }			
	
		/**
		 * Validates if the movieclip has 
		 * the start and end labels
		 */
		public function Validate() :void{
			for(var i :int = 0; i < _view.currentLabels.length; i++){
				if(_view.currentLabels[i].name == "Start")_hasFadeIn = true;
				if(_view.currentLabels[i].name == "End")_hasFadeOut = true;
			}
		}
	}
} // package