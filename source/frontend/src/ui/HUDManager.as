﻿package ui {
import buttons.RollOutBtn;

import com.greensock.TweenLite;
import com.greensock.easing.EaseLookup;

import communication.cn.CNCom;
import communication.database.DBManager;

import enemies.EnemiesManager;
import enemies.Enemy;

import flash.display.MovieClip;
import flash.display.SimpleButton;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.text.TextField;

import levels.LevelBullseye;
import levels.LevelFlags;

import txt.TextLoader;

import utils.MovieClipUtils;
import utils.StringUtils;
import utils.TextFieldUtils;

import view.Camera;
import view.LayerFactory;

public class HUDManager {
		
		/*- Variables -*/
		private	var	_view			: MovieClip	= null;
		public function get View()	: MovieClip	{	return	_view;	}
		
		// Score
		private var _scoreMc		: MovieClip	= null;
		private var _scoreTxt		: TextField	= null;
		private var _scoreNumDigits	: int = 10;
		
		// Combo
		private var _comboContainer		: MovieClip	= null;
		private var _comboTxtContainer	: MovieClip	= null;
		private var _comboTxt			: TextField	= null;
		
		// Achievement
		private var _achievementMc			: MovieClip	= null;
		private var _achievementTxt			: TextField	= null;
		private var _achievementTitleTxt	: TextField	= null;
		private var _achievementStringsQueue	: Vector.<String>	= null;
		
		// Time
		private var _timeLeftToStartBeepingMs	: int = 10000;
		private var _playingTimeBeepSound		: Boolean	= false;
		
		// Debug
		private var _debugEnemies	: Boolean	= false;
		
		private var _plusScoreMcs		: Vector.<MovieClip>	= null;
		private var _plusScoreDestX		: Number 	= 100;
		private var _plusScoreDestY		: Number 	= 100;
		private var _plusScoreTimeSec	: Number 	= 1;
		private var _plusScoreEaseFunct	: Function	= null;
		
		// Player Sign
		private var _playerSignMc	: MovieClip = null;
		private var _playerDx		: Number	= 0;
		private var _playerDy		: Number	= 0;
		
		// Capturables
		private var _capturables : Vector.<Capturable> = null;
		
		private static var _instance : HUDManager;
		
		public static function get Instance() : HUDManager {
			if(!(HUDManager._instance is HUDManager))
				HUDManager._instance = new HUDManager(new SingletonBlocker());
				
			return _instance;
		}
		
		
		
		public function HUDManager(singletonBlocker:SingletonBlocker) {
			
			if (singletonBlocker == null) return;
		}
		
		private function addSoundsToButtons():void 
		{
			RollOutBtn(View.getChildByName("exit_btn")).clickSoundId	= "BtnOkClick";
			RollOutBtn(View.getChildByName("exit_btn")).OverSoundId		= "BtnOver";
			RollOutBtn(View.getChildByName("pause_btn")).clickSoundId	= "BtnOkClick";
			RollOutBtn(View.getChildByName("pause_btn")).OverSoundId		= "BtnOver";
			RollOutBtn(View.getChildByName("sound_btn")).clickSoundId	= "BtnOkClick";
			RollOutBtn(View.getChildByName("sound_btn")).OverSoundId		= "BtnOver";
			RollOutBtn(View.getChildByName("mute_btn")).clickSoundId	= "BtnOkClick";
			RollOutBtn(View.getChildByName("mute_btn")).OverSoundId		= "BtnOver";
		}
		
		
		
		public function Initialize(view:MovieClip):void {
			
			_view		= view;
			_view.tabChildren	= false;
			
			_scoreMc	= _view.getChildByName("score_mc") as MovieClip;
			_scoreTxt	= _scoreMc.getChildByName("text_txt") as TextField;
			TextFieldUtils.SetHtmlTextPreservingFormat(_scoreTxt, StringUtils.IntToStr(0, 2));
			
			RollOutBtn(View.getChildByName("exit_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnExitButtonClick);
			RollOutBtn(View.getChildByName("pause_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnPauseButtonClick);
			RollOutBtn(View.getChildByName("sound_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnSndButtonClick);
			RollOutBtn(View.getChildByName("mute_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnMuteButtonClick);
			RollOutBtn(_view.getChildByName("mute_btn")).visible = false;
			
			_comboContainer		= _view.getChildByName("combo_mc") as MovieClip;
			_comboTxtContainer	= _comboContainer.getChildByName("comboNumber_mc") as MovieClip;
			_comboTxt			= ((_comboTxtContainer.getChildByName("textContainer_mc") as MovieClip).getChildByName("textContainerInside_mc") as MovieClip).getChildByName("text_txt") as TextField;
			
			_comboContainer.gotoAndStop(1);
			
			_achievementMc			= _view.getChildByName("achievement_mc") as MovieClip;
			_achievementTxt			= (_achievementMc.getChildByName("textContainer_mc") as MovieClip).getChildByName("achievementName_txt") as TextField;
			_achievementTitleTxt	= (_achievementMc.getChildByName("textContainer_mc") as MovieClip).getChildByName("achievementTitle_txt") as TextField;
			_achievementMc.gotoAndStop(1);
			
			_timeLeftToStartBeepingMs	= Config.getValue("time_left_to_start_beeping_ms");
			_playingTimeBeepSound		= false;
			
			_debugEnemies				= (Config.getValue("debug_enemies") == "true");
			
			_plusScoreMcs		= new Vector.<MovieClip>();
			_plusScoreDestX		= Config.getValue("hud_plus_score_dest_x");
			_plusScoreDestY		= Config.getValue("hud_plus_score_dest_y");
			_plusScoreTimeSec	= Config.getValue("hud_plus_score_time_seconds");
			_plusScoreEaseFunct = EaseLookup.find(Config.getValue("hud_plus_score_ease_function"));
			
			// TMP - Add powerups
			if (Config.debug)
			{
				View.getChildByName("powerup_shots_power").addEventListener(MouseEvent.CLICK, onPowerupClicked);
				View.getChildByName("powerup_shots_distance").addEventListener(MouseEvent.CLICK, onPowerupClicked);
				View.getChildByName("powerup_grenades").addEventListener(MouseEvent.CLICK, onPowerupClicked);
			}
			//View.getChildByName("powerup_extra_life_slots").addEventListener(MouseEvent.CLICK, onPowerupClicked);
			
			addSoundsToButtons();
		}
		
		private function onPowerupClicked(e:MouseEvent):void 
		{
			var powerupName	: String 	= e.currentTarget.name;
			powerupName	= powerupName.substr(8);
			
			var currentValue	: int = DBManager.instance.user.getVar(powerupName);
			
			var valueToAdd : int = Config.getValue("powerup_added_on_click_" + powerupName);
			DBManager.instance.user.setVar(powerupName, currentValue + valueToAdd);
			
			GameManager.Instance.updatePlayerWeaponAsset();
		}
		
		private function OnExitButtonClick(e:MouseEvent):void {
			MainApp.instance.OnGameEvent("OnExitBtnClick");
		}
		
		
		private function OnPauseButtonClick(e:MouseEvent):void {
			MainApp.instance.OnGameEvent("OnPauseBtnClick");
		}
		
		private function OnMuteButtonClick(e:MouseEvent):void {
			Sfx.ToggleMute(false);
			updateSndButtonsVisibility();
		}
		
		private function OnSndButtonClick(e:MouseEvent):void {
			Sfx.ToggleMute(true);
			updateSndButtonsVisibility();
		}
		
		private function updateSndButtonsVisibility():void
		{
			RollOutBtn(_view.getChildByName("mute_btn")).visible = Sfx.Muted;
			RollOutBtn(_view.getChildByName("sound_btn")).visible = !Sfx.Muted;
		}
		
		public function prepareLevel(levelNbr:int):void
		{
			// TMP
			(View.getChildByName("timeUp_btn") as SimpleButton).addEventListener(MouseEvent.CLICK, OnTimeUpButtonClick);
			
			_scoreNumDigits	= Config.getValue("hud_score_num_digits_level_" + levelNbr.toString());
			View.gotoAndStop(levelNbr);
			
			var energyMc	: MovieClip = View.getChildByName("energy_mc") as MovieClip;
			var playerId	: String	= GameManager.Instance.activePlayer.id;
			var numExtraLifeSlots	: String = DBManager.instance.user.getVar("extra_life_slots");
			energyMc.gotoAndStop(playerId + "_" + numExtraLifeSlots);
			
			
			if (GameManager.Instance.currentLevel is LevelBullseye)
			{
				_capturables = (GameManager.Instance.currentLevel as LevelBullseye).capturables;
			}
			else if (GameManager.Instance.currentLevel is LevelFlags)
			{
				_capturables = (GameManager.Instance.currentLevel as LevelFlags).capturables;
			}
			else
			{
				_capturables	= null;
			}
			
			hideCombo();
			hideAchievement();
			showScore(0);
			
			(View.getChildByName("time_mc") as MovieClip).gotoAndStop("normal");
			
			showPowerupValue("shots_power", true);
			showPowerupValue("shots_distance", true);
			showPowerupValue("grenades", true);
			
			_playingTimeBeepSound		= false;
			
			updateSndButtonsVisibility();
			
			// TMP
			Update(33);
		}
		
		private function OnTimeUpButtonClick(e:MouseEvent):void 
		{
			(View.getChildByName("timeUp_btn") as SimpleButton).removeEventListener(MouseEvent.CLICK, OnTimeUpButtonClick);
			var objectiveValue : int = int((View.getChildByName("score_itxt") as TextField).text);
			if (isNaN(objectiveValue)) objectiveValue = 0;
			var coinsGrabbed : int = int((View.getChildByName("coins_itxt") as TextField).text);
			if (isNaN(coinsGrabbed)) coinsGrabbed = 0;
			GameManager.Instance.forceLevelEnd(objectiveValue, coinsGrabbed);
		}
		
		public function Update(dtMs:int):void {
			
			//if (View.currentLabel != "idle") {
				//View.gotoAndStop("idle");
			//}
			//View.visible = true;
			
			
			/* Check achievement texts queue */
			if (_achievementStringsQueue != null) {
				if(_achievementStringsQueue.length > 0 && _achievementMc.currentFrame == 1) {
					ShowAchievementText(_achievementStringsQueue.shift());
				}
				if (_achievementStringsQueue.length == 0) _achievementStringsQueue = null;
			}
			
			
			// SCORE (Lo updateo cuando llega el plusScoreMc)
			//TextFieldUtils.SetTextPreservingFormat(_scoreTxt, StringUtils.IntToStr(GameManager.Instance.currentLevel.objectiveValue, _scoreNumDigits));
			
			// TIME
			var timeLeftMs	: int	= GameManager.Instance.timeLeftMs;
			var timeStr	: String	= StringUtils.MsToSS(timeLeftMs);
			
			if (timeLeftMs <= _timeLeftToStartBeepingMs && !_playingTimeBeepSound) {
				Sfx.PlaySoundLoop("TimeAlert");
				_playingTimeBeepSound = true;
				(View.getChildByName("time_mc") as MovieClip).gotoAndStop("finishing");
			}
			//if (timeLeftMs <= _timeLeftToStartBeepingMs && timeTxt.text != timeStr) {
				//Sfx.PlaySound("TimeAlert");
			//}
			TextFieldUtils.SetTextPreservingFormat((View.getChildByName("time_mc") as MovieClip).getChildByName("timeNormal_txt") as TextField, timeStr);
			TextFieldUtils.SetTextPreservingFormat((View.getChildByName("time_mc") as MovieClip).getChildByName("timeFinishing_txt") as TextField, timeStr);
			
			// POWERUPS
			showPowerupValue("shots_power");
			showPowerupValue("shots_distance");
			showPowerupValue("grenades");
			//showPowerupValue("extra_life_slots");
			
			// ENERGY
			var energyMc	: MovieClip = View.getChildByName("energy_mc") as MovieClip;
			var barMc	: MovieClip	= energyMc.getChildByName("bar_mc") as MovieClip;
			if (barMc != null)
			{
				var energyPct : int	= Math.ceil(GameManager.Instance.activePlayer.energy * 100 / GameManager.Instance.activePlayer.maxEnergy);
				barMc.gotoAndStop(100 - energyPct + 1);
			}
			
			
			// UPDATE PLUS SCORE MCS
			for each(var plusScoreMc : MovieClip in _plusScoreMcs)
			{
				if (plusScoreMc.currentLabel == "enter_complete")
				{
					// TWEEN TO SCORE
					tweenPlusScoreToScorePos(plusScoreMc);
				}
			}
			
			// CAPTURABLE TIMES
			if (_capturables != null)
			{
				updateCapturableTimes();
			}
			
			// PLAYER SIGN
			if (_playerSignMc != null && _playerSignMc.currentFrameLabel != "ready")
			{
				updatePlayerSign();
			}
			
			
			if (_debugEnemies) {
				for each (var enemy:Enemy in EnemiesManager.instance.theEnemies)
				{
					if (!enemy.IsCurrentState(GameObject.ST_READY))
					{
						DebugEnemy(enemy);
					}
				}
			}
		}
		
		private function updateCapturableTimes():void 
		{
			for each (var capturable : Capturable in _capturables)
			{
				var capturableTimeMc	: MovieClip = capturable.gObj.View.getChildByName("captureTime_mc") as MovieClip;
				if (capturable.timeLeftToClearMs <= 0)
				{
					capturableTimeMc.gotoAndStop(1);
					capturableTimeMc.visible = false;
				}
				else
				{
					var barMc	: MovieClip	= capturableTimeMc.getChildByName("bar_mc") as MovieClip;
					var timePct : int	= Math.ceil(capturable.timeLeftToClearMs * 100 / capturable.timeToClearMs);
					barMc.gotoAndStop(100 - timePct + 1);
					capturableTimeMc.visible = true;
				}
			}
		}
		
		private function updatePlayerSign():void
		{
			if (_playerSignMc == null) return;
			var playerScreenPos : Point = Camera.Instance.ConvertGamePosToScreenPos(new GameAreaPoint(GameManager.Instance.activePlayer.xForCamera, GameManager.Instance.activePlayer.yForCamera, GameManager.Instance.activePlayer.zForCamera));
			
			//Debugger.Log("playerScreenPos: " + playerScreenPos);
			_playerSignMc.x = playerScreenPos.x + _playerDx;
			_playerSignMc.y = playerScreenPos.y + _playerDy;
		}
		
		
		private function tweenPlusScoreToScorePos(plusScoreMc:MovieClip):void
		{
			plusScoreMc.gotoAndStop("to_hud");
			var innerMc	: MovieClip = plusScoreMc.getChildByName("score_mc") as MovieClip;
			
			var destX	: Number 	= _plusScoreDestX - innerMc.x;
			var destY	: Number 	= _plusScoreDestY - innerMc.y;
			
			//Debugger.Log("_plusScoreTimeSec: " + _plusScoreTimeSec);
			//Debugger.Log("_plusScoreEaseFunct: " + _plusScoreEaseFunct);
			//Debugger.Log("destX: " + destX);
			//Debugger.Log("destY: " + destY);
			//Debugger.Log("innerMc: " + innerMc.x);
			//Debugger.Log("innerMc: " + innerMc.y);
			//Debugger.Log("plusScoreMc: " + plusScoreMc.x);
			//Debugger.Log("plusScoreMc: " + plusScoreMc.y);
			TweenLite.to(plusScoreMc, _plusScoreTimeSec, { x:destX, y:destY, ease:_plusScoreEaseFunct, onComplete:onPlusScoreTweenFinish, onCompleteParams:[plusScoreMc] } );
		}
		
		private function onPlusScoreTweenFinish(plusScoreMc:MovieClip):void 
		{
			Debugger.Log("onPlusScoreTweenFinish: " + plusScoreMc);
			plusScoreMc.gotoAndPlay("exit");
			showScore(GameManager.Instance.currentLevel.objectiveValue);
		}
		
		private function showScore(value:int):void 
		{
			TextFieldUtils.SetTextPreservingFormat(_scoreTxt, StringUtils.IntToStr(value, _scoreNumDigits));
		}
		
		
		private function showPowerupValue(powerupName:String, initialUpdate:Boolean = false):void 
		{
			var powerupValue	: int = DBManager.instance.user.getVar(powerupName);
			var powerupMc	: MovieClip	= View.getChildByName("powerup_" + powerupName) as MovieClip;
			if (powerupValue == 0)
			{
				if (initialUpdate)
				{
					powerupMc.gotoAndStop("empty");
				}
				else if (powerupMc.currentFrameLabel == "has")
				{
					powerupMc.gotoAndPlay("runs_out");
					Sfx.PlaySound("PowerupEmpty");
				}
			}
			else if (powerupMc.currentFrameLabel != "has")
			{
				powerupMc.gotoAndStop("has");
			}
			var valueTxt	: TextField	= (powerupMc.getChildByName("value_mc") as MovieClip).getChildByName("value_txt") as TextField;
			var valueStr	: String	= DBManager.instance.user.getVar(powerupName);
			TextFieldUtils.SetTextPreservingFormat(valueTxt, valueStr);
		}
		
		public function onTryToThrowEmptyGrenade():void
		{
			var powerupMc	: MovieClip	= View.getChildByName("powerup_grenades") as MovieClip;
			if (powerupMc.currentFrameLabel == "empty")
			{
				powerupMc.gotoAndPlay("trying_to_use_empty");
				Sfx.PlaySound("NoGrenades");
			}
		}
		
		private function DebugEnemy(gObj:Enemy):void 
		{
			if (gObj.currentBehavior == null || gObj.currentBehavior.currentAction == null) return;
			
			var debugTxtMc	: MovieClip	= gObj.View.getChildByName("debug_mc") as MovieClip;
			if (!debugTxtMc) debugTxtMc	= AddNewDebugMc(gObj);
			var debugTxt	: TextField	= debugTxtMc.getChildByName("debug_txt") as TextField;
			var debugStr	: String	= "Behavior: " + gObj.currentBehavior.id;
			debugStr	+= "\n Action: " + gObj.currentBehavior.currentAction.debugStr;
			debugStr	+= "\n State: " + gObj.currentState;
			TextFieldUtils.SetTextPreservingFormat(debugTxt, debugStr);
			if (gObj.IsFacingLeft) debugTxtMc.scaleX	= -1;
			else debugTxtMc.scaleX	= 1;
		}
		
		
		private function AddNewDebugMc(gObj:GameObject):MovieClip 
		{
			var debugTxtMc	: MovieClip	= getNewDebugMc();
			debugTxtMc.name	= "debug_mc"
			gObj.View.addChild(debugTxtMc);
			return debugTxtMc;
		}
		
		private function getNewDebugMc():MovieClip 
		{
			return ExternalLoader.Instance.GetNewInstance("GameObjectDebugAsset", "debug_assets.swf") as MovieClip;
		}
		
		
		
		public function HideStuffButMuteBtn() : void {
			_scoreMc.visible = false;
			RollOutBtn(View.getChildByName("exit_btn")).visible = false;
			RollOutBtn(View.getChildByName("pause_btn")).visible = false;
		}
		
		public function UnHideStuff() : void {
			_scoreMc.visible = true;
			RollOutBtn(View.getChildByName("exit_btn")).visible = true;
			RollOutBtn(View.getChildByName("pause_btn")).visible = true;
		}
		
		public function ClearStuff():void {
			_comboContainer.gotoAndStop(1);
			_achievementMc.gotoAndStop(1);
		}
		
		public function Show(immediately:Boolean = false) : void {
			//if (_view.visible && _view.alpha > 0) return;
			if (immediately) _view.alpha = 1;
			else	TweenLite.to(_view, 1, { alpha:1 } );
			_view.visible = true;
		}
		
		public function Hide(immediately:Boolean = false) : void {
			if (immediately) _view.alpha = 0;
			else	TweenLite.to(_view, 1, { alpha:0} );
		}
		
		
		public function ComboFeedbackFx(comboNbr:int):void {
			if (_comboContainer.currentFrame == 1) {
				_comboContainer.gotoAndPlay("EntraCombo");
			}
			_comboTxtContainer.gotoAndPlay(2);
			TextFieldUtils.SetHtmlTextPreservingFormat(_comboTxt, "x" + comboNbr.toString());
		}
		
		public function ComboFeedbackEnd():void {
			if (_comboContainer.currentFrame > 1 && _comboContainer.currentFrameLabel != "SaleCombo")
			{
				_comboContainer.gotoAndPlay("SaleCombo");
			}
		}
		
		private function hideCombo():void 
		{
			_comboContainer.gotoAndStop("SinCombo");
		}
		
		
		public function ShowAchievement(id:String):void {
			Debugger.Log("ShowAchievement: " + id);
			var achievementString : String	= TextLoader.Instance.Texts["achievement_" + id];
			Debugger.Log("achievementString: " + achievementString);
			ShowAchievementText(achievementString);
		}
		
		private function ShowAchievementText(achievementString:String):void {
			var achievementName 	: String = "";
			var achievementTitle	: String = "";
			if (achievementString.indexOf("::") >= 0)
			{
				var achievementArr	: Array = achievementString.split("::");
				achievementTitle = achievementArr[0];
				achievementName = achievementArr[1];
			}
			else
			{
				achievementName	= achievementString;
			}
			
			if(_achievementMc.currentFrame == 1){
				_achievementMc.gotoAndPlay(2);
				TextFieldUtils.SetHtmlTextPreservingFormat(_achievementTxt, achievementName);
				TextFieldUtils.SetHtmlTextPreservingFormat(_achievementTitleTxt, achievementTitle);
				_achievementMc.visible = true;
			} else {
				// If it's not being shown, queue it
				if(_achievementTxt.text != achievementName)
				{
					if (_achievementStringsQueue == null) _achievementStringsQueue = new Vector.<String>();
					_achievementStringsQueue.push(achievementString);
				}
			}
		}
		
		private function hideAchievement():void 
		{
			_achievementMc.gotoAndStop("ready");
		}
		
		public function showPlusScore(position:GameAreaPoint, score:int):void
		{
			var plusScoreMc	: MovieClip	= getNextPlusScoreMc();
			var screenPos	: Point	= Camera.Instance.ConvertGamePosToScreenPos(position);
			View.addChild(plusScoreMc);
			plusScoreMc.x = screenPos.x;
			plusScoreMc.y = screenPos.y;
			plusScoreMc.gotoAndPlay("enter");
			plusScoreMc.visible = true;
			Debugger.Log("SHOW PLUS SCORE @" + StringUtils.ToCoord(screenPos.x, screenPos.y));
		}
		
		private function getNextPlusScoreMc():MovieClip
		{
			for each(var plusScoreMc : MovieClip in _plusScoreMcs)
			{
				if (plusScoreMc.currentLabel == "ready") return plusScoreMc;
			}
			
			var newPlusScoreMc	: MovieClip = ExternalLoader.Instance.GetNewInstance("PlusScoreAsset", "hud.swf") as MovieClip;
			_plusScoreMcs.push(newPlusScoreMc);
			return newPlusScoreMc;
		}
		
		public function showPlayerSign():void
		{
			if (_playerSignMc == null)
			{
				_playerSignMc = ExternalLoader.Instance.GetSingleton("PlayerSignAsset", "hud.swf") as MovieClip;
				View.addChild(_playerSignMc);
			}
			
			_playerDx	= Config.getValue("player_sign_pos_x_" + GameManager.Instance.activePlayer.id);
			if (isNaN(_playerDx)) _playerDx = 0;
			_playerDy	= Config.getValue("player_sign_pos_y_" + GameManager.Instance.activePlayer.id);
			if (isNaN(_playerDy)) _playerDx = 0;
			
			var nameStr : String = "";
			if (Config.is_guest)
			{
				nameStr = TextLoader.Instance.Texts["player_sign_guest_name"];
				(_playerSignMc.getChildByName("avatar_mc") as MovieClip).gotoAndStop("guest");
			}
			else
			{	
//				nameStr = CNCom.instance.user.userName;
				//Debugger.Log("nameStr: " + nameStr);
				//nameStr = "tu.nombre";
                nameStr = TextLoader.Instance.Texts["player_sign_guest_name"];
				
				(_playerSignMc.getChildByName("avatar_mc") as MovieClip).gotoAndStop("loggedin");
			}
			
			var nameTxt : TextField = (_playerSignMc.getChildByName("name_mc") as MovieClip).getChildByName("name_txt") as TextField;
			TextFieldUtils.SetTextPreservingFormat(nameTxt, nameStr);
			
			updatePlayerSign();
			_playerSignMc.gotoAndPlay("show");
			_playerSignMc.visible = true;
		}
		
		public function showThreeTwoOneGo():void
		{
			var _321GoMc : MovieClip	= ExternalLoader.Instance.GetSingleton("ThreeTwoOneGoAsset", "hud.swf") as MovieClip;
			_321GoMc.gotoAndPlay("show");
			_321GoMc.name = "threetwoonego_mc";
			_321GoMc.visible = true;
			LayerFactory.Instance.GetLayer(LayerFactory.FOREGROUND).addChild(_321GoMc);
			//View.addChild(_321GoMc);
			View.addEventListener(Event.ENTER_FRAME, check321GoAnimationComplete);
		}
		
		private function check321GoAnimationComplete(e:Event):void 
		{
			//var _321GoMc : MovieClip	= View.getChildByName("threetwoonego_mc") as MovieClip;
			var _321GoMc : MovieClip	= LayerFactory.Instance.GetLayer(LayerFactory.FOREGROUND).getChildByName("threetwoonego_mc") as MovieClip;
			if (MovieClipUtils.AnimationEnded(_321GoMc))
			{
				View.removeEventListener(Event.ENTER_FRAME, check321GoAnimationComplete);
				MovieClipUtils.Remove(_321GoMc);
				GameManager.Instance.on321GoAnimationComplete();
			}
		}
		
		public function OnPause(pauseLabel:String = null):void {
			Debugger.Log("HUD PAUSE");
			if (pauseLabel == null || pauseLabel == "") pauseLabel = "pause";
			View.gotoAndStop(pauseLabel);
		}
		
		public function OnResume():void {
			Debugger.Log("HUD RESUME");
			View.gotoAndStop(Config.level_number);
		}
		
		public function Reset():void {
			Sfx.StopSound("TimeAlert");
		}
		
		
		
		
		public function onLevelFinishWaiting():void {
			Sfx.StopSound("TimeAlert");
			showTimeUp();
		}
		
		public function showTimeUp():void
		{
			var _timeUpMc : MovieClip	= ExternalLoader.Instance.GetSingleton("TimeUpAsset", "hud.swf") as MovieClip;
			_timeUpMc.gotoAndPlay("show");
			_timeUpMc.name = "timeup_mc";
			_timeUpMc.visible = true;
			LayerFactory.Instance.GetLayer(LayerFactory.FOREGROUND).addChild(_timeUpMc);
			View.addEventListener(Event.ENTER_FRAME, checkTimeUpAnimationComplete);
		}
		
		private function checkTimeUpAnimationComplete(e:Event):void 
		{
			var _timeUpMc : MovieClip	= LayerFactory.Instance.GetLayer(LayerFactory.FOREGROUND).getChildByName("timeup_mc") as MovieClip;
			if (MovieClipUtils.AnimationEnded(_timeUpMc))
			{
				View.removeEventListener(Event.ENTER_FRAME, checkTimeUpAnimationComplete);
				MovieClipUtils.Remove(_timeUpMc);
			}
		}
		
		public function onLevelFinish():void {
			Sfx.StopSound("TimeAlert");
		}
	}
	
}

internal class SingletonBlocker { }