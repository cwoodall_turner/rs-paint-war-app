﻿package ui 
{
import buttons.RollOutBtn;

import flash.display.MovieClip;
import flash.events.Event;
import flash.events.MouseEvent;

public class UIInstructions  extends UIWidget
	{
		
		protected var _continueBtn	: RollOutBtn	= null;
		protected var _container	: MovieClip		= null;
		
		public function UIInstructions(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
			InitializeChildren();
			//SetTexts();
		}
		
		
		protected function InitializeChildren() : void {
			
			_container	= (_view.getChildByName("popup_mc")) as MovieClip;
			if (_container == null) _container = View;
			
			RollOutBtn(_container.getChildByName("continue_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnContinueButtonClick);
		}
		
		public function OnContinueButtonClick(e:MouseEvent):void {
			
			if(View.currentLabel == "FadedIn")
				MainApp.instance.OnGameEvent("OnInstructionsContinueBtnClick");
		}
		
		
		
		/**
		 * Called when the instructions screen finishes fading in.
		 */
		public override function OnFadeInOver(e:Event) :void {
			
			super.OnFadeInOver(e);
			MainApp.instance.OnGameEvent("InstructionsFadeInOver");
		}
		
		
		
		
		/**
		 * Called when the instructions screen finishes fading out.
		 */
		public override function OnFadeOutOver(e:Event) :void {
			
			super.OnFadeOutOver(e);
			MainApp.instance.OnGameEvent("InstructionsFadeOutOver");
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public override function FadeIn() :void {
			super.FadeIn();
			
			/** [Camera01] *
			HUDManager.Instance.Hide(true);
			Camera.Instance.CaptureBackground();
			HUDManager.Instance.Show(true);
			Camera.Instance.BlurBitmap();
			HUDManager.Instance.Hide();
			** /[Camera01] */
		}
		
		/**
		 * Starts playing the MC from the end label.
		 */
		public override function FadeOut() :void {
			super.FadeOut();
			/** [Camera01] *
			Camera.Instance.UnBlurBitmap(.5);
			** /[Camera01] */
			HUDManager.Instance.Show();
		}
		
	}
	
}
