﻿package ui {

import flash.display.DisplayObjectContainer;
import flash.display.MovieClip;

public class UIFactory {
		
		/*** Constants ***/		
		
		
		/*** Private variables ***/
		
		private static var _instance:UIFactory;
		
		private var _container 					:DisplayObjectContainer	= null;
		private var _simpleTransition			:UISimpleTransition		= null;
		private var _LevelLoadingTransition		:UILevelLoadingTransition	= null;
		private var _mainMenu					:UIMainMenu					= null;
		private var _levelFinish					:UILevelFinish					= null;
		private var _exit						:UIExit						= null;
		private var _map						:UIMap						= null;
		private var _characterSelect			:UICharacterSelect			= null;
		private var _shop						:UIShop						= null;
		private var _ranking					:UIRanking					= null;
		private var _weekResults				:UIWeekResults					= null;
		private var _dayResults				:UIDayResults					= null;
		private var _dayComplete				:UIDayComplete	= null;
			
		
		
		
		/*** Mehods ***/
	
		/**
		 * Constructor.
		 */
		public function UIFactory(blocker :SingletonBlocker) {
			
			if(blocker == null)
				throw new Error("Public construction not allowed."+
								 "Use GetInstance()");
		}
		
		
		
		/**
		 * Singleton Implementation.
		 * 
		 * @return  UIFactory
		 */
		public static function get Instance() :UIFactory {
			
			if(!(UIFactory._instance is UIFactory))
				UIFactory._instance = new UIFactory(new SingletonBlocker());
				
			return _instance;
		}
		
		
		
		/**
		 * Initializes the widgets in a particular order
	     */
		public function Initialize(container:DisplayObjectContainer) :void {
			
			_container	= container;
			
			Map.Hide();
			CharacterSelect.Hide();
			WeekResults.Hide();
			DayResults.Hide();
			MainMenu.Hide();
			Ranking.Hide();
			Shop.Hide();
			DayComplete.Hide();
			LevelLoadingTransition.Hide();
			SimpleTransition.Hide();
			
			Map.View.gotoAndStop(1);
			CharacterSelect.View.gotoAndStop(1);
			Shop.View.gotoAndStop(1);
			Ranking.View.gotoAndStop(1);
			WeekResults.View.gotoAndStop(1);
			DayResults.View.gotoAndStop(1);
			MainMenu.View.gotoAndStop(1);
			DayComplete.View.gotoAndStop(1);
			LevelLoadingTransition.View.gotoAndStop(1);
			SimpleTransition.View.gotoAndStop(1);
			
			Map.View.tabChildren				= false;
			CharacterSelect.View.tabChildren				= false;
			Shop.View.tabChildren				= false;
			Ranking.View.tabChildren				= false;
			WeekResults.View.tabChildren				= false;
			DayResults.View.tabChildren				= false;
			MainMenu.View.tabChildren				= false;
			DayComplete.View.tabChildren			= false;
			LevelLoadingTransition.View.tabChildren	= false;
			SimpleTransition.View.tabChildren		= false;
		}
		
		public function allAssetsLoaded():void 
		{
			_container.addChild(Exit.View);
			_container.addChild(Map.View);
			_container.addChild(CharacterSelect.View);
			_container.addChild(WeekResults.View);
			_container.addChild(DayResults.View);
			_container.addChild(MainMenu.View);
			_container.addChild(LevelFinish.View);
			_container.addChild(Ranking.View);
			_container.addChild(Shop.View);
			_container.addChild(DayComplete.View);
			_container.addChild(LevelLoadingTransition.View);
			_container.addChild(SimpleTransition.View);
			
			Exit.Hide();
			LevelFinish.Hide();
			
			Exit.View.tabChildren			= false;
			LevelFinish.View.tabChildren	= false;
		}
		
		/**
		 * Returns the SimpleTransition Widget
	     */
		public function get SimpleTransition() :UISimpleTransition {
			
			if (!(_simpleTransition is UISimpleTransition)) {
				_simpleTransition = new UISimpleTransition(ExternalLoader.Instance.GetNewInstance("SimpleTransitionAsset","transitions.swf") as MovieClip);
				_container.addChild(_simpleTransition.View);
				
			}
			return _simpleTransition;
		}
		
		/**
		 * Returns the LevelLoadingTransition Widget
	     */
		public function get LevelLoadingTransition() :UILevelLoadingTransition {
			
			if (!(_LevelLoadingTransition is UILevelLoadingTransition)) {
				_LevelLoadingTransition = new UILevelLoadingTransition(ExternalLoader.Instance.GetNewInstance("LevelLoadingTransitionAsset","transitions.swf") as MovieClip);
				_container.addChild(_LevelLoadingTransition.View);
				
			}
			return _LevelLoadingTransition;
		}
		
		/**
		 * Returns the MainMenu Widget
	     */
		public function get MainMenu() :UIMainMenu {
			
			if (!(_mainMenu is UIMainMenu)) {
				_mainMenu = new UIMainMenu(ExternalLoader.Instance.GetNewInstance("MainMenuAsset","splash.swf") as MovieClip);
				_container.addChild(_mainMenu.View);
				
			}
			return _mainMenu;
		}
		
		/**
		 * Returns the LevelFinish Widget
	     */
		public function get LevelFinish() :UILevelFinish {
			
			if (!(_levelFinish is UILevelFinish)) {
				_levelFinish = new UILevelFinish(ExternalLoader.Instance.GetNewInstance("LevelFinishAsset","levelfinish.swf") as MovieClip);
				_container.addChild(_levelFinish.View);
				
			}
			return _levelFinish;
		}
		
		
		/**
		 * Returns the Exit Widget
	     */
		public function get Exit() :UIExit {
			
			if (!(_exit is UIExit)) {
				_exit = new UIExit(ExternalLoader.Instance.GetNewInstance("ExitAsset", "exit.swf") as MovieClip);
				_container.addChild(_exit.View);
			}
			return _exit;
		}
		
		
		/**
		 * Returns the Map Widget
	     */
		public function get Map() :UIMap {
			
			if (!(_map is UIMap)) {
				_map = new UIMap(ExternalLoader.Instance.GetNewInstance("MapAsset","map.swf") as MovieClip);
				_container.addChild(_map.View);
			}
			return _map;
		}
		
		
		/**
		 * Returns the CharacterSelect Widget
	     */
		public function get CharacterSelect() :UICharacterSelect {
			
			if (!(_characterSelect is UICharacterSelect)) {
				_characterSelect = new UICharacterSelect(ExternalLoader.Instance.GetNewInstance("CharacterSelectAsset","characterselect.swf") as MovieClip);
				_container.addChild(_characterSelect.View);
			}
			return _characterSelect;
		}
		
		
		/**
		 * Returns the Shop Widget
	     */
		public function get Shop() :UIShop {
			
			if (!(_shop is UIShop)) {
				_shop = new UIShop(ExternalLoader.Instance.GetNewInstance("ShopAsset","shop.swf") as MovieClip);
				_container.addChild(_shop.View);
			}
			return _shop;
		}
		
		
		/**
		 * Returns the Ranking Widget
	     */
		public function get Ranking() :UIRanking {
			
			if (!(_ranking is UIRanking)) {
				_ranking = new UIRanking(ExternalLoader.Instance.GetNewInstance("RankingAsset","ranking.swf") as MovieClip);
				_container.addChild(_ranking.View);
			}
			return _ranking;
		}
		
		
		/**
		 * Returns the WeekResults Widget
	     */
		public function get WeekResults() :UIWeekResults {
			
			if (!(_weekResults is UIWeekResults)) {
				_weekResults = new UIWeekResults(ExternalLoader.Instance.GetNewInstance("WeekResultsAsset","weekresults.swf") as MovieClip);
				_container.addChild(_weekResults.View);
			}
			return _weekResults;
		}
		
		
		/**
		 * Returns the DayResults Widget
	     */
		public function get DayResults() :UIDayResults {
			
			if (!(_dayResults is UIDayResults)) {
				_dayResults = new UIDayResults(ExternalLoader.Instance.GetNewInstance("DayResultsAsset","dayresults.swf") as MovieClip);
				_container.addChild(_dayResults.View);
			}
			return _dayResults;
		}
		
		
		/**
		 * Returns the DayComplete Widget
	     */
		public function get DayComplete() :UIDayComplete {
			
			if (!(_dayComplete is UIDayComplete)) {
				_dayComplete = new UIDayComplete(ExternalLoader.Instance.GetNewInstance("DayCompleteAsset","daycomplete.swf") as MovieClip);
				_container.addChild(_dayComplete.View);
			}
			return _dayComplete;
		}
	}
	
}

internal class SingletonBlocker{}
