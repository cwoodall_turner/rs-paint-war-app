﻿package ui 
{
import buttons.FlagRollOutBtn;
import buttons.MapRollOutBtn;
import buttons.RollOutBtn;

import com.turner.caf.business.SimpleResponder;

import communication.cn.CNCom;
import communication.cn.Omniture;
import communication.database.DBManager;
import communication.database.DBTeam;

import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.external.ExternalInterface;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.utils.Timer;
import flash.utils.getTimer;

import txt.TextLoader;

import utils.MathUtils;
import utils.StringUtils;
import utils.TextFieldUtils;

public class UIMap  extends UIWidget
	{
		protected var _container	: MovieClip		= null;
		protected var _newUserCaptainShown	: Boolean	= false;
		
		// FLASH TEXTFIELD HACK
		private var _usernameFormat	: TextFormat	= null;
		private var _passwordFormat	: TextFormat	= null;
		
		// Captain texts to test
		private var _captainTextsToTest	: Array	= null;
		
		public function UIMap(asset :MovieClip, depth :int = -1) {
			super(asset, depth);
			addSoundsToButtons();
			prepareTextFormat();
			
			var captainsTextsToTextStr = Config.getValue("test_captain_texts");
			if (captainsTextsToTextStr != null && captainsTextsToTextStr!= "" && captainsTextsToTextStr != "false")
			{
				_captainTextsToTest	= captainsTextsToTextStr.split(",");
			}
		}
		
		
		private function prepareTextFormat():void 
		{
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			
			var userNameTxt : TextField = (loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField;
			_usernameFormat	= userNameTxt.getTextFormat();
			userNameTxt.addEventListener(Event.CHANGE, resetLoginTextFormat);
			
			var passwordTxt : TextField = (loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField;
			_passwordFormat	= userNameTxt.getTextFormat();
			passwordTxt.addEventListener(Event.CHANGE, resetLoginTextFormat);
		}
		
		private function resetLoginTextFormat(e:Event):void 
		{
			//Debugger.Log("resetLoginTextFormat: " + resetLoginTextFormat);
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			var userNameTxt : TextField = (loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField;
			userNameTxt.setTextFormat(_usernameFormat);
			var passwordTxt : TextField = (loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField;
			passwordTxt.setTextFormat(_passwordFormat);
			
			var usernameStr : String = '<FONT LETTERSPACING="2.6" KERNING="1">' + userNameTxt.text + '</FONT>';
			userNameTxt.htmlText = usernameStr;
			//Debugger.Log("usernameStr: " + usernameStr);
			
			var passwordStr : String = '<FONT LETTERSPACING="2.6" KERNING="1">' + passwordTxt.text + '</FONT>';
			passwordTxt.htmlText = passwordStr;
		}
		
		
		private function prepare():void 
		{
			Captain.updateOnMapPrepare();
			
			showUserInfo();
			showTeamsScores();
			prepareLevelButtons();
			showDayAndTime();
			AddButtonsListeners();
			blockScreen();
			resetFlags();
			
			RollOutBtn(View.getChildByName("sound_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnSndButtonClick);
			RollOutBtn(View.getChildByName("mute_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnMuteButtonClick);
			updateSndButtonsVisibility();
			
			Sfx.PlayMusic("MusicMap", 1);

            if(!Config.show_ranking) View.getChildByName("ranking_btn").visible = false;
		}
		
		private function addSoundsToButtons():void 
		{
			RollOutBtn(View.getChildByName("sound_btn")).OverSoundId		= "BtnOver";
			RollOutBtn(View.getChildByName("mute_btn")).OverSoundId		= "BtnOver";
			
			var userInfoMc	: MovieClip	= View.getChildByName("userInfo_mc") as MovieClip;
			(userInfoMc.getChildByName("login_btn") as RollOutBtn).clickSoundId	= "BtnPerillaClick";
			(userInfoMc.getChildByName("login_btn") as RollOutBtn).OverSoundId	= "BtnPerillaOver";
			
			(View.getChildByName("shop_btn") as RollOutBtn).clickSoundId	= "BtnPerillaClick";
			(View.getChildByName("shop_btn") as RollOutBtn).OverSoundId	= "BtnPerillaOver";
			(View.getChildByName("ranking_btn") as RollOutBtn).clickSoundId	= "BtnPerillaClick";
			(View.getChildByName("ranking_btn") as RollOutBtn).OverSoundId	= "BtnPerillaOver";
			

			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			(loginMc.getChildByName("ok_btn") as RollOutBtn).clickSoundId	= "BtnOkClick";
			(loginMc.getChildByName("ok_btn") as RollOutBtn).OverSoundId	= "BtnOver";
			(loginMc.getChildByName("back_btn") as RollOutBtn).clickSoundId	= "BtnNoClick";
			(loginMc.getChildByName("back_btn") as RollOutBtn).OverSoundId	= "BtnOver";
			

			var captainPopupMc	: MovieClip	= View.getChildByName("captainPopup_mc") as MovieClip;
			(captainPopupMc.getChildByName("ok_btn") as RollOutBtn).clickSoundId	= "BtnOkClick";
			(captainPopupMc.getChildByName("ok_btn") as RollOutBtn).OverSoundId		= "BtnOver";
		}
		
		private function blockScreen():void 
		{
			(View.getChildByName("screenBlocker_mc") as MovieClip).visible = true;
		}
		
		private function unblockScreen():void 
		{
			(View.getChildByName("screenBlocker_mc") as MovieClip).visible = false;
		}
		
		private function prepareInitialNotifications():void 
		{
			Debugger.Log("prepareInitialNotifications: " + prepareInitialNotifications);
			if (_captainTextsToTest != null && _captainTextsToTest.length >= 1)
			{
				showCaptainPopup(Captain.getText(_captainTextsToTest.shift(), DBManager.instance.user.teamId));
				return;
			}
			
			// If new user, show captain info
			if (DBManager.instance.isNewUser && !_newUserCaptainShown)
			{
				RemoveButtonsListeners();
				_newUserCaptainShown	= true;
				showCaptainPopup(Captain.getText("new_user", DBManager.instance.user.teamId));
			}
			else if (!DBManager.instance.isNewUser)
			{
				var hasToShowLastDayScores : Boolean = MainApp.instance.hasToShowLastDayScores;
				var hasToShowLastWeekScores : Boolean = MainApp.instance.hasToShowLastWeekScores;
				Debugger.Log("MainApp.instance.hasToShowLastWeekScores: " + hasToShowLastWeekScores);
				Debugger.Log("MainApp.instance.hasToShowLastDayScores: " + hasToShowLastWeekScores);
				if (hasToShowLastWeekScores)
				{
					RemoveButtonsListeners();
					UIFactory.Instance.WeekResults.FadeIn();
				}
				else if (hasToShowLastDayScores)
				{
					Debugger.Log("A MOSTRAR EL DIA");
					RemoveButtonsListeners();
					UIFactory.Instance.DayResults.FadeIn();
				}
				else
				{
					prepareCaptanNotification();
				}
			}
			else
			{
				prepareCaptanNotification();
			}
		}
		
		private function prepareCaptanNotification():void 
		{	
			// 1st: Forced in config
			if (Config.getValue("force_captain_text") == "true")
			{
				Captain.sayText(Config.getValue("force_captain_text_id"));
				showCaptainPopup(Captain.getText(Config.getValue("force_captain_text_id"), DBManager.instance.user.teamId));
				return;
			}
			
			// 2nd: Notifications to say every time
			var mandatoryLines	: Vector.<String>	= Captain.mandatoryLines;
			if (mandatoryLines != null && mandatoryLines.length > 0)
			{
				var mandatoryLineIdx	: int = MathUtils.random(0, mandatoryLines.length - 1, int);
				Captain.sayText(mandatoryLines[mandatoryLineIdx]);
				showCaptainPopup(Captain.getText(mandatoryLines[mandatoryLineIdx], DBManager.instance.user.teamId));
				return;
			}
			
			// 3rd: Probability
			var probabilityToShowCaptain : Number = Config.getValue("captain_probability_percentage");
			var decidingValue	: Number	= MathUtils.random(0, 100);
			if (decidingValue <= probabilityToShowCaptain)
			{
				var possibleLines	: Vector.<String>	= Captain.possibleLines;
				for each(var possibleLine : String in possibleLines)
				{
					Debugger.Log("CAPTAIN Possible line: " + possibleLine);
				}
				var possibleLineIdx	: int = MathUtils.random(0, possibleLines.length - 1, int);
				Captain.sayText(possibleLines[possibleLineIdx]);
				showCaptainPopup(Captain.getText(possibleLines[possibleLineIdx], DBManager.instance.user.teamId));
				return;
			}
			
			// Show nothing
			Captain.sayNothing();
			unblockScreen();
		}
		
		private function resetFlags():void 
		{
			for (var levelNbr: int = 1; levelNbr <= Config.num_levels; levelNbr++)
			{
				for (var flagNbr : int = 1; flagNbr <= 5; flagNbr++)
				{
					var flagMc : MovieClip = (View.getChildByName("flags_mc") as MovieClip).getChildByName("level_" + levelNbr.toString() + "_flag_" + flagNbr.toString()) as MovieClip;
					if (flagMc != null)
					{
						var innerFlag : FlagRollOutBtn = flagMc.getChildByName("inner" + flagMc.currentFrame.toString() + "_mc") as FlagRollOutBtn;
						if (innerFlag != null)
						{
							innerFlag.reset();
						}
					}
				}
			}
		}
		
		private function AddButtonsListeners():void 
		{
			var userInfoMc	: MovieClip	= View.getChildByName("userInfo_mc") as MovieClip;
			if (Config.is_guest)
			{
				(userInfoMc.getChildByName("login_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, showCNLoginPopup);
			}
			
			
			for (var levelNbr: int = 1; levelNbr <= Config.num_levels; levelNbr++)
			{
				var levelBtn: MapRollOutBtn	= View.getChildByName("level" + levelNbr.toString() + "_btn") as MapRollOutBtn;
				levelBtn.HitArea.addEventListener(MouseEvent.CLICK, onLevelSelect);
				levelBtn.addEventListener(MapRollOutBtn.EVENT_CHILD_ROLLOVER, onFlagRollOver);
				levelBtn.addEventListener(MapRollOutBtn.EVENT_CHILD_ROLLOUT, onFlagRollOut);
				levelBtn.addEventListener(MapRollOutBtn.EVENT_CHILD_MOUSE_DOWN, onFlagMouseDown);
				levelBtn.addEventListener(MapRollOutBtn.EVENT_CHILD_MOUSE_UP, onFlagMouseUp);
			}
			
			(View.getChildByName("shop_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onShopBtnClicked);

            if(Config.show_ranking)
            {
                if (Config.is_guest)
                {
                    (View.getChildByName("ranking_btn") as RollOutBtn).Disable();
                } else {
                    (View.getChildByName("ranking_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onRankingBtnClicked);
                    (View.getChildByName("ranking_btn") as RollOutBtn).Enable();
                }
            }
		}
		
		private function onFlagRollOver(e:Event):void 
		{
			var levelNbrStr : String = (e.currentTarget as DisplayObject).name.substr(5, 1);
			var flagNbrStr 	: String = (e.target as DisplayObject).name.substr(7, 1);
			var flagMc : MovieClip = (View.getChildByName("flags_mc") as MovieClip).getChildByName("level_" + levelNbrStr + "_flag_" + flagNbrStr) as MovieClip;
			
			if (flagMc != null)
			{
				var innerFlag : FlagRollOutBtn = flagMc.getChildByName("inner" + flagMc.currentFrame.toString() + "_mc") as FlagRollOutBtn;
				if (innerFlag != null)
				{
					innerFlag.OnRollOver();
				}
			}
		}
		
		private function onFlagRollOut(e:Event):void 
		{
			var levelNbrStr : String = (e.currentTarget as DisplayObject).name.substr(5, 1);
			var flagNbrStr 	: String = (e.target as DisplayObject).name.substr(7, 1);
			var flagMc : MovieClip = (View.getChildByName("flags_mc") as MovieClip).getChildByName("level_" + levelNbrStr + "_flag_" + flagNbrStr) as MovieClip;
			
			if (flagMc != null)
			{
				var innerFlag : FlagRollOutBtn = flagMc.getChildByName("inner" + flagMc.currentFrame.toString() + "_mc") as FlagRollOutBtn;
				if (innerFlag != null)
				{
					innerFlag.OnRollOut();
				}
			}
		}
		
		private function onFlagMouseDown(e:Event):void 
		{
			var levelNbrStr : String = (e.currentTarget as DisplayObject).name.substr(5, 1);
			var flagNbrStr 	: String = (e.target as DisplayObject).name.substr(7, 1);
			var flagMc : MovieClip = (View.getChildByName("flags_mc") as MovieClip).getChildByName("level_" + levelNbrStr + "_flag_" + flagNbrStr) as MovieClip;
			
			if (flagMc != null)
			{
				var innerFlag : FlagRollOutBtn = flagMc.getChildByName("inner" + flagMc.currentFrame.toString() + "_mc") as FlagRollOutBtn;
				if (innerFlag != null)
				{
					innerFlag.OnMouseDown();
				}
			}
		}
		
		private function onFlagMouseUp(e:Event):void 
		{
			var levelNbrStr : String = (e.currentTarget as DisplayObject).name.substr(5, 1);
			var flagNbrStr 	: String = (e.target as DisplayObject).name.substr(7, 1);
			var flagMc : MovieClip = (View.getChildByName("flags_mc") as MovieClip).getChildByName("level_" + levelNbrStr + "_flag_" + flagNbrStr) as MovieClip;
			
			if (flagMc != null)
			{
				var innerFlag : FlagRollOutBtn = flagMc.getChildByName("inner" + flagMc.currentFrame.toString() + "_mc") as FlagRollOutBtn;
				if (innerFlag != null)
				{
					innerFlag.OnMouseUp();
				}
			}
		}
		
		private function RemoveButtonsListeners():void 
		{
			var userInfoMc	: MovieClip	= View.getChildByName("userInfo_mc") as MovieClip;
			(userInfoMc.getChildByName("login_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, showCNLoginPopup);
			
			for (var levelNbr: int = 1; levelNbr <= Config.num_levels; levelNbr++)
			{
				var levelBtn: MapRollOutBtn	= View.getChildByName("level" + levelNbr.toString() + "_btn") as MapRollOutBtn;
				levelBtn.removeEventListener(MouseEvent.CLICK, onLevelSelect);
			}
			
			(View.getChildByName("shop_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onShopBtnClicked);
            if(Config.show_ranking) (View.getChildByName("ranking_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onRankingBtnClicked);
		}
		
		private function showDayAndTime():void 
		{
			TextFieldUtils.SetTextPreservingFormat(
				(View.getChildByName("dateTime_mc") as MovieClip).getChildByName("day_txt") as TextField,
				DBManager.instance.game.getVar("weekday").toString());
			
			updateTime();
			View.getChildByName("dateTime_mc").visible = true;
			GameContext.GlobalStage.addEventListener(Event.ENTER_FRAME, updateTime);
		}
		
		private function hideDayAndTime():void 
		{
			View.getChildByName("dateTime_mc").visible = false;
			GameContext.GlobalStage.removeEventListener(Event.ENTER_FRAME, updateTime);
		}
		
		private function updateTime(e:Event = null):void 
		{
			var currentTimerMs		: int = getTimer();
			var timeOfDayOnCheckMs	: int = int(DBManager.instance.game.getVar("time_of_day_ms"));
			var timerOnCheckMs		: int = int(DBManager.instance.game.getVar("game_time_at_time_of_day_ms"));
			
			var currentTimeOfDayMs	: int	= timeOfDayOnCheckMs + currentTimerMs - timerOnCheckMs;
			var totalMsInADay		: int	= 24 * 60 * 60 * 1000;
			
			var msLeftToNextDay	: int	= totalMsInADay - currentTimeOfDayMs;
			
			if (msLeftToNextDay < 0)
			{
				msLeftToNextDay = 0;
				
				// Go to "day complete" only if it's called by ENTER_FRAME
				if (e != null)	onDayCompleteWaiting();
			}
			TextFieldUtils.SetTextPreservingFormat(
				(View.getChildByName("dateTime_mc") as MovieClip).getChildByName("time_txt") as TextField,
				StringUtils.MsToHHMMSS(msLeftToNextDay));
		}
		
		private function onDayCompleteWaiting():void 
		{
			Debugger.Log("DAY COMPLETE (Waiting..)!!!");
			RemoveButtonsListeners();
			GameContext.GlobalStage.removeEventListener(Event.ENTER_FRAME, updateTime);
			
			var tmpTimer	: Timer	= new Timer(ConfigLoader.Instance.Config["day_complete_time_to_show_popup"], 1);
			tmpTimer.addEventListener(TimerEvent.TIMER, onDayComplete);
			tmpTimer.start();
		}
		
		private function onDayComplete(e:TimerEvent = null):void 
		{
			if (e != null)	e.currentTarget.removeEventListener(TimerEvent.TIMER, onDayComplete);
			MainApp.instance.OnGameEvent("DayComplete");
		}
		
		private function onLevelSelect(e:MouseEvent):void 
		{
			var levelBtn : MapRollOutBtn = (e.currentTarget is MapRollOutBtn)?(e.currentTarget as MapRollOutBtn):((e.currentTarget as MovieClip).parent as MapRollOutBtn);
			var levelNbr : int = int(levelBtn.name.substr(5, 1));
			Config.level_number = levelNbr;
			MainApp.instance.OnGameEvent("LevelSelected");
		}
		
		private function onShopBtnClicked(e:MouseEvent):void 
		{
			RemoveButtonsListeners();
			MainApp.instance.OnGameEvent("ShopBtnClicked");
		}
		
		private function onRankingBtnClicked(e:MouseEvent):void 
		{
			RemoveButtonsListeners();
			MainApp.instance.OnGameEvent("RankingBtnClicked");
		}
		
		private function showUserInfo():void 
		{
			var userInfoMc	: MovieClip	= View.getChildByName("userInfo_mc") as MovieClip;
			//var genderStr	: String	= "boy";
			
			
			Debugger.Log("showUserInfo - Config.is_guest: " + Config.is_guest);
			if (Config.is_guest)
			{
				userInfoMc.gotoAndStop("guest");
				(userInfoMc.getChildByName("avatar_mc") as MovieClip).gotoAndStop("guest");
			}
			else
			{
				userInfoMc.gotoAndStop("loggedin");
				var dbManagerInstance : DBManager = DBManager.instance;
				Debugger.Log("dbManagerInstance: " + dbManagerInstance);
				Debugger.Log("dbManagerInstance.user: " + dbManagerInstance.user);
                TextFieldUtils.SetTextPreservingFormat(userInfoMc.getChildByName("name_txt") as TextField, CNCom.instance.user.userName);
                if(Config.show_ranking)
                {
                    var rankingStr : String = "#" + DBManager.instance.user.rank.toString();
                    TextFieldUtils.SetTextPreservingFormat(userInfoMc.getChildByName("ranking_txt") as TextField, rankingStr);
                }
                else
                {
                    userInfoMc.getChildByName("ranking_txt").visible = false;
                }
				//if ( CNCom.instance.user.gender.toUpperCase() != "MALE" ) genderStr = "girl";

				(userInfoMc.getChildByName("avatar_mc") as MovieClip).gotoAndStop("loggedin");
			}
			
			Debugger.Log("DBManager.instance.user.teamId: " + DBManager.instance.user.teamId);
			
			//(userInfoMc.getChildByName("avatar_mc") as MovieClip).gotoAndStop(genderStr);
			
			(userInfoMc.getChildByName("team_mc") as MovieClip).gotoAndStop("team" + DBManager.instance.user.teamId);
			TextFieldUtils.SetHtmlTextPreservingFormat(userInfoMc.getChildByName("money_txt") as TextField, DBManager.instance.user.money.toString());
			
		}
		
		
		private function showTeamsScores():void 
		{
			Debugger.Log("showTeamsScores: " + showTeamsScores);
			var teams : Array = new Array();
			var team	: DBTeam	= null;
			var sumOfAllScores	: int = 0;
			for each (team in DBManager.instance.teams)
			{
				team.sortValue = team.getTotalScore();
				sumOfAllScores	+= team.getTotalScore();
				Debugger.Log("team: " + team + "  -  team.sortValue: " + team.sortValue);
				teams.push(team);
			}
			teams.sort(compareTeams, Array.DESCENDING);
			var percentages : Vector.<int> = new Vector.<int>();
			var sumOfAllPercentages	: int = 0;
			for (var i: int = teams.length - 1; i >= 0; i--)
			{
				team	= teams[i] as DBTeam;
				Debugger.Log("teams[" + i + "]: " + teams[i]);
				var totalScore : int = team.sortValue;
				Debugger.Log("Score team " + team.teamId + ": " + totalScore + "  --  " + team.sortValue);
				Debugger.Log("team.getTotalScore(): " + team.getTotalScore());
				
				var totalPct : int = 0;
				if (totalScore > 0)
				{
					totalPct = Math.round(totalScore * 100 / sumOfAllScores);
				}
				sumOfAllPercentages += totalPct;
				percentages.unshift(totalPct);
			}
			Debugger.Log("sumOfAllPercentages: " + sumOfAllPercentages);
			if (sumOfAllPercentages > 0)
			{
				if (sumOfAllPercentages < 100)
				{
					// Si sumándole al 1ro no me paso de 100, le sumo al 1ro
					if ((percentages[0] + (100 - sumOfAllPercentages)) <= 100)
					{
						percentages[0] += (100 - sumOfAllPercentages);
					}
					else
					{
						percentages[percentages.length - 1] += (100 - sumOfAllPercentages);
					}
				}
				else if (sumOfAllPercentages > 100)
				{
					// Si restándole al último no queda menor a 0, le resto al último
					if ((percentages[percentages.length - 1] - (100 - sumOfAllPercentages)) >= 0)
					{
						percentages[percentages.length - 1] -= (sumOfAllPercentages - 100);
					}
					else
					{
						percentages[0] -= (sumOfAllPercentages - 100);
					}
				}
			}
			for (var j: int = percentages.length - 1; j >= 0; j--)
			{
				team	= teams[j] as DBTeam;
				var scoreMc		: MovieClip = View.getChildByName("totalScore" + (j + 1) +"_mc") as MovieClip;
				scoreMc.gotoAndStop(team.teamId);
				var scoreTxt	: TextField = scoreMc.getChildByName("score_txt") as TextField;
				TextFieldUtils.SetTextPreservingFormat(scoreTxt, percentages[j].toString() + "%");
			}
			
			Captain.updateTeamsPosition(teams[0] as DBTeam, teams[1] as DBTeam, teams[2] as DBTeam);
		}
		
		
		private function prepareLevelButtons():void 
		{
			// CAPTAIN
			var positionsPerLevel : Vector.<Vector.<DBTeam>>	= new Vector.<Vector.<DBTeam>>();
			
			for (var levelNbr: int = 1; levelNbr <= Config.num_levels; levelNbr++)
			{
				var levelBtnMc	: MovieClip	= View.getChildByName("level" + levelNbr.toString() + "_btn") as MovieClip;
				
				// BTN IMAGES
				(levelBtnMc.getChildByName("back_mc") as MovieClip).gotoAndStop(levelNbr);
				//(levelBtnMc.getChildByName("image_mc") as MovieClip).gotoAndStop(levelNbr);
				
				// INFO POPUP
				var infoPopup : MovieClip = levelBtnMc.getChildByName("infoPopup_mc") as MovieClip;
				// TITLE
				var infoTitleTxt: TextField = (infoPopup.getChildByName("title_mc") as MovieClip).getChildByName("level_info_title") as TextField;
				var infoTitleStr	: String	= TextLoader.Instance.Texts["map_level_title_level_" + levelNbr.toString()];
				TextFieldUtils.SetHtmlTextPreservingFormat(infoTitleTxt, infoTitleStr);
				// TEAM SCORES
				var levelTotalScore : int = 0;
				var teams : Array = new Array();
				for each (var team:DBTeam in DBManager.instance.teams)
				{
					team.sortValue = team.getLevelScore(levelNbr);
					teams.push(team);
					levelTotalScore += team.sortValue;
				}
				teams.sort(compareTeams, Array.DESCENDING);
				var percentages : Vector.<Number> = new Vector.<Number>();
				for (var teamPosIdx: int = teams.length - 1; teamPosIdx >= 0; teamPosIdx--)
				{
					var t:DBTeam	= teams[teamPosIdx] as DBTeam;
					var levelScore : int = t.sortValue;
					var scoreMc		: MovieClip = infoPopup.getChildByName("score" + (teamPosIdx + 1).toString() + "_mc") as MovieClip;
					(scoreMc.getChildByName("team_mc") as MovieClip).gotoAndStop(t.teamId);
					// SCORE PERCENTAGE
					//var scorePercentage : int = 0;
					var realScorePercentage : Number = 0;
					if (levelTotalScore > 0)
					{
						//scorePercentage = Math.ceil(100 * (levelScore / levelTotalScore) + 1);
						realScorePercentage = 100 * levelScore / levelTotalScore;
					}
					
					(scoreMc.getChildByName("percentageBar_mc") as MovieClip).gotoAndStop(Math.ceil(realScorePercentage) + 1);
					((scoreMc.getChildByName("percentageBar_mc") as MovieClip).getChildByName("progressBarInside_mc") as MovieClip).gotoAndStop(t.teamId);
					Debugger.Log("Score level " + levelNbr + " team " + t.teamId + ": " + levelScore + " / " + levelTotalScore + " (" + realScorePercentage + " %)");
					// SCORE NUMBER
					var scoreTxt	: TextField = scoreMc.getChildByName("score_txt") as TextField;
					TextFieldUtils.SetTextPreservingFormat(scoreTxt, levelScore.toString());
					// LEVEL ICON
					(scoreMc.getChildByName("image_mc") as MovieClip).gotoAndStop(levelNbr);
					
					if (levelTotalScore > 0)
						percentages.unshift(100 * (levelScore / levelTotalScore));
					else
						percentages.unshift(0);
				}
				
				var winningTeamMc : MovieClip = levelBtnMc.getChildByName("winningTeam_mc") as MovieClip;
				winningTeamMc.gotoAndStop((teams[0] as DBTeam).teamId);
				
				// FLAGS
				if (levelTotalScore == 0)
				{
					percentages = new Vector.<Number>();
					percentages.push(100 / 3);
					percentages.push(100 / 3);
					percentages.push(100 / 3);
				}
				var flagsLeft 		: int 		= 5;
				var percentageLeft	: Number	= 100;
				var nextFlag	: int = 1;
				for (var i : int = 0; i <= teams.length - 1; i++)
				{
					var team:DBTeam	= teams[i] as DBTeam;
					var percentage:Number = percentages[i];
					var percentagePerFlag : Number = 0;
					var numFlagsForTeam : int = 0;
					if (flagsLeft > 0 && percentageLeft > 0)
					{
						percentagePerFlag = percentageLeft / flagsLeft;
						numFlagsForTeam = Math.round(percentage / percentagePerFlag);
						if (numFlagsForTeam > flagsLeft) numFlagsForTeam = flagsLeft;
						//Debugger.Log("Level " + levelNbr.toString() + " - Team " + team.teamId + " = " + percentage + "% (" + numFlagsForTeam.toString() + " flags)");
						
						flagsLeft -= numFlagsForTeam;
						percentageLeft -= percentage;
					}
					
					while (numFlagsForTeam > 0)
					{
						var flagMc : MovieClip = (View.getChildByName("flags_mc") as MovieClip).getChildByName("level_" + levelNbr.toString() + "_flag_" + nextFlag.toString()) as MovieClip;
						flagMc.gotoAndStop(int(team.teamId));
						nextFlag++;
						numFlagsForTeam--;
						
						//Debugger.Log("++LEVEL " + levelNbr.toString() + " FLAG " + (nextFlag - 1).toString() + " -> Team " + int(team.teamId).toString());
					}
				}
				
				// CAPTAIN
				var levelPos : Vector.<DBTeam>	= new Vector.<DBTeam>();
				for (var j : int = 0; j <= teams.length - 1; j++)
				{
					levelPos.push(teams[j] as DBTeam);
				}
				positionsPerLevel.push(levelPos);
			}
			
			// CAPTAIN
			Captain.updateLevelsScores(positionsPerLevel);
		}
		
		/**
		 * Custom team ranking sort function
		 */
		private function compareTeams( t1:DBTeam, t2:DBTeam ):int
		{
			return (t1.sortValue - t2.sortValue);
		}
		
		
		
		/**
		 * CN LOGIN
		 */
		private function showCNLoginPopup(e:MouseEvent):void 
		{
            if(Config.getValue("use_ingame_login") != "true")
            {
                var url:String = MainApp.instance.loaderInfo.url;
                Debugger.Log("url.indexOf(file://): " + url.indexOf("file://"));
                Debugger.Log("ExternalInterface.available: " + ExternalInterface.available);
                if (url.indexOf("file://") < 0 && ExternalInterface.available) {
                    Debugger.Log("VA...");
//                        ExternalInterface.call("function(){alert('Calling LoginModule.showLoginWindow(...)...'); LoginModule.showLoginWindow({visible: true}, 'login');}");
//                        ExternalInterface.call("function(){alert('After showLoginWindow');}");
                    ExternalInterface.call("function(){LoginModule.showLoginWindow({visible: true}, 'login');}");

                    startCheckingLoggedIn();
                }
            }
            else
            {

                var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
                loginMc.getChildByName("loading_mc").visible = false;
                loginMc.getChildByName("connectionError_mc").visible = false;
                loginMc.gotoAndPlay("enter");
                loginMc.visible = true;

                Sfx.PlaySound("LoginPopupEnter");

                addLoginButtonsListeners();

                TextFieldUtils.SetTextPreservingFormat((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField, Config.getValue("cn_default_user"));
                TextFieldUtils.SetTextPreservingFormat((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField, Config.getValue("cn_default_password"));
            }
		}




    // Temporary patch (MSIB Login Module)
    private function startCheckingLoggedIn():void {
        GameContext.GlobalStage.addEventListener(Event.ENTER_FRAME, checkLoggedIn);
    }

    private function checkLoggedIn(event:Event):void {
        if(CNCom.instance.getLoggedUser() != null)
        {
            onUserLoggedIn();
        }
    }

    private function onUserLoggedIn():void {
        GameContext.GlobalStage.removeEventListener(Event.ENTER_FRAME, checkLoggedIn);
        onLoginOk(CNCom.instance.user);
    }

		
		private function addLoginButtonsListeners():void 
		{
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			(loginMc.getChildByName("ok_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onCNLoginOkBtn);
			(loginMc.getChildByName("back_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onCNLoginBackBtn);
			
			// TAB & FOCUS
			loginMc.parent.tabChildren = true;
			GameContext.GlobalStage.focus = (loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField;
			((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField).tabIndex = 1;
			((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField).tabIndex = 2;
			
			((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField).addEventListener(KeyboardEvent.KEY_DOWN, checkLoginEnter);
			((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField).addEventListener(KeyboardEvent.KEY_DOWN, checkLoginEnter);
		}
		
		private function removeLoginButtonsListeners():void 
		{
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			(loginMc.getChildByName("ok_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onCNLoginOkBtn);
			(loginMc.getChildByName("back_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onCNLoginBackBtn);
			
			// TAB & FOCUS
			loginMc.parent.tabChildren = false;
			((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField).tabIndex = 0;
			((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField).tabIndex = 0;
			
			((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField).removeEventListener(KeyboardEvent.KEY_DOWN, checkLoginEnter);
			((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField).removeEventListener(KeyboardEvent.KEY_DOWN, checkLoginEnter);
		}
		
		private function checkLoginEnter(event:KeyboardEvent)
		{
			if (event.charCode == 13) // ENTER
			{
				onCNLoginOkBtn();
			}
			else if (event.charCode == 27) // ESC
			{
				onCNLoginBackBtn();
			}
		}
		
		private function onCNLoginBackBtn(e:MouseEvent = null):void
		{
			removeLoginButtonsListeners();
			
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			loginMc.gotoAndPlay("exit");
			
			AddButtonsListeners();
		}
		
		private function onCNLoginOkBtn(e:MouseEvent = null):void
		{
			removeLoginButtonsListeners();
			
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			loginMc.getChildByName("loading_mc").visible = true;
			var usernameStr : String = ((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField).text;
			var passwordStr : String = ((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField).text;
			
			CNCom.instance.doLogin(usernameStr, passwordStr,  new SimpleResponder(onLoginOk, onLoginError));
		}
		
		private function onLoginOk(data:Object):void 
		{
			Debugger.Log("onLoginOk");
			//showCNUserInfo();
			
			getDBData();
			
			// OMNITURE
			Omniture.trackMilestone("login_mode", "map_login");
		}
		
		private function onLoginError(info:Object):void 
		{
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			loginMc.getChildByName("loading_mc").visible = false;
			
			showErrorMc();
		
			addLoginButtonsListeners();
		}
		
		private function showErrorMc():void 
		{
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			(loginMc.getChildByName("connectionError_mc") as MovieClip).gotoAndPlay(2);
			loginMc.getChildByName("connectionError_mc").visible = true;
		}
		
		private function exitLoginPopup():void 
		{
			
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			
			loginMc.getChildByName("loading_mc").visible = false;
			loginMc.gotoAndPlay("exit");
			
			Sfx.PlaySound("LoginPopupExit");
		}
		
		
		
		/*
		 * =============================================
		 * *********** DB DATA  *************
		 * =============================================
		 * */
		private function getDBData():void 
		{
			DBManager.instance.getDBData(CNCom.instance.user.id, 2, new SimpleResponder(onGetDBDataComplete, onGetDBDataError));
		}
		
		private function onGetDBDataError(info:Object):void 
		{
			Debugger.Log("UIMap - onGetDBDataError: " + info);
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			loginMc.getChildByName("loading_mc").visible = false;
			
			addLoginButtonsListeners();
		}
		
		private function onGetDBDataComplete(data:Object):void 
		{
			Debugger.Log("UIMap - onGetDBDataComplete: " + data);
			Config.is_guest = false;
			
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			
			loginMc.getChildByName("loading_mc").visible = false;
			loginMc.gotoAndPlay("exit");
			
			prepare(); // TMP
			unblockScreen();
		}
		
		
		public function showCaptainPopup(captainTxtStr:String):void
		{
			var captainPopupMc	: MovieClip	= View.getChildByName("captainPopup_mc") as MovieClip;
			var captainBackMc	: MovieClip	= captainPopupMc.getChildByName("back_mc") as MovieClip;
			captainBackMc.gotoAndStop(DBManager.instance.user.teamId);
			
			var captainTxt	: TextField	= (captainPopupMc.getChildByName("text_mc") as MovieClip).getChildByName("text_txt") as TextField;
			TextFieldUtils.SetHtmlTextPreservingFormat(captainTxt, captainTxtStr);
			
			addCaptainPopupButtonsListeners();
			
			captainPopupMc.gotoAndPlay("enter");
			captainPopupMc.visible = true;
			
			Sfx.PlaySound("CaptainEnter");
		}
		
		private function addCaptainPopupButtonsListeners():void 
		{
			var captainPopupMc	: MovieClip	= View.getChildByName("captainPopup_mc") as MovieClip;
			(captainPopupMc.getChildByName("ok_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onCaptainPopupOkBtn);
		}
		
		private function removeCaptainPopupButtonsListeners():void 
		{
			var captainPopupMc	: MovieClip	= View.getChildByName("captainPopup_mc") as MovieClip;
			(captainPopupMc.getChildByName("ok_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onCaptainPopupOkBtn);
		}
		
		private function onCaptainPopupOkBtn(e:MouseEvent):void
		{
			removeCaptainPopupButtonsListeners();
			
			var captainPopupMc	: MovieClip	= View.getChildByName("captainPopup_mc") as MovieClip;
			captainPopupMc.gotoAndPlay("exit");
			
			if (_captainTextsToTest != null && _captainTextsToTest.length >= 1)
			{
				showCaptainPopup(Captain.getText(_captainTextsToTest.shift(), DBManager.instance.user.teamId));
				return;
			}
			
			AddButtonsListeners();
			unblockScreen();
		}
		
		
		
		
		
		/**
		 * Public method to handle the state of the widget once
		 * the fading in has completed (might be overridden)
		 */
		public override function OnFadeInOver(e:Event) :void
		{
			super.OnFadeInOver(e);
			prepareInitialNotifications();
		}
		
		
		/**
		 * Shows the MC Faded In
		 */
		public override function ShowFadedIn() :void {
			super.ShowFadedIn();
			prepare();
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public override function FadeIn() :void {
			super.FadeIn();
			prepare();
		}
		
		/**
		 * Hide the MC.
		 */
		public override function Hide() :void {
			super.Hide();
			hideDayAndTime();
		}
		
		public function OnDayCompleteFadeOutOver():void 
		{
			unblockScreen();
			prepare();
			prepareInitialNotifications();
		}
		
		public function OnWeekResultsFadeOutOver():void 
		{
			unblockScreen();
		}
		
		public function OnDayResultsFadeOutOver():void 
		{
			unblockScreen();
		}
		
		public function OnRankingFadeOutOver():void 
		{
			unblockScreen();
		}
		
		
		private function OnMuteButtonClick(e:MouseEvent):void {
			Sfx.ToggleMute(false);
			updateSndButtonsVisibility();
		}
		
		private function OnSndButtonClick(e:MouseEvent):void {
			Sfx.ToggleMute(true);
			updateSndButtonsVisibility();
		}
		
		private function updateSndButtonsVisibility():void
		{
			RollOutBtn(_view.getChildByName("mute_btn")).visible = Sfx.Muted;
			RollOutBtn(_view.getChildByName("sound_btn")).visible = !Sfx.Muted;
		}
		
	}
	
}
