﻿package ui {
import buttons.RollOutBtn;

import com.turner.caf.business.SimpleResponder;

import communication.cn.CNCom;
import communication.cn.Omniture;
import communication.database.DBComSharedObject;
import communication.database.DBManager;

import flash.display.MovieClip;
import flash.display.SimpleButton;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.external.ExternalInterface;
import flash.text.TextField;
import flash.text.TextFormat;

import txt.TextLoader;

import utils.StringUtils;
import utils.TextFieldUtils;

public class UIMainMenu  extends UIWidget
	{
		
		// FLASH TEXTFIELD HACK
		private var _usernameFormat	: TextFormat	= null;
		private var _passwordFormat	: TextFormat	= null;
		
		public function UIMainMenu(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
			addSoundsToButtons();
			prepareTextFormat();
		}
		
		private function prepareTextFormat():void 
		{
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			
			var userNameTxt : TextField = (loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField;
			_usernameFormat	= userNameTxt.getTextFormat();
			userNameTxt.addEventListener(Event.CHANGE, resetLoginTextFormat);
			
			var passwordTxt : TextField = (loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField;
			_passwordFormat	= userNameTxt.getTextFormat();
			passwordTxt.addEventListener(Event.CHANGE, resetLoginTextFormat);
		}
		
		private function resetLoginTextFormat(e:Event):void 
		{
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			var userNameTxt : TextField = (loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField;
			userNameTxt.setTextFormat(_usernameFormat);
			var passwordTxt : TextField = (loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField;
			passwordTxt.setTextFormat(_passwordFormat);
			
			var usernameStr : String = '<FONT LETTERSPACING="2.6" KERNING="1">' + userNameTxt.text + '</FONT>';
			userNameTxt.htmlText = usernameStr;
			//Debugger.Log("usernameStr: " + usernameStr);
			
			var passwordStr : String = '<FONT LETTERSPACING="2.6" KERNING="1">' + passwordTxt.text + '</FONT>';
			passwordTxt.htmlText = passwordStr;
			//TextFieldUtils.SetHtmlTextPreservingFormat(userNameTxt, usernameStr);
		}
		
		private function prepare():void 
		{
			(View.getChildByName("CNLogin_mc") as MovieClip).gotoAndStop(0);
			View.getChildByName("CNLogin_mc").visible = false;
			View.getChildByName("loading_mc").visible = false;
			View.getChildByName("connectionError_mc").visible = false;

            if(!Config.allow_guests) View.getChildByName("guest_btn").visible = false;
			
			Debugger.Log("1. CNCom.instance.isLogged: " + CNCom.instance.isLogged);
			if (CNCom.instance.isLogged)
			{
				View.getChildByName("guest_btn").visible = false;
				showCNUserInfo();			
				// OMNITURE
				Omniture.trackMilestone("login_mode", "already_loggedin");
			}
			else
			{
                if(Config.allow_guests) View.getChildByName("guest_btn").visible = true;
				TextFieldUtils.SetTextPreservingFormat(	View.getChildByName("userInfo_txt") as TextField,
														"Not logged to CN");
			}
			
			var codeMc		: MovieClip	= (View.getChildByName("container_mc") as MovieClip).getChildByName("code_mc") as MovieClip;
			if (Config.getValue("use_codes") == "true")
			{
				beginCodeHandle();
			}
			else
			{
				codeMc.gotoAndStop("disabled");
                codeMc.visible = false;
                View.getChildByName("code_btn").visible = false;
			}
			var codeInput	: TextField	= codeMc.getChildByName("code_input") as TextField;
			TextFieldUtils.SetHtmlTextPreservingFormat(codeInput, TextLoader.Instance.Texts["code_input"]);
		}
		
		private function showCNUserInfo():void 
		{
			TextFieldUtils.SetTextPreservingFormat(	View.getChildByName("userInfo_txt") as TextField,
													"Hello, " + CNCom.instance.user.userName);
		}
		
		/**
		 * Called when the main menu screen finishes fading in.
		 */
		public override function OnFadeInOver(e:Event) :void {
			
			super.OnFadeInOver(e);
			MainApp.instance.OnGameEvent("MainMenuFadeInOver");
			
			AddListenersToButtons();
			
			HUDManager.Instance.Hide(true);
		}
		
		private function AddListenersToButtons():void {
			RollOutBtn(View.getChildByName("play_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnPlayButtonClick);
            if(Config.allow_guests) RollOutBtn(View.getChildByName("guest_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnGuestButtonClick);
			(View.getChildByName("clearLocalData_btn") as SimpleButton).addEventListener(MouseEvent.CLICK, OnClearLocalDataClick);
		}
		
		private function RemoveListenersFromButtons():void {
			RollOutBtn(View.getChildByName("play_btn")).HitArea.removeEventListener(MouseEvent.CLICK, OnPlayButtonClick);
            if(Config.allow_guests) RollOutBtn(View.getChildByName("guest_btn")).HitArea.removeEventListener(MouseEvent.CLICK, OnGuestButtonClick);
			(View.getChildByName("clearLocalData_btn") as SimpleButton).removeEventListener(MouseEvent.CLICK, OnClearLocalDataClick);
		}
		
		private function addSoundsToButtons():void 
		{
			RollOutBtn(View.getChildByName("play_btn")).clickSoundId	= "BtnPerillaClick";
			RollOutBtn(View.getChildByName("play_btn")).OverSoundId		= "BtnPerillaOver";
			RollOutBtn(View.getChildByName("guest_btn")).clickSoundId	= "BtnPerillaClick";
			RollOutBtn(View.getChildByName("guest_btn")).OverSoundId	= "BtnPerillaOver";
			RollOutBtn(View.getChildByName("code_btn")).clickSoundId	= "BtnOkClick";
			RollOutBtn(View.getChildByName("code_btn")).OverSoundId		= "BtnOver";
			
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			(loginMc.getChildByName("ok_btn") as RollOutBtn).clickSoundId	= "BtnOkClick";
			(loginMc.getChildByName("ok_btn") as RollOutBtn).OverSoundId	= "BtnOver";
			(loginMc.getChildByName("back_btn") as RollOutBtn).clickSoundId	= "BtnNoClick";
			(loginMc.getChildByName("back_btn") as RollOutBtn).OverSoundId	= "BtnOver";
			
			var newTeamMc	: MovieClip	= View.getChildByName("newTeam_mc") as MovieClip;
			(newTeamMc.getChildByName("ok_btn") as RollOutBtn).clickSoundId	= "BtnOkClick";
			(newTeamMc.getChildByName("ok_btn") as RollOutBtn).OverSoundId	= "BtnOver";
		}
		
		
		
		
		/*
		 * =============================================
		 * *********** GUEST  *************
		 * =============================================
		 * */
		private function OnGuestButtonClick(e:MouseEvent):void {
			
			Config.is_guest = true;
			View.getChildByName("loading_mc").visible = true;
			DBManager.instance.getDBData(Config.guest_cn_id, 1, new SimpleResponder(onGetDBDataComplete, onGetDBDataError));
            GameContext.GlobalStage.removeEventListener(Event.ENTER_FRAME, checkLoggedIn);
		}
		
		
		
		
		/*
		 * =============================================
		 * *********** PLAY  *************
		 * =============================================
		 * */
		private function OnPlayButtonClick(e:MouseEvent):void {
			
			Config.is_guest = false;
			RemoveListenersFromButtons();
			
			if (CNCom.instance.isLogged)
			{
				View.getChildByName("loading_mc").visible = true;
				getDBData(hideLoading);
			}
			else
			{
                if(Config.getValue("use_ingame_login") == "true")
                {
				    showCNLoginPopup();
                }
                else
                {
                    var url:String = MainApp.instance.loaderInfo.url;
                    Debugger.Log("url.indexOf(file://): " + url.indexOf("file://"));
                    Debugger.Log("ExternalInterface.available: " + ExternalInterface.available);
                    if (url.indexOf("file://") < 0 && ExternalInterface.available) {
                        Debugger.Log("VA...");
//                        ExternalInterface.call("function(){alert('Calling LoginModule.showLoginWindow(...)...'); LoginModule.showLoginWindow({visible: true}, 'login');}");
//                        ExternalInterface.call("function(){alert('After showLoginWindow');}");
                        ExternalInterface.call("function(){LoginModule.showLoginWindow({visible: true}, 'login');}");

                        startCheckingLoggedIn();
                    }
                }
			}
		}

    // Temporary patch (MSIB Login Module)
    private function startCheckingLoggedIn():void {
        AddListenersToButtons();
        GameContext.GlobalStage.addEventListener(Event.ENTER_FRAME, checkLoggedIn);
    }

    private function checkLoggedIn(event:Event):void {
        if(CNCom.instance.getLoggedUser() != null)
        {
            onUserLoggedIn();
        }
    }

    private function onUserLoggedIn():void {
        GameContext.GlobalStage.removeEventListener(Event.ENTER_FRAME, checkLoggedIn);
        onLoginOk(CNCom.instance.user);
    }
		
		private function hideLoading():void 
		{
			View.getChildByName("loading_mc").visible = false;
		}
		
		
		
		
		/*
		 * =============================================
		 * *********** CN LOGIN  *************
		 * =============================================
		 * */
		private function showCNLoginPopup():void 
		{
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			loginMc.getChildByName("loading_mc").visible = false;
			loginMc.gotoAndPlay("enter");
			loginMc.visible = true;
			
			Sfx.PlaySound("LoginPopupEnter");
			
			addLoginButtonsListeners();
			
			TextFieldUtils.SetTextPreservingFormat((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField, Config.getValue("cn_default_user"));
			TextFieldUtils.SetTextPreservingFormat((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField, Config.getValue("cn_default_password"));
		}
		
		private function addLoginButtonsListeners():void 
		{
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			(loginMc.getChildByName("ok_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onCNLoginOkBtn);
			(loginMc.getChildByName("back_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onCNLoginBackBtn);
			
			// TAB & FOCUS
			loginMc.parent.tabChildren = true;
			GameContext.GlobalStage.focus = (loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField;
			((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField).tabIndex = 1;
			((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField).tabIndex = 2;
			
			((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField).addEventListener(KeyboardEvent.KEY_DOWN, checkLoginEnter);
			((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField).addEventListener(KeyboardEvent.KEY_DOWN, checkLoginEnter);
		}
		
		private function removeLoginButtonsListeners():void 
		{
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			(loginMc.getChildByName("ok_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onCNLoginOkBtn);
			(loginMc.getChildByName("back_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onCNLoginBackBtn);
			
			// TAB & FOCUS
			loginMc.parent.tabChildren = false;
			((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField).tabIndex = 0;
			((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField).tabIndex = 0;
			
			((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField).removeEventListener(KeyboardEvent.KEY_DOWN, checkLoginEnter);
			((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField).removeEventListener(KeyboardEvent.KEY_DOWN, checkLoginEnter);
		}
		
		private function checkLoginEnter(event:KeyboardEvent)
		{
			if (event.charCode == 13) // ENTER
			{
				onCNLoginOkBtn();
			}
			else if (event.charCode == 27) // ESC
			{
				onCNLoginBackBtn();
			}
		}
		
		private function onCNLoginBackBtn(e:MouseEvent = null):void
		{
			removeLoginButtonsListeners();
			
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			loginMc.gotoAndPlay("exit");
			
			AddListenersToButtons();
		}
		
		private function onCNLoginOkBtn(e:MouseEvent = null):void
		{
			removeLoginButtonsListeners();
			
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			loginMc.getChildByName("loading_mc").visible = true;
			var usernameStr : String = ((loginMc.getChildByName("inputUsername_mc") as MovieClip).getChildByName("i_username_txt") as TextField).text;
			var passwordStr : String = ((loginMc.getChildByName("inputPassword_mc") as MovieClip).getChildByName("i_password_txt") as TextField).text;
			
			CNCom.instance.doLogin(usernameStr, passwordStr,  new SimpleResponder(onLoginOk, onLoginError));
		}
		
		private function onLoginOk(data:Object):void 
		{
			Debugger.Log("onLoginOk: " + data);
			showCNUserInfo();
			
			getDBData(exitLoginPopup);
			
			// OMNITURE
			Omniture.trackMilestone("login_mode", "menu_login");
		}
		
		private function onLoginError(info:Object):void 
		{
			Debugger.Log("onLoginError: " + info);
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			loginMc.getChildByName("loading_mc").visible = false;
			
			showErrorMc();
		
			addLoginButtonsListeners();
		}
		
		private function showErrorMc():void 
		{
			(View.getChildByName("connectionError_mc") as MovieClip).gotoAndPlay(2);
			View.getChildByName("connectionError_mc").visible = true;
		}
		
		private function exitLoginPopup():void 
		{
			
			var loginMc	: MovieClip	= View.getChildByName("CNLogin_mc") as MovieClip;
			
			loginMc.getChildByName("loading_mc").visible = false;
			loginMc.gotoAndPlay("exit");
			
			Sfx.PlaySound("LoginPopupExit");
		}
		
		
		
		
		/*
		 * =============================================
		 * *********** DB DATA  *************
		 * =============================================
		 * */
		private var _onGetDBDataComplete	: Function	= null;
		private function getDBData(onComplete:Function):void 
		{
			_onGetDBDataComplete = onComplete;
			DBManager.instance.getDBData(CNCom.instance.user.id, 2, new SimpleResponder(onGetDBDataComplete, onGetDBDataError));
		}
		
		private function onGetDBDataError(info:Object):void 
		{
			View.getChildByName("loading_mc").visible = false;
			if (_onGetDBDataComplete != null) _onGetDBDataComplete.call();
			showErrorMc();
			AddListenersToButtons();
		}
		
		private function onGetDBDataComplete(data:Object):void 
		{
			View.getChildByName("loading_mc").visible = false;
			
			if (_onGetDBDataComplete != null) _onGetDBDataComplete.call();
			showDBUserInfo();
			
			var isNewUser : Boolean	= DBManager.instance.isNewUser;
			if (isNewUser)
			{
				showAssignedTeam();
			}
			else
			{
				readyToPlay();
			}
			
			// OMNITURE
			Debugger.Log("onGetDBDataComplete!");
			// Country
			var country	: String = Config.getValue("default_cn_country");
			var url:String = MainApp.instance.loaderInfo.url;
			if (url.indexOf("file://") < 0 && ExternalInterface.available) {
				country	= ExternalInterface.call('function(){ return CNConfig.country; }');
				if (country == null || country == "null")	country	= Config.getValue("default_cn_country");
			}
			Omniture.trackMilestone("country", country);
			// Day and time
			var weekDay	: String = DBManager.instance.game.getVar("weekday");
			var timeOfDayMs	: int = int(DBManager.instance.game.getVar("time_of_day_ms"));
			var timeOfDayStr : String = StringUtils.MsToHHMM(timeOfDayMs);
			Omniture.trackMilestone("day_and_time", weekDay + "-" + timeOfDayStr);
		}
		
		
		private function showDBUserInfo():void 
		{
			var userInfoStr : String 	= "";
			for (var varName in DBManager.instance.user.vars) {
				userInfoStr = userInfoStr.concat(varName + " => " + DBManager.instance.user.vars[varName] + "\n");
			}
			TextFieldUtils.SetTextPreservingFormat(	View.getChildByName("dbInfo_txt") as TextField,
													userInfoStr);
		}
		
		
		
		
		/*
		 * =============================================
		 * *********** SHOW NEW TEAM  *************
		 * =============================================
		 * */
		private function showAssignedTeam():void 
		{
			var newTeamMc	: MovieClip	= View.getChildByName("newTeam_mc") as MovieClip;
			newTeamMc.gotoAndPlay("enter");
			newTeamMc.visible = true;
			
			var newTeamBackMc	: MovieClip	= newTeamMc.getChildByName("back_mc") as MovieClip;
			newTeamBackMc.gotoAndStop(DBManager.instance.user.teamId);
			
			(newTeamMc.getChildByName("ok_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onNewTeamOkBtnClick);
			
			Sfx.PlaySound("NewTeamPopup");
		}
		
		private function onNewTeamOkBtnClick(e:MouseEvent):void 
		{
			var newTeamMc	: MovieClip	= View.getChildByName("newTeam_mc") as MovieClip;
			(newTeamMc.getChildByName("ok_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onNewTeamOkBtnClick);
			newTeamMc.gotoAndPlay("exit");
			
			readyToPlay();
		}
		
		
		
		
		/*
		 * =============================================
		 * *********** CODE  *************
		 * =============================================
		 * */
		
		private function beginCodeHandle():void 
		{
			var codeMc		: MovieClip	= (View.getChildByName("container_mc") as MovieClip).getChildByName("code_mc") as MovieClip;
			codeMc.gotoAndStop("idle");
			var codeInput	: TextField	= codeMc.getChildByName("code_input") as TextField;
			codeInput.addEventListener(MouseEvent.CLICK, OnCodeTxtFldClicked);
			RollOutBtn(View.getChildByName("code_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnSendClick);
			
			TextFieldUtils.SetHtmlTextPreservingFormat(codeInput, TextLoader.Instance.Texts["code_input"]);
		}
		
		private function endCodeHandle():void 
		{
			var codeMc		: MovieClip	= (View.getChildByName("container_mc") as MovieClip).getChildByName("code_mc") as MovieClip;
			var codeInput	: TextField	= codeMc.getChildByName("code_input") as TextField;
			codeInput.removeEventListener(MouseEvent.CLICK, OnCodeTxtFldClicked);
			RollOutBtn(View.getChildByName("code_btn")).HitArea.removeEventListener(MouseEvent.CLICK, OnSendClick);
			
		}
		
		private function checkCodeEnter(e:KeyboardEvent):void 
		{
			if (e.charCode == 13) // ENTER
			{
				OnSendClick();
			}
		}
		
		private function OnSendClick(e:MouseEvent = null):void 
		{
			var codeMc		: MovieClip	= (View.getChildByName("container_mc") as MovieClip).getChildByName("code_mc") as MovieClip;
			var codeInput	: TextField	= codeMc.getChildByName("code_input") as TextField;
			var code	: String	= codeInput.text;
			Debugger.Log("code: " + code);
			var codeResult	: String	= CheatsManager.AddCode(code);
			if (codeResult == CheatsManager.WRONG_CODE) {
				Debugger.Log("TO WRONG");
				codeMc.gotoAndStop("wrong");
			} else {
				Debugger.Log("TO RIGHT");
				var codeTxt	: String	= TextLoader.Instance.Texts["code_" + codeResult];
				var codeResultMc	: MovieClip	= codeMc.getChildByName("result_mc") as MovieClip;
				var codeResultTxtFld	: TextField	= (codeResultMc.getChildByName("inner_mc") as MovieClip).getChildByName("result_txt") as TextField;
				TextFieldUtils.SetHtmlTextPreservingFormat(codeResultTxtFld, codeTxt);
				codeResultMc.gotoAndPlay(1);
				codeMc.gotoAndStop("right");
				Omniture.trackCode(code.toLowerCase());
			}
			GameContext.GlobalStage.addEventListener(Event.ENTER_FRAME, CheckCodeResultAnimationEnd);
			codeInput.addEventListener(MouseEvent.CLICK, OnCodeTxtFldClicked);
			RollOutBtn(View.getChildByName("code_btn")).HitArea.removeEventListener(MouseEvent.CLICK, OnSendClick);
			codeInput.removeEventListener(KeyboardEvent.KEY_DOWN, checkCodeEnter);
		}
		
		private function CheckCodeResultAnimationEnd(e:Event):void 
		{
			var codeMc		: MovieClip	= (View.getChildByName("container_mc") as MovieClip).getChildByName("code_mc") as MovieClip;
			var innerMc	: MovieClip	= codeMc.getChildByName("inner_mc") as MovieClip;
			if (innerMc && innerMc.currentFrame == innerMc.totalFrames) {
				GameContext.GlobalStage.removeEventListener(Event.ENTER_FRAME, CheckCodeResultAnimationEnd);
				codeMc.gotoAndStop("idle");
				RollOutBtn(View.getChildByName("code_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnSendClick);
			}
		}
		
		private function OnCodeTxtFldClicked(e:MouseEvent):void 
		{
			var codeMc		: MovieClip	= (View.getChildByName("container_mc") as MovieClip).getChildByName("code_mc") as MovieClip;
			var codeInput	: TextField	= codeMc.getChildByName("code_input") as TextField;
			codeInput.removeEventListener(MouseEvent.CLICK, OnCodeTxtFldClicked);
			codeInput.text	= "";
			codeInput.addEventListener(KeyboardEvent.KEY_DOWN, checkCodeEnter);
		}
		
		
		
		
		
		
		
		/*
		 * =============================================
		 * *********** READY TO PLAY  *************
		 * =============================================
		 * */
		private function readyToPlay():void 
		{
			if (Config.is_guest)
			{
				MainApp.instance.OnGameEvent("PlayAsGuestClicked");
			}
			else
			{
				MainApp.instance.OnGameEvent("PlayClicked");
			}
			
		}
		
		
		
		
		
		/*
		 * =============================================
		 * *********** TMP: Clear local data ***********
		 * =============================================
		 * */
		private function OnClearLocalDataClick(e:MouseEvent):void 
		{
			Debugger.Log("CLEAR LOCAL DATA!");
			DBComSharedObject.eraseData();
		}
		
		/**
		 * Shows the MC Faded In
		 */
		public override function ShowFadedIn() :void {
			super.ShowFadedIn();
			prepare();
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public override function FadeIn() :void {
			super.FadeIn();
			prepare();
		}
	}
	
}
