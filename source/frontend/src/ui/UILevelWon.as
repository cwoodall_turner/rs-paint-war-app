﻿package ui {
import buttons.RollOutBtn;

import flash.display.MovieClip;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;

import players.Player;

public class UILevelWon  extends UIWidget
	{
		
		public function UILevelWon(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
		}
		
		/**
		 * Called when the main menu starts fading in
		 */
		public override function FadeIn() :void {
			
			//HUDManager.Instance.Hide(true);
			//Camera.Instance.CaptureBackground();
			//Camera.Instance.BlurBitmap();
			// TMP
			GameManager.Instance.activePlayer.SetState(Player.ST_READY);
			
			super.FadeIn();
			
			//Sfx.PlayMusic(Config.music_id_silence, 1);
			//Sfx.PlaySound("LevelWon" + GameManager.Instance.currentLevel.difficulty);
		}
		
		
		/**
		 * Called when the main menu screen finishes fading in.
		 */
		public override function OnFadeInOver(e:Event) :void {
			
			super.OnFadeInOver(e);
			MainApp.instance.OnGameEvent("LevelWonFadeInOver");
			
			AddButtonsEventListeners();
		}
		
		private function AddButtonsEventListeners():void {
			RollOutBtn(View.getChildByName("play_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnPlayButtonClick);
			RollOutBtn(View.getChildByName("menu_btn")).HitArea.addEventListener(MouseEvent.CLICK, OnMenuButtonClick);
			
			RollOutBtn(View.getChildByName("play_btn")).Disable();
			RollOutBtn(View.getChildByName("play_btn")).visible = false;
			RollOutBtn(View.getChildByName("menu_btn")).Enable();
			
			GameContext.GlobalStage.addEventListener(KeyboardEvent.KEY_DOWN, OnKeyDown);
		}
		
		private function RemoveButtonsEventListeners():void {
			RollOutBtn(View.getChildByName("play_btn")).HitArea.removeEventListener(MouseEvent.CLICK, OnPlayButtonClick);
			RollOutBtn(View.getChildByName("menu_btn")).HitArea.removeEventListener(MouseEvent.CLICK, OnMenuButtonClick);
			
			GameContext.GlobalStage.removeEventListener(KeyboardEvent.KEY_DOWN, OnKeyDown);
		}
		
		private function OnKeyDown(e:KeyboardEvent):void {
			
			OnPlayButtonClick(new MouseEvent(MouseEvent.CLICK));
		}
		
		private function OnPlayButtonClick(e:MouseEvent):void {
			
			RemoveButtonsEventListeners();
			MainApp.instance.OnGameEvent("LevelWonPlayClicked");
			//Sfx.StopSound("LevelWon" + GameManager.Instance.currentLevel.difficulty);
		}
		
		private function OnMenuButtonClick(e:MouseEvent):void {
			
			RemoveButtonsEventListeners();
			MainApp.instance.OnGameEvent("LevelWonMenuClicked");
			//Sfx.StopSound("LevelWon" + GameManager.Instance.currentLevel.difficulty);
		}
	}
	
}
