﻿package ui {
import communication.database.DBManager;

import flash.display.MovieClip;
import flash.events.Event;

public class UILevelLoadingTransition  extends UIPreloaderTransition
	{
		
		public function UILevelLoadingTransition(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
		}
		
		override public function PrepareToLoadLevel(levelNumber:int, isNewGame:Boolean):void {
			super.PrepareToLoadLevel(levelNumber, isNewGame);
			
			Debugger.Log("PrepareToLoadLevel: " + levelNumber);
			(View.getChildByName("instructions_mc") as MovieClip).gotoAndStop(levelNumber);
			(View.getChildByName("team_mc") as MovieClip).gotoAndStop(int(DBManager.instance.user.teamId));
		}
		
		
		/**
		 * Called when the transition screen finishes fading in.
		 */
		public override function OnFadeInOver(e:Event) :void {
			
			super.OnFadeInOver(e);
			MainApp.instance.OnGameEvent("LevelLoadingTransitionFadeInOver");
		}
		
		
		
		/**
		 * Called when the transition screen finishes fading out.
		 */
		public override function OnFadeOutOver(e:Event) :void {
			
			super.OnFadeOutOver(e);
			MainApp.instance.OnGameEvent("LevelLoadingTransitionFadeOutOver");
		}
		
		
		/**
		 * Called when the transition screen starts fading out.
		 */
		public override function OnFadeOut(e:Event) :void {
			
			super.OnFadeOut(e);
			MainApp.instance.OnGameEvent("LevelLoadingTransitionFadeOutBegin");
		}
	}
	
}
