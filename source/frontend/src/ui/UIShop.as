﻿package ui 
{
import buttons.RollOutBtn;

import com.turner.caf.business.SimpleResponder;

import communication.cn.CNCom;
import communication.cn.Omniture;
import communication.database.DBManager;

import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.utils.Dictionary;

import txt.TextLoader;

import utils.TextFieldUtils;

public class UIShop  extends UIWidget
	{
		protected var _container	: MovieClip		= null;
		
		private var _itemsBought	: Dictionary = null;
		private var _moneyLeft		: int = 0;
		
		private var _currentBtnNbr : int = 1;
		private var _buying			: Boolean = false;
		
		public function UIShop(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
			addSoundsToButtons();
		}
		
		private function addSoundsToButtons():void 
		{
			(View.getChildByName("back_btn") as RollOutBtn).clickSoundId		= "BtnNoClick";
			(View.getChildByName("back_btn") as RollOutBtn).OverSoundId			= "BtnOver";
			(View.getChildByName("ok_btn") as RollOutBtn).clickSoundId			= "BtnOkClick";
			(View.getChildByName("ok_btn") as RollOutBtn).OverSoundId			= "BtnOver";
			
			var confirmMc	: MovieClip	= View.getChildByName("buyConfirm_mc") as MovieClip;
			(confirmMc.getChildByName("back_btn") as RollOutBtn).clickSoundId	= "BtnNoClick";
			(confirmMc.getChildByName("back_btn") as RollOutBtn).OverSoundId	= "BtnOver";
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).clickSoundId		= "BuyItemOk";
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).OverSoundId		= "BtnOver";
			
			var getCNCreditsMc	: MovieClip	= View.getChildByName("getCNCredits_mc") as MovieClip;
			(getCNCreditsMc.getChildByName("ok_btn") as RollOutBtn).clickSoundId	= "BtnOkClick";
			(getCNCreditsMc.getChildByName("ok_btn") as RollOutBtn).OverSoundId		= "BtnOver";
			
			confirmMc	= View.getChildByName("exitConfirm_mc") as MovieClip;
			(confirmMc.getChildByName("back_btn") as RollOutBtn).clickSoundId	= "BtnNoClick";
			(confirmMc.getChildByName("back_btn") as RollOutBtn).OverSoundId	= "BtnOver";
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).clickSoundId		= "BtnOkClick";
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).OverSoundId		= "BtnOver";
			
			var itemsMc : MovieClip	= View.getChildByName("items_mc") as MovieClip;
			for (var i : int = 1; i <= 9; i++)
			{
				var itemBtn			: MovieClip	= itemsMc.getChildByName("btn" + i + "_mc") as MovieClip;
				(itemBtn.getChildByName("buy_btn") as RollOutBtn).clickSoundId	= "BtnOkClick";
				(itemBtn.getChildByName("buy_btn") as RollOutBtn).OverSoundId	= "BtnOver";
			}
		}
		
		
		private function prepare():void 
		{
			Debugger.Log("UI SHOP prepare");
			resetCurrentBuys();
			showMoneyAfterCurrentBuys();
			showValuesAfterCurrentBuys();
			AddListenersToButtons();
			_buying = false;
			
			Sfx.PlayMusic("MusicShop", 1);

            if(!Config.allow_cnla_credits)
            {
                View.getChildByName("getCNCredits_mc").visible = false;
                View.getChildByName("buyCoins_btn").visible = false;
            }
		}
		
		private function AddListenersToButtons():void 
		{
			(View.getChildByName("back_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onBackBtnClicked);
			(View.getChildByName("ok_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onOkBtnClicked);
			
			updateItemButtons();
		}
		
		private function RemoveListenersFromButtons():void 
		{
			(View.getChildByName("back_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onBackBtnClicked);
			(View.getChildByName("ok_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onOkBtnClicked);
		}
		
		private function resetCurrentBuys():void 
		{
			_itemsBought 	= new Dictionary();
			_moneyLeft		= DBManager.instance.user.money;
		}
		
		private function showMoneyAfterCurrentBuys():void 
		{
			TextFieldUtils.SetTextPreservingFormat(
				(View.getChildByName("money_mc") as MovieClip).getChildByName("money_txt") as TextField,
				_moneyLeft.toString());
		}
		
		
		private function showValuesAfterCurrentBuys():void 
		{
			updateValue("shots_power");
			updateValue("shots_distance");
			updateValue("grenades");
			updateValue("extra_life_slots");
		}
		
		private function updateValue(powerupName:String):void 
		{
			var infoMc : MovieClip = View.getChildByName("currentValue_" + powerupName + "_mc") as MovieClip;
			var valueAfterCurrentBuys : int = getValueAfterCurrentBuys(powerupName);
			
			TextFieldUtils.SetTextPreservingFormat(infoMc.getChildByName("value_txt") as TextField, valueAfterCurrentBuys.toString());
		}
		
		private function updateItemButtons():void 
		{
			Debugger.Log("updateItemButtons: " + updateItemButtons);
			var itemsMc : MovieClip	= View.getChildByName("items_mc") as MovieClip;
			for (var i : int = 1; i <= 9; i++)
			{
				var itemBtn			: MovieClip	= itemsMc.getChildByName("btn" + i + "_mc") as MovieClip;
				var powerupName		: String 	= Config.getValue("shop_item_" + i.toString() + "_name");
				var cost			: int 		= Config.getValue("shop_item_" + i.toString() + "_cost");
				var powerupValue	: int 		= Config.getValue("shop_item_" + i.toString() + "_value");
				
				var powerupNameStr : String = TextLoader.Instance.Texts["powerup_name_" + powerupName];
				TextFieldUtils.SetHtmlTextPreservingFormat((itemBtn.getChildByName("name1_mc") as MovieClip).getChildByName("name_txt") as TextField, powerupNameStr);
				TextFieldUtils.SetHtmlTextPreservingFormat((itemBtn.getChildByName("name2_mc") as MovieClip).getChildByName("name_txt") as TextField, powerupNameStr);
				Debugger.Log("powerupNameStr: " + powerupNameStr);
				//TextFieldUtils.SetTextPreservingFormat(itemBtn.getChildByName("cost_txt") as TextField, cost.toString());
				//TextFieldUtils.SetTextPreservingFormat(itemBtn.getChildByName("value_txt") as TextField, powerupValue.toString());
				//(itemBtn.getChildByName("icon_mc") as MovieClip).gotoAndStop(i);
				
				var buyBtn	: RollOutBtn	= itemBtn.getChildByName("buy_btn") as RollOutBtn;
				buyBtn.Enable(itemBtn);
				
				var btnLabel : String = "";
				buyBtn.HitArea.addEventListener(MouseEvent.CLICK, onItemClicked);
				buyBtn.HitArea.addEventListener(MouseEvent.ROLL_OVER, onItemRollOver);
				buyBtn.HitArea.addEventListener(MouseEvent.ROLL_OUT, onItemRollOut);
				if (isMaxAfterCurrentBuys(powerupName))
				{
					btnLabel	= "maxLevel";
					buyBtn.HitArea.removeEventListener(MouseEvent.CLICK, onItemClicked);
					buyBtn.OverSoundId	= "BtnShopOver";
				} else if (cost > _moneyLeft) {
					btnLabel	= "canNotBuy";
					buyBtn.HitArea.removeEventListener(MouseEvent.CLICK, onItemClicked);
					buyBtn.OverSoundId	= "BtnShopNoMoneyOver";
				} else {
					btnLabel	= "canBuy";
					buyBtn.OverSoundId	= "BtnShopOver";
				}
				Debugger.Log("btnLabel: " + btnLabel);
				itemBtn.gotoAndStop(btnLabel);
				
				
				var buyBtns : Array = [itemBtn.getChildByName("buyNoMoney_btn"), itemBtn.getChildByName("buyFull_btn"), itemBtn.getChildByName("buy_btn")];
				for each (buyBtn in buyBtns)
				{
					buyBtn.Enable(itemBtn);
					
					// Update values INSIDE buttons
					var buyInnerMcs : Array = [buyBtn.upMc, buyBtn.overMc, buyBtn.outMc, buyBtn.downMc];
					for each(var innerMc : MovieClip in buyInnerMcs)
					{
						if (!(innerMc is MovieClip)) continue;
						// COST
						var costMc : MovieClip = innerMc.getChildByName("cost_mc") as MovieClip;
						if (costMc != null)
						{
							costMc.gotoAndStop(btnLabel);
							TextFieldUtils.SetTextPreservingFormat(costMc.getChildByName("cost_txt") as TextField, cost.toString());
						}
						// ITEM AND VALUE
						var itemAndValueMc	 : MovieClip = innerMc.getChildByName("itemAndValue_mc") as MovieClip
						if (itemAndValueMc != null)
						{
							itemAndValueMc.gotoAndStop(btnLabel);
							TextFieldUtils.SetTextPreservingFormat(itemAndValueMc.getChildByName("value_txt") as TextField, powerupValue.toString());
							(itemAndValueMc.getChildByName("icon_mc") as MovieClip).gotoAndStop(i);
						}
					}
				}
				

				
			}
			updateCharacterView();
		}
		
		private function onItemRollOver(e:MouseEvent):void 
		{
			var itemNbrStr : String	= (e.currentTarget as DisplayObject).name.substr(3, 1);
			var powerupName		: String 	= Config.getValue("shop_item_" + itemNbrStr + "_name");
			var powerupValue	: int 		= Config.getValue("shop_item_" + itemNbrStr + "_value");
			updateCharacterView(powerupName, powerupValue);
		}
		
		private function onItemRollOut(e:MouseEvent):void 
		{
			//Debugger.Log("onItemRollOut");
			updateCharacterView();
		}
		
		private function updateCharacterView(powerupBought:String = null, powerupExtraValue:int = 0):void 
		{
			// TMP
			if (_buying) return;
			
			var powerupNames : Array = ["shots_power", "shots_distance", "grenades", "extra_life_slots"];
			var powerupValues : Dictionary = new Dictionary();
			for each ( var powerupName:String in powerupNames)
			{
				var powerupValue : int = int(DBManager.instance.user.getVar(powerupName));
				if (powerupName == powerupBought)
				{
					powerupValue += powerupExtraValue;
				}
				
				if (_itemsBought[powerupName] != undefined)
				{
					powerupValue	+= _itemsBought[powerupName];
				}
				
				powerupValues[powerupName] = powerupValue;
			}
			
			// Update weapon
			var weaponStr : String = "";
			if (powerupValues["shots_distance"] > 0) weaponStr += "distance";
			if (powerupValues["shots_power"] > 0) weaponStr += "power";
			if (weaponStr == "") weaponStr = "none";
			(View.getChildByName("rigby_mc") as MovieClip).gotoAndStop(weaponStr);
			(View.getChildByName("mordecai_mc") as MovieClip).gotoAndStop(weaponStr);
			
			// Update energy
			var numLifeSlots : int = powerupValues["extra_life_slots"];
			(View.getChildByName("energy_mc") as MovieClip).gotoAndStop(numLifeSlots + 1);
		}
		
		
		private function onItemClicked(e:MouseEvent):void 
		{
			_buying = true;
			showBuyConfirmPopup();
			_currentBtnNbr = int((e.currentTarget as DisplayObject).name.substr(3, 1));
			var powerupName		: String 	= Config.getValue("shop_item_" + _currentBtnNbr.toString() + "_name");
			var powerupValue	: int 		= Config.getValue("shop_item_" + _currentBtnNbr.toString() + "_value");
			updateCharacterView(powerupName, powerupValue);
		}
		
		private function showBuyConfirmPopup():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("buyConfirm_mc") as MovieClip;
			confirmMc.gotoAndPlay("enter");
			confirmMc.visible = true;
			addBuyConfirmButtonsListeners();
		}
		
		private function addBuyConfirmButtonsListeners():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("buyConfirm_mc") as MovieClip;
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onConfirmBuyOkBtn);
			(confirmMc.getChildByName("back_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onConfirmBuyBackBtn);
			//Debugger.Log("OK BTN: " + (confirmMc.getChildByName("ok_btn") as RollOutBtn));
		}
		
		private function removeBuyConfirmButtonsListeners():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("buyConfirm_mc") as MovieClip;
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onConfirmBuyOkBtn);
			(confirmMc.getChildByName("back_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onConfirmBuyBackBtn);
		}
		
		private function onConfirmBuyOkBtn(e:MouseEvent):void 
		{
			//Debugger.Log("onConfirmBuyOkBtn: " + onConfirmBuyOkBtn);
			_buying = false;
			removeBuyConfirmButtonsListeners();
			exitBuyConfirmPopup();
			onItemBought(_currentBtnNbr);
			updateCharacterView();
		}
		
		private function onConfirmBuyBackBtn(e:MouseEvent):void 
		{
			_buying = false;
			removeBuyConfirmButtonsListeners();
			exitBuyConfirmPopup();
			updateCharacterView();
		}
		
		private function exitBuyConfirmPopup():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("buyConfirm_mc") as MovieClip;
			confirmMc.gotoAndPlay("exit");
		}
		
		
		
		private function onItemBought(btnNbr:int):void 
		{
			var powerupName	: String 	= Config.getValue("shop_item_" + btnNbr.toString() + "_name");
			var cost			: int = Config.getValue("shop_item_" + btnNbr.toString() + "_cost");
			var powerupValue	: int 	= Config.getValue("shop_item_" + btnNbr.toString() + "_value");
			var maxValue 		: int = Config.getValue("powerup_" + powerupName + "_max_value");
			
			_moneyLeft -= cost;
			
			var previousValue	: int = int(DBManager.instance.user.getVar(powerupName));
			
			var boughtValue : int = powerupValue;
			if (_itemsBought[powerupName] != undefined)
			{
				boughtValue	+= _itemsBought[powerupName];
			}
			
			//Debugger.Log("On Item " + btnNbr + " Bought: " + powerupName + " -- " + previousValue + " -> " + (previousValue + boughtValue).toString() + "  -- Max: " + maxValue);

			if ((previousValue + boughtValue) > maxValue) boughtValue = maxValue - previousValue;
			_itemsBought[powerupName] = boughtValue;
			
			
			showMoneyAfterCurrentBuys();
			showValuesAfterCurrentBuys();
			updateItemButtons();
			updateCharacterView();
			
			// SHOW FX
			var buyFx : MovieClip = null;
			switch(powerupName)
			{
				case "shots_power":
				case "shots_distance":
				case "grenades":
					buyFx	= View.getChildByName("buy_" + powerupName + "_fx") as MovieClip;
					break;
				case "extra_life_slots":
					buyFx	= View.getChildByName("buy_" + powerupName + "_" + boughtValue.toString() + "_fx") as MovieClip;
					break;
			}
			if (buyFx != null)
			{
				buyFx.gotoAndPlay("show");
				
				var numberTxt	: TextField	= (buyFx.getChildByName("number_mc") as MovieClip).getChildByName("number_txt") as TextField;
				TextFieldUtils.SetTextPreservingFormat(numberTxt, powerupValue.toString());
			}
			
			// OMNITURE
			Omniture.trackMilestone("buy_item", powerupName + "x" + powerupValue.toString());
		}
		
		
		private function isMaxAfterCurrentBuys(powerupName:String):Boolean 
		{
			
			var valueAfterCurrentBuys : int = getValueAfterCurrentBuys(powerupName);
			var maxValue : int = Config.getValue("powerup_" + powerupName + "_max_value");
			
			//Debugger.Log("isMaxAfterCurrentBuys("+powerupName+")?: previousValue: " + previousValue + "  -- valueBought: " + valueBought + "  --  maxValue: " + maxValue);
			return (valueAfterCurrentBuys >= maxValue);
		}
		
		private function getValueAfterCurrentBuys(powerupName:String):int 
		{
			var previousValue	: int = int(DBManager.instance.user.getVar(powerupName));
			var valueBought		: int = 0;
			if (_itemsBought[powerupName] != undefined)
			{
				valueBought += int(_itemsBought[powerupName]);
			}
			return (previousValue + valueBought);
		}
		
		
		
		
		
		
		private function onOkBtnClicked(e:MouseEvent):void 
		{
			RemoveListenersFromButtons();
			
			updateActualValues();
			MainApp.instance.OnGameEvent("OnShopOk");
		}
		
		private function updateActualValues():void 
		{
			if (DBManager.instance.user.money == _moneyLeft)
			{
				// No money spent
				return;
			}
			
			// Update Money
			DBManager.instance.user.money = _moneyLeft;
			
			// Update powerups
			for (var powerupName:String in _itemsBought) {
				var boughtValue : int = _itemsBought[powerupName];
				var prevValue : int = DBManager.instance.user.getVar(powerupName);
				DBManager.instance.user.setVar(powerupName, prevValue + boughtValue);
			}
			
			DBManager.instance.saveUserData(new SimpleResponder(onSaveUserDataComplete, onSaveUserDataError));
			
			
			// BADGES
			if (	getValueAfterCurrentBuys("shots_power") > 0
				&&	getValueAfterCurrentBuys("shots_distance") > 0
				&&	getValueAfterCurrentBuys("grenades") > 0
				&&	getValueAfterCurrentBuys("extra_life_slots") > 0
				)
			{
				MainApp.instance.EarnBadge("all_items");
			}
		}
		
		private function onSaveUserDataComplete(data:Object):void 
		{
			Debugger.Log("onSaveUserDataComplete: " + data);
		}
		private function onSaveUserDataError(info:Object):void 
		{
			Debugger.Log("onSaveUserDataError: " + info);
		}
		
		
		
		
		private function onBackBtnClicked(e:MouseEvent):void 
		{
			if (DBManager.instance.user.money == _moneyLeft)
			{
				RemoveListenersFromButtons();
				MainApp.instance.OnGameEvent("OnShopBack");
			}
			else
			{
				showExitConfirmPopup();
			}
		}
		
		private function showExitConfirmPopup():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("exitConfirm_mc") as MovieClip;
			confirmMc.gotoAndPlay("enter");
			confirmMc.visible = true;
			addExitConfirmButtonsListeners();
		}
		
		private function addExitConfirmButtonsListeners():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("exitConfirm_mc") as MovieClip;
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onConfirmExitOkBtn);
			(confirmMc.getChildByName("back_btn") as RollOutBtn).HitArea.addEventListener(MouseEvent.CLICK, onConfirmExitBackBtn);
		}
		
		private function removeExitConfirmButtonsListeners():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("exitConfirm_mc") as MovieClip;
			(confirmMc.getChildByName("ok_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onConfirmExitOkBtn);
			(confirmMc.getChildByName("back_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onConfirmExitBackBtn);
			(confirmMc.getChildByName("back_btn") as RollOutBtn).HitArea.removeEventListener(MouseEvent.CLICK, onConfirmExitBackBtn);
		}
		
		private function onConfirmExitOkBtn(e:MouseEvent):void 
		{
			removeExitConfirmButtonsListeners();
			exitExitConfirmPopup();
			RemoveListenersFromButtons();
			MainApp.instance.OnGameEvent("OnShopBack");
		}
		
		private function onConfirmExitBackBtn(e:MouseEvent):void 
		{
			removeExitConfirmButtonsListeners();
			exitExitConfirmPopup();
		}
		
		private function exitExitConfirmPopup():void 
		{
			var confirmMc	: MovieClip	= View.getChildByName("exitConfirm_mc") as MovieClip;
			confirmMc.gotoAndPlay("exit");
		}
		
		
		
		
		
		/**
		 * Shows the MC Faded In
		 */
		public override function ShowFadedIn() :void {
			super.ShowFadedIn();
			prepare();
		}
		
		/**
		 * Starts playing the MC from the start label.
		 */
		public override function FadeIn() :void {
			super.FadeIn();
			prepare();
		}
		
	}
	
}
