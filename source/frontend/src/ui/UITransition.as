package ui {
import flash.display.MovieClip;

/**
	 * ...
	 * @author alfred
	 */
	public class UITransition extends UIWidget {
		
		public function UITransition(asset :MovieClip, depth :int = -1) {
			
			super(asset, depth);
		}
		
		public function set HtmlText(htmlText:String):void { }
		
		public function set HtmlText2(htmlText:String):void {}
		
		public function set Text(text:String):void {}
		
	}

}