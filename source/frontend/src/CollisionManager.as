package  
{
import enemies.EnemiesManager;
import enemies.Enemy;

import flash.display.Sprite;

import levels.Level;

import obstacles.Obstacle;
import obstacles.ObstaclesManager;

import players.Player;

import powerups.Powerup;
import powerups.PowerupsManager;

import shots.Shot;
import shots.ShotsManager;

import view.Camera;
import view.LayerFactory;

/**
	 * ...
	 * @author ...
	 */
	public class CollisionManager 
	{
		public static const BLOCK_NONE	: uint = 0;
		public static const BLOCK_LEFT	: uint = 1;
		public static const BLOCK_RIGHT	: uint = 2;
		public static const BLOCK_BACK	: uint = 4;
		public static const BLOCK_FRONT	: uint = 8;
		
		public static const LEFT 	: String = "left";
		public static const RIGHT 	: String = "right";
		public static const BACK 	: String = "back";
		public static const FRONT 	: String = "front";
		public static const UP 		: String = "up";
		public static const DOWN 	: String = "down";
		
		
		private var _attackDebugSprite	: Sprite	= null;
		
		private static var _instance:CollisionManager = null;
		
		
		public function CollisionManager() 
		{
			_instance = this;
		}
		
		public function Prepare():void {
			
			if (Config.debug_attack_area && !_attackDebugSprite) {
				_attackDebugSprite	= new Sprite();
				LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_BACK).addChild(_attackDebugSprite);
			}
		}
		
		public function Update(dtMs:int):void {
			
			// DEBUG
			//if (Config.debug_attack_area) {
				//ClearCollisionBoundariesDrawing();
			//}
			
			CheckLevelBoundaries();
			//CheckPlayerToEnemyHit();
			//CheckEnemyToPlayerHit();
			CheckShotToPlayerHit();
			CheckShotToEnemyHit();
			CheckShotToObstacleHit();
			CheckPlayerToObstacleCollision();
			CheckEnemyToObstacleCollision();
			//CheckPlayerToObstacleHit();
			//CheckEnemyToObstacleHit();
			CheckPlayerToPowerupCollision();
			CheckEnemyToPowerupCollision();
		}
		
		/*
		private function CheckPlayerToEnemyHit():void 
		{
			var _player : GameObject = null;
			var playerIdx:int = _playersAndSidekick.length;
			while (playerIdx--) {
				_player = _playersAndSidekick[playerIdx];
				//Debugger.Log("_player.canHit: " + _player.canHit);
				if (!(_player.canHit & Targets.ENEMY)) continue;
				//Debugger.Log("_player.canHit: " + _player.canHit);
				
				var playerAbsAttackBound	: Boundaries	= getAbsoluteAttackBoundaries(_player);
				if (!playerAbsAttackBound) continue;
				
				var theEnemies : Vector.<Enemy> = EnemiesManager.instance.theEnemies;
				var i : int	= theEnemies.length;
				while (i--) {
					var enemy:Enemy = theEnemies[i];
					//Debugger.Log("enemy.canBeHit: " + enemy.canBeHit + " - Attack seq: " + enemy.lastHitByAttackSeq + " vs " + _player.attackSeq + "  --  Enemy: " + enemy );
					if (!enemy.canBeHit) continue;
					if (enemy.lastHitBy == _player && enemy.lastHitByAttackSeq == _player.attackSeq) continue;
					
					var enemyAbsHitBound	: Boundaries	= getAbsoluteHitBoundaries(enemy);
					if (!enemyAbsHitBound) continue;
					
					if (boundariesIntersect(playerAbsAttackBound, enemyAbsHitBound)) {
						
						// Enemy Hit
						//Debugger.Log("=== ENEMY HIT ===");
						//Debugger.Log("ENEMY SCALE: " + StringUtils.ToCoord(enemy.innerMc.scaleX, enemy.innerMc.scaleY) + "  --  AttackBoundaries Height: [" + enemyHitBoundariesBackZ.toFixed(1) + " - " + enemyHitBoundariesFrontZ.toFixed(1) + "] (" + (enemyHitBoundariesFrontZ - enemyHitBoundariesBackZ).toFixed(2) + ")");
						GameManager.Instance.PlayerHitEnemy(_player, enemy);
					} else {
						//Debugger.Log("NO HIT");
					}
					
					// DEBUG
					if (Config.debug_attack_area) {
						DrawCollisionBoundaries(_player, playerAbsAttackBound, enemy, enemyAbsHitBound);
					}
				}
			}
			
		}
		
		private function CheckEnemyToPlayerHit():void 
		{
			var _player : GameObject = null;
			var playerIdx:int = _playersAndSidekick.length;
			while (playerIdx--) {
				_player = _playersAndSidekick[playerIdx];
				
				if (!_player.canBeHit) continue;
				
				var playerAbsHitBound	: Boundaries	= getAbsoluteHitBoundaries(_player);
				if (!playerAbsHitBound) continue;
					
				var theEnemies : Vector.<Enemy> = EnemiesManager.instance.theEnemies;
				var i : int	= theEnemies.length;
				while (i--) {
					var enemy:Enemy = theEnemies[i];
					//Debugger.Log("Checking hit... (" + _player.lastHitByAttackSeq + " vs " + enemy.attackSeq + ") - Enemy: " + enemy);
					if (!(enemy.canHit && Targets.CHARACTER)) continue;
					if (_player.lastHitBy == enemy && _player.lastHitByAttackSeq == enemy.attackSeq) continue;
				
					var enemyAbsAttackBound	: Boundaries	= getAbsoluteAttackBoundaries(enemy);
					if (!enemyAbsAttackBound) continue;
					
					if (boundariesIntersect(enemyAbsAttackBound, playerAbsHitBound)) {
						// Enemy Hit
						GameManager.Instance.EnemyHitPlayer(enemy, _player);
					}
						
					// DEBUG
					if (Config.debug_attack_area) {
						DrawCollisionBoundaries(enemy, enemyAbsAttackBound, _player, playerAbsHitBound);
					}
				}
			}
			
		}
		
		private function CheckPlayerToPlayerHit():void 
		{
			//Debugger.Log("CheckPlayerToPlayerHit");
			if (_playersAndSidekick.length < 2) {
				Debugger.Log("WARNING! Trying to check Player-Player collision in 1-Player mode. Skipping...");
				return;
			}
			var _player : GameObject = null;
			var playerIdx:int = _playersAndSidekick.length;
			while (playerIdx--) {
				_player = _playersAndSidekick[playerIdx];
				
				if (!(_player.canHit & Targets.CHARACTER)) continue;
				//Debugger.Log("_player.canHit: " + _player.canHit);
				
				var playerAbsAttackBound	: Boundaries	= getAbsoluteAttackBoundaries(_player);
				if (!playerAbsAttackBound) continue;
				
				var theOtherPlayerIdx : int = (playerIdx == 0)?1:0;
				var theOtherPlayer:GameObject = _playersAndSidekick[theOtherPlayerIdx];
				//Debugger.Log("theOtherPlayer.canBeHit: " + theOtherPlayer.canBeHit + " - Attack seq: " + theOtherPlayer.lastHitByAttackSeq + " vs " + _player.attackSeq );
				if (!theOtherPlayer.canBeHit) continue;
				if (theOtherPlayer.lastHitBy == _player && theOtherPlayer.lastHitByAttackSeq == _player.attackSeq) continue;
				
				//Debugger.Log("A VER!");
				var theOtherPlayerAbsHitBound	: Boundaries	= getAbsoluteHitBoundaries(theOtherPlayer);
				if (!theOtherPlayerAbsHitBound) continue;
					
					if (boundariesIntersect(playerAbsAttackBound, theOtherPlayerAbsHitBound)) {
					//Debugger.Log("=== PLAYER - PLAYER HIT ===");
					GameManager.Instance.PlayerHitPlayer(_player, theOtherPlayer);
				} else {
					//Debugger.Log("NO HIT");
				}
				
				// DEBUG
				if (Config.debug_attack_area) {
					DrawCollisionBoundaries(_player, playerAbsAttackBound, theOtherPlayer, theOtherPlayerAbsHitBound);
				}
			}
			
		}
		
		private function CheckPlayerToObstacleHit():void 
		{
			var _player : GameObject = null;
			var playerIdx:int = _playersAndSidekick.length;
			while (playerIdx--) {
				_player = _playersAndSidekick[playerIdx];
				
				if (!_player.canHit) continue;
				
				//Debugger.Log("_player.canHit: " + _player.canHit);
				
				var playerAbsAttackBound	: Boundaries	= getAbsoluteAttackBoundaries(_player);
				if (!playerAbsAttackBound) continue;
				
				var theObstacles : Vector.<Obstacle> = ObstaclesManager.instance.theObstacles;
				var i : int	= theObstacles.length;
				while (i--) {
					var obstacle:Obstacle = theObstacles[i];
					//Debugger.Log("obstacle.canBeHit: " + obstacle.canBeHit + " - Attack seq: " + obstacle.lastHitByAttackSeq + " vs " + _player.attackSeq );
					if (!obstacle.canBeHit) continue;
					if (!(_player.canHit & obstacle.targetId)) continue;
					if (obstacle.lastHitBy == _player && obstacle.lastHitByAttackSeq == _player.attackSeq) continue;
					
					var obstacleAbsHitBound	: Boundaries	= getAbsoluteHitBoundaries(obstacle);
					if (!obstacleAbsHitBound) continue;
					
					if (boundariesIntersect(playerAbsAttackBound, obstacleAbsHitBound)) {
						// Obstacle Hit
						//Debugger.Log("=== OBSTACLE HIT ===");
						GameManager.Instance.PlayerHitObstacle(_player, obstacle);
					} else {
						//Debugger.Log("NO HIT");
					}
					
					// DEBUG
					if (Config.debug_attack_area) {
						DrawCollisionBoundaries(_player, playerAbsAttackBound, obstacle, obstacleAbsHitBound);
					}
				}
			}
			
		}
		
		private function CheckEnemyToObstacleHit():void 
		{
			var _enemy : Enemy = null;
			var theEnemies	: Vector.<Enemy>	= EnemiesManager.instance.theEnemies;
			var enemyIdx : int	= theEnemies.length;
			while (enemyIdx--) {
				_enemy = theEnemies[enemyIdx];
				
				if (!_enemy.canHit) continue;
				
				//Debugger.Log("_enemy.canHit: " + _enemy.canHit);
				
				var enemyAbsAttackBound	: Boundaries	= getAbsoluteAttackBoundaries(_enemy);
				if (!enemyAbsAttackBound) continue;
				
				var theObstacles : Vector.<Obstacle> = ObstaclesManager.instance.theObstacles;
				var i : int	= theObstacles.length;
				while (i--) {
					var obstacle:Obstacle = theObstacles[i];
					//Debugger.Log("obstacle.canBeHit: " + obstacle.canBeHit + " - Attack seq: " + obstacle.lastHitByAttackSeq + " vs " + _enemy.attackSeq );
					if (!obstacle.canBeHit) continue;
					if (!(_enemy.canHit & obstacle.targetId)) continue;
					if (obstacle.lastHitBy == _enemy && obstacle.lastHitByAttackSeq == _enemy.attackSeq) continue;
					
					var obstacleAbsHitBound	: Boundaries	= getAbsoluteHitBoundaries(obstacle);
					if (!obstacleAbsHitBound) continue;
					
					if (boundariesIntersect(enemyAbsAttackBound, obstacleAbsHitBound)) {
						// Obstacle Hit
						//Debugger.Log("=== OBSTACLE HIT ===");
						GameManager.Instance.EnemyHitObstacle(_enemy, obstacle);
					} else {
						//Debugger.Log("NO HIT");
					}
					
					// DEBUG
					if (Config.debug_attack_area) {
						DrawCollisionBoundaries(_enemy, enemyAbsAttackBound, obstacle, obstacleAbsHitBound);
					}
				}
			}
			
		}
		*/
		
		
		private function CheckLevelBoundaries():void 
		{
			if (Camera.Instance.freeMoving) return;
			
			var currentLevel : Level	= GameManager.Instance.currentLevel;
			
			// Player
			var playerLimits	: AbsoluteRangeArea	= currentLevel.playerLimits;
			var _player : Player = GameManager.Instance.activePlayer;
			//_player.isAgainstSectorLimits = false;
			if (_player.x < playerLimits.leftX) {
				_player.PassedLevelLimitLeft(playerLimits.leftX);
			} else if (_player.x > playerLimits.rightX) {
				_player.PassedLevelLimitRight(playerLimits.rightX);
			}
			
			if (_player.z < playerLimits.backZ) _player.PassedLevelLimitBack(playerLimits.backZ);
			else if (_player.z > playerLimits.frontZ) _player.PassedLevelLimitFront(playerLimits.frontZ);
			
			
			// Enemies
			var enemyLimits	: AbsoluteRangeArea	= currentLevel.playerLimits; // TMP
			for each(var enemy:Enemy in EnemiesManager.instance.theEnemies)
			{
				var stopEnemyAtBoundaries:Boolean = true;
				//enemy.isAgainstSectorLimits = false;
				if (enemy.checkScreenLimits) {
					if (stopEnemyAtBoundaries) {
						if (enemy.x < enemyLimits.leftX) {
							enemy.PassedLevelLimitLeft(enemyLimits.leftX);
						}
						else if (enemy.x > enemyLimits.rightX) {
							enemy.PassedLevelLimitRight(enemyLimits.rightX);
						}
					} else {
						//Debugger.Log("BOUNDARIES CHECK: " + enemy.x.toFixed(2) + " en " + StringUtils.ToCoord(enemyLimits.leftX, enemyLimits.rightX));
						if (enemy.x < enemyLimits.leftX - 10 && enemy.currentBehavior) {
							//enemy.PassedLevelLimitLeft();
							Debugger.Log("enemy.PassedLevelLimitLeft - avisar a Behavior");
							//enemy.currentBehavior.GotoActionExternal(Behavior.ACTION_ID_REENTER); // TMP
						}
						else if (enemy.x > enemyLimits.rightX + 10 && enemy.currentBehavior) {
							//enemy.PassedLevelLimitRight();
							Debugger.Log("enemy.PassedLevelLimitRight - avisar a Behavior");
							//enemy.currentBehavior.GotoActionExternal(Behavior.ACTION_ID_REENTER); // TMP
						}
					}
					if (enemy.z < enemyLimits.backZ) enemy.PassedLevelLimitBack(enemyLimits.backZ);
					else if (enemy.z > enemyLimits.frontZ) enemy.PassedLevelLimitFront(enemyLimits.frontZ);
				} else if (enemy.checkEnterScreen) {
					
				}
					
			}
			
			/*
			// Shots
			if (!Camera.Instance.freeMoving) {
				var theShots : Vector.<Shot> = ShotsManager.instance.theShots;
				i	= theShots.length;
				while (i--) {
					var shot:Shot = theShots[i];
					if (shot.x < Camera.Instance.leftX - shot.View.width) shot.PassedLevelLimitLeft();
					else if (shot.x > Camera.Instance.rightX + shot.View.width) shot.PassedLevelLimitRight();
					
					if ((Config.floor_y + shot.z) < Camera.Instance.upY - shot.View.height) shot.PassedLevelLimitBack();
					else if ((Config.floor_y + shot.z) > Camera.Instance.downY + shot.View.height) shot.PassedLevelLimitFront();
				}
			}
			*/
		}
		
		
		private function CheckShotToPlayerHit():void 
		{
			var _player : Player = GameManager.Instance.activePlayer;
			if (!_player.canBeHit) return;
			
			var playerAbsHitBound	: Boundaries	= getAbsoluteHitBoundaries(_player);
			if (!playerAbsHitBound) return;
			
			for each (var shot:Shot in ShotsManager.instance.theShots)
			{
				//Debugger.Log("Checking hit... (" + _player.lastHitByAttackSeq + " vs " + shot.attackSeq + ") - Enemy: " + shot);
				if (!(shot.canHit & Targets.PLAYER)) continue;
				
				if (shot.shooter.teamId == _player.teamId) continue;
				
				if (_player.lastHitBy == shot && _player.lastHitByAttackSeq == shot.attackSeq) continue;
			
				var shotAbsAttackBound	: Boundaries	= getAbsoluteAttackBoundaries(shot);
				if (!shotAbsAttackBound) continue;
				
				if (boundariesIntersect(shotAbsAttackBound, playerAbsHitBound)) {
					// Shot Hit
					Debugger.Log("SHOT HITS PLAYER");
					Debugger.Log("_player.lastHitBy: " + _player.lastHitBy);
					Debugger.Log("_player.lastHitByAttackSeq: " + _player.lastHitByAttackSeq);
					Debugger.Log("shot.attackSeq: " + shot.attackSeq);
					GameManager.Instance.ShotHitPlayer(shot, _player);
				}
				
				// DEBUG
				if (Config.debug_attack_area) {
					DrawCollisionBoundaries(shot, shotAbsAttackBound, _player, playerAbsHitBound);
				}
			}
		}
		
		
		private function CheckShotToEnemyHit():void 
		{
			for each (var shot:Shot in ShotsManager.instance.theShots)
			{
				//Debugger.Log("Checking hit... (" + _player.lastHitByAttackSeq + " vs " + shot.attackSeq + ") - Enemy: " + shot);
				if (!(shot.canHit & Targets.ENEMY)) continue;
				
				var shotAbsAttackBound	: Boundaries	= getAbsoluteAttackBoundaries(shot);
				if (!shotAbsAttackBound) continue;
				
				for each(var enemy:Enemy in EnemiesManager.instance.theEnemies)
				{
					//Debugger.Log("enemy.canBeHit: " + enemy.canBeHit + " - Attack seq: " + enemy.lastHitByAttackSeq + " vs grenade Can Hit: " + shot.canHit);
					if (!enemy.canBeHit) continue;
					
					if (shot.shooter.teamId == enemy.teamId) continue;
				
					if (enemy.lastHitBy == shot && enemy.lastHitByAttackSeq == shot.attackSeq) continue;
					
					var enemyAbsHitBound	: Boundaries	= getAbsoluteHitBoundaries(enemy);
					if (!enemyAbsHitBound) continue;
					
					if (boundariesIntersect(shotAbsAttackBound, enemyAbsHitBound)) {
						
						Debugger.Log("shot.attackSeq: " + shot.attackSeq);
						// Enemy Hit
						//Debugger.Log("=== ENEMY HIT ===");
						//Debugger.Log("ENEMY SCALE: " + StringUtils.ToCoord(enemy.innerMc.scaleX, enemy.innerMc.scaleY) + "  --  AttackBoundaries Height: [" + enemyHitBoundariesBackZ.toFixed(1) + " - " + enemyHitBoundariesFrontZ.toFixed(1) + "] (" + (enemyHitBoundariesFrontZ - enemyHitBoundariesBackZ).toFixed(2) + ")");
						GameManager.Instance.ShotHitEnemy(shot, enemy);
					} else {
						//Debugger.Log("NO HIT");
					}
					
					// DEBUG
					if (Config.debug_attack_area) {
						DrawCollisionBoundaries(shot, shotAbsAttackBound, enemy, enemyAbsHitBound);
					}
				}
			}
		}
		
		
		private function CheckShotToObstacleHit():void 
		{
			var theShots : Vector.<Shot> = ShotsManager.instance.theShots;
			var i : int	= theShots.length;
			while (i--) {
				var shot:Shot = theShots[i];
				//Debugger.Log("Checking hit... (" + _player.lastHitByAttackSeq + " vs " + shot.attackSeq + ") - Enemy: " + shot);
				if (!shot.canHit) continue;
				
				var shotAbsAttackBound	: Boundaries	= getAbsoluteAttackBoundaries(shot);
				var shotPrevAbsAttackBound	: Boundaries	= getPrevAbsoluteAttackBoundaries(shot);
				if (!shotAbsAttackBound) continue;
				
				
				var theObstacles : Vector.<Obstacle> = ObstaclesManager.instance.theObstacles;
				var j : int	= theObstacles.length;
				while (j--) {
					var obstacle:Obstacle = theObstacles[j];
					if (!obstacle.canBeHit) continue;
					if (!(shot.canHit & obstacle.targetId)) continue;
					if (obstacle.lastHitBy == shot && obstacle.lastHitByAttackSeq == shot.attackSeq) continue;
					
					var obstacleAbsHitBound	: Boundaries	= getAbsoluteHitBoundaries(obstacle);
					if (!obstacleAbsHitBound) continue;
					
					if (boundariesIntersect(shotAbsAttackBound, obstacleAbsHitBound)) {
						
						
						
						
						Debugger.Log("SHOT HITS OBSTACLE");
						Debugger.Log("obstacle.lastHitBy: " + obstacle.lastHitBy);
						Debugger.Log("obstacle.lastHitByAttackSeq: " + obstacle.lastHitByAttackSeq);
						Debugger.Log("shot.attackSeq: " + shot.attackSeq);
						//Debugger.Log("shotAbsAttackBound: " + shotAbsAttackBound);
						//Debugger.Log("shotPrevAbsAttackBound: " + shotPrevAbsAttackBound);
						var collisionDirection	: String	= "";
						
						// Horizontal collision
						if (shotPrevAbsAttackBound.x1 <= obstacleAbsHitBound.x0 && shotAbsAttackBound.x1 > obstacleAbsHitBound.x0) {
							//Debugger.Log("COLISION POR IZQUIERDA");
							collisionDirection = LEFT;
						} else if (shotPrevAbsAttackBound.x0 >= obstacleAbsHitBound.x1 && shotAbsAttackBound.x0 < obstacleAbsHitBound.x1) {
							//Debugger.Log("COLISION POR DERECHA");
							collisionDirection = RIGHT;
						} else if (obstacleAbsHitBound.x0 <= shotAbsAttackBound.x1 && obstacleAbsHitBound.x1 >= shotAbsAttackBound.x0) {
							//Debugger.Log("WARNING! Colision horizontal desde adentro");
						}
						// Vertical collision
						if (shotPrevAbsAttackBound.y1 <= obstacleAbsHitBound.y0 && shotAbsAttackBound.y1 > obstacleAbsHitBound.y0) {
							//Debugger.Log("COLISION POR ARRIBA");
							collisionDirection = UP;
						} else if (shotPrevAbsAttackBound.y0 >= obstacleAbsHitBound.y1 && shotAbsAttackBound.y0 < obstacleAbsHitBound.y1) {
							//Debugger.Log("COLISION POR ABAJO");
							collisionDirection = DOWN;
						} else if (obstacleAbsHitBound.y0 <= shotAbsAttackBound.y1 && obstacleAbsHitBound.y1 >= shotAbsAttackBound.y0) {
							//Debugger.Log("WARNING! Colision vertical desde adentro");
						}
						// Depth collision
						if (shotPrevAbsAttackBound.z1 <= obstacleAbsHitBound.z0 && shotAbsAttackBound.z1 > obstacleAbsHitBound.z0) {
							//Debugger.Log("COLISION POR DETRAS (profundidad)");
							collisionDirection = BACK;
						} else if (shotPrevAbsAttackBound.z0 >= obstacleAbsHitBound.z1 && shotAbsAttackBound.z0 < obstacleAbsHitBound.z1) {
							//Debugger.Log("COLISION POR EL FRENTE (profundidad)");
							collisionDirection = FRONT;
						} else if (obstacleAbsHitBound.z0 <= shotAbsAttackBound.z1 && obstacleAbsHitBound.z1 >= shotAbsAttackBound.z0) {
							//Debugger.Log("WARNING! Colision en profundidad desde adentro");
						}
						
						// Enemy Hit
						//Debugger.Log("=== ENEMY HIT ===");
						//Debugger.Log("ENEMY SCALE: " + StringUtils.ToCoord(enemy.innerMc.scaleX, enemy.innerMc.scaleY) + "  --  AttackBoundaries Height: [" + enemyHitBoundariesBackZ.toFixed(1) + " - " + enemyHitBoundariesFrontZ.toFixed(1) + "] (" + (enemyHitBoundariesFrontZ - enemyHitBoundariesBackZ).toFixed(2) + ")");
						GameManager.Instance.ShotHitObstacle(shot, obstacle, collisionDirection);
					} else {
						//Debugger.Log("NO HIT");
					}
					
					// DEBUG
					if (Config.debug_attack_area) {
						DrawCollisionBoundaries(shot, shotAbsAttackBound, obstacle, obstacleAbsHitBound);
					}
				}
			}
		}
		
		private function CheckPlayerToObstacleCollision():void 
		{
			var _player : Player	= GameManager.Instance.activePlayer;
			
			var playerHitAbsBoundaries		: Boundaries	= getAbsoluteHitBoundaries(_player);
			var playerPrevHitAbsBoundaries	: Boundaries	= getPrevAbsoluteHitBoundaries(_player);
			
			_player.blockedDirections = BLOCK_NONE;
			for each(var obstacle:Obstacle in ObstaclesManager.instance.theObstacles)
			{
				if (!obstacle.canStopPlayer) continue;
				//if ((obstacle.canHit & Targets.CHARACTER) && (!_player.canBeHit || _player.lastHitBy == obstacle)) continue;
				
				// TMP
				//Debugger.Log("Checking hit... (" + _player.lastHitByAttackSeq + " vs " + shot.attackSeq + ") - Enemy: " + shot);
				//if (!shot.canHit) continue;
				//if (_player.lastHitByAttackSeq == shot.attackSeq) continue;
				
				//obstacle.RecalculateHitBoundaries(); // TMP
				var obstacleHitAbsBoundaries	: Boundaries	= getAbsoluteHitBoundaries(obstacle);
				if (!obstacleHitAbsBoundaries) continue;
				
				//Debugger.Log("obstacleHitAbsBoundaries: " + obstacleHitAbsBoundaries);
				//Debugger.Log("playerPrevHitAbsBoundaries: " + playerPrevHitAbsBoundaries);
				//Debugger.Log("playerHitAbsBoundaries: " + playerHitAbsBoundaries);
				
				if (boundariesIntersect(playerHitAbsBoundaries, obstacleHitAbsBoundaries)) {
					
					//Debugger.Log("OBSTACLE STOPS PLAYER");
					var distanceToSeparate	: Number = 0;
					// Horizontal collision
					if (playerPrevHitAbsBoundaries.x1 <= obstacleHitAbsBoundaries.x0 && playerHitAbsBoundaries.x1 > obstacleHitAbsBoundaries.x0) {
						//Debugger.Log("COLISION POR IZQUIERDA");
						_player.MoveX(obstacleHitAbsBoundaries.x0 - playerHitAbsBoundaries.x1 - distanceToSeparate);
						_player.againstObstacle(CollisionManager.LEFT, obstacle);
						_player.blockedDirections += BLOCK_RIGHT;
					} else if (playerPrevHitAbsBoundaries.x0 >= obstacleHitAbsBoundaries.x1 && playerHitAbsBoundaries.x0 < obstacleHitAbsBoundaries.x1) {
						//Debugger.Log("COLISION POR DERECHA");
						_player.MoveX(obstacleHitAbsBoundaries.x1 - playerHitAbsBoundaries.x0 + distanceToSeparate);
						_player.againstObstacle(CollisionManager.RIGHT, obstacle);
						_player.blockedDirections += BLOCK_LEFT;
					} else if (obstacleHitAbsBoundaries.x0 <= playerHitAbsBoundaries.x1 && obstacleHitAbsBoundaries.x1 >= playerHitAbsBoundaries.x0) {
						//Debugger.Log("WARNING! Colision horizontal desde adentro");
					}
					// Vertical collision
					if (playerPrevHitAbsBoundaries.y1 <= obstacleHitAbsBoundaries.y0 && playerHitAbsBoundaries.y1 > obstacleHitAbsBoundaries.y0) {
						//Debugger.Log("COLISION POR ARRIBA");
						//_player.MoveZ(obstacleHitAbsBoundaries.y0 - playerHitAbsBoundaries.y1 - distanceToSeparate);
					} else if (playerPrevHitAbsBoundaries.y0 >= obstacleHitAbsBoundaries.y1 && playerHitAbsBoundaries.y0 < obstacleHitAbsBoundaries.y1) {
						//Debugger.Log("COLISION POR ABAJO");
						//_player.MoveZ(obstacleHitAbsBoundaries.y1 - playerHitAbsBoundaries.y0 + distanceToSeparate);
					} else if (obstacleHitAbsBoundaries.y0 <= playerHitAbsBoundaries.y1 && obstacleHitAbsBoundaries.y1 >= playerHitAbsBoundaries.y0) {
						//Debugger.Log("WARNING! Colision vertical desde adentro");
					}
					// Depth collision
					if (playerPrevHitAbsBoundaries.z1 <= obstacleHitAbsBoundaries.z0 && playerHitAbsBoundaries.z1 > obstacleHitAbsBoundaries.z0) {
						//Debugger.Log("COLISION POR DETRAS (profundidad)");
						_player.MoveZ(obstacleHitAbsBoundaries.z0 - playerHitAbsBoundaries.z1 - distanceToSeparate);
						_player.againstObstacle(CollisionManager.BACK, obstacle);
						_player.blockedDirections += BLOCK_FRONT;
					} else if (playerPrevHitAbsBoundaries.z0 >= obstacleHitAbsBoundaries.z1 && playerHitAbsBoundaries.z0 < obstacleHitAbsBoundaries.z1) {
						//Debugger.Log("COLISION POR EL FRENTE (profundidad)");
						_player.MoveZ(obstacleHitAbsBoundaries.z1 - playerHitAbsBoundaries.z0 + distanceToSeparate);	
						_player.againstObstacle(CollisionManager.FRONT, obstacle);
						_player.blockedDirections += BLOCK_BACK;
					} else if (obstacleHitAbsBoundaries.z0 <= playerHitAbsBoundaries.z1 && obstacleHitAbsBoundaries.z1 >= playerHitAbsBoundaries.z0) {
						//Debugger.Log("WARNING! Colision en profundidad desde adentro");
					}
					
					//_player.StoppedByObstacle(obstacle);
					
				}
				
				//DEBUG
				if (Config.debug_attack_area) {
					DrawCollisionBoundaries(obstacle, obstacleHitAbsBoundaries, _player, playerHitAbsBoundaries);
				}
			}
			
			
		}

		
		private function CheckEnemyToObstacleCollision():void 
		{
			for each(var enemy:Enemy in EnemiesManager.instance.theEnemies)
			{
				var enemyHitAbsBoundaries		: Boundaries	= getAbsoluteHitBoundaries(enemy);
				var enemyPrevHitAbsBoundaries	: Boundaries	= getPrevAbsoluteHitBoundaries(enemy);
				
				enemy.prevBlockedDirections	= enemy.blockedDirections;
				enemy.blockedDirections = BLOCK_NONE;
				for each(var obstacle:Obstacle in ObstaclesManager.instance.theObstacles)
				{
					if (!obstacle.canStopPlayer) continue;
					if ((obstacle.canHit & Targets.ENEMY) && (!enemy.canBeHit || enemy.lastHitBy == obstacle)) continue;
					
					// TMP
					//Debugger.Log("Checking hit... (" + enemy.lastHitByAttackSeq + " vs " + shot.attackSeq + ") - Enemy: " + shot);
					//if (!shot.canHit) continue;
					//if (enemy.lastHitByAttackSeq == shot.attackSeq) continue;
					
					//obstacle.RecalculateHitBoundaries(); // TMP
					var obstacleHitAbsBoundaries	: Boundaries	= getAbsoluteHitBoundaries(obstacle);
					if (!obstacleHitAbsBoundaries) continue;
					
					//Debugger.Log("obstacleHitAbsBoundaries: " + obstacleHitAbsBoundaries);
					//Debugger.Log("enemyHitAbsBoundaries: " + enemyHitAbsBoundaries);
					
					if (boundariesIntersect(enemyHitAbsBoundaries, obstacleHitAbsBoundaries)) {
						
						if (obstacle.canHit & Targets.ENEMY) {
							//Debugger.Log("OBSTACLE HITS ENEMY");
							//GameManager.Instance.ObstacleHitEnemy(obstacle, enemy);
						} else {
							//Debugger.Log("OBSTACLE STOPS ENEMY");
							var distanceToSeparate	: Number = 0;
							//Debugger.Log("enemyHitAbsBoundaries.x0: " + enemyHitAbsBoundaries.x0);
							//Debugger.Log("enemyPrevHitAbsBoundaries.x1: " + enemyPrevHitAbsBoundaries.x1);
							//Debugger.Log("obstacleHitAbsBoundaries.x0: " + obstacleHitAbsBoundaries.x0);
							//Debugger.Log("obstacleHitAbsBoundaries.x1: " + obstacleHitAbsBoundaries.x1);
							// Horizontal collision
							if (enemyPrevHitAbsBoundaries.x1 <= obstacleHitAbsBoundaries.x0 && enemyHitAbsBoundaries.x1 >= obstacleHitAbsBoundaries.x0) {
								//Debugger.Log("COLISION POR IZQUIERDA");
								enemy.MoveX(obstacleHitAbsBoundaries.x0 - enemyHitAbsBoundaries.x1 - distanceToSeparate);
								enemy.againstObstacle(CollisionManager.LEFT, obstacle);
								enemy.blockedDirections += BLOCK_RIGHT;
							} else if (enemyPrevHitAbsBoundaries.x0 >= obstacleHitAbsBoundaries.x1 && enemyHitAbsBoundaries.x0 <= obstacleHitAbsBoundaries.x1) {
								//Debugger.Log("COLISION POR DERECHA");
								enemy.MoveX(obstacleHitAbsBoundaries.x1 - enemyHitAbsBoundaries.x0 + distanceToSeparate);
								enemy.againstObstacle(CollisionManager.RIGHT, obstacle);
								enemy.blockedDirections += BLOCK_LEFT;
							} else if (obstacleHitAbsBoundaries.x0 <= enemyHitAbsBoundaries.x1 && obstacleHitAbsBoundaries.x1 >= enemyHitAbsBoundaries.x0) {
								//Debugger.Log("WARNING! Colision horizontal desde adentro");
							}
							// Vertical collision
							if (enemyPrevHitAbsBoundaries.y1 <= obstacleHitAbsBoundaries.y0 && enemyHitAbsBoundaries.y1 >= obstacleHitAbsBoundaries.y0) {
								//Debugger.Log("COLISION POR ARRIBA");
								//enemy.MoveZ(obstacleHitAbsBoundaries.y0 - enemyHitAbsBoundaries.y1 - 1);
							} else if (enemyPrevHitAbsBoundaries.y0 >= obstacleHitAbsBoundaries.y1 && enemyHitAbsBoundaries.y0 <= obstacleHitAbsBoundaries.y1) {
								//Debugger.Log("COLISION POR ABAJO");
								//enemy.MoveZ(obstacleHitAbsBoundaries.y1 - enemyHitAbsBoundaries.y0 + 1);
							} else if (obstacleHitAbsBoundaries.y0 <= enemyHitAbsBoundaries.y1 && obstacleHitAbsBoundaries.y1 >= enemyHitAbsBoundaries.y0) {
								//Debugger.Log("WARNING! Colision vertical desde adentro");
							}
							// Depth collision
							if (enemyPrevHitAbsBoundaries.z1 <= obstacleHitAbsBoundaries.z0 && enemyHitAbsBoundaries.z1 >= obstacleHitAbsBoundaries.z0) {
								//Debugger.Log("COLISION POR DETRAS (profundidad)");
								enemy.MoveZ(obstacleHitAbsBoundaries.z0 - enemyHitAbsBoundaries.z1 - distanceToSeparate);
								enemy.againstObstacle(CollisionManager.BACK, obstacle);
								enemy.blockedDirections += BLOCK_FRONT;
							} else if (enemyPrevHitAbsBoundaries.z0 >= obstacleHitAbsBoundaries.z1 && enemyHitAbsBoundaries.z0 <= obstacleHitAbsBoundaries.z1) {
								//Debugger.Log("COLISION POR EL FRENTE (profundidad)");
								enemy.MoveZ(obstacleHitAbsBoundaries.z1 - enemyHitAbsBoundaries.z0 + distanceToSeparate);
								enemy.againstObstacle(CollisionManager.FRONT, obstacle);
								enemy.blockedDirections += BLOCK_BACK;
							} else if (obstacleHitAbsBoundaries.z0 <= enemyHitAbsBoundaries.z1 && obstacleHitAbsBoundaries.z1 >= enemyHitAbsBoundaries.z0) {
								//Debugger.Log("WARNING! Colision en profundidad desde adentro");
							}
						}
					}
					
					//DEBUG
					if (Config.debug_attack_area) {
						DrawCollisionBoundaries(obstacle, obstacleHitAbsBoundaries, enemy, enemyHitAbsBoundaries);
					}
				}
			}
		}
		
		
		private function CheckPlayerToPowerupCollision():void 
		{
			var _player : Player	= GameManager.Instance.activePlayer;
			//_player.grabbedPowerup = false;
			var playerGrabbedPowerup : Boolean = false;
			
			if (!_player.canGrabPowerups) return;
			
			var playerHitAbsBoundaries		: Boundaries	= getAbsoluteHitBoundaries(_player);
			if (!playerHitAbsBoundaries) return;
			
			for each(var powerup:Powerup in PowerupsManager.instance.thePowerups)
			{
				//Debugger.Log("Checking collision for powerup: " + powerup + "  --  powerup.canBeGrabbed: " + powerup.canBeGrabbed + "  --  powerup.canBeGrabbedBy(" + _player.id + "): " + powerup.canBeGrabbedBy(_player));
				if (!powerup.canBeGrabbed || !powerup.canBeGrabbedBy(_player)) continue;
				
				var powerupHitAbsBoundaries	: Boundaries	= getAbsoluteHitBoundaries(powerup);
				if (!powerupHitAbsBoundaries) continue;
				
				//Debugger.Log("obstacleHitAbsBoundaries: " + obstacleHitAbsBoundaries);
				//Debugger.Log("playerHitAbsBoundaries: " + playerHitAbsBoundaries);
				
				if (boundariesIntersect(playerHitAbsBoundaries, powerupHitAbsBoundaries)) {
					GameManager.Instance.PlayerGrabbedPowerup(_player, powerup);
					playerGrabbedPowerup = true;
				} else {
					//Debugger.Log("OSO!");
				}
				
				//DEBUG
				if (Config.debug_attack_area) {
					DrawCollisionBoundaries(powerup, powerupHitAbsBoundaries, _player, playerHitAbsBoundaries);
				}
			}
			
			_player.grabbedPowerup = playerGrabbedPowerup;
			
		}
		
		
		private function CheckEnemyToPowerupCollision():void 
		{
			for each(var enemy:Enemy in EnemiesManager.instance.theEnemies)
			{
				var enemyHitAbsBoundaries		: Boundaries	= getAbsoluteHitBoundaries(enemy);
				var enemyPrevHitAbsBoundaries	: Boundaries	= getPrevAbsoluteHitBoundaries(enemy);
				//enemy.grabbedPowerup	= false;
				var enemyGrabbedPowerup : Boolean = false;
			
				if (!enemy.canGrabPowerups) continue;
			
				for each(var powerup:Powerup in PowerupsManager.instance.thePowerups)
				{
					//Debugger.Log("Checking collision for powerup: " + powerup + "  --  powerup.canBeGrabbed: " + powerup.canBeGrabbed + "  --  powerup.canBeGrabbedBy(" + _player.id + "): " + powerup.canBeGrabbedBy(_player));
					if (!powerup.canBeGrabbed || !powerup.canBeGrabbedBy(enemy)) continue;
					
					var powerupHitAbsBoundaries	: Boundaries	= getAbsoluteHitBoundaries(powerup);
					if (!powerupHitAbsBoundaries) continue;
					
					//Debugger.Log("obstacleHitAbsBoundaries: " + obstacleHitAbsBoundaries);
					//Debugger.Log("playerHitAbsBoundaries: " + playerHitAbsBoundaries);
					
					if (boundariesIntersect(enemyHitAbsBoundaries, powerupHitAbsBoundaries)) {
						GameManager.Instance.EnemyGrabbedPowerup(enemy, powerup);
						enemyGrabbedPowerup	= true;
					} else {
						//Debugger.Log("OSO!");
					}
					
					//DEBUG
					if (Config.debug_attack_area) {
						DrawCollisionBoundaries(powerup, powerupHitAbsBoundaries, enemy, enemyHitAbsBoundaries);
					}
				}
				enemy.grabbedPowerup	= enemyGrabbedPowerup;
			}
			
		}
		
		
		
		public function getAbsoluteHitBoundaries(gObj:GameObject):Boundaries 
		{
			var hitBoundaries : Boundaries			= gObj.hitBoundaries;
			if (!hitBoundaries) return null;
			
			var hitBoundariesLeftX		: Number	= gObj.IsFacingRight?gObj.x + hitBoundaries.x0:gObj.x - hitBoundaries.x1;
			var hitBoundariesRightX		: Number	= hitBoundariesLeftX + hitBoundaries.x1 - hitBoundaries.x0;
			var hitBoundariesTopY		: Number	= gObj.y + hitBoundaries.y0;
			var hitBoundariesBottomY	: Number	= hitBoundariesTopY + hitBoundaries.y1 - hitBoundaries.y0;
			var hitBoundariesBackZ		: Number	= gObj.IsFacingRight?gObj.z + hitBoundaries.z0:gObj.z - hitBoundaries.z1;
			var hitBoundariesFrontZ		: Number	= hitBoundariesBackZ + hitBoundaries.z1 - hitBoundaries.z0;
				
			return new Boundaries(hitBoundariesLeftX, hitBoundariesRightX, hitBoundariesTopY, hitBoundariesBottomY, hitBoundariesBackZ, hitBoundariesFrontZ);
		}
		
		
		private function getPrevAbsoluteHitBoundaries(gObj:GameObject):Boundaries 
		{
			var hitBoundaries : Boundaries		= gObj.hitBoundaries;
			if (!hitBoundaries) return null;
			
			var hitBoundariesLeftX		: Number	= gObj.wasFacingRight?gObj.prevX + hitBoundaries.x0:gObj.prevX - hitBoundaries.x1;
			var hitBoundariesRightX		: Number	= hitBoundariesLeftX + hitBoundaries.x1 - hitBoundaries.x0;
			var hitBoundariesTopY		: Number	= gObj.prevY + hitBoundaries.y0;
			var hitBoundariesBottomY	: Number	= hitBoundariesTopY + hitBoundaries.y1 - hitBoundaries.y0;
			var hitBoundariesBackZ		: Number	= gObj.wasFacingRight?gObj.prevZ + hitBoundaries.z0:gObj.prevZ - hitBoundaries.z1;
			var hitBoundariesFrontZ		: Number	= hitBoundariesBackZ + hitBoundaries.z1 - hitBoundaries.z0;
			
			return new Boundaries(hitBoundariesLeftX, hitBoundariesRightX, hitBoundariesTopY, hitBoundariesBottomY, hitBoundariesBackZ, hitBoundariesFrontZ);
		}
		
		
		private function getAbsoluteAttackBoundaries(gObj:GameObject):Boundaries 
		{
			var attackBoundaries : Boundaries			= gObj.attackBoundaries;
			if (!attackBoundaries) return null;
			
			var attackBoundariesLeftX		: Number	= gObj.IsFacingRight?gObj.x + attackBoundaries.x0:gObj.x - attackBoundaries.x1;
			var attackBoundariesRightX		: Number	= attackBoundariesLeftX + attackBoundaries.x1 - attackBoundaries.x0;
			var attackBoundariesTopY		: Number	= gObj.y + attackBoundaries.y0;
			var attackBoundariesBottomY		: Number	= attackBoundariesTopY + attackBoundaries.y1 - attackBoundaries.y0;
			var attackBoundariesBackZ		: Number	= gObj.IsFacingRight?gObj.z + attackBoundaries.z0:gObj.z - attackBoundaries.z1;
			var attackBoundariesFrontZ		: Number	= attackBoundariesBackZ + attackBoundaries.z1 - attackBoundaries.z0;
				
			return new Boundaries(attackBoundariesLeftX, attackBoundariesRightX, attackBoundariesTopY, attackBoundariesBottomY, attackBoundariesBackZ, attackBoundariesFrontZ);
		}
		
		
		private function getPrevAbsoluteAttackBoundaries(gObj:GameObject):Boundaries 
		{
			var attackBoundaries : Boundaries			= gObj.attackBoundaries;
			if (!attackBoundaries) return null;
			
			var attackBoundariesLeftX		: Number	= gObj.IsFacingRight?gObj.x + attackBoundaries.x0:gObj.x - attackBoundaries.x1;
			var attackBoundariesRightX		: Number	= attackBoundariesLeftX + attackBoundaries.x1 - attackBoundaries.x0;
			var attackBoundariesTopY		: Number	= gObj.y + attackBoundaries.y0;
			var attackBoundariesBottomY		: Number	= attackBoundariesTopY + attackBoundaries.y1 - attackBoundaries.y0;
			var attackBoundariesBackZ		: Number	= gObj.IsFacingRight?gObj.z + attackBoundaries.z0:gObj.z - attackBoundaries.z1;
			var attackBoundariesFrontZ		: Number	= attackBoundariesBackZ + attackBoundaries.z1 - attackBoundaries.z0;
			
			// DELTA POSITION USING PREVIOUS GOBJ POSITION
			var dX	: Number = gObj.prevX - gObj.x;
			attackBoundariesLeftX += dX;
			attackBoundariesRightX += dX;
			var dY	: Number = gObj.prevY - gObj.y;
			attackBoundariesTopY += dY;
			attackBoundariesBottomY += dY;
			var dZ	: Number = gObj.prevZ - gObj.z;
			attackBoundariesBackZ += dZ;
			attackBoundariesFrontZ += dZ;
			
			return new Boundaries(attackBoundariesLeftX, attackBoundariesRightX, attackBoundariesTopY, attackBoundariesBottomY, attackBoundariesBackZ, attackBoundariesFrontZ);
		}
		
		
		public function boundariesIntersect(boundaries1:Boundaries, boundaries2:Boundaries):Boolean 
		{
			//return (boundaries1.x0 <= boundaries2.x1 && boundaries1.x1 >= boundaries2.x0
						//&&	boundaries1.y0 <= boundaries2.y1 && boundaries1.y1 >= boundaries2.y0
						//&&	boundaries1.z0 <= boundaries2.z1 && boundaries1.z1 >= boundaries2.z0);
			return (boundaries1.x0 < boundaries2.x1 && boundaries1.x1 > boundaries2.x0
						&&	boundaries1.y0 < boundaries2.y1 && boundaries1.y1 > boundaries2.y0
						&&	boundaries1.z0 < boundaries2.z1 && boundaries1.z1 > boundaries2.z0);
		}
		
		public function isInArea(gObj:GameObject, areaBoundaries:Boundaries):Boolean 
		{
			return (	gObj.x >= areaBoundaries.x0 && gObj.x <= areaBoundaries.x1
					&& gObj.y >= areaBoundaries.y0 && gObj.y <= areaBoundaries.y1
					&& gObj.z >= areaBoundaries.z0 && gObj.z <= areaBoundaries.z1);
		}
		
		private function DrawCollisionBoundaries( attacker: GameObject, attackAbsBoundaries: Boundaries, victim: GameObject, victimAbsHitBoundaries: Boundaries) {
			
			AreaDebugger.instance.DrawCollisionBoundaries(attacker, attackAbsBoundaries, victim, victimAbsHitBoundaries);
		}
		
		static public function get instance():CollisionManager 
		{
			return _instance;
		}
		
		static public function set instance(value:CollisionManager):void 
		{
			_instance = value;
		}
		
	}

}