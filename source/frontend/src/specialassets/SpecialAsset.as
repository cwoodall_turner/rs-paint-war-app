package specialassets
{
import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.utils.Dictionary;
import flash.utils.getQualifiedSuperclassName;

/**
	 * ...
	 * @author alfred
	 */
	public class SpecialAsset extends MovieClip
	{
		private static var _views	: Dictionary	= null;
		
		public function SpecialAsset() 
		{
			super();
			
			var gObjView : GameObjectView	= getGameObjectView(this);
			var superClassName	: String 	= getQualifiedSuperclassName(this);
			//trace("gObjView: " + gObjView);
			//trace("getQualifiedSuperclassName(this): " + superClassName);
			
			var assetState : String	= getSpecialState(gObjView, superClassName);
			//trace("assetState: " + assetState);
			if (assetState != "") this.gotoAndStop(assetState);
		}
		
		private static function getGameObjectView(specialAsset:SpecialAsset):GameObjectView 
		{
			var currentAsset : DisplayObject = specialAsset;
			while (currentAsset != null)
			{
				if (currentAsset is GameObjectView) return (currentAsset as GameObjectView);
				else currentAsset = currentAsset.parent;
			}
			return null;
		}
		
		public static function setSpecialState(view:GameObjectView, key:String, state:String):void
		{
			if (_views == null) _views = new Dictionary(true);
			if (_views[view] == undefined) _views[view] = new Dictionary(true);
			
			_views[view][key] = state;
		}
		
		public static function getSpecialState(view:GameObjectView, key:String):String
		{
			if (_views == null || _views[view] == undefined || _views[view][key] == undefined) return "";
			return _views[view][key];
		}
		
	}

}