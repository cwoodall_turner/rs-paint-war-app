﻿package  
{
import behaviors.BehaviorsManager;
import behaviors.GamePathFinder;
import behaviors.common.*;
import behaviors.enemies.*;

import com.turner.caf.business.SimpleResponder;

import communication.cn.Omniture;
import communication.database.DBManager;

import enemies.EnemiesManager;
import enemies.Enemy;

import flash.display.MovieClip;
import flash.events.Event;
import flash.events.TimerEvent;
import flash.utils.Timer;
import flash.utils.getTimer;

import fx.FxManager;

import levels.Level;
import levels.LevelBattle;
import levels.LevelBullseye;
import levels.LevelDogTags;
import levels.LevelFlags;

import obstacles.Obstacle;
import obstacles.ObstaclesManager;

import players.Player;

import powerups.Powerup;
import powerups.PowerupsManager;

import shots.Grenade;
import shots.Shot;
import shots.ShotsManager;

import specialassets.SpecialAsset;

import ui.HUDManager;

import utils.MathUtils;

import view.Camera;
import view.GameObjectToScreenMapper;
import view.LayerFactory;

/**
	 * ...
	 * @author alfred
	 */
	public class GameManager {
		
		public static const ID_TEAM_1	: String	= "1";
		public static const ID_TEAM_2	: String	= "2";
		public static const ID_TEAM_3	: String	= "3";
		
		
		private	static var _instance	: GameManager	= new GameManager();
		
		
		/* TimeControl */
		private var _lastTime		: int	= 0;
		private var _currentTime	: int	= 0;
		private var _dtMs			: int	= 0;
		private var _timeLeftMs		: int	= 0;
		protected var _levelFinished	: Boolean	= false;
		
		/* Note: To use an object pool (LRU)  (Ref: [ObjPool01] */
		/* [ObjPool01] *
		public var shotsPool		: ObjectPoolLRU	= null;
		* /[ObjPool01] */
		
		/* Pause control */
		private var _paused			: Boolean	= true;
		
		/* Level */
		private var _currentLevel	: Level	= null;
		public function get currentLevel():Level {	return _currentLevel; }
		
		/* Collisions */
		private var _collisionManager:CollisionManager = null;
		
		/* Screen & Camera */
		private var _gObjToScreenMapper:GameObjectToScreenMapper = null;
		
		/* Player(s) */
		private var _player	: Player	= null;
		
		/* Behaviors */
		private var _behaviorsManager:BehaviorsManager = null;
		
		/* Enemies */
		private var _enemiesManager	:EnemiesManager= null;
		
		/* Shots */
		private var _shotsManager :ShotsManager	= null;
		
		/* Obstacles */
		private var _obstaclesManager	: ObstaclesManager	= null;
		
		/* Fx */
		private var _fxManager	: FxManager	= null;
		
		/* Powerups */
		private var _powerupsManager	: PowerupsManager	= null;
		
		/* Participants (every contestant in the level) */
		private var _participants : Vector.<GameObject>	= null;
		
		/* Badges */
		private var _enemiesToKillForBadge	: int	= 0;
		private var _playerHit				: Boolean	= false;
		
		/**
		* Constructor
		*/
		public function GameManager() {
		}
		
		/**
		* Singleton implementation.
		*/
		public static function get Instance() :GameManager {
			return _instance;
		}
		
		public function get timeLeftMs():int { return _timeLeftMs; }
		
		
		public function Initialize() : void {
			
			Input.initialize(GameContext.GlobalStage);
			GameInput.initialize();
			
			/* [ObjPool01] **
			shotsPool	= new ObjectPoolLRU();
			shotsPool.Fill(Config.num_shots_in_pool, samples.Shot);
			for (var i:int = 0; i < Config.num_shots_in_pool; i++ ) {
				var shot : samples.Shot	=  = shotsPool.GetNext() as samples.Shot;
				shot.Initialize();
				// ...
			}
			** /[ObjPool01] */
			
			// COLLISION MANAGER
			_collisionManager = new CollisionManager();
			
			// BEHAVIORS
			_behaviorsManager = new BehaviorsManager();
			// ENEMIES
			_enemiesManager	= new EnemiesManager();
			
			// SHOTS
			_shotsManager	= new ShotsManager();
			
			// OBSTACLES MANAGER
			_obstaclesManager	= new ObstaclesManager();
			
			// FX MANAGER
			_fxManager	= new FxManager();
			
			// POWERUPS MANAGER
			_powerupsManager	= new PowerupsManager();
			
			// PATHFINDER
			GamePathFinder.instance.initialize()
			
			// TMP
			new BehaviorAction_CheckRange();
			new BehaviorAction_FollowTPRelative();
			new BehaviorAction_FollowPath();
			new BehaviorAction_TakeCover();
			new BehaviorAction_Wait();
			new BehaviorAction_Bridge();
			new BehaviorAction_GoToScreenPos();
			new BehaviorAction_FireBullseye();
			new BehaviorAction_Fire();
			new BehaviorAction_Grenade();
			new BehaviorAction_CheckScreenPosXGreaterThan();
			new BehaviorAction_CheckTargetWithinRectRange();
			new BehaviorAction_CheckNumEnemiesGreaterThan();
			new BehaviorAction_CheckEnergyPctGreaterThan();
			new BehaviorAction_CheckTargetEnergyPctGreaterThan();
			
			// HUD
			HUDManager.Instance.Initialize(ExternalLoader.Instance.GetNewInstance("HUDAsset","hud.swf") as MovieClip);
			HUDManager.Instance.Hide(true);
			LayerFactory.Instance.GetLayer(LayerFactory.HUD).addChild(HUDManager.Instance.View);
			
			/* Note: You can initialize FX manager here (Ref: [Fx01]) */
			/* [Fx01] **
			Fx.instance.Initialize();
			** /[Fx01] */
			
			// SCREEN & CAMERA
			_gObjToScreenMapper = new GameObjectToScreenMapper(LayerFactory.Instance.GetLayer(LayerFactory.GAME), LayerFactory.Instance.GetLayer(LayerFactory.GAME_SHADOWS));
			Camera.Instance.Initialize();
			
			//_player = new Player(ExternalLoader.Instance.GetNewInstance("PlayerAsset", "player.swf") as GameObjectView, ExternalLoader.Instance.GetNewInstance("PlayerShadowAsset", "player.swf") as MovieClip);
			//_gObjToScreenMapper.AddGameObject(_player);
			//Camera.Instance.target = _player;
			
			// Num players
			ActionInputMapper.SetNumberOfPlayers(1);
		}
		
		

		
		
		/****************************************************************\
		 * 																*
		 * NOTE: A Level has the following stages:						*
		 * 1) Load 		-> Load external assets							*
		 * 2) Prepare 	-> Create instances, initialize objects, etc	*
		 * 3) Start 	-> Begin main loop								*
		 * 																*
		 * **************************************************************/
		
		public function PrepareLevel(levelNumber:int, levelDifficulty:String, checkpointIdx:int = -1):void {
			Debugger.Log("PrepareLevel (" + levelNumber.toString() + levelDifficulty + ")");
			
			// Update Config values
			Config.UpdateValues(levelNumber, levelDifficulty);
			
			// RESET CAMERA
			Camera.Instance.Reset();
			
			// Player
			var playerSwfPath : String = Config.characted_id + ".swf";
			_player = new Player(ExternalLoader.Instance.GetSingleton("PlayerAsset", playerSwfPath) as GameObjectView, ExternalLoader.Instance.GetNewInstance("PlayerShadowAsset", playerSwfPath) as MovieClip, Config.characted_id);
			_player.teamId = DBManager.instance.user.teamId;
			_gObjToScreenMapper.AddGameObject(_player);
			_player.SetState(GameObject.ST_READY);
			
			
			// CODE: PLAY WITH GUY
			if (CheatsManager.isActive(CheatsManager.PLAY_WITH_GUY))
			{
				var possibleGuyFacesStr	: String	= Config.getValue("code-guy-possible_guy_faces");
				var possibleGuyFaces	: Array = possibleGuyFacesStr.split(",");
				
				// If playing against bald enemies, player can't be bald
				if (CheatsManager.isActive(CheatsManager.BALD_ENEMIES))
				{
					for each(var possibleGuyFace : String in possibleGuyFaces)
					{
						if (possibleGuyFace == "1")
						{
							possibleGuyFaces.splice(possibleGuyFaces.indexOf(possibleGuyFace), 1);
						}
					}
				}
				Debugger.Log("possibleGuyFaces (" + possibleGuyFaces.length + "): " + possibleGuyFaces);
				
				var guyFaceIdx	: int = MathUtils.random(0, possibleGuyFaces.length - 1, true);
				SpecialAsset.setSpecialState(_player.View, "specialassets::Cara", possibleGuyFaces[guyFaceIdx]);
			}
			
			// LEVEL
			var possibleConfigsStr	: String 	= Config.getValue("level_" + levelNumber.toString() + "_possible_configs");
			var possibleConfigsArr	: Array		= possibleConfigsStr.split(",");
			var levelConfig			: String	= possibleConfigsArr[int(MathUtils.random(0, possibleConfigsArr.length))];
			_currentLevel	= null;
			switch(levelNumber)
			{
				case 1:
					_currentLevel = new LevelBattle(levelNumber, levelDifficulty, levelConfig);
					break;
				case 2:
					_currentLevel = new LevelBullseye(levelNumber, levelDifficulty, levelConfig);
					break;
				case 3:
					_currentLevel = new LevelFlags(levelNumber, levelDifficulty, levelConfig);
					break;
				case 4:
					_currentLevel = new LevelDogTags(levelNumber, levelDifficulty, levelConfig);
					break;
			}
			Debugger.Log("ANTES DE LEVEL UPDATE - activePlayer.x: " + activePlayer.x);
			_currentLevel.PrepareFromSwf();
			Debugger.Log("DESPUES DE LEVEL UPDATE - activePlayer.x: " + activePlayer.x);
			
			// COLLISIONS
			_collisionManager.Prepare();
			
			// TMP
			_powerupsManager.PrepareFromSwf();
			_obstaclesManager.PrepareFromSwf();
			_fxManager.prepareLevel(levelNumber);
			
			_enemiesManager.PrepareFromSwf();
			GamePathFinder.instance.prepareLevel(_currentLevel.id);
			
			// TMP
			var numEnemies:int = 0;
			var enemiesPerTeamStr	: String	= Config.getValue("level_" + _currentLevel.id + "_num_enemies_per_team");
			var enemiesPerTeamArr	: Array 	= enemiesPerTeamStr.split(",");
			var possibleTeamIds		: Array = ["1", "2", "3"];
			possibleTeamIds.splice(possibleTeamIds.indexOf(_player.teamId), 1);
			switch(_player.id)
			{
				case Player.ID_MORDECAI:
					addLevelEnemies(Enemy.ID_ENEMY_RIGBY, possibleTeamIds[0], enemiesPerTeamArr[0]);
					var guyOrMusclemanId : String = (Config.getValue("muscleman_available") == "true")?Enemy.ID_ENEMY_MUSCLEMAN:Enemy.ID_ENEMY_GUY;
					addLevelEnemies(guyOrMusclemanId, possibleTeamIds[1], enemiesPerTeamArr[1]);
					break;
				case Player.ID_RIGBY:
					addLevelEnemies(Enemy.ID_ENEMY_MORDECAI, possibleTeamIds[0], enemiesPerTeamArr[0]);
					var guyOrMusclemanId : String = (Config.getValue("muscleman_available") == "true")?Enemy.ID_ENEMY_MUSCLEMAN:Enemy.ID_ENEMY_GUY;
					addLevelEnemies(guyOrMusclemanId, possibleTeamIds[1], enemiesPerTeamArr[1]);
					break;
				case Player.ID_MUSCLEMAN:
					addLevelEnemies(Enemy.ID_ENEMY_MORDECAI, possibleTeamIds[0], enemiesPerTeamArr[0]);
					addLevelEnemies(Enemy.ID_ENEMY_RIGBY, possibleTeamIds[1], enemiesPerTeamArr[1]);
					break;
				case Player.ID_GUY:
					addLevelEnemies(Enemy.ID_ENEMY_MORDECAI, possibleTeamIds[0], enemiesPerTeamArr[0]);
					addLevelEnemies(Enemy.ID_ENEMY_RIGBY, possibleTeamIds[1], enemiesPerTeamArr[1]);
					break;
			}
			
			
			_timeLeftMs		= Config.getValue("time_ms_level_" + levelNumber.toString());
			
			
			_currentLevel.prepareLevelAfterPreparingManagers();
			
			// INITIAL GOBJs & CAMERA POSITIONS
			_gObjToScreenMapper.Update();
			Camera.Instance.PrepareLevel(levelNumber);
			
			// HUD
			HUDManager.Instance.prepareLevel(levelNumber);
			HUDManager.Instance.Show(true);
			
			updatePlayerWeaponAsset();
			
			// BADGES
			_enemiesToKillForBadge	= Config.getValue("badge_enemies_killed_num_enemies");
			_playerHit				= false;
			
			// OMNITURE
			Omniture.trackMilestone("play_level", levelNumber.toString());
			Omniture.trackMilestone("character_select", _player.id);
		}
		
		private function addLevelEnemies(idHeadEnemy:String, teamId:String, numEnemies:int):void 
		{
			Debugger.Log("addLevelEnemies(" + numEnemies + "): " + idHeadEnemy + " - Team " + teamId);
			if (numEnemies < 1) return;
			
			for (var i : int = 1; i <= numEnemies; i++)
			{
				var enemyId	: String	= (i == 1)?idHeadEnemy:Enemy.ID_ENEMY_GUY;
				
				// CODE: BALD_ENEMIES
				if (CheatsManager.isActive(CheatsManager.BALD_ENEMIES))
				{
					enemyId	= Enemy.ID_ENEMY_GUY;
				}
				
				_enemiesManager.addEnemy(enemyId, teamId);
			}
		}
		
		public function initializeCurrentLevel():void {
			
			_participants	= new Vector.<GameObject>();
			_participants.push(_player);
			for each(var enemy:Enemy in _enemiesManager.theEnemies)
			{
				_participants.push(enemy);
				_currentLevel.teleportToSpawnArea(enemy);
				enemy.SetState(Enemy.ST_SPAWN);
			}
			
			_player.SetState(Player.ST_SPAWN);
			
			GameObjectToScreenMapper.instance.Update();
			
			Sfx.PlayMusic(Config.music_id_silence, .6);
			HUDManager.Instance.showThreeTwoOneGo();
		}
		
		public function StartCurrentLevel():void {
			
			
			// Time Control
			_lastTime 		= 0;
			_levelFinished	= false;
			Debugger.Log("StartCurrentLevel (" + currentLevel.number + ")");
			
			/* NOTE: Show HUD **
			HUDManager.Instance.Show();
			**/
			
			/* NOTE: Play Music **/
			Sfx.PlayMusic(Config.current_level_music_id);
			/**/
			
			// Position Player
			//activePlayer.TeleportTo(300, 300);
			//activePlayer.SetState(Player.ST_HURDLES_IDLE); // TMP
			for each(var enemy:Enemy in _enemiesManager.theEnemies)
			{
				enemy.currentBehavior.GoToEntryAction("por-defecto"); //TMP
			}
			
			currentLevel.Start();
			
			TogglePause(false);
			
			//HUDManager.Instance.Show();
			
			/* NOTE: Update Shared Object - Level Reached (Ref:[SharedObj01]) */
			/* [SharedObj01] **
			var shared : SharedObject	= SharedObject.getLocal(Config.shared_object_string);
			shared.data.levelReached	= _currentLevelNumber;
			if (shared.data.maxLevelReached != undefined) {
				var prevMaxLevel	: int	= shared.data.maxLevelReached;
				var currentMaxLevel	= Math.max(prevMaxLevel, _currentLevelNumber);
				shared.data.maxLevelReached	= currentMaxLevel;
			} else {
				shared.data.maxLevelReached	= _currentLevelNumber;
			}
			shared.flush();
			shared.close();
			** /[SharedObj01] */
		}
		
		public function DestroyCurrentLevel():void {
			
			Debugger.Log("Destroy Current Level (" + currentLevel.number + ")");
			Reset();
		}
		
		
		public function TogglePause(pause:Boolean, captureBackground:Boolean = false, pauseLabel:String = null):void {
			Debugger.Log("TogglePause: " + pause);
			//Debugger.LogStack();
			if ((pause && _paused) || (!pause && !_paused))
				return;
			
			if (pause && !_paused)
			{
				// PAUSE
				Debugger.Log("=== PAUSE ===");
				_paused = true;
				GameContext.GlobalStage.removeEventListener(Event.ENTER_FRAME, MainLoop);
				
				if (captureBackground) {
					HUDManager.Instance.Hide(true);
					Camera.Instance.CaptureBackground();
					HUDManager.Instance.Show(true);
					HUDManager.Instance.OnPause(pauseLabel);
					Camera.Instance.BlurBitmap();
				}
			}
			else if (!pause && _paused)
			{
				// RESUME
				if (Camera.Instance.backgroundCaptured) {
					Debugger.Log("=== RESUME ANIMATION START ===");
					Camera.Instance.UnBlurBitmap(.6, DoResume);
				} else {
					DoResume();
				}
			}
		}
		
		private function DoResume():void {
			Debugger.Log("=== DO RESUME ===");
			_lastTime = 0;
			GameContext.GlobalStage.addEventListener(Event.ENTER_FRAME, MainLoop);
			HUDManager.Instance.OnResume();
			_paused	= false;
		}
		
		private function MainLoop(e:Event):void {
			
			if (_paused) {
				Debugger.Log("Blocking paused update!");
				return;
			}
			
			//Debugger.Log("MAIN LOOP START!");
			
			
			/* Time control - at loop start */
			if (_lastTime == 0) {
				_lastTime	= getTimer();
				return;
			}
			_currentTime	= getTimer();
			_dtMs	= _currentTime - _lastTime;
			
			if (!_levelFinished)
			{
				_timeLeftMs	-= _dtMs;
			}
			
			/* MAIN LOOP!!! Do stuff here :P  */
			
			/* Input */
			Input.update();
			GameInput.update();
			
			/* Debug */
			if(Config.debug){
				DebugUpdate(_dtMs);
			}
			if (Config.debug_attack_area)
			{
				AreaDebugger.instance.update(_dtMs);
			}
			
			/* Obstacles*/
			_obstaclesManager.Update(_dtMs);
			
			/* Powerups*/
			_powerupsManager.Update(_dtMs);
			
			/* Player */
			_player.Update(_dtMs);
			// Level finished in player update?
			if (_paused) return;
			
			/* Behaviors */
			_behaviorsManager.Update(_dtMs);
			
			/* Enemies */
			_enemiesManager.Update(_dtMs);
			
			/* SHOTS */
			_shotsManager.Update(_dtMs);
			
			/* CollisionManager */
			_collisionManager.Update(_dtMs);
			
			/* FX */
			_fxManager.update(_dtMs);
			
			/* Level Loop */
			_currentLevel.Update(_dtMs);
			
			/* Screen & Camera */
			_gObjToScreenMapper.Update();
			Camera.Instance.Update(_dtMs);
			
			/* Update HUD */
			HUDManager.Instance.Update(_dtMs);
			
			/* Update FX */
			/* [Fx01] **
			Fx.instance.Update(_dtMs);
			** /[Fx01] */
			
			
			/* Time control - at loop end */
			_lastTime	= _currentTime;
			
			
			/* Control time left */
			if (!_levelFinished && _timeLeftMs <= 0)
			{
				_timeLeftMs = 0;
				Debugger.Log("TIME'S UP!");
				onLevelFinishWaiting();
			}
		}
		
		
		
		
		public function onLevelFinishWaiting():void {
			_levelFinished	= true;
			var timeToWaitMs : Number	= Config.getValue("level_finish_time_to_capture_ms");
			var tmpTimer	: Timer	= new Timer(timeToWaitMs, 1);
			tmpTimer.addEventListener(TimerEvent.TIMER, actuallyFinishLevel);
			tmpTimer.start();
			
			Sfx.PlayMusic(Config.music_id_silence, .6);
			
			HUDManager.Instance.onLevelFinishWaiting();
			
			// CAPTAINS
			Captain.updateAfterLevelFinish();
			
			// BADGES
			if (!_playerHit)
			{
				MainApp.instance.EarnBadge("unpaintable");
			}
			
			// OMNITURE
			Omniture.trackMilestone("level_score", _currentLevel.number.toString() + "-" + _currentLevel.getScore().toString());
		}
		
		private function actuallyFinishLevel(e:TimerEvent):void 
		{
			e.currentTarget.removeEventListener(TimerEvent.TIMER, actuallyFinishLevel);
			onLevelFinish();
		}
		
		public function onLevelFinish():void {
			
			//Camera.Instance.CaptureBackground();
			//Camera.Instance.BlurBitmap();
			TogglePause(true);
			
			HUDManager.Instance.onLevelFinish();
			MainApp.instance.OnGameEvent("LevelFinish");
		}
		
		private function DebugUpdate(dtMs:int):void 
		{	
			if (Input.keyDown(Input.NUMBER_1)) {
				setPlayerTeamTmp(1);
			}
			
			if (Input.keyDown(Input.NUMBER_2)) {
				setPlayerTeamTmp(2);
			}
			
			if (Input.keyDown(Input.NUMBER_3)) {
				setPlayerTeamTmp(3);
			}
			
			if (Input.keyDown(Input.NUMBER_4)) {
				setPlayerWeaponTmp("none");
			}
			
			if (Input.keyDown(Input.NUMBER_5)) {
				setPlayerWeaponTmp("distance");
			}
			
			if (Input.keyDown(Input.NUMBER_6)) {
				setPlayerWeaponTmp("power");
			}
			
			if (Input.keyDown(Input.NUMBER_7)) {
				setPlayerWeaponTmp("distancepower");
			}
			/*
			if (Config.use_pathfinding_to_avoid_obstacles)
			{
				var width : int = GamePathFinder.instance.map.GetWidth();
				var height : int = GamePathFinder.instance.map.GetHeight();
				for (var tileX:int = 0; tileX < width; tileX++)
				{
					for (var tileY:int = 0; tileY < height; tileY++)
					{
						var tile : Tile = GamePathFinder.instance.map.GetTile(tileY, tileX);
						if (!tile.walkable)
						{
							//Debugger.Log("TILE NOT WALKABLE: " + tile);
							AreaDebugger.instance.DrawBoundaries(tile.posX, tile.posX + tile.width, tile.posY - Config.floor_y, tile.posY - Config.floor_y + tile.height);
						}
					}
						
						//
					//var leftX	: Number	= _horizontalMargin + column * _tileWidth;
					//var backZ	: Number	= _depthMargin + row * _tileDepth - Config.floor_y;
					//var tileArea	: AbsoluteRangeArea	= new AbsoluteRangeArea(leftX, leftX + _tileWidth, Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY, backZ, backZ + _tileDepth);
					//var areaBoundaries	: Boundaries	= new Boundaries(leftX, leftX + _tileWidth, Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY, backZ, backZ + _tileDepth);
						//AreaDebugger.instance.DrawBoundaries(tile.posX, tile.posX+tile.width
					//}
				}
			}
			*/
		}
		
		public function updatePlayerWeaponAsset():void
		{
			if (_player == null) return;
			
			var numDIstanceShots	: int	= DBManager.instance.user.getVar("shots_distance");
			if (isNaN(numDIstanceShots)) numDIstanceShots = 0;
			
			var numPowerShots	: int	= DBManager.instance.user.getVar("shots_power");
			if (isNaN(numPowerShots)) numPowerShots = 0;
			
			if (numPowerShots > 0 && numDIstanceShots > 0) setWeaponAsset(_player, "distancepower");
			else if (numPowerShots > 0) setWeaponAsset(_player, "power");
			else if (numDIstanceShots > 0) setWeaponAsset(_player, "distance");
			else setWeaponAsset(_player, "none");
		}
		
		private function setWeaponAsset(gObj:GameObject, weaponStr: String):void 
		{
			if (_player == null) return;
			SpecialAsset.setSpecialState(gObj.View, "specialassets::Arma", weaponStr);
		}
		
		private function setPlayerWeaponTmp(weaponStr:String):void 
		{
			Debugger.Log("setPlayerWeaponTmp: " + weaponStr);
			setWeaponAsset(_player, weaponStr);
		}
		
		
		
		private function setPlayerTeamTmp(team:int):void 
		{
			Debugger.Log("setPlayerTeamTmp: " + team);
			if (_player == null) return;
			SpecialAsset.setSpecialState(_player.View, "specialassets::Vincha", team.toString());
			_player.teamId = team.toString();
		}
		
		public function get activePlayer():Player {
			return _player;
		}
		
		public function get paused():Boolean 
		{
			return _paused;
		}
		
		public function get participants():Vector.<GameObject> 
		{
			return _participants;
		}
		
		public function Reset():void {
			Debugger.Log("GAME MANAGER RESET - BEGIN");
			_behaviorsManager.Reset();
			_gObjToScreenMapper.Reset();
			_enemiesManager.Reset();
			_shotsManager.Reset();
			Camera.Instance.UnBlurBitmap();
			//Camera.Instance.UnBWBitmap();
			Camera.Instance.Finalize();
			if (currentLevel) {
				currentLevel.Destroy();
				_currentLevel	= null;
			}
			HUDManager.Instance.Reset();
			if (_player)
			{
				_player.Reset();
				_gObjToScreenMapper.RemoveGameObject(_player);
				_player.Destroy();
			}
			Debugger.Log("GAME MANAGER RESET - END");
			TogglePause(true);
			_obstaclesManager.Reset();
			_fxManager.reset();
			_powerupsManager.Reset();
			_participants	= null;
		}
		
		
		public function updateDataAndDestroyLevel():void 
		{
			Debugger.Log("updateDataAndDestroyLevel");
			
			// Update User Data
			DBManager.instance.user.money += (_currentLevel.coinsGrabbed + _currentLevel.getCoinsPerObjective());
			DBManager.instance.user.setScoreLevel(_currentLevel.number, DBManager.instance.user.getScoreLevel(_currentLevel.number) + _currentLevel.getScore());
			
			// CODE: CONSTANT_EXTRA_LIFE_SLOTS
			if (CheatsManager.isActive(CheatsManager.CONSTANT_EXTRA_LIFE_SLOTS))
			{
				var maxLifeSlots	: int = Config.getValue("powerup_extra_life_slots_max_value");
				DBManager.instance.user.setVar("extra_life_slots", maxLifeSlots);
			}
			else
			{
				DBManager.instance.user.setVar("extra_life_slots", 0);
			}
			
			// TMP HACK! Check if user belongs to a valid team
			var teamId : String = DBManager.instance.user.teamId;
			if (teamId != "1" && teamId != "2" && teamId != "3")
			{
				// SET NEW TEAM!
				if (_player.teamId != "1" && _player.teamId != "2" && _player.teamId != "3")
				{
					var teamInt : int = MathUtils.random(1, 3, true);
					_player.teamId	= teamInt.toString();
				}
				DBManager.instance.user.setVar("team_id", _player.teamId);
			}
			
			var teamDeltaScore	: int	= _currentLevel.getScore();
			
			DBManager.instance.saveAfterLevel(DBManager.instance.user.cnId, DBManager.instance.user.teamId, _currentLevel.number, teamDeltaScore, new SimpleResponder(onSaveAfterLevelComplete, onSaveAfterLevelError));
			DestroyCurrentLevel();
		}
		
		
		private function onSaveAfterLevelComplete(data:Object ):void 
		{
			Debugger.Log("onSaveAfterLevelComplete: " + data);
		}
		
		private function onSaveAfterLevelError(data:Object ):void 
		{
			Debugger.Log("onSaveAfterLevelError: " + onSaveAfterLevelError);
		}
		
		// TMP
		public function forceLevelEnd(objectiveValue:int, coinsGrabbed:int):void
		{
			Debugger.Log("FORCE LEVEL END - SCORE: " + objectiveValue);
			//_currentLevel.coinsGrabbed		= coinsGrabbed;
			//_currentLevel.objectiveValue	= objectiveValue;
			_timeLeftMs = 0;
		}
		
		
		private function OnEnemyKilled(enemy:Enemy, theKiller:GameObject):void 
		{
			currentLevel.OnEnemyKilled(enemy, theKiller);
			
			if (_enemiesToKillForBadge > 0 && theKiller is Player)
			{
				_enemiesToKillForBadge--;
				if (_enemiesToKillForBadge == 0)
				{
					MainApp.instance.EarnBadge("enemies_killed");
				}
			}
		}
		public function OnEnemyKilledAnimationEnd(enemy:Enemy):void 
		{
			currentLevel.OnEnemyKilledAnimationEnd(enemy);
		}
		
		
		private function OnPlayerKilled($player:Player, theKiller:GameObject):void 
		{
			currentLevel.OnPlayerKilled($player, theKiller);
		}
		public function OnPlayerKilledAnimationEnd($player:Player):void 
		{
			currentLevel.OnPlayerKilledAnimationEnd($player);
		}
		
		
/* ========================================================*/
/* ============= GAME OBJECTS HITS/COLLISIONS =============*/
/* ========================================================*/
		
		public function ShotHitObstacle(shot:Shot, obstacle:Obstacle, collisionDirection:String):void 
		{
			shot.SetState(Shot.ST_HIT, [obstacle, collisionDirection]);
			obstacle.hitByShot(shot, collisionDirection);
		}
		
		public function ShotHitPlayer(shot:Shot, thePlayer:Player):void 
		{
			Debugger.Log("SHOT HIT PLAYER. Energy: " + thePlayer.energy + "/" + thePlayer.maxEnergy + "  --  Shot power: " + shot.attackPower + "  -- Player lastHitBySeq " + thePlayer.lastHitByAttackSeq + " vs " + shot.attackSeq);
			shot.HitPlayer(thePlayer);
			thePlayer.HitByShot(shot);
			Debugger.Log("DESPUÉS - Energy: " + thePlayer.energy + "/" + thePlayer.maxEnergy + "  --  Shot power: " + shot.attackPower);
			
			// Check Player Killed
			if (thePlayer.killed)
			{
				thePlayer.SetState(Player.ST_KILLED);
				OnPlayerKilled(thePlayer, shot.shooter);
				Sfx.PlaySound("PlayerKilled");
			}
			else
			{
				thePlayer.SetState(Player.ST_HIT);
				Sfx.PlaySound("PlayerHit");
			}
			
			// BADGES
			_playerHit	= true;
		}
		
		public function ShotHitEnemy(shot:Shot, enemy:Enemy):void 
		{
			shot.HitEnemy(enemy);
			enemy.HitByShot(shot);
			Debugger.Log("Enemy energy: " + enemy.energy + " / "  + enemy.maxEnergy);
			// Check Enemy Killed
			if (enemy.killed) {
				enemy.SetState(Enemy.ST_KILLED);
				OnEnemyKilled(enemy, shot.shooter);
				Sfx.PlaySound("EnemyKilled");
				
				// ACHIEVEMENT: Matar con granada
				if (shot is Grenade)
				{
					(shot as Grenade).onEnemyKilled();
					HUDManager.Instance.ShowAchievement("grenade_kill");
				}
			} else {
				enemy.SetState(Enemy.ST_HIT);
				Sfx.PlaySound("EnemyHit");
			}
			
			// TMP
			//HUDManager.Instance.PlayerHitEnemy(enemy);
		}
		
		public function PlayerGrabbedPowerup(thePlayer:Player, powerup:Powerup):void 
		{
			//Debugger.Log("PlayerGrabbedPowerup");
			if (!thePlayer.grabbedPowerup)
			{
				powerup.GrabbedBy(thePlayer);
				currentLevel.PlayerGrabbedPowerup(thePlayer, powerup);
				
				//Sfx.PlaySound(powerup.id + "Grabbed");
			}
		}
		
		public function EnemyGrabbedPowerup(enemy:Enemy, powerup:Powerup):void 
		{
			//Debugger.Log("EnemyGrabbedPowerup");
			if (!enemy.grabbedPowerup)
			{
				powerup.GrabbedBy(enemy);
				currentLevel.EnemyGrabbedPowerup(enemy, powerup);
			}
		}
		
		public function on321GoAnimationComplete():void 
		{
			Debugger.Log("on321GoAnimationComplete: " + on321GoAnimationComplete);
			StartCurrentLevel();
		}
		
	}

}