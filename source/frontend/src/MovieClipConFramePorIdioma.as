﻿package {
import flash.display.MovieClip;

import txt.TextLoader;

/**
	 * ...
	 * @author ahodes
	 */
	public class MovieClipConFramePorIdioma extends MovieClip {
		
		public function MovieClipConFramePorIdioma() {
			super();
			var languageLabel	: String	= TextLoader.Instance.Texts["frame_label_for_" + this.name];
			if (languageLabel == null) {
				languageLabel	= TextLoader.Instance.Texts["frame_label_default"];
			}
			if (languageLabel != null) {
				this.gotoAndStop(languageLabel);
			}
		}
	}
}