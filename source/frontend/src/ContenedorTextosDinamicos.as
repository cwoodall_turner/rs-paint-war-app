﻿package {
import flash.display.MovieClip;
import flash.text.TextField;

import txt.TextLoader;

import utils.TextFieldUtils;

/**
	 * ...
	 * @author ahodes
	 */
	public class ContenedorTextosDinamicos extends MovieClip {
		
		
		public function ContenedorTextosDinamicos() {
			super();
			var idx : int = this.numChildren;
			while (--idx >= 0) {
				var txtFld : TextField = this.getChildAt(idx) as TextField;
				if (txtFld is TextField && txtFld.name != "") {
					var newHtmlText	= TextLoader.Instance.Texts[txtFld.name];
					//Debugger.Log("Contenedor [" + this.name + "] - Nombre [" + txtFld.name + "] - HtmlText: [" + newHtmlText + "]");
					if (newHtmlText != undefined) {
						var testString : String = newHtmlText;
						if (testString.charAt(0) == "<") {
							txtFld.htmlText = newHtmlText;
						} else {
							TextFieldUtils.SetHtmlTextPreservingFormat(txtFld,newHtmlText);
						}
						txtFld.selectable = false;
					} else {
						//Debugger.Log("WARNING! - No hay texto dinamico para TextField [" + this.name + "." + txtFld.name + "]");
					}
					txtFld.selectable = false;
				}
			}
		}
	}
}