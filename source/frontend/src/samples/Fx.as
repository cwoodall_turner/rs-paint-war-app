package {
import com.touchmypixel.utils.AnimationCache;
import com.touchmypixel.utils.SimpleAnimation;

import flash.display.MovieClip;
import flash.display.Sprite;

import view.LayerFactory;

/**
	 * ...
	 * @author alfred
	 */
	public class Fx	{
		
		public static var instance	: Fx	= new Fx();
		
		private var _enemyShotInitFxPool	: ObjectPoolLRU	= new ObjectPoolLRU();
		private var _benShotInitFxPool		: ObjectPoolLRU	= new ObjectPoolLRU();
		private var _explosionsPool			: ObjectPoolLRU	= new ObjectPoolLRU();
		private var _explosionsBenPool		: ObjectPoolLRU	= new ObjectPoolLRU();
		private var _explosionsVilgaxPool	: ObjectPoolLRU	= new ObjectPoolLRU();
		
		/* Estela */
		private var _esteladores						: Vector.<Estelador>	= new Vector.<Estelador>();
		private var _esteladoresEnemigosMoveClipPool	: ObjectPoolLRU	= new ObjectPoolLRU();
		private var _esteladoresEnemigosGraphicsPool	: ObjectPoolLRU	= new ObjectPoolLRU();
		
		/* Fx Posesi�n */
		private var _possessionFx	: MovieClip	= null;
		
		/* Combo */
		private var _comboFxPool	: ObjectPoolLRU	= new ObjectPoolLRU();
		
		
		public function Fx() {
			
		}
		
		public function Initialize():void {
			var i : int;
			var initAsset	: Sprite	= null;
			for (i = 0; i < Config.enemy_shot_init_fx_in_pool; i++ ) {
				initAsset	= ExternalLoader.Instance.GetFxAsset("EnemyShotInitFxAsset");
				//initAsset.visible = false;
				LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_FRONT).addChild(initAsset);
				_enemyShotInitFxPool.Add(initAsset);
			}
			for (i = 0; i < Config.ben_shot_init_fx_in_pool; i++ ) {
				initAsset	= ExternalLoader.Instance.GetFxAsset("BenShotInitFxAsset");
				//initAsset.visible = false;
				LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_FRONT).addChild(initAsset);
				_benShotInitFxPool.Add(initAsset);
			}
			for (i = 0; i < Config.explosions_in_pool; i++ ) {
				//initAsset	= ExternalLoader.Instance.GetFxAsset("ExplosionAsset");
				initAsset	= AnimationCache.Instance.getAnimation("ExplosionAssetForCache") as SimpleAnimation;
				//initAsset.visible = false;
				LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_BACK).addChild(initAsset);
				_explosionsPool.Add(initAsset);
			}
			for (i = 0; i < Config.explosions_ben_in_pool; i++ ) {
				//initAsset	= ExternalLoader.Instance.GetFxAsset("ExplosionAsset");
				initAsset	= AnimationCache.Instance.getAnimation("ExplosionAssetForCache") as SimpleAnimation;
				//initAsset.visible = false;
				LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_FRONT).addChild(initAsset);
				_explosionsBenPool.Add(initAsset);
			}
			for (i = 0; i < Config.explosions_vilgax_in_pool; i++ ) {
				//initAsset	= ExternalLoader.Instance.GetFxAsset("ExplosionAsset");
				initAsset	= AnimationCache.Instance.getAnimation("ExplosionAssetForCache") as SimpleAnimation;
				//initAsset.visible = false;
				LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_FRONT).addChild(initAsset);
				_explosionsVilgaxPool.Add(initAsset);
			}
			var estelador : Estelador	= null;
			for (i = 0; i < Config.esteladores_enemigos_in_pool; i++ ) {
				if (Config.estela_enemies_usar_mcs) {
					estelador	= new EsteladorMovieClips("TrailEnemyAsset");
					_esteladoresEnemigosMoveClipPool.Add(estelador);
					_esteladores.push(estelador);
				}
				if (Config.estela_enemies_usar_graphics) {
					estelador	= new EsteladorGraphics();
					_esteladoresEnemigosGraphicsPool.Add(estelador);
					_esteladores.push(estelador);
				}
			}
			
			var esteladoresBen	: Vector.<Estelador>	= new Vector.<Estelador>();
			if (Config.estela_ben_usar_mcs) {
				estelador	= new EsteladorMovieClips("TrailBenAsset",true);
				estelador.SetNave(Player.instance);
				_esteladores.push(estelador);
				esteladoresBen.push(estelador);
			}
			if (Config.estela_ben_usar_graphics) {
				estelador	= new EsteladorGraphics(true);
				estelador.SetNave(Player.instance);
				_esteladores.push(estelador);
				esteladoresBen.push(estelador);
			}
			Player.instance.SetEsteladores(esteladoresBen);
			
			_possessionFx = ExternalLoader.Instance.GetFxAsset("PossessionAsset");
			_possessionFx.gotoAndStop(1);
			LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_FRONT).addChild(_possessionFx)
			
			for (i = 0; i < Config.combo_text_fx_in_pool; i++ ) {
				var comboAsset : MovieClip	= ExternalLoader.Instance.GetFxAsset("ComboAsset");
				comboAsset.visible = false;
				comboAsset.gotoAndStop(1);
				LayerFactory.Instance.GetLayer(LayerFactory.FX_GAME_FRONT).addChild(comboAsset);
				_comboFxPool.Add(comboAsset);
			}
		}
		
		public function BenShot(posX:Number, posY:Number, angleDeg:Number):void {
			var initAsset	: MovieClip	= _benShotInitFxPool.GetNext() as MovieClip;
			initAsset.x	= posX;
			initAsset.y = posY;
			initAsset.rotation	= angleDeg;
			//initAsset.visible = true;
			initAsset.play();
		}
		
		public function EnemyShot(posX:Number, posY:Number, angleDeg:Number):void {
			var initAsset	: MovieClip	= _enemyShotInitFxPool.GetNext() as MovieClip;
			initAsset.x	= posX;
			initAsset.y = posY;
			initAsset.rotation	= angleDeg;
			//initAsset.visible = true;
			initAsset.gotoAndPlay(2);
		}
		
		public function EnemyExplosion(posX:Number, posY:Number):void {
			var initAsset	: SimpleAnimation	= _explosionsPool.GetNext() as SimpleAnimation;
			initAsset.x	= posX;
			initAsset.y = posY;
			initAsset.gotoAndPlay(2);
		}
		
		public function BenExplosion(posX:Number, posY:Number):void {
			var initAsset	: SimpleAnimation	= _explosionsBenPool.GetNext() as SimpleAnimation;
			initAsset.x	= posX;
			initAsset.y = posY;
			initAsset.gotoAndPlay(2);
		}
		
		public function VilgaxExplosion(posX:Number, posY:Number):void {
			var initAsset	: SimpleAnimation	= _explosionsVilgaxPool.GetNext() as SimpleAnimation;
			initAsset.x	= posX;
			initAsset.y = posY;
			initAsset.gotoAndPlay(2);
		}
		
		public function Possession(posX:Number, posY:Number):void {
			_possessionFx.x	= posX;
			_possessionFx.y = posY;
			_possessionFx.gotoAndPlay(1);
		}
		
		public function AddNaveToEstelador(nave:GameObject):void {
			var estelador : Estelador	= null;
			if (Config.estela_enemies_usar_mcs) {
				estelador = _esteladoresEnemigosMoveClipPool.GetNext() as EsteladorMovieClips;
				estelador.SetNave(nave);
			}
			if (Config.estela_enemies_usar_graphics) {
				estelador = _esteladoresEnemigosGraphicsPool.GetNext() as EsteladorGraphics;
				estelador.SetNave(nave);
			}
		}
		
		public function Update(dtMs:int):void {
			for (var i:int = _esteladores.length - 1; i >= 0; i--) {
				if (_esteladores[i].active)
					_esteladores[i].Update(dtMs);
			}
		}
		
		public function OnLevelDestroyed():void {
			for (var i:int = _esteladores.length - 1; i >= 0; i--) {
				if (_esteladores[i].active)
					_esteladores[i].OnLevelDestroyed();
			}
		}
		
		public function ComboFeedbackFx(posX:Number, posY:Number, textStr:String ):void {
			var comboAsset	: ComboFeedback	= _comboFxPool.GetNext() as ComboFeedback;
			comboAsset.x		= posX;
			comboAsset.y 		= posY;
			comboAsset.htmlText	= textStr;
			comboAsset.visible = true;
			comboAsset.gotoAndPlay(1);
		}
		
		
		
		
		
	}

}