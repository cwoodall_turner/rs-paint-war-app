﻿package samples {

import flash.display.MovieClip;
import flash.events.Event;
import flash.geom.Point;

import utils.MovieClipUtils;

/**
	 * ...
	 * @author alfred
	 */
	public class Shot {
		
		private static const RAD_TO_DEG	: Number	= 180 / Math.PI;
		
		private var _view		: MovieClip	= null;
		private var _speedX		: Number	= .5;
		private var _speedY		: Number	= .5;
		private var _active		: Boolean	= false;
		private var _type		: int		= 0;
		private var _strength	: Number	= 1;
		private var _timeLeft	: int		= 3000;
		private var _hitArea	: MovieClip	= null;
		
		public function Shot(view:MovieClip) {
			
			_view	= view;
		}
		
		
		public function Initialize(type:int):void {
			
			_type	= type;
			
			_hitArea	= View.getChildByName("hitArea_mc") as MovieClip;
			if (_hitArea == null)	_hitArea	= _view;
			else _hitArea.alpha	= 0;
			
			_view.addEventListener("AnimationEnd", AnimationEnd);
		}
		
		
		public function Update(dtMs:int):void {
			
			if (!Active) return;
			
			_view.x	+= _speedX * dtMs;
			_view.y	+= _speedY * dtMs;
			
			if (--_timeLeft <= 0) {
				Active = false;
				Hide();
			}
		}
		
		public function Reset():void {
			
			View.gotoAndStop("Idle");
			Hide();
		}
		
		public function Finalize() : void {
			
			MovieClipUtils.Remove(View);
		}
		
		private function AnimationEnd(e:Event):void {
			Hide();
		}
		
		
		public function SetValues(position:Point, speed:Number, angleRad:Number, strength:Number, shotLifeTime:int):void {
			
			_view.x	= position.x;
			_view.y	= position.y;
			
			_speedX	= speed * Math.cos(angleRad);
			_speedY	= speed * Math.sin(angleRad);
			
			_view.rotation	= angleRad * RAD_TO_DEG;
			
			_strength	= strength;
			
			_timeLeft	= shotLifeTime;
			
			View.gotoAndStop("Idle");
		}
		
		public function Hit() : void {
			Active	= false;
			
			//Hide();
			View.gotoAndStop("Hit");
		}
		
		public function Show() : void {	_view.visible	= true; }
		public function Hide() : void {	_view.visible	= false; }
		
		
		public function get View() : MovieClip { return _view; }
		
		public function get Active() : Boolean	{	return	_active;	}
		public function set Active(bVal:Boolean) : void {	_active	= bVal;	}
		
		public function get X() : Number	{	return	_view.x;	}
		public function set X(nVal:Number) : void	{	_view.x	= nVal;	}
		
		public function get Y() : Number	{	return	_view.y;	}
		public function set Y(nVal:Number) : void	{	_view.y	= nVal;	}
		
		public function get Strength():Number { return _strength; }
		
		public function get HitArea():MovieClip	{	return _hitArea;	}
		
	}
	
}