﻿package {
import flash.display.Loader;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.net.URLRequest;
import flash.system.ApplicationDomain;
import flash.utils.Dictionary;

/**
	 * ...
	 * @author ahodes
	 */
	public class ExternalLoader extends EventDispatcher {
		
		public static const 	LOAD_COMPLETE	: String	= "LoadComplete";
		public static const 	LOAD_CANCELED	: String	= "LoadCanceled";
		
		private	static var _instance	: ExternalLoader	= new ExternalLoader();
		
		private var _loaders	: Dictionary	= new Dictionary(true);
		private var _singletons	: Dictionary	= new Dictionary(true);
		
		private var _currentLoadersExternalInfo	: Array		= null; // Paths & sizes
		private var _currentLoaderIdx			: int		= -1;
		private var _currentTotalExternalsize	: Number	= 0;
		private var _currentLoadersizePct		: Number	= 0;
		
		public function ExternalLoader() {
		}
		
		/**
		* Singleton implementation.
		*/
		public static function get Instance() :ExternalLoader {
			return _instance;
		}
		
		
		public function Load(swfPaths:Array, sizes:Array = null):void {
			
			// Reset ExternalInfos
			_currentLoadersExternalInfo	= new Array();
			
			// Create loaders
			var i : int	= 0;
			var swfPathsLength	: int	= swfPaths.length;
			for (i = 0; i < swfPathsLength; i++ ) {
				var swfPath	: String	= swfPaths[i] as String;
				// Create Loader
				_loaders[swfPath]	= new Loader();
				
				// Create ExternalInfo
				var size	: Number	= 100; // Default size
				if (sizes && sizes[i] && !isNaN(sizes[i])) {
					size	= sizes[i];
				} else {
					size = Config.getValue("filesize_" + swfPath);
					if (isNaN(size)) size = 100; 
				}
				_currentLoadersExternalInfo.push(new LoaderExternalInfo(swfPath, size));
			}
			
			// Calculate total external size
			_currentTotalExternalsize	= 0;
			i	= _currentLoadersExternalInfo.length;
			while (--i >= 0) {
				_currentTotalExternalsize += (_currentLoadersExternalInfo[i] as LoaderExternalInfo).size;
			}
			Debugger.Log("Total external size: " + _currentTotalExternalsize + "...");
			
			// Start loading
			_currentLoaderIdx	= 0;
			LoadCurrentLoader();
		}
		
		private function LoadCurrentLoader():void {
			
			if (_currentLoaderIdx < _currentLoadersExternalInfo.length) {
				
				var currentSwfsize	: Number	= (_currentLoadersExternalInfo[_currentLoaderIdx] as LoaderExternalInfo).size;
				_currentLoadersizePct	= (currentSwfsize * 100) / _currentTotalExternalsize;
				
				
				// Load current loader
				var currentSwfPath		: String	= (_currentLoadersExternalInfo[_currentLoaderIdx] as LoaderExternalInfo).path;
				var currentLoader	: Loader	= _loaders[currentSwfPath] as Loader;
				Debugger.Log("Loading " + currentSwfPath + "...");
				
				var myUrlReq:URLRequest = new URLRequest(GameContext.BasePath + currentSwfPath);
				currentLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, MoveToNextLoader, false, 0, true);
				currentLoader.load(myUrlReq);
			} else {
				dispatchEvent(new Event(LOAD_COMPLETE));
			}
		}
		
		private function MoveToNextLoader(e:Event):void {
			if (e && e.currentTarget)
				e.currentTarget.removeEventListener(Event.COMPLETE, MoveToNextLoader);
			
			_currentLoaderIdx++;
			LoadCurrentLoader();
		}
		
		public function GetLoadedPercent() : Number {
			
			if (_currentLoaderIdx < 0)
				return 0;
			if (loadComplete)
				return	100;
			
			var alreadyLoadedsize	: Number	= 0;
			for (var i:int = 0; i < _currentLoaderIdx; i++ ) {
				alreadyLoadedsize	+= (_currentLoadersExternalInfo[i] as LoaderExternalInfo).size;
			}
			var alreadyLoadedsizePct	: Number	= 100 * alreadyLoadedsize / _currentTotalExternalsize;
			
			var currentSwfPath		: String	= (_currentLoadersExternalInfo[_currentLoaderIdx] as LoaderExternalInfo).path;
			var currentLoader	: Loader	= _loaders[currentSwfPath] as Loader;
			var currentLoaderLoadedsizePct	: Number	= (currentLoader.contentLoaderInfo.bytesLoaded / currentLoader.contentLoaderInfo.bytesTotal) * _currentLoadersizePct;
			
			//Debugger.Log("alreadyLoadedsize: " + alreadyLoadedsize + " - alreadyLoadedsizePct: " + alreadyLoadedsizePct + " - currentLoaderLoadedsizePct: " + currentLoaderLoadedsizePct);
			
			if (!isNaN(currentLoaderLoadedsizePct)) {
				alreadyLoadedsizePct += currentLoaderLoadedsizePct;
			}
			
			return	alreadyLoadedsizePct;
		}
		
		public function GetLoader(swfPath:String): Loader {
			
			return _loaders[swfPath];
		}
		
		public function GetNewInstance(className:String, swfPath:String = null) : Object {
			
			Debugger.Log("Get New Instance: " + className + " in " + swfPath);
			var classObject	: Class	= GetApplicationDomain(swfPath).getDefinition(className) as Class;
			return new classObject();
		}
		
		public function GetSingleton(className:String, swfPath:String = null) : Object {
			
			if (!_singletons[swfPath]) {
				_singletons[swfPath] = new Dictionary(true);
			}
			
			// 2nd: Check if there's an instance using class name as a key
			var singletonsDictForSwfPath	: Dictionary	= _singletons[swfPath] as Dictionary;
			if (!singletonsDictForSwfPath[className])
				singletonsDictForSwfPath[className]	= GetNewInstance(className, swfPath);
			
			return	singletonsDictForSwfPath[className];
		}
		
		public function GetClass(className:String, swfPath:String = null) : Class {
			
			return GetApplicationDomain(swfPath).getDefinition(className) as Class;
		}
		
		/**
		 * @param	swfPath
		 * @return Application domain of passed swf, or of current swf if none is passed
		 */
		private function GetApplicationDomain(swfPath:String = null):ApplicationDomain {
			var applicationDomain	: ApplicationDomain	= null;
			if (swfPath) {
				var loader	: Loader	= _loaders[swfPath];
				if (loader && loader.contentLoaderInfo)
					applicationDomain	= loader.contentLoaderInfo.applicationDomain;
			}
			if (!applicationDomain is ApplicationDomain)
				applicationDomain = ApplicationDomain.currentDomain;
			
			return	applicationDomain;
		}
		
		public function IsLoaded(swfPath:String): Boolean {
			
			return (_loaders[swfPath] && !IsCurrentlyBeingLoaded(swfPath));
		}
		
		private function IsCurrentlyBeingLoaded(swfPath:String):Boolean {
			return (
						_currentLoadersExternalInfo
					&& _currentLoadersExternalInfo[_currentLoaderIdx] is LoaderExternalInfo
					&& ((_currentLoadersExternalInfo[_currentLoaderIdx] as LoaderExternalInfo).path == swfPath)
					);
		}
		
		public function get loadComplete() : Boolean {
			return (!_currentLoadersExternalInfo || (_currentLoaderIdx >= _currentLoadersExternalInfo.length));
		}
		
		public function CancelLoad(swfPath:String):void {
			Debugger.Log("CancelLoad " + swfPath + " ... ");
			var loader : Loader = _loaders[swfPath] as Loader;
			if (!loader) return;
			if (loader.contentLoaderInfo && loader.contentLoaderInfo.bytesTotal > 0) {
				loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, MoveToNextLoader);
				loader.close();
			}
			delete _loaders[swfPath];
		}
		
		public function CancelCurrentLoads():void 
		{
			dispatchEvent(new Event(LOAD_CANCELED));
			if (_currentLoadersExternalInfo) {
				for (var i : int = _currentLoadersExternalInfo.length -1 ; i >= _currentLoaderIdx; i--) {
					CancelLoad((_currentLoadersExternalInfo[i] as LoaderExternalInfo).path);
				}
			}
		}
	}

}

internal class LoaderExternalInfo 
{
	private var _path		: String	= "";
	private var _size		: Number	= 100;

	public function LoaderExternalInfo ( $path : String, $size: int ):void
	{
		_path	=	$path;
		_size	=	$size;
	}
	
	public function get path():String { return _path; }
	
	public function get size():Number { return _size; }
}