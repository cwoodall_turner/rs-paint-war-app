package buttons
{
	import flash.display.DisplayObject;
	import flash.display.FrameLabel;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class MapRollOutBtn extends MovieClip {
		
		public static const EVENT_CHILD_ROLLOVER 	: String = "child_rollover";
		public static const EVENT_CHILD_ROLLOUT 	: String = "child_rollout";
		public static const EVENT_CHILD_MOUSE_DOWN 	: String = "child_mousedown";
		public static const EVENT_CHILD_MOUSE_UP 	: String = "child_mouseup";
		
		protected var _hitArea		: MovieClip	= null;
		
		protected var _overSoundId	: String	= null;
		protected var _outSoundId	: String	= null;
		protected var _clickSoundId	: String	= null;
		
		protected var _overFrameNbr				: int = 2;
		protected var _overAnimationNumFrames	: int = 2;
		protected var _outFrameNbr				: int = 2;
		protected var _outAnimationNumFrames	: int = 2;
		
		public function MapRollOutBtn($hitArea:MovieClip = null) {
			
			//trace("VA");
			Construct();
			Enable($hitArea);
		}
		
		private function Construct():void {
			
			enabled			= false;
			mouseEnabled	= false;
			
			_hitArea	= getChildByName("hitArea_mc") as MovieClip;
			if (_hitArea == null)	_hitArea	= this;
			else _hitArea.alpha = 0;
			
			//_hitArea.mouseChildren 	= false;
			
			var overCompleteFrameNbr : int = 0;
			var outCompleteFrameNbr 	: int = 0;
			for each (var j:FrameLabel in this.currentLabels)
			{
				//trace("Label " + j.name + ": @" + j.frame);
				switch(j.name)
				{
					case "over":
						_overFrameNbr = j.frame;
						break;
					case "out":
						_outFrameNbr = j.frame;
						break;
					case "over_complete":
						overCompleteFrameNbr = j.frame;
						break;
					case "out_complete":
						outCompleteFrameNbr = j.frame;
						break;
				}
			}
			_overAnimationNumFrames = overCompleteFrameNbr - _overFrameNbr + 1;
			_outAnimationNumFrames	= outCompleteFrameNbr - _outFrameNbr + 1;
			
			//_overSoundId	= "BtnOver";
			//_outSoundId		= "BtnOut";
			//_clickSoundId	= "BtnClick";
		}
		
		public function Enable($hitArea:MovieClip = null):void	{
			
			if ($hitArea)
			{
				_hitArea = $hitArea;
			}
			
			_hitArea.buttonMode		= true;
			_hitArea.enabled		= true;
			_hitArea.mouseEnabled	= true;
			//_hitArea.mouseChildren	= false;
			
			//_hitArea.addEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
			//_hitArea.addEventListener(MouseEvent.MOUSE_UP, OnMouseUp);
			_hitArea.addEventListener(MouseEvent.ROLL_OVER, OnRollOver);
			_hitArea.addEventListener(MouseEvent.ROLL_OUT, OnRollOut);
			
			// TMP
			for (var i : int = 1; i <= 5; i++)
			{
				var hitAreaChild : MovieClip = _hitArea.getChildByName("hitArea" + i.toString() +"_mc") as MovieClip;
				if (hitAreaChild == null) continue;
				hitAreaChild.addEventListener(MouseEvent.ROLL_OVER, OnChildRollOver);
				hitAreaChild.addEventListener(MouseEvent.ROLL_OUT, OnChildRollOut);
				hitAreaChild.addEventListener(MouseEvent.MOUSE_DOWN, OnChildMouseDown);
				hitAreaChild.addEventListener(MouseEvent.MOUSE_UP, OnChildMouseUp);
			}
			
			gotoAndStop("up");
		}
		
		private function OnChildRollOver(e:MouseEvent):void 
		{
			(e.currentTarget as DisplayObject).dispatchEvent(new Event(EVENT_CHILD_ROLLOVER, true));
		}
		
		private function OnChildRollOut(e:MouseEvent):void 
		{
			(e.currentTarget as DisplayObject).dispatchEvent(new Event(EVENT_CHILD_ROLLOUT, true));
		}
		
		private function OnChildMouseDown(e:MouseEvent):void 
		{
			(e.currentTarget as DisplayObject).dispatchEvent(new Event(EVENT_CHILD_MOUSE_DOWN, true));
		}
		
		private function OnChildMouseUp(e:MouseEvent):void 
		{
			(e.currentTarget as DisplayObject).dispatchEvent(new Event(EVENT_CHILD_MOUSE_UP, true));
		}
		
		public function Disable():void	{
			
			_hitArea.buttonMode		= false;
			_hitArea.enabled		= false;
			_hitArea.mouseEnabled	= false;
			
			//_hitArea.removeEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
			//_hitArea.removeEventListener(MouseEvent.MOUSE_UP, OnMouseUp);
			_hitArea.removeEventListener(MouseEvent.ROLL_OVER, OnRollOver);
			_hitArea.removeEventListener(MouseEvent.ROLL_OUT, OnRollOut);
			// TMP
			for (var i : int = 1; i <= 5; i++)
			{
				var hitAreaChild : MovieClip = _hitArea.getChildByName("hitArea" + i.toString() +"_mc") as MovieClip;
				if (hitAreaChild == null) continue;
				hitAreaChild.removeEventListener(MouseEvent.ROLL_OVER, OnChildRollOver);
				hitAreaChild.removeEventListener(MouseEvent.ROLL_OUT, OnChildRollOut);
				hitAreaChild.removeEventListener(MouseEvent.MOUSE_DOWN, OnChildMouseDown);
				hitAreaChild.removeEventListener(MouseEvent.MOUSE_UP, OnChildMouseUp);
			}
		}
		
		
		/**
		 * Returns the Hit Area MovieClip
		 */
		public function get HitArea () : MovieClip {
			
			return	_hitArea;
		}
		
		
		/**
		 * 
		 * @param	event
		 */
		protected function OnMouseDown (event :MouseEvent) :void {
			
			gotoAndStop("down");
			
			if (_clickSoundId)	Sfx.PlaySound(_clickSoundId);
			
			event.stopPropagation();
		}
		
		
		/**
		 * 
		 * @param	event
		 */
		protected function OnRollOver (event :MouseEvent) :void {
			
			if (currentLabel == "over" || currentLabel == "over_complete") return;
			
			if (currentLabel == "up" || currentLabel == "down")
			{
				gotoAndPlay("over");
			} else if (currentLabel == "out" || currentLabel == "out_complete")
			{
				var outPctPlayed : int = Math.floor(100 * (currentFrame - _outFrameNbr) / _outAnimationNumFrames);
				var overPctToPlay : int = 100 - outPctPlayed;
				var overFrame : int = _overFrameNbr + overPctToPlay * _overAnimationNumFrames / 100;
				gotoAndPlay(overFrame);
			}
			
			if (_outSoundId)	Sfx.StopSound(_outSoundId);
			if (_overSoundId)	Sfx.PlaySound(_overSoundId);
		}
		
		
		/**
		 * 
		 * @param	event
		 */
		protected function OnMouseUp (event :MouseEvent) :void {
			
			//trace("OnMouseUp: " + OnMouseUp);
			if (currentLabel == "down") gotoAndStop("up");
			else if (currentLabel == "over" || currentLabel == "over_complete")
			{
				var overPctPlayed : int = Math.floor(100 * (currentFrame - _overFrameNbr) / _overAnimationNumFrames);
				var outPctToPlay : int = 100 - overPctPlayed;
				var outFrame : int = _outFrameNbr + outPctToPlay * _outAnimationNumFrames / 100;
				gotoAndPlay(outFrame);
			}
		}
		
		
		/**
		 * 
		 * @param	event
		 */
		protected function OnRollOut (event :MouseEvent) :void {
			
			//trace("ROLL OUT! - currentLabel: " + currentLabel);
			if (currentLabel == "over" || currentLabel == "over_complete")
			{
				var overPctPlayed : int = Math.floor(100 * (currentFrame - _overFrameNbr) / _overAnimationNumFrames);
				var outPctToPlay : int = 100 - overPctPlayed;
				var outFrame : int = _outFrameNbr + outPctToPlay * _outAnimationNumFrames / 100;
				gotoAndPlay(outFrame);
				
				if (_outSoundId)	Sfx.PlaySound(_outSoundId);
			}
			else if (currentLabel == "down")
			{
				gotoAndPlay("out");
				if (_outSoundId)	Sfx.PlaySound(_outSoundId);
			}
			
			//if (_overSoundId)	Sfx.StopSound(_overSoundId);
		}
		
		
		public function get OverSoundId():String { return _overSoundId; }
		public function set OverSoundId(value:String):void {	_overSoundId = value;	}
		
		public function get OutSoundId():String { return _outSoundId; }
		public function set OutSoundId(value:String):void {	_outSoundId = value;	}
		
		public function get clickSoundId():String {	return _clickSoundId; }
		public function set clickSoundId(value:String):void {	_clickSoundId = value; }
	}
}