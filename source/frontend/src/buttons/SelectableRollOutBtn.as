﻿package buttons
{
import flash.display.*;
import flash.events.*;
import flash.text.*;

import utils.TextFieldUtils;

public class SelectableRollOutBtn extends MovieClip {
		
		protected var _hitArea		: MovieClip	= null;
		protected var _upMc			: MovieClip	= null;
		protected var _overMc		: MovieClip	= null;
		protected var _downMc		: MovieClip	= null;
		protected var _outMc		: MovieClip	= null;
		protected var _disabledMc	: MovieClip	= null;
		protected var _selectedMc	: MovieClip	= null;
		protected var _totalFrames	: int	= 1;
		
		
		protected var _overSoundId	: String	= null;
		protected var _outSoundId	: String	= null;
		protected var _clickSoundId	: String	= null;
		
		protected var _selected	: Boolean	= false;
		
		protected var _disabled	: Boolean = false;
		
		public function SelectableRollOutBtn() {
			
			Construct();
			Enable();
		}
		
		private function Construct():void {
			
			_disabled		= false;
			enabled			= false;
			mouseEnabled	= false;
			
			_hitArea	= getChildByName("hitArea_mc") as MovieClip;
			if (_hitArea == null)	_hitArea	= this;
			else _hitArea.alpha = 0;
			_hitArea.mouseChildren 	= false;
			
			_upMc		= getChildByName("up_mc") as MovieClip;
			_overMc		= getChildByName("over_mc") as MovieClip;
			_downMc		= getChildByName("down_mc") as MovieClip;
			_outMc		= getChildByName("out_mc") as MovieClip;
			_disabledMc	= getChildByName("disabled_mc") as MovieClip;
			_selectedMc	= getChildByName("selected_mc") as MovieClip;
			
			if (GameContext.DebugMode)	CheckChildren();
			
			/* NOTE: To assign a rollover sound (Ref:[RollOverSnd01])
			 * This example shows how to change the sound after it's played */
			/** [RollOverSnd01] */
			//_overSoundId	= "BtnRollOver";
		}
		
		public function Enable():void	{
			
			_hitArea.buttonMode		= true;
			_hitArea.enabled		= true;
			_hitArea.mouseEnabled	= true;
			
			_hitArea.addEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
			_hitArea.addEventListener(MouseEvent.MOUSE_UP, OnMouseUp);
			_hitArea.addEventListener(MouseEvent.ROLL_OVER, OnRollOver);
			_hitArea.addEventListener(MouseEvent.ROLL_OUT, OnRollOut);
			_hitArea.addEventListener(MouseEvent.CLICK, OnClick);
			
			_upMc.visible	= false;
			_overMc.visible	= false;
			_overMc.gotoAndStop(1);
			_totalFrames	= _overMc.totalFrames;
			_downMc.visible	= false;
			if (_outMc != null) {
				_outMc.visible	= false;
				_outMc.gotoAndStop(_totalFrames);
			}
			
			_disabled	= false;
			selected = false;
			ShowState(_upMc);
		}
		
		public function Disable():void	{
			
			_disabled	= true;
			_hitArea.buttonMode		= false;
			_hitArea.enabled		= false;
			_hitArea.mouseEnabled	= false;
			
			_hitArea.removeEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
			_hitArea.removeEventListener(MouseEvent.MOUSE_UP, OnMouseUp);
			_hitArea.removeEventListener(MouseEvent.ROLL_OVER, OnRollOver);
			_hitArea.removeEventListener(MouseEvent.ROLL_OUT, OnRollOut);
			_hitArea.removeEventListener(MouseEvent.CLICK, OnClick);
			
			if (_disabledMc) {
				ShowState(_disabledMc);
			} else {
				Debugger.Log("Warning: se intenta deshabilitar un boton sin disabled_mc.");
				ShowState(_upMc);
			}
		}
		
		private function ShowState(stateMc:MovieClip, frame:int	= 1):void {
			
			if (stateMc == null || stateMc.visible) return;
			
			_outMc.removeEventListener(Event.ENTER_FRAME, OnOutEnterFrame);
			
			_upMc.visible		= false;
			_overMc.visible		= false;
			_downMc.visible		= false;
			_outMc.visible		= false;
			_selectedMc.visible	= false;
			if (_disabledMc) _disabledMc.visible = false;
			
			
			if (stateMc.currentFrame != frame)	stateMc.gotoAndPlay(frame);
			stateMc.visible	= true;
		}
		
		
		/*Check when the out animation finishes*/
		private function OnOutEnterFrame(e:Event):void {
			
			if (_outMc.currentFrame	== _outMc.totalFrames) ShowState(_upMc);
		}
		
		private function CheckChildren():void {
			
			if (_upMc == null) trace("WARNING: No up_mc in button");
			if (_overMc == null) trace("WARNING: No over_mc in button");
			if (_downMc == null) trace("WARNING: No down_mc in button");
			if (_outMc == null) trace("WARNING: No out_mc in button");
			
		}
		
		
		
		/**
		 * Returns the Hit Area MovieClip
		 */
		public function get HitArea () : MovieClip {
			
			return	_hitArea;
		}
		
		
		
		/**
		 * 
		 * @param	event
		 */
		protected function OnMouseDown (event :MouseEvent) :void {
			
			ShowState(_downMc);
			_overMc.gotoAndStop(1);
			if (_outMc != null) _outMc.gotoAndStop(_totalFrames);
			
			if (_clickSoundId)	Sfx.PlaySound(_clickSoundId);
		}
		
		
		
		/**
		 * 
		 * @param	event
		 */
		protected function OnRollOver (event :MouseEvent) :void {
			
			var startingFrame	: int	= _outMc.totalFrames - _outMc.currentFrame;
			ShowState(_overMc, startingFrame);
			
			/* NOTE: To assign a rollover sound (Ref:[RollOverSnd01])
			 * This example shows how to change the sound after it's played */
			/** [RollOverSnd01] **/
			if (_outSoundId)	Sfx.StopSound(_outSoundId);
			if (_overSoundId)	Sfx.PlaySound(_overSoundId);
		}
		
		
		
		/**
		 * 
		 * @param	event
		 */
		protected function OnMouseUp (event :MouseEvent) :void {
			
			var startingFrame	: int	= _overMc.totalFrames - _overMc.currentFrame;
			if (startingFrame == _totalFrames - 1) startingFrame ++;
			
			ShowState(_outMc, startingFrame);
		}
		
		
		
		/**
		 * 
		 * @param	event
		 */
		protected function OnRollOut (event :MouseEvent) :void {
			
			var startingFrame	: int	= _overMc.totalFrames - _overMc.currentFrame;
			if (startingFrame == _totalFrames - 1) startingFrame ++;
			
			if (_downMc.visible)
			{
				ShowState(_outMc, 1);
				_outMc.addEventListener(Event.ENTER_FRAME, OnOutEnterFrame);
			}
			else if (startingFrame < _totalFrames) {
				ShowState(_outMc, startingFrame);
				_outMc.addEventListener(Event.ENTER_FRAME, OnOutEnterFrame);
			}
			else {
				ShowState(_upMc);
			}
			if (_outSoundId)	Sfx.PlaySound(_outSoundId);
		}
		
		private function OnClick(e:MouseEvent):void 
		{
			selected = true;
		}
		
		
		
		public function set Text(text : String):void
        {
			
			// Common
			SetText((getChildByName("textContainer_mc") as MovieClip), text);
			
			// MovieClips
			SetText((_upMc.getChildByName("textContainer_mc") as MovieClip), text);
			SetText((_downMc.getChildByName("textContainer_mc") as MovieClip), text);
			SetText((_overMc.getChildByName("textContainer_mc") as MovieClip), text);
			if(_outMc != null)	SetText((_outMc.getChildByName("textContainer_mc") as MovieClip), text);
		}
		
		
		
		public function set HtmlText(htmlText : String):void {
			
			// Common
			SetText((getChildByName("textContainer_mc") as MovieClip), htmlText, true);
			
			// MovieClips
			SetText((_upMc.getChildByName("textContainer_mc") as MovieClip), htmlText, true);
			SetText((_downMc.getChildByName("textContainer_mc") as MovieClip), htmlText, true);
			SetText((_overMc.getChildByName("textContainer_mc") as MovieClip), htmlText, true);
			if(_outMc != null)	SetText((_outMc.getChildByName("textContainer_mc") as MovieClip), htmlText, true);
		}
		
		
		public function get OverSoundId():String { return _overSoundId; }
		public function set OverSoundId(value:String):void {	_overSoundId = value;	}
		
		public function get OutSoundId():String { return _outSoundId; }
		public function set OutSoundId(value:String):void {	_outSoundId = value;	}
		
		public function get clickSoundId():String {	return _clickSoundId; }
		public function set clickSoundId(value:String):void {	_clickSoundId = value; }
		
		public function get selected():Boolean 
		{
			return _selected;
		}
		
		public function set selected(newSelectedValue:Boolean):void 
		{
			if (newSelectedValue) {
				ShowState(_selectedMc);
				_hitArea.removeEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
				_hitArea.removeEventListener(MouseEvent.MOUSE_UP, OnMouseUp);
				_hitArea.removeEventListener(MouseEvent.ROLL_OVER, OnRollOver);
				_hitArea.removeEventListener(MouseEvent.ROLL_OUT, OnRollOut);
				_hitArea.removeEventListener(MouseEvent.CLICK, OnClick);
			} else {
				if (!disabled)
				{
					if (_selected) {
						ShowState(_outMc);
						_outMc.addEventListener(Event.ENTER_FRAME, OnOutEnterFrame);
					} else {
						ShowState(_upMc);
					}
					_hitArea.addEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
					_hitArea.addEventListener(MouseEvent.MOUSE_UP, OnMouseUp);
					_hitArea.addEventListener(MouseEvent.ROLL_OVER, OnRollOver);
					_hitArea.addEventListener(MouseEvent.ROLL_OUT, OnRollOut);
					_hitArea.addEventListener(MouseEvent.CLICK, OnClick);
				}
			}
			_selected = newSelectedValue;
		}
		
		public function get disabled():Boolean 
		{
			return _disabled;
		}
		
		private function SetText(textContainer:MovieClip, text:String, html:Boolean	= false):Boolean {
			
			if (textContainer == null) return false;
			
			var txtFld	: TextField	= textContainer.getChildByName("text_txt") as TextField;
			if (txtFld == null) return false;
			
			if (html) 	TextFieldUtils.SetHtmlTextPreservingFormat(txtFld, text);
			else 		TextFieldUtils.SetTextPreservingFormat(txtFld, text);
			return true;
		}
		
	}
}