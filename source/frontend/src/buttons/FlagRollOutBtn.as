package buttons
{
import flash.display.FrameLabel;
import flash.display.MovieClip;

import utils.MathUtils;

public class FlagRollOutBtn extends MovieClip {
		
		protected var _overFrameNbr				: int = 2;
		protected var _overAnimationNumFrames	: int = 2;
		protected var _outFrameNbr				: int = 2;
		protected var _outAnimationNumFrames	: int = 2;
		
		public function FlagRollOutBtn($hitArea:MovieClip = null) {
			
			Construct();
			gotoAndStop("up");
		}
		
		private function Construct():void {
			
			var overCompleteFrameNbr 	: int = 0;
			var outCompleteFrameNbr 	: int = 0;
			for each (var j:FrameLabel in this.currentLabels)
			{
				//trace("Label " + j.name + ": @" + j.frame);
				switch(j.name)
				{
					case "over":
						_overFrameNbr = j.frame;
						break;
					case "out":
						_outFrameNbr = j.frame;
						break;
					case "over_complete":
						overCompleteFrameNbr = j.frame;
						break;
					case "out_complete":
						outCompleteFrameNbr = j.frame;
						break;
				}
			}
			_overAnimationNumFrames = overCompleteFrameNbr - _overFrameNbr + 1;
			_outAnimationNumFrames	= outCompleteFrameNbr - _outFrameNbr + 1;
		}
		
		public function reset():void
		{
			gotoAndStop("up");
		}
		
		
		public function OnMouseDown () :void {
			
			gotoAndStop("down");
		}
		
		
		public function OnRollOver () :void {
			
			if (currentLabel == "over" || currentLabel == "over_complete") return;
			
			if (currentLabel == "up" || currentLabel == "down")
			{
				gotoAndPlay("over");
			} else if (currentLabel == "out" || currentLabel == "out_complete")
			{
				var outPctPlayed : int = Math.floor(100 * (currentFrame - _outFrameNbr) / _outAnimationNumFrames);
				var overPctToPlay : int = 100 - outPctPlayed;
				var overFrame : int = _overFrameNbr + overPctToPlay * _overAnimationNumFrames / 100;
				gotoAndPlay(overFrame);
			}
			
			var sndNbr : int = MathUtils.random(1, 4, true);
			var sndId : String = "FlagRollover" + sndNbr;
			Sfx.PlaySound(sndId);
		}
		
		
		public function OnMouseUp () :void {
			
			if (currentLabel == "down") gotoAndStop("up");
			else if (currentLabel == "over" || currentLabel == "over_complete")
			{
				var overPctPlayed : int = Math.floor(100 * (currentFrame - _overFrameNbr) / _overAnimationNumFrames);
				var outPctToPlay : int = 100 - overPctPlayed;
				var outFrame : int = _outFrameNbr + outPctToPlay * _outAnimationNumFrames / 100;
				gotoAndPlay(outFrame);
				
				//var overNumFramesAnimated : int = currentFrame - _overFrameNbr;
				//var outFinalFrame : int = _outFrameNbr + _overAnimationNumFrames - 1;
				//gotoAndPlay(outFinalFrame - overNumFramesAnimated);
			}
		}
		
		
		public function OnRollOut () :void {
			
			if (currentLabel == "over" || currentLabel == "over_complete")
			{
				var overPctPlayed : int = Math.floor(100 * (currentFrame - _overFrameNbr) / _overAnimationNumFrames);
				var outPctToPlay : int = 100 - overPctPlayed;
				var outFrame : int = _outFrameNbr + outPctToPlay * _outAnimationNumFrames / 100 - 1;
				gotoAndPlay(outFrame);
			}
			else if (currentLabel == "down")
			{
				gotoAndPlay("out");
			}
		}
	}
}