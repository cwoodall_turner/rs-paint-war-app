package  
{
import communication.database.DBManager;

import txt.TextLoader;

/**
	 * ...
	 * @author ...
	 */
	public class CheatsManager 
	{
		public static const WRONG_CODE			: String	= "wrong_code";
		/*
1) Destrabar a Muscleman
2) Tener la energía extra siempre
3) 20 disparos largos
4) 20 disparos poderosos
5) 20 granadas
6) granadas infinitas
7) duplicar las monedas ganadas en cada juego
8) Que todos los enemigos sean pelados
9) Tener todos los ítems al máx. por una partida
10) Jugar con un guy
		*/
		
		public static const	UNLOCK_MUSCLEMAN			: String	= "unlock_muscleman";
		public static const	CONSTANT_EXTRA_LIFE_SLOTS	: String	= "constant_extra_life_slots";
		public static const	EXTRA_SHOTS_DISTANCE		: String	= "extra_shots_distance";
		public static const	EXTRA_SHOTS_POWER			: String	= "extra_shots_power";
		public static const	EXTRA_GRENADES				: String	= "extra_grenades";
		public static const	INFINITE_GRENADES			: String	= "infinite_grenades";
		public static const	DOUBLE_COINS				: String	= "double_coins";
		public static const	BALD_ENEMIES				: String	= "bald_enemies";
		public static const	MAX_ITEMS					: String	= "max_items";
		public static const	PLAY_WITH_GUY				: String	= "play_with_guy";
		
		private static var _activeCodes	: Vector.<String> = new Vector.<String>();
		
		
		public function CheatsManager() 
		{
			
		}
		
		
		public static function AddCode(code:String):String {
			
			// INVINCIBILITY
			Debugger.Log("AddCode: " + code);
			var codeIds : Array = [UNLOCK_MUSCLEMAN, CONSTANT_EXTRA_LIFE_SLOTS, EXTRA_SHOTS_DISTANCE, EXTRA_SHOTS_POWER, EXTRA_GRENADES, INFINITE_GRENADES, DOUBLE_COINS, BALD_ENEMIES, MAX_ITEMS, PLAY_WITH_GUY];
			
			for each (var possibleCodeId : String in codeIds)
			{
				if (CodeExists(code, TextLoader.Instance.Texts["codes-" + possibleCodeId])) {
					_activeCodes.push(possibleCodeId);
					return possibleCodeId;
				}
			}
			
			return WRONG_CODE;
		}
		
		static public function onPlayClicked():void 
		{
			// UNLOCK_MUSCLEMAN
			if (isActive(UNLOCK_MUSCLEMAN))
			{
				DBManager.instance.user.setVar("muscleman", "1");
			}
			
			// CONSTANT_EXTRA_LIFE_SLOTS
			if (isActive(CONSTANT_EXTRA_LIFE_SLOTS))
			{
				var maxLifeSlots	: int = Config.getValue("powerup_extra_life_slots_max_value");
				Debugger.Log("maxLifeSlots: " + maxLifeSlots);
				DBManager.instance.user.setVar("extra_life_slots", maxLifeSlots);
			}
			
			// EXTRA_SHOTS_DISTANCE
			if (isActive(EXTRA_SHOTS_DISTANCE))
			{
				addPowerup("shots_distance");
			}
			
			// EXTRA_SHOTS_POWER
			if (isActive(EXTRA_SHOTS_POWER))
			{
				addPowerup("shots_power");
			}
			
			// EXTRA_GRENADES
			if (isActive(EXTRA_GRENADES))
			{
				addPowerup("grenades");
			}
			
			// INFINITE_GRENADES
			if (isActive(INFINITE_GRENADES))
			{
				var maxGrenades: int = Config.getValue("powerup_grenades_max_value");
				DBManager.instance.user.setVar("grenades", maxGrenades);
			}
			
			// MAX_ITEMS
			if (isActive(MAX_ITEMS))
			{
				addPowerup("shots_distance", 999999);
				addPowerup("shots_power", 999999);
				addPowerup("grenades", 999999);
				addPowerup("extra_life_slots", 999999);
			}
		}
		
		static private function addPowerup(powerupId:String, deltaValue: int = 0):void 
		{
			Debugger.Log("addPowerup: " + powerupId);
			if (deltaValue == 0)
			{
				deltaValue	= Config.getValue("code-extra_" + powerupId + "-value");
			}
			Debugger.Log("deltaValue: " + deltaValue);
			var powerupNewValue : int = int(DBManager.instance.user.getVar(powerupId)) + deltaValue;
			Debugger.Log("powerupNewValue: " + powerupNewValue);
			var powerupMaxValue	: int = Config.getValue("powerup_" + powerupId + "_max_value");
			Debugger.Log("powerupMaxValue: " + powerupMaxValue);
			if (powerupNewValue < 0 || powerupNewValue > powerupMaxValue)
			{
				powerupNewValue = powerupMaxValue;
			}
			DBManager.instance.user.setVar(powerupId, powerupNewValue);
		}
		
		private static function CodeExists(code:String, validCodes:String):Boolean {
			var validCodesArray	: Array	= validCodes.split(",");
			for each (var validCode:String in validCodesArray) {
				//Debugger.Log("validCode: " + validCode);
				Debugger.Log("code: " + code);
				if (code == validCode) {
					Debugger.Log("ES BUENO!");
					return true;
				}
			}
			return false;
		}
		
		public static function isActive(codeId:String):Boolean
		{
			for each (var activeCode:String in _activeCodes) {
				Debugger.Log("activeCode: " + activeCode);
				Debugger.Log("codeId: " + codeId);
				if (codeId == activeCode) {
					Debugger.Log("ACTIVO!");
					return true;
				}
			}
			return false;
		}
		
	}

}