﻿package utils {
	
	public class MathUtils {
		
		public static const RAD_TO_DEG	: Number	= 180 / Math.PI;
		public static const DEG_TO_RAD	: Number	= Math.PI / 180;
		public static const TWO_PI		: Number	= Math.PI * 2;
		public static const HALF_PI		: Number	= Math.PI / 2;
		
		public function MathUtils() { }
		
		/**
		 * Returns n clamped between min and max
		 */	
		public static function clamp(n:Number, min:Number, max:Number):Number {
			if (n < min) return min;
			if (n > max) return max;
			return n;
		}
		
		/**
		 * Returns 1 if the value is >= 0. Returns -1 if the value is < 0.
		 */	
		public static function sign(val:Number):int {
			if (val < 0) return -1
			return 1;
		}
		
		/**
		 * Returns a random number between min and max. If integ is true, it "floors" the return value.
		 */	
		public static function random(min:Number,max:Number,integ:Boolean = false):Number {
			var rnd	: Number;
			if (integ)
				rnd = Math.floor(min + Math.random() * (max + 1 - min));
			else
				rnd = min + Math.random() * (max - min);
			return rnd;
		}
		
		/**
		 * Returns a linear interpolation.
		 * 
		 * @param	y0
		 * @param	y1
		 * @param	x0
		 * @param	x1
		 * @param	x
		 * @return
		 */
		public static function linearInterpolationY(y0:Number, y1:Number, x0:Number, x1:Number, x:Number):Number {
			//Debugger.Log(" *** linearInterpolationY  --  [ y0: " + y0.toFixed(2) + " ; y1: " + y1.toFixed(2) + " ; x0: " + x0.toFixed(2) + " ; x1: " + x1.toFixed(2) + " ; x: " + x.toFixed(2) + " ]");
			//Debugger.Log(" *** RESULT: " + (y0 + (x - x0) * (y1 - y0) / (x1 - x0)).toString());
			return y0 + (x - x0) * (y1 - y0) / (x1 - x0);
		}
		
	}	
}