package utils {
	/**
	 * ...
	 * @author alfred
	 */
	public class VectorUtils	{
		
		public function VectorUtils() {
			
		}
		
		
		public static function ToStringForEachInt(v:Vector.<int>,name:String = ""):void {
			var len : int = v.length;
			Debugger.Log("*** ToString For Each *** (" + name + ")");
			for (var i : int = 0; i < len; i++ ) {
				Debugger.Log("--" + name + "[" + i + "]: " + v[i].toString());
			}
		}
		
		
		public static function ToStringForEachString(v:Vector.<String>,name:String = ""):void {
			var len : int = v.length;
			Debugger.Log("*** ToString For Each *** (" + name + ")");
			for (var i : int = 0; i < len; i++ ) {
				Debugger.Log("--" + name + "[" + i + "]: " + v[i].toString());
			}
		}
		
		
		public static function ToStringForEachNumber(v:Vector.<Number>,decimalPos:int = 2,name:String = ""):void {
			var len : int = v.length;
			Debugger.Log("*** ToString For Each *** (" + name + ")");
			for (var i : int = 0; i < len; i++ ) {
				Debugger.Log("--" + name + "[" + i + "]: " + StringUtils.IntToStr(v[i], decimalPos));
			}
		}
		
		/**
		 * Shuffles Vector with Fisher-Yates shuffling
		 * using com.gskinner.utils.Rndm class
		 * @param vec
		 * example: VectorUtils.shuffleVector(myVector as Vector.<*>);
		 */
		public static function shuffle(vec:Vector.<*>):void{
			if (vec.length > 1){
				var i:int = vec.length - 1;
				while (i > 0) {
					var s:Number = MathUtils.random(0, vec.length - 1, true);
					var temp:* = vec[s];
					vec[s] = vec[i];
					vec[i] = temp;
					i--;
				}
			}
		}
		
	}

}