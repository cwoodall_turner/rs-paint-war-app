﻿package utils {
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class TextFieldUtils {
		
		public function TextFieldUtils() { }
		
		
		public static function SetTextPreservingFormat(t:TextField, s:String):void {
			if (s == null) return;
			var fmt:TextFormat= t.getTextFormat();
			t.text= s;
			t.setTextFormat(fmt);
		}
		
		
		public static function SetHtmlTextPreservingFormat(t:TextField, s:String):void {
			if (s == null) return;
			var fmt:TextFormat= t.getTextFormat();
			t.htmlText = s;
			t.setTextFormat(fmt);
		}
	}	
}
