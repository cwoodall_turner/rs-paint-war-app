﻿package utils {

public class StringUtils {
		
		public function StringUtils() { }
		
		/**
		 * Returns the string representation of nVal with numChars characters
		 */	
		public static function IntToStr(nVal:int, numChars:int = 0):String {
			
			var str : String	= nVal.toString();
			
			while (str.length < numChars) {
				str = "0" + str;
			}
			
			return str;
		}
		
		/**
		 * Returns the string representation of nVal with numChars characters and numFloat decimal positions
		 */	
		public static function NumberToStr(nVal:Number, numChars:int = 0, numFloat:int = 0):String {
			
			var str : String	= nVal.toFixed(numFloat).toString();
			
			var decimalChars	: int	= (numFloat == 0)?0:numFloat + 1;
			while (str.length < (numChars + decimalChars)) {
				str = "0" + str;
			}
			
			return str;
		}
		
		
		public static function ToCoord(x:Number, y:Number, decimalPlaces:int = 2):String {
			return "(" + x.toFixed(decimalPlaces) + "," + y.toFixed(decimalPlaces) + ")";
		}
		
		public static function ToCoord3D(x:Number, y:Number, z:Number, decimalPlaces:int = 2):String {
			return "(" + x.toFixed(decimalPlaces) + "," + y.toFixed(decimalPlaces) + "," + z.toFixed(decimalPlaces) + ")";
		}
		
		
		private static const MS_TO_SEC	: Number	= .001;
		private static const SEC_TO_MIN	: Number	= .01666;
		private static const SEC_TO_HH	: Number	= .0002777777777777778;
		public static function MsToHHMMSS(ms:int):String {
			var totalSeconds	: Number = ms * MS_TO_SEC;
			var s:int = totalSeconds % 60;
			var m:int = Math.floor((totalSeconds % 3600 ) * SEC_TO_MIN);
			var h:int = Math.floor(totalSeconds * SEC_TO_HH);
			return IntToStr(h, 2) + ":" +IntToStr(m, 2) + ":" + IntToStr(s , 2);
		}
		public static function MsToMMSS(ms:int):String {
			return IntToStr(ms * MS_TO_SEC * SEC_TO_MIN, 2) + ":" + IntToStr((ms * MS_TO_SEC) % 60, 2);
		}
		public static function MsToMMSSDD(ms:int):String {
			return IntToStr(ms * MS_TO_SEC * SEC_TO_MIN, 2) + ":" + NumberToStr((ms * MS_TO_SEC) % 60, 2, 1);
		}
		public static function MsToHHMM(ms:int):String {
			var totalSeconds	: Number = ms * MS_TO_SEC;
			var m:int = Math.floor((totalSeconds % 3600 ) * SEC_TO_MIN);
			var h:int = Math.floor(totalSeconds * SEC_TO_HH);
			return IntToStr(h, 2) + ":" +IntToStr(m, 2);
		}
		
		static public function MsToSS(ms:int):String 
		{
			return IntToStr(Math.ceil(ms * MS_TO_SEC), 2);
		}
		
	}	
}