﻿package utils {

import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.MovieClip;
import flash.geom.Point;

public class MovieClipUtils {
		
		private static const POINT_ZERO : Point	= new Point(0, 0);
		
		private var _container:Array;
		private var _stop:Boolean = false;
		private static var _instance:MovieClipUtils	= null;
		
		/**
		 * Constructor
		 * 
		 * @param	lock
		 */
		public function MovieClipUtils(lock:SingletonEnforcer) {
			
			Reset();
		}

		/**
		 * Method to get Instance
		 * 
		 * @return MovieClipUtils
		 */
		public static function get Instance():MovieClipUtils{
			if ( _instance == null ) {
				_instance = new MovieClipUtils(new SingletonEnforcer());
			}
			return _instance;
		}	
		
		public function get Stop():Boolean { return _stop; }
		
		/**
		* Method to reset.
		*
		* @param	void.
		* @return	void.
		*/
		public function Reset():void {
			_container = new Array();
		}
		
		/**
		* Method to add new MovieClip.
		* 
		* @param	MovieClip.
		* @return	void.
		*/
		public function AddMovieClip(mc:MovieClip) :void {
			_container.push(mc);
		}
		
		
		/**
		* Method to remove MovieClip.
		* 
		* @param	MovieClip.
		* @return	void.
		*/
		public function RemoveMovieClip(mc:MovieClip) :void {
			if (_container.length == 0) return
			for (var i:int; i < _container.length; i++ ) {
				_container.splice(i,1);
			}
		}
		
		/**
		 * Method to stop animation of MovieClip.
		 * 
		 */
		public function AnimationMovieClipStop():void {
			if (_container.length == 0) return;
			for each(var mc : MovieClip in _container) {
				//MovieClip(mc).gotoAndStop(MovieClip(mc).currentFrame);
				mc.stop();
			}
			_stop = true;
		}
		
		/**
		 * Method to start animation of MovieClip.
		 * 
		 */
		public function AnimationMovieClipStart():void {
			if (_container.length == 0) return;
			for each(var mc : MovieClip in _container) {
				//MovieClip(mc).gotoAndPlay(MovieClip(mc).currentFrame);
				mc.play();
			}
			_stop = false;
		}
		/**
		 * Method to play or stop one MovieClip.
		 * 
		 * @param	mc:MovieClip.
		 * @param	stop:Boolean.
		 * @param	play:Boolean.
		 */
		public function AnimationMovieClip(mc:MovieClip,stop:Boolean = false,play:Boolean = false):void {
			if (mc == null) return;
			if (stop && !play)
				//mc.gotoAndStop(mc.currentFrame);
				mc.stop();
			if (play && !stop)
				//mc.gotoAndPlay(mc.currentFrame);
				mc.play();
		}
		/**
		 * Method to tinte MovieClip.
		 * 
		 * @param	view:MovieClip
		 * @param	color:uint
		 * @param	alpha:Number
		 
		public function PaintMC(view:MovieClip,color:uint,alpha:Number = 0):void {
			var c:Color = new Color();
			c.setTint( color , alpha );
			view.transform.colorTransform = c;
		}
		*/
		
		
		
		/**
		 * Gets the screen position of a Display Object
		 */	
		public static function ScreenPosition(view:DisplayObject):Point {
			return (view.localToGlobal(POINT_ZERO).subtract(GameContext.GlobalScreenPos));
		}
		
		/**
		 * Removes a Disp Obj from its parent
		 */	
		public static function Remove(displayObject:DisplayObject):void {
			if (displayObject != null && displayObject.parent != null) displayObject.parent.removeChild(displayObject);
		}
		
		
		/**
		 * Gets the global position of a Display Object
		 */	
		public static function GlobalPos(displayObject:DisplayObject):Point {
			return displayObject.localToGlobal(POINT_ZERO);
		}
		
		/**
		 * Moves the passed MovieClip to a random frame
		 */	
		public static function ToRandomFrame(mc:MovieClip,forceDifferent:Boolean = false):void {
			var frame	: int	= Math.ceil(Math.random() * mc.totalFrames);
			if (mc.currentFrame != frame)	mc.gotoAndPlay(frame);
			else if (forceDifferent) mc.play();
		}
		
		/**
		 * Empties the DisplayObjectContainer
		 */	
		public static function Empty(mc:DisplayObjectContainer):void {
			var numChildren	: int	= mc.numChildren;
			while (numChildren--) mc.removeChildAt(0);
		}
		
		/**
		 * Moves a DisplayObject to another parent
		 * 
		 * @param dobj: The display Object
		 * @param newParent: The new parent
		 * @param keepGlobalPos: If true, it keeps the global position. Otherwise dobj keeps its relative position. Defalut: true
		 * 
		 */	
		public static function ChangeParent(dobj:DisplayObject, newParent:DisplayObjectContainer, keepGlobalPos:Boolean = true):void {
			var globalPos : Point = (keepGlobalPos)?dobj.localToGlobal(POINT_ZERO):null;
			newParent.addChild(dobj);
			if (keepGlobalPos) {
				var newLocalPos : Point = newParent.globalToLocal(globalPos);
				dobj.x = newLocalPos.x;
				dobj.y = newLocalPos.y;
			}
		}
		
		/**
		 * 
		 * @param	dobj: The display Object
		 * @param	reference:  The DObj for reference
		 * @return	The position of dObj, relative to "reference"
		 */
		public static function GetRelativePos(dobj:DisplayObject, reference:DisplayObject):Point {
			var globalPos 	: Point = dobj.localToGlobal(POINT_ZERO);
			var relativePos	: Point = reference.globalToLocal(globalPos);
			return relativePos;
		}
		
		
		public static function AnimationEnded(mc:MovieClip):Boolean {
			return (mc && mc.currentFrame == mc.totalFrames);
		}
		
		static public function LocalRectToString(dObj:DisplayObject):String 
		{
			return "(" + dObj.x.toFixed(2) + ", " + dObj.y.toFixed(2) + ", " + (dObj.x + dObj.width).toFixed(2) + ", " + (dObj.y + dObj.height).toFixed(2) + ")";
		}
	}
}
internal class SingletonEnforcer {}