package  
{
import com.greensock.TweenMax;
import com.greensock.easing.EaseLookup;

/**
	 * ...
	 * @author alfred
	 */
	public class ChargeController 
	{
		protected var _barValue		: Number	= 2;
		protected var _barMinValue	: Number	= 0;
		protected var _barMaxValue	: Number	= 100;
		protected var _loopCycle	: Boolean	= false;
		protected var _cycleEndedWithoutSelection	: Boolean	= false;
		protected var _currentTween	: TweenMax	= null;
		
		protected var _id : String	= "";
		
		public function ChargeController($id:String) 
		{
			_id = $id;
			//Debugger.Log("ChargeController: " + $id);
		}
		
		public function start():void 
		{
			_barMinValue	= Config.getValue("charge_" + _id + "_min_percentage");
			_barMaxValue	= Config.getValue("charge_" + _id + "_max_percentage");
			_barValue		= Config.getValue("charge_" + _id + "_initial_value");
			_loopCycle		= (Config.getValue("charge_" + _id + "_loop") == "true");
			_cycleEndedWithoutSelection	= false;
			
			if (Config.getValue("charge_" + _id + "_start_going_up") == "true")
			{
				goUp();
			}
			else 
			{
				goDown();
			}
		}
		
		private function goUp():void 
		{
			if (_currentTween) _currentTween.pause();
			TweenMax.killTweensOf(this);
			var time			: Number	= Config.getValue("charge_" + _id + "_up_duration");
			var easeFunction	: Function	=	EaseLookup.find(Config.getValue("charge_" + _id + "_up_ease_function"));
			_currentTween		= TweenMax.to(this, time, { barValueForTween:_barMaxValue, ease:easeFunction, onComplete:goDown } );
		}
		
		private function goDown():void 
		{
			if (_currentTween) _currentTween.pause();
			TweenMax.killTweensOf(this);
			var time			: Number	= Config.getValue("charge_" + _id + "_down_duration");
			var easeFunction	: Function	=	EaseLookup.find(Config.getValue("charge_" + _id + "_down_ease_function"));
			_currentTween		= TweenMax.to(this, time, { barValueForTween:_barMinValue, ease:easeFunction, onComplete:goUp } );
		}
		
		public function destroy():void 
		{
			if (_currentTween) _currentTween.pause();
			TweenMax.killTweensOf(this);
			_currentTween	= null;
		}
		
		public function update(dtMs:int):void {
		}
		
		public function getChargePercentage():Number 
		{
			return _barValue;
		}
		
		public function onValueSelected():void 
		{
			if (_currentTween) _currentTween.pause();
			TweenMax.killTweensOf(this);
			_currentTween	= null;
		}
		
		public function get barValueForTween():Number 
		{
			return _barValue;
		}
		
		public function set barValueForTween(value:Number):void 
		{
			_barValue = value;
		}
	}

}