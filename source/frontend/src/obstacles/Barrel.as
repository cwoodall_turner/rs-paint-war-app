package obstacles 
{
import flash.display.MovieClip;
import flash.utils.Dictionary;

import shots.Shot;

/**
	 * ...
	 * @author ...
	 */
	public class Barrel extends Obstacle
	{
		
		public function Barrel(view:GameObjectView, shadow:MovieClip = null, $id:String="barrel", $name:String = "", $sfxHitId:String = "") 
		{
			super(view, shadow, $id, $name, $sfxHitId);
			
			_states = new Dictionary();
			_states[ST_IDLE]	= new ObstacleState_Idle(this);
			_states[ST_READY]	= new ObstacleState_Ready(this);
			_states[ST_HIT]		= new ObstacleState_Hit(this);
			
			_maxHitNbr	= 2;
		}
		
		
		override public function hitByShot(shot:Shot, collisionDirection:String):void 
		{
			super.hitByShot(shot, collisionDirection);
			Sfx.PlaySound("Hit" + _sfxHitId);
			SetState(ST_HIT);
		}
		
	}

}