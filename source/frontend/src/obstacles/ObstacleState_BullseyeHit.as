package obstacles 
{
/**
	 * ...
	 * @author ...
	 */
	public class ObstacleState_BullseyeHit extends ObstacleState
	{
		protected var _teamIdHit	: String	= "";
		
		public function ObstacleState_BullseyeHit(obstacle:Obstacle) 
		{
			super(obstacle);
			_canReEnterSameState = true;
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canBeHit = true;
			_gObj.canHit = Targets.NONE;
		}
		
		
		override public function EnterWithArgs(args:Array):void {
			super.EnterWithArgs(args);
			if (args != null && args.length > 0)	_teamIdHit	= args[0];
			else {
				Debugger.Log("WARNING! - Bullseye hit: enter state without args");
				_teamIdHit	= "1";
			}
			Enter();
		}
		
		override public function Enter():void {
			super.Enter();
			_obstacle.ViewGoToAndStop("hit_team" + _teamIdHit);
			_canStopPlayer	= true;
		}
		
		
		
	}

}
