package obstacles 
{
/**
	 * ...
	 * @author ...
	 */
	public class ObstacleState_Ready extends ObstacleState
	{
		public function ObstacleState_Ready(obstacle:Obstacle) 
		{
			super(obstacle);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canBeHit = false;
			_gObj.canHit = Targets.NONE;
		}
		
		override public function Enter():void {
			super.Enter();
			_obstacle.speedX = _obstacle.speedZ = _obstacle.speedY	= 0;
			_obstacle.visible	= false;
			_canStopPlayer	= false;
			_checkEnterScreen	= _checkScreenLimits	= false;
			_gObj.View.gotoAndStop(1);
		}
		
		override public function Leave():void {
			super.Leave();
			_obstacle.visible	= true;
		}
		
		override public function Update(dtMs:int):void {
			//super.Update(dtMs);
		}
		
	}

}
