package obstacles 
{
import flash.display.MovieClip;
import flash.utils.Dictionary;

import shots.Grenade;
import shots.Shot;

/**
	 * ...
	 * @author ...
	 */
	public class Bullseye extends Obstacle implements Capturable
	{
		protected var _direction	: String	= "";
		protected var _paintedByTeamId	: String	= "";
		
		protected var _timeLeftToClearMs	: int	= 0;
		protected var _timeToClearMs		: int	= 0;
		
		public function Bullseye(view:GameObjectView, shadow:MovieClip = null, $id:String="bullseye", $name:String = "", $direction:String = "", $sfxHitId:String = "") 
		{
			super(view, shadow, $id, $name, $sfxHitId);
			
			_direction	= $direction;
			
			_states = new Dictionary();
			_states[ST_IDLE]	= new ObstacleState_Idle(this);
			_states[ST_READY]	= new ObstacleState_Ready(this);
			_states[ST_HIT]		= new ObstacleState_BullseyeHit(this);
			
			_maxHitNbr	= 2;
			_paintedByTeamId	= null;
			
			_timeToClearMs		= Config.getValue("time_to_clear_bullseye_ms");
		}
		
		public function get direction():String 
		{
			return _direction;
		}
		
		public function get paintedByTeamId():String 
		{
			return _paintedByTeamId;
		}
		
		public function set paintedByTeamId(value:String):void 
		{
			_paintedByTeamId = value;
		}
		
		public function get timeLeftToClearMs():int 
		{
			return _timeLeftToClearMs;
		}
		
		public function get timeToClearMs():int 
		{
			return _timeToClearMs;
		}
		
		public function get gObj():GameObject
		{
			return this;
		}
		
		
		override public function hitByShot(shot:Shot, collisionDirection:String):void 
		{
			super.hitByShot(shot, collisionDirection);
			Sfx.PlaySound("Hit" + _sfxHitId);
			if ((collisionDirection == _direction || (shot is Grenade)) && _paintedByTeamId != shot.teamId)
			{
				_paintedByTeamId	= shot.teamId;
				SetState(ST_HIT, [shot.teamId]);
				
				GameManager.Instance.currentLevel.onBullseyePainted(this, shot);
				_timeLeftToClearMs	= _timeToClearMs;
				Debugger.Log("_timeLeftToClearMs: " + _timeLeftToClearMs);
			}
		}
		
		override public function Update(dtMs:int):void
		{
			super.Update(dtMs);
			if (_timeLeftToClearMs > 0)
			{
				_timeLeftToClearMs -= dtMs;
				if (_timeLeftToClearMs <= 0)
				{
					resetPaintedBy();
				}
			}
		}
		
		public function resetPaintedBy():void
		{
			GameManager.Instance.currentLevel.onBullseyePaintedTimeEnd(this);
			_paintedByTeamId = null;
			SetState(ST_IDLE);
		}
		
	}

}