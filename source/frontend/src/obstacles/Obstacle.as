package obstacles 
{
import flash.display.MovieClip;

import shots.Shot;

import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class Obstacle extends GameObject
	{
		public static const ST_IDLE					: String	= "idle";
		public static const ST_HIT					: String	= "hit";
		
		protected var _maxHitNbr	: int		= 2;
		protected var _powerupId	: String	= "";
		protected var _name 		: String	= "";
		protected var _sfxHitId		: String	= "";
		
		static protected var _paintFxs	: Vector.<FxPaint>	= null;
		
		public function Obstacle(view:GameObjectView, shadow:MovieClip = null, $id:String="obstacle", $name:String = "", $sfxHitId:String = "") 
		{
			super(view, shadow, $id);
			_targetId	= Targets.OBSTACLE;
			_name		= $name;
			_sfxHitId	= $sfxHitId;
			
			var paintMc : MovieClip = View.getChildByName("paint_mc") as MovieClip;
			if (paintMc != null)
			{
				resetPaintSide("left");
				resetPaintSide("right");
				resetPaintSide("front");
				
				if (_paintFxs == null) _paintFxs = new Vector.<FxPaint>();
			}
		}
		
		private function resetPaintSide(side:String):void 
		{
			var paintMc : MovieClip = View.getChildByName("paint_mc") as MovieClip;
			if (paintMc == null) return;
			var paintSideMc	: MovieClip	= paintMc.getChildByName(side + "_mc") as MovieClip;
			MovieClipUtils.Empty(paintSideMc);
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			_currentState.Update(dtMs);
		}
		
		override public function get canStopPlayer():Boolean {
			return (_currentState as ObstacleState).canStopPlayer;
		}
		
		public function get name():String 
		{
			return _name;
		}
		
		
		override public function Reset():void {
			SetState(ST_READY);
		}
		
		public function Enable():void {
			SetState(ST_IDLE);
		}
		
		
		public function hitByShot(shot:Shot, collisionDirection:String):void 
		{
			if (Config.paint_obstacles)
			{
				paintSide(this, collisionDirection, shot.shooter.teamId);
			}
			_lastHitBy = shot;
			lastHitByAttackSeq = shot.attackSeq;
		}
		
		private static function paintSide(obstacle:Obstacle, side:String, teamId:String):void 
		{
			//Debugger.Log("paintSide(" + side + ": " + teamId);
			var paintMc : MovieClip	= obstacle.View.getChildByName("paint_mc") as MovieClip;
			if (paintMc == null) return;
			
			var paintSideMc	: MovieClip	= paintMc.getChildByName(side + "_mc") as MovieClip;
			//Debugger.Log("paintSideMc: " + paintSideMc);
			if (paintSideMc == null) return;
			
			var paintFx	: FxPaint = getPaintFx(side, teamId);
			
			paintSideMc.addChild(paintFx.View);
			paintFx.start();
		}
		
		static private function getPaintFx(side:String, teamId:String):FxPaint 
		{
			if (_paintFxs == null) _paintFxs = new Vector.<FxPaint>();
			for each(var paintFx: FxPaint in _paintFxs)
			{
				//Debugger.Log("paintFx.side: " + paintFx.side);
				//Debugger.Log("paintFx.teamId : " + paintFx.teamId );
				//Debugger.Log("paintFx.ready: " + paintFx.ready);
				if (paintFx.side == side && paintFx.teamId == teamId && paintFx.ready)
				{
					return paintFx;
				}
			}
			
			var newPaintFx : FxPaint = createNewPaintFx(side, teamId);
			_paintFxs.push(newPaintFx);
			return newPaintFx;
		}
		
		static private function createNewPaintFx(side:String, teamId:String):FxPaint 
		{
			return new FxPaint(side, teamId, ExternalLoader.Instance.GetNewInstance("Paint_" + side + "_" + teamId, "obstacles.swf") as MovieClip);
		}
		
		
		
	}

}