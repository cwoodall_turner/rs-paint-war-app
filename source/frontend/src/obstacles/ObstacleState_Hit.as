package obstacles 
{
/**
	 * ...
	 * @author ...
	 */
	public class ObstacleState_Hit extends ObstacleState
	{
		public function ObstacleState_Hit(obstacle:Obstacle) 
		{
			super(obstacle);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canBeHit = true;
			_gObj.canHit = Targets.NONE;
		}
		
		override public function Enter():void {
			super.Enter();
			//_obstacle.ViewGoToAndStop("hit");
			_obstacle.ViewGoToAndPlay("hit");
			_canStopPlayer	= true;
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (_gObj.View.currentFrameLabel == "idle")
			{
				_obstacle.SetState(Obstacle.ST_IDLE);
			}
		}
		
		
		
	}

}
