package obstacles 
{
/**
	 * ...
	 * @author ...
	 */
	public class ObstacleState_Idle extends ObstacleState
	{
		public function ObstacleState_Idle(obstacle:Obstacle) 
		{
			super(obstacle);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canBeHit = true;
			_gObj.canHit = Targets.NONE;
		}
		
		override public function Enter():void {
			super.Enter();
			_obstacle.ViewGoToAndStop("idle");
			_canStopPlayer	= true;
		}
		
		override public function HitBy(hitter:GameObject):void {
			super.HitBy(hitter);
			_obstacle.SetState(Obstacle.ST_HIT);
		}
		
	}

}
