package obstacles 
{
import flash.display.MovieClip;

/**
	 * ...
	 * @author alfred
	 */
	public class FxPaint 
	{
		
		private var _side 	: String = "";
		private var _teamId	: String = "";
		private var _view	: MovieClip = null;
		
		public function FxPaint($side:String, $teamId:String, $view:MovieClip) 
		{
			_side = $side;
			_teamId = $teamId;
			_view = $view;
			_view.gotoAndStop(1);
			_view.visible = false;
		}
		
		public function start():void 
		{
			_view.gotoAndPlay(1);
			_view.visible = true;
		}
		
		public function get side():String 
		{
			return _side;
		}
		
		public function get teamId():String 
		{
			return _teamId;
		}
		
		public function get View():MovieClip 
		{
			return _view;
		}
		
		public function get ready():Boolean
		{
			return _view.parent == null;
		}
		
	}

}