package obstacles 
{
/**
	 * ...
	 * @author ...
	 */
	public class ObstacleState_SceneryObstacleIdle extends ObstacleState
	{
		public function ObstacleState_SceneryObstacleIdle(obstacle:Obstacle) 
		{
			super(obstacle);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canBeHit = true;
			_gObj.canHit = Targets.NONE;
		}
		
		override public function Enter():void {
			super.Enter();
			_obstacle.ViewGoToAndStop("idle");
			_canStopPlayer	= true;
		}
		
	}

}
