package obstacles 
{
import flash.display.MovieClip;
import flash.utils.Dictionary;

import shots.Shot;

/**
	 * ...
	 * @author ...
	 */
	public class SceneryObstacle extends Obstacle
	{
		
		public function SceneryObstacle(view:GameObjectView, shadow:MovieClip = null, $id:String="sceneryobstacle", $name:String = "") 
		{
			super(view, shadow, $id, $name);
			
			_states = new Dictionary();
			_states[ST_IDLE]	= new ObstacleState_SceneryObstacleIdle(this);
		}
		
		
		override public function hitByShot(shot:Shot, collisionDirection:String):void 
		{
			super.hitByShot(shot, collisionDirection);
			//Sfx.PlaySound("HitBarrel");
		}
		
	}

}