package obstacles 
{
import flash.display.MovieClip;
import flash.geom.Point;
import flash.utils.getQualifiedClassName;

import utils.MathUtils;
import utils.MovieClipUtils;

import view.Camera;
import view.GameObjectToScreenMapper;

/**
	 * ...
	 * @author ...
	 */
	public class ObstaclesManager 
	{
		
		private static var _instance:ObstaclesManager = null;
		
		private var _obstacles		:	Vector.<Obstacle> 		= null;
		
		public function ObstaclesManager()
		{
			_instance		= this;
			_obstacles		= new Vector.<Obstacle>();
		}
		
		static public function get instance():ObstaclesManager 
		{
			return _instance;
		}
		
		public function get theObstacles():Vector.<Obstacle> 
		{
			return _obstacles;
		}
		
		public function PrepareFromSwf():void {
			var levelAsset	: MovieClip	= GameManager.Instance.currentLevel.levelAsset;
			
			var obstaclesContainer : MovieClip	= levelAsset.getChildByName("obstacles_mc") as MovieClip;
			var i : int = obstaclesContainer.numChildren;
			while (i--)
			{
				var obstacleRef : MovieClip	= obstaclesContainer.getChildAt(i) as MovieClip;
				addObstacle(obstacleRef, MovieClipUtils.GetRelativePos(obstacleRef, levelAsset));
			}
		}
		
		public function addObstacle(obstacleRef: MovieClip, screenPos:Point):void
		{
			var id:String = getQualifiedClassName(obstacleRef).split("_").reverse()[0];
			
			var newObstacle : Obstacle	= null;
			
			var name : String = obstacleRef.name;
			if (name.indexOf("instance") >= 0) name = "";
			
			switch(id) {
				case "Barril":
					newObstacle	= new Barrel(ExternalLoader.Instance.GetNewInstance("BarrelAsset", "obstacles.swf") as GameObjectView, null, id, name, "Barrel");
					break;
				case "Llantas":
					newObstacle	= new Barrel(ExternalLoader.Instance.GetNewInstance("TiresAsset", "obstacles.swf") as GameObjectView, null, id, name, "SandBag");
					break;
				case "MaderaCostado":
					newObstacle	= new Barrel(ExternalLoader.Instance.GetNewInstance("WoodSideAsset", "obstacles.swf") as GameObjectView, null, id, name, "Wood");
					break;
				case "MaderaFrente":
					newObstacle	= new Barrel(ExternalLoader.Instance.GetNewInstance("WoodFrontAsset", "obstacles.swf") as GameObjectView, null, id, name, "Wood");
					break;
				case "BolsaDeArena":
					newObstacle	= new Barrel(ExternalLoader.Instance.GetNewInstance("SandbagAsset", "obstacles.swf") as GameObjectView, null, id, name, "SandBag");
					break;
				case "BlancoDerecha":
					newObstacle	= new Bullseye(ExternalLoader.Instance.GetNewInstance("BullseyeRightAsset", "obstacles.swf") as GameObjectView, null, id, name, CollisionManager.RIGHT, "Wood");
					break;
				case "BlancoIzquierda":
					newObstacle	= new Bullseye(ExternalLoader.Instance.GetNewInstance("BullseyeLeftAsset", "obstacles.swf") as GameObjectView, null, id, name, CollisionManager.LEFT, "Wood");
					break;
				case "BlancoFrente":
					newObstacle	= new Bullseye(ExternalLoader.Instance.GetNewInstance("BullseyeFrontAsset", "obstacles.swf") as GameObjectView, null, id, name, CollisionManager.FRONT, "Wood");
					break;
			}
			
			_obstacles.push(newObstacle);
			GameObjectToScreenMapper.instance.AddGameObject(newObstacle);
			var newObstaclePos	: GameAreaPoint = Camera.Instance.ConvertScreenPosToGamePos(screenPos, Config.floor_y);
			newObstacle.TeleportTo(newObstaclePos.x, newObstaclePos.y, newObstaclePos.z);
			newObstacle.SetState(Obstacle.ST_IDLE);
			newObstacle.FaceRight();
			newObstacle.RecalculateHitBoundaries();
			
			var obstacleAlpha	: Number = Config.getValue("debug_obstacles_alpha");
			if (!isNaN(obstacleAlpha) && obstacleAlpha < 1)
			{
				newObstacle.View.alpha = obstacleAlpha;
			}
		}
		
		
		public function Update(dtMs:int):void
		{
			for each (var obstacle:Obstacle in _obstacles) {
				obstacle.Update(dtMs);
			}
		}
		
		public function Reset():void {
			for each (var obstacle : Obstacle in _obstacles) {
				Debugger.Log("DESTROYING  OBSTACLE " + obstacle);
				obstacle.Reset();
				GameObjectToScreenMapper.instance.RemoveGameObject(obstacle);
				obstacle.Destroy();
			}
			_obstacles		= new Vector.<Obstacle>();
		}
		
		public function getObstacle(obstacleName:String):Obstacle 
		{
			for each(var obstacle:Obstacle in theObstacles)
			{
				if (obstacleName == obstacle.name) return obstacle;
			}
			return null;
		}
		
		public function getNearestObstacle(gObj:GameObject, hasToHaveName:Boolean = true):Obstacle  
		{
			var shortestDistance	: Number	= Number.MAX_VALUE;
			var nearestObstacle		: Obstacle	= null;
			
			for each(var obstacle:Obstacle in theObstacles)
			{
				if (hasToHaveName && obstacle.name == "") continue;
				var distance : Number	= (obstacle.x - gObj.x) * (obstacle.x - gObj.x) + (obstacle.z - gObj.z) * (obstacle.z - gObj.z);
				if (distance < shortestDistance)
				{
					shortestDistance = distance;
					nearestObstacle = obstacle;
				}
			}
			return nearestObstacle;
		}
		
		public function getFarthestObstacle(gObj:GameObject, hasToHaveName:Boolean = true):Obstacle  
		{
			var longestDistance		: Number	= Number.NEGATIVE_INFINITY;
			var farthestObstacle	: Obstacle	= null;
			
			for each(var obstacle:Obstacle in theObstacles)
			{
				if (hasToHaveName && obstacle.name == "") continue;
				var distance : Number	= (obstacle.x - gObj.x) * (obstacle.x - gObj.x) + (obstacle.z - gObj.z) * (obstacle.z - gObj.z);
				if (distance > longestDistance)
				{
					longestDistance = distance;
					farthestObstacle = obstacle;
				}
			}
			return farthestObstacle;
		}
		
		public function getRandomObstacle():Obstacle  
		{
			if (theObstacles == null || theObstacles.length == 0) return null;
			var obstacleIdx : int = MathUtils.random(0, theObstacles.length - 1);
			return theObstacles[obstacleIdx];
		}
		
		public function getNearestPaintableBullseye(gObj:GameObject):Bullseye  
		{
			var shortestDistance	: Number	= Number.MAX_VALUE;
			var nearestObstacle		: Obstacle	= null;
			
			for each(var obstacle:Obstacle in theObstacles)
			{
				if (!(obstacle is Bullseye)) continue;
				if ((obstacle as Bullseye).paintedByTeamId == gObj.teamId) continue;
				
				var distance : Number	= (obstacle.x - gObj.x) * (obstacle.x - gObj.x) + (obstacle.z - gObj.z) * (obstacle.z - gObj.z);
				if (distance < shortestDistance)
				{
					shortestDistance = distance;
					nearestObstacle = obstacle;
				}
			}
			return nearestObstacle as Bullseye;
		}
		
		
		public function intersectsAnyObstacle(testBoundaries: Boundaries):Boolean 
		{
			for each(var obstacle:Obstacle in _obstacles)
			{
				if (obstacle.IsCurrentState(GameObject.ST_READY)) continue;
				var obstacleHitAbsBoundaries	: Boundaries	= CollisionManager.instance.getAbsoluteHitBoundaries(obstacle);
				if (!obstacleHitAbsBoundaries) continue;
				
				if (CollisionManager.instance.boundariesIntersect(testBoundaries, obstacleHitAbsBoundaries)) return true;
			}
			return false;
		}
		
	}
	
}
