package obstacles 
{
/**
	 * ...
	 * @author ...
	 */
	public class ObstacleState extends GameObjectState
	{
		
		protected var _obstacle			: Obstacle	= null;
		protected var _canStopPlayer	: Boolean	= true;
		
		public function ObstacleState(obstacle:Obstacle) 
		{
			super(obstacle);
			_obstacle				= obstacle;
		}
		
		override public function Enter():void {
			super.Enter();
		}
		/*
		public function HitByPlayer(thePlayer:GameObject):void {}
		public function HitByEnemy(theEnemy:Enemy):void {}
		public function HitByShot(theShot:Shot):void { }
		*/
		public function HitBy(hitter:GameObject):void {}
		
		public function get canStopPlayer():Boolean {
			return _canStopPlayer;
		}
		
	}

}
