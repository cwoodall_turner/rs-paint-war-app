package obstacles 
{
import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class ObstacleState_Broken extends ObstacleState
	{
		public function ObstacleState_Broken(obstacle:Obstacle) 
		{
			super(obstacle);
		}
		
		override protected function updateCanHitAndCanBeHit():void 
		{
			_gObj.canBeHit = false;
			_gObj.canHit = Targets.NONE;
		}
		
		override public function Enter():void {
			super.Enter();
			if(_obstacle.lastHitBy.x <= _obstacle.x)	_obstacle.ViewGoToAndStop("broken_to_right");
			else	_obstacle.ViewGoToAndStop("broken_to_left");
			_canStopPlayer	= false;
			//if (_obstacle.powerupId) {
				//PowerupsManager.instance.AddPowerup(_obstacle);
			//}
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			
			if (MovieClipUtils.AnimationEnded(_obstacle.innerMc)) {
				_obstacle.SetState(Obstacle.ST_READY);
			}
		}
		
		
	}

}
