﻿package
{
public class GameInput
	{
		
		// INPUT
		protected static var _keyLeft			: Boolean	= false;
		protected static var _keyLeftHit		: Boolean	= false;
		protected static var _keyRight			: Boolean	= false;
		protected static var _keyRightHit		: Boolean	= false;
		protected static var _keyUp				: Boolean	= false;
		protected static var _keyDown			: Boolean	= false;
		protected static var _keyJump			: Boolean	= false;
		protected static var _keyAction1Hit		: Boolean	= false;
		protected static var _keyAction1Down	: Boolean	= false;
		protected static var _keyAction2Hit		: Boolean	= false;
		protected static var _keyAction2Down	: Boolean	= false;
		protected static var _keyAction1FramesPressed	: int	= 0;
		protected static var _keyAction2FramesPressed	: int	= 0;
		protected static var _simultaneousKeysFramesTolerance	: int = 2;
		
		private static var _doubleClickEnabled:Boolean
		
		public static function initialize( ):void
		{
			_simultaneousKeysFramesTolerance	= Config.getValue("simultaneous_keys_frames_tolerance");
		}
		
		
		public static function update():void
		{
			// INPUT
			_keyLeft		= ActionInputMapper.keyDown(ActionInputMapper.LEFT, 1);
			_keyLeftHit		= ActionInputMapper.keyHit(ActionInputMapper.LEFT, 1);
			_keyRight		= ActionInputMapper.keyDown(ActionInputMapper.RIGHT, 1);
			_keyRightHit	= ActionInputMapper.keyHit(ActionInputMapper.RIGHT, 1);
			_keyUp			= ActionInputMapper.keyDown(ActionInputMapper.UP, 1);
			_keyDown		= ActionInputMapper.keyDown(ActionInputMapper.DOWN, 1);
			_keyAction1Hit	= ActionInputMapper.keyHit(ActionInputMapper.ACTION_1, 1);
			_keyAction1Down	= ActionInputMapper.keyDown(ActionInputMapper.ACTION_1, 1);
			_keyAction2Hit	= ActionInputMapper.keyHit(ActionInputMapper.ACTION_2, 1);
			_keyAction2Down	= ActionInputMapper.keyDown(ActionInputMapper.ACTION_2, 1);
			if (ActionInputMapper.keyDown(ActionInputMapper.ACTION_1, 1)) _keyAction1FramesPressed += 1;
			else _keyAction1FramesPressed	= 0;
			if (ActionInputMapper.keyDown(ActionInputMapper.ACTION_2, 1)) _keyAction2FramesPressed += 1;
			else _keyAction2FramesPressed	= 0;
		}
		
		static public function get left():Boolean 
		{
			return _keyLeft;
		}
		
		static public function get right():Boolean 
		{
			return _keyRight;
		}
		
		static public function get up():Boolean 
		{
			return _keyUp;
		}
		
		static public function get down():Boolean 
		{
			return _keyDown;
		}
		
		static public function get action1Hit():Boolean 
		{
			return _keyAction1Hit;
		}
		
		static public function get action1Down():Boolean 
		{
			return _keyAction1Down;
		}
		
		static public function get action2Hit():Boolean 
		{
			return _keyAction2Hit;
		}
		
		static public function get action2Down():Boolean 
		{
			return _keyAction2Down;
		}
		
		static public function get leftHit():Boolean 
		{
			return _keyLeftHit;
		}
		
		static public function get rightHit():Boolean 
		{
			return _keyRightHit;
		}
	}	
}
