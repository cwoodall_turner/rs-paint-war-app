﻿package
{
import flash.display.DisplayObjectContainer;
import flash.display.Stage;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;

/**
	 * @author Ariel Nehmad
	 */
	public class Input
	{
		private static var _downs:Array = new Array()
		private static var _hits:Array = new Array()
		private static var _ignored:Array = new Array()
		private static var _keyCode:int = 0		
		private static var _delta:int = 0
		private static var _mouseUp:int = 0		
		private static var _mouseHit:int = 0
		private static var _mouseDobleClick:int = 0
		private static var _mouseX:Number
		private static var _mouseY:Number
		private static var _mouseXSpeed:Number
		private static var _mouseYSpeed:Number
		private static var _mouseUpdate:Boolean = true		
		private static var _mouseDown:int		
		private static var _target:DisplayObjectContainer
		private static var _stage:Stage
		
		private static var _doubleClickEnabled:Boolean
		
		public static function initialize( target:DisplayObjectContainer ):void
		{
			_target = target			
			
			if ( _target.stage == null ) return
			
			_stage = target.stage
			
			_mouseX = _stage.mouseX
			_mouseY = _stage.mouseY					
			
			_stage.removeEventListener( MouseEvent.MOUSE_MOVE, mouseUpdate )
			_stage.removeEventListener( KeyboardEvent.KEY_DOWN, keyDownEvent )
			_stage.removeEventListener( KeyboardEvent.KEY_UP, keyUpEvent )
			_stage.removeEventListener( MouseEvent.MOUSE_DOWN, mouseDownEvent )
			_stage.removeEventListener( MouseEvent.MOUSE_UP, mouseUpEvent )
			_stage.removeEventListener( MouseEvent.MOUSE_WHEEL, mouseWheelEvent )
			
			_stage.addEventListener( MouseEvent.MOUSE_MOVE, mouseUpdate )
			_stage.addEventListener( KeyboardEvent.KEY_DOWN, keyDownEvent )
			_stage.addEventListener( KeyboardEvent.KEY_UP, keyUpEvent )
			_stage.addEventListener( MouseEvent.MOUSE_DOWN, mouseDownEvent )
			_stage.addEventListener( MouseEvent.MOUSE_UP, mouseUpEvent )
			_stage.addEventListener( MouseEvent.MOUSE_WHEEL, mouseWheelEvent )
			
			doubleClickEnabled = _doubleClickEnabled
		}
		
		static private function mouseUpdate(e:MouseEvent):void 
		{
			_mouseUpdate = true
		}
		
		public static function reset():void
		{
			for (var i:int = 0; i < 255; i++)
			{
				_downs[i] = false
				_hits[i] = false
				_ignored[i] = false
			}
			_mouseXSpeed = 0
			_mouseYSpeed = 0
			_mouseUp = 0
			_mouseDown = 0
			_mouseDobleClick = 0
			_mouseHit = 0
		}
		
		private static function sendKeys( value:int ):void
		{
			if (_ignored[value]) return;
			if (!_downs[ value ]) _hits[ value ] = true
			_downs[ value ] = true
		}
		
		public static function update():void
		{
			if ( !_stage ) return
			if ( _mouseUpdate )
			{
				_mouseXSpeed = _stage.mouseX - _mouseX
				_mouseYSpeed = _stage.mouseY - _mouseY
				_mouseUpdate = false
			}
			else
			{
				_mouseXSpeed = 0
				_mouseYSpeed = 0
			}
			_mouseX			= 	_stage.mouseX
			_mouseY			= 	_stage.mouseY
		}
		
		private static function keyDownEvent( e:KeyboardEvent ):void 
		{
			if (_ignored[ e.keyCode ]) return;
			if ( !_downs [ e.keyCode ] ) _hits[ e.keyCode ] = true
			_downs[ e.keyCode ] = true
			
			_keyCode = e.keyCode
		}

		private static function keyUpEvent( e:KeyboardEvent ):void 
		{
			_downs[ e.keyCode ] = false
			_hits[ e.keyCode ] = false
			
			_keyCode = 0
		}
		
		private static function mouseDownEvent( e:MouseEvent ):void 
		{
			_mouseDown = 1
			_mouseHit = 1
			_mouseUp = 0
		}
		
		private static function mouseWheelEvent( e:MouseEvent ):void
		{
			_delta = e.delta
		}
		
		private static function mouseUpEvent( e:MouseEvent ):void 
		{
			_mouseDown = 0
			_mouseHit = 0
			_mouseUp = 1
		}
		
		private static function mouseDobleClickEvent( e:MouseEvent ):void
		{
			_mouseDobleClick = 1
		}
		
		public static function get keyCode():int
		{
			return _keyCode
		}
		
		public static function keyDown( keyCode:int ):Boolean 
		{
			return _downs[ keyCode ]
		}
		
		public static function keyHit( keyCode:int ):Boolean 
		{
			if ( _hits[ keyCode ] ) 
			{
				_hits[ keyCode ] = false
				return true
			}
			else 
			{
				return false
			}
		}
		
		public static function get mouseHit():int
		{
			var value:int = _mouseHit
			_mouseHit = 0
			return value
		}
		
		public static function get mouseDobleClick():int
		{
			var value:int = _mouseDobleClick
			_mouseDobleClick = 0
			return value
		}
		
		public static function get delta():int
		{
			var value:int = _delta
			_delta = 0
			return value
		}
		
		static public function set delta(value:int):void 
		{
			_delta = value;
		}
		
		public static function get mouseUp():int 
		{
			var val:int = _mouseUp
			_mouseUp = 0
			return val
		}
		
		static public function get mouseYSpeed():Number { return _mouseYSpeed; }
		
		static public function get mouseDown():int { return _mouseDown; }
		
		static public function get mouseXSpeed():Number { return _mouseXSpeed; }
		
		static public function get mouseY():Number { return _mouseY; }
		
		static public function set mouseY(value:Number):void 
		{
			_mouseY = value;
		}
		
		static public function get mouseX():Number { return _mouseX; }
		
		static public function set mouseX(value:Number):void 
		{
			_mouseX = value;
		}
		
		static public function get doubleClickEnabled():Boolean { return _doubleClickEnabled; }
		
		static public function set doubleClickEnabled(value:Boolean):void 
		{
			_doubleClickEnabled = value;
			if ( _target ) 
			{
				_target.removeEventListener( MouseEvent.DOUBLE_CLICK, mouseDobleClickEvent )
				_target.doubleClickEnabled = value
				if ( value )
					_target.addEventListener( MouseEvent.DOUBLE_CLICK, mouseDobleClickEvent )
			}
			if ( _stage ) 
			{
				_stage.removeEventListener( MouseEvent.DOUBLE_CLICK, mouseDobleClickEvent )
				_stage.doubleClickEnabled = value
				if ( value )
					_stage.addEventListener( MouseEvent.DOUBLE_CLICK, mouseDobleClickEvent )
			}
		}
		
		static public function ignoreKey (value:int):void {
			_ignored[value] = true;
			_hits[value] 	= false;
			_downs[value]	= false;
		}
		
		static public function unignoreKey (value:int):void {
			_ignored[value] = false;
		}
		
		static public function unignoreAllKeys ():void {
			for (var i:int = 0; i < 255; i++)
			{
				_ignored[i] = false
			}
		}
		
		static public function ignoreAllKeys ():void {
			for (var i:int = 0; i < 255; i++)
			{
				_ignored[i] = true;
				_hits[i] 	= false;
				_downs[i]	= false;
			}
		}
		
		public static const A:uint = 65;
		public static const B:uint = 66;
		public static const C:uint = 67;
		public static const D:uint = 68;
		public static const E:uint = 69;
		public static const F:uint = 70;
		public static const G:uint = 71;
		public static const H:uint = 72;
		public static const I:uint = 73;
		public static const J:uint = 74;
		public static const K:uint = 75;
		public static const L:uint = 76;
		public static const M:uint = 77;
		public static const N:uint = 78;
		public static const O:uint = 79;
		public static const P:uint = 80;
		public static const Q:uint = 81;
		public static const R:uint = 82;
		public static const S:uint = 83;
		public static const T:uint = 84;
		public static const U:uint = 85;
		public static const V:uint = 86;
		public static const W:uint = 87;
		public static const X:uint = 88;
		public static const Y:uint = 89;
		public static const Z:uint = 90;
		
		public static const ALTERNATE:uint = 18;
		public static const BACKQUOTE:uint = 192;
		public static const BACKSLASH:uint = 220;
		public static const BACKSPACE:uint = 8;
		public static const CAPS_LOCK:uint = 20;
		public static const COMMA:uint = 188;
		public static const COMMAND:uint = 19;
		public static const CONTROL:uint = 17;
		public static const DELETE:uint = 46;
		public static const DOWN:uint = 40;
		public static const END:uint = 35;
		public static const ENTER:uint = 13;
		public static const EQUAL:uint = 187;
		public static const ESCAPE:uint = 27;
		public static const F1:uint = 112;
		public static const F10:uint = 121;
		public static const F11:uint = 122;
		public static const F12:uint = 123;
		public static const F13:uint = 124;
		public static const F14:uint = 125;
		public static const F15:uint = 126;
		public static const F2:uint = 113;
		public static const F3:uint = 114;
		public static const F4:uint = 115;
		public static const F5:uint = 116;
		public static const F6:uint = 117;
		public static const F7:uint = 118;
		public static const F8:uint = 119;
		public static const F9:uint = 120;
		public static const HOME:uint = 36;
		public static const INSERT:uint = 45;
		public static const LEFT:uint = 37;
		public static const LEFTBRACKET:uint = 219;
		public static const MINUS:uint = 189;
		public static const NUMBER_0:uint = 48;
		public static const NUMBER_1:uint = 49;
		public static const NUMBER_2:uint = 50;
		public static const NUMBER_3:uint = 51;
		public static const NUMBER_4:uint = 52;
		public static const NUMBER_5:uint = 53;
		public static const NUMBER_6:uint = 54;
		public static const NUMBER_7:uint = 55;
		public static const NUMBER_8:uint = 56;
		public static const NUMBER_9:uint = 57;
		public static const NUMPAD:uint = 21;
		public static const NUMPAD_0:uint = 96;
		public static const NUMPAD_1:uint = 97;
		public static const NUMPAD_2:uint = 98;
		public static const NUMPAD_3:uint = 99;
		public static const NUMPAD_4:uint = 100;
		public static const NUMPAD_5:uint = 101;
		public static const NUMPAD_6:uint = 102;
		public static const NUMPAD_7:uint = 103;
		public static const NUMPAD_8:uint = 104;
		public static const NUMPAD_9:uint = 105;
		public static const NUMPAD_ADD:uint = 107;
		public static const NUMPAD_DECIMAL:uint = 110;
		public static const NUMPAD_DIVIDE:uint = 111;
		public static const NUMPAD_ENTER:uint = 108;
		public static const NUMPAD_MULTIPLY:uint = 106;
		public static const NUMPAD_SUBTRACT:uint = 109;
		public static const PAGE_DOWN:uint = 34;
		public static const PAGE_UP:uint = 33;
		public static const PERIOD:uint = 190;
		public static const QUOTE:uint = 222;
		public static const RIGHT:uint = 39;
		public static const RIGHTBRACKET:uint = 221;
		public static const SEMICOLON:uint = 186;
		public static const SHIFT:uint = 16;
		public static const SLASH:uint = 191;
		public static const SPACE:uint = 32;
		public static const TAB:uint = 9;
		public static const UP:uint = 38;
	}	
}
