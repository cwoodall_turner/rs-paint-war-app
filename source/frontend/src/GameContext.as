package {
	
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.geom.Point;

	public class GameContext {
		
		private static var _basePath			: String	= "";
		private static var _lang				: String	= "";
		private static var _stage				: Stage		= null;
		private static var _gameSprite			: Sprite	= null;
		private static var _globalScreenPos		: Point		= null;
		private static var _gameWidth			: int		= 600;
		private static var _gameHeight			: int		= 400;
		private static var _debugMode			: Boolean	= false;
		
		
		public function GameContext() {	}
		
		
		public static function Initialize(gameSprite:Sprite, width:int, height:int, debugMode:Boolean, lang:String) :void {
			
			_gameSprite			= gameSprite;
			_stage				= gameSprite.stage;
			_globalScreenPos	= _gameSprite.localToGlobal(new Point(0,0) );
			_gameWidth			= width;
			_gameHeight			= height;
			_debugMode			= debugMode;
			_lang				= lang;
		}
		
		
		public static function get BasePath():String { return _basePath; }
		public static function set BasePath(value:String):void {	_basePath = value;	}
		
		public static function get Lang():String { return _lang; }
		static public function set Lang(value:String):void {	_lang = value;	}
		
		public static function get GlobalStage():Stage { return _stage; }
		
		public static function get GameSprite():Sprite { return _gameSprite; }
		
		static public function get GlobalScreenPos():Point { return _globalScreenPos; }
		
		static public function get GameWidth():int { return _gameWidth; }
		
		static public function get GameHeight():int { return _gameHeight; }
		
		static public function get DebugMode():Boolean { return _debugMode; }
		
		
		
		/*CALCULAR STAGE "LOCAL"*/
		/*
		 * 		var containerPos	: Point	= null;
				if (GameContext.GameSprite.parent != null) {
					containerPos	= GameContext.GameSprite.parent.localToGlobal(new Point);
				} else {
					containerPos	= new Point();
				}
				var localStagePos	: Point	= new Point(e.stageX - containerPos.x, e.stageY - containerPos.y );
		 * */
	}
}
