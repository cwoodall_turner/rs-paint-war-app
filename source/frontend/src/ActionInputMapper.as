﻿package
{
/**
	 * @author Ariel Nehmad
	 */
	public class ActionInputMapper
	{
		public static const	UP			: int	= 0;
		public static const	DOWN		: int	= 1;
		public static const	LEFT		: int	= 2;
		public static const	RIGHT		: int	= 3;
		public static const	ACTION_1	: int	= 4;
		public static const	ACTION_2	: int	= 5;
		public static const	ACTION_3	: int	= 6;
		
		private static var _player1Keys	: Vector.<int>	= null;
		private static var _player2Keys	: Vector.<int>	= null;
		
		public static function SetNumberOfPlayers( numPlayers:int ):void
		{
			if (numPlayers == 1) {
				_player1Keys	= new Vector.<int>();
				// UP
				_player1Keys.push(Input.UP);
				// DOWN
				_player1Keys.push(Input.DOWN);
				// LEFT
				_player1Keys.push(Input.LEFT);
				// RIGHT
				_player1Keys.push(Input.RIGHT);
				// ACTION_1
				_player1Keys.push(Input.Z);
				// ACTION_2
				_player1Keys.push(Input.X);
				// ACTION_3
				_player1Keys.push(Input.C);
			} else {
				// PLAYER 1
				_player1Keys	= new Vector.<int>();
				// UP
				_player1Keys.push(Input.S);
				// DOWN
				_player1Keys.push(Input.X);
				// LEFT
				_player1Keys.push(Input.Z);
				// RIGHT
				_player1Keys.push(Input.C);
				// PUNCH
				_player1Keys.push(Input.Q);
				// KICK
				_player1Keys.push(Input.W);
				// JUMP
				_player1Keys.push(Input.E);
				// PLAYER 2
				_player2Keys	= new Vector.<int>();
				// UP
				_player2Keys.push(Input.UP);
				// DOWN
				_player2Keys.push(Input.DOWN);
				// LEFT
				_player2Keys.push(Input.LEFT);
				// RIGHT
				_player2Keys.push(Input.RIGHT);
				// ACTION_1
				_player2Keys.push(Input.J);
				// ACTION_2
				_player2Keys.push(Input.K);
				// ACTION_3
				_player2Keys.push(Input.L);
			}
		}
		
		public static function keyDown( action:int, playerNumber:int ):Boolean 
		{
			// keyCode.
			if (playerNumber == 1) return Input.keyDown(_player1Keys[action]);
			else if (_player2Keys) return Input.keyDown(_player2Keys[action]);
			return false;
		}
		
		public static function keyHit( action:int, playerNumber:int ):Boolean 
		{
			// keyCode.
			if (playerNumber == 1) return Input.keyHit(_player1Keys[action]);
			else if (_player2Keys) return Input.keyHit(_player2Keys[action]);
			return false;
		}
		
		public static function keyForAction( action:int, playerNumber:int):int {
			if (playerNumber == 1) return _player1Keys[action];
			else if (_player2Keys) return _player2Keys[action];
			return Input.ESCAPE;
		}
	}	
}
