package {
	
	/**
	 * ...
	 * @author alfred
	 */
	public class ObjectPoolLRU {
		
		private var _objects 	: Vector.<Object>
		private var _size		: int = 0;
		private var _currentIdx	: int = 0;
		
		public function ObjectPoolLRU()	{
			_objects = new Vector.<Object>();
			_size = 0;
		}
		
		public function Fill(size:uint, C:Class, parms:Array = null):void {
			
			_size = size;
			var i : int = 0;
			if (parms == null) {
				for (i = 0; i < size; i++ ) {
					_objects.push(new C());
				}
			} else {
				for (i = 0; i < size; i++ ) {
					_objects.push(new C(parms[i]));
				}
			}
		}
		
		public function GetNext() : Object {
			var o : Object = _objects[_currentIdx];
			if (++_currentIdx >= _size) _currentIdx = 0;
			return o;
		}
		
		public function Add(o:Object):void {
			_objects.push(o);
			_size++;
		}
		
		public function destroy():void {
			_objects = null;
		}
		
		/**
		 * Helper method for applying a function to all objects in the pool.
		 * 
		 * @param func The function's name.
		 * @param args The function's arguments.
		 */
		public function initialze(func:String, args:Array):void {
			
			for (var i:int = 0; i < _size; i++ ) {
				_objects[i][func].apply(_objects[i], args);
			}
		}
		
		public function get objects():Vector.<Object> 
		{
			return _objects;
		}
	}
	
	
	
	
}