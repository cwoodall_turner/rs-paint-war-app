package view 
{
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.display.MovieClip;
import flash.utils.Dictionary;

import fx.Fx;

import utils.MovieClipUtils;

//import fx.FxGobjAsset;
/**
	 * ...
	 * @author ...
	 */
	public class GameObjectToScreenMapper 
	{
		private var _gameCanvas		: DisplayObjectContainer	= null;
		private var _shadowsCanvas	: DisplayObjectContainer	= null;
		private var _gameObjects	: Array						= null;
		
		private var _fxAssets	: Dictionary	= null;
		
		private static var _instance	: GameObjectToScreenMapper	= null;
		
		public function GameObjectToScreenMapper(gameCanvas:DisplayObjectContainer, shadowsCanvas:DisplayObjectContainer) 
		{
			_instance		= this;
			_gameCanvas		= gameCanvas;
			_shadowsCanvas	= shadowsCanvas;
			_gameObjects	= new Array();
			_fxAssets		= new Dictionary(true);
			
			//FxGobjAsset.addEventListener(FxAsset.NEW_FX, showFx);
			//Debugger.Log("__FX__ FxAsset.addEventListener");
		}
		
		public function AddGameObject(gObj:GameObject):void {
			if(_gameObjects.indexOf(gObj) >= 0) {
				Debugger.Log("WARNING: Trying to add gObj " + gObj + " for the 2nd time");
				return;
			}
			_gameObjects.push(gObj);
			_gameCanvas.addChild(gObj.View);
			if (gObj.shadow) {
				_shadowsCanvas.addChild(gObj.shadow);
			}
			//Debugger.Log("PERF -- GAME OBJECT ADDED (" + gObj + ") - Num Children in Game Canvas: " + _gameCanvas.numChildren + "  --  Num Children in Shadow Canvas: " + _shadowsCanvas.numChildren + "  -- Num Game Objects: " + _gameObjects.length);
		}
		
		public function RemoveGameObject(gObj:GameObject):void {
			if (gObj == null) return;
			
			var idx	: int = _gameObjects.indexOf(gObj);
			//Debugger.Log("RemoveGameObject " + gObj + " - idx: " + idx);
			
			MovieClipUtils.Remove(gObj.View);
			if (gObj.shadow) {
				MovieClipUtils.Remove(gObj.shadow);
			}
			
			if (idx < 0) {
				Debugger.Log("WARNING: Trying to remove not added gameObject " + gObj);
				return;
			}
			
			_gameObjects.splice(idx, 1);
			//Debugger.Log("PERF -- GAME OBJECT REMOVED (" + gObj + ") - Num Children in Game Canvas: " + _gameCanvas.numChildren + "  --  Num Children in Shadow Canvas: " + _shadowsCanvas.numChildren + "  -- Num Game Objects: " + _gameObjects.length);
		}
		
		public function Update():void {
			_gameObjects.sortOn("z", Array.NUMERIC | Array.DESCENDING);
			var i 	: int	= _gameObjects.length;
			while (i--) {
				var gObj : GameObject	= _gameObjects[i] as GameObject;
				gObj.View.x = gObj.x;
				gObj.View.y = gObj.y + gObj.z;
				
				var shadow	: MovieClip	= gObj.shadow;
				if (shadow) {
					shadow.x = gObj.x;
					shadow.y = Config.floor_y + gObj.z;
					
					//var scaleSign : Number = gObj.IsFacingRight?1: -1;
					//var dZ : Number = gObj.z - shadow.z;
					//if (dZ <= 0) {
						//shadow.alpha = Config.shadow_max_alpha;
						//shadow.scaleX = shadow.scaleY = 1;
					//} else {
						//shadow.alpha 	= Config.shadow_max_alpha - dZ * Config.shadow_max_alpha / Config.shadow_max_z;
						//shadow.scaleX	= 1 - (1 - Config.shadow_min_scale_x) * dZ / Config.shadow_max_z;
						//shadow.scaleY	= 1 - (1 - Config.shadow_min_scale_y) * dZ / Config.shadow_max_z;
					//}
					//shadow.scaleX *= scaleSign;
				}
				
				// Depth
				if (gObj.View.parent) gObj.View.parent.addChild(gObj.View);
			}
		}
		
		public function Reset():void {
			/*
			var i : int = _gameObjects.length;
			while (i--) {
				var gObj : GameObject	= _gameObjects[i] as GameObject;
				if (gObj.removeFromCanvasOnReset) {
					RemoveGameObject(gObj);
					// TODO: Sacar sombra?
				}
			}
			*/
			
			// REMOVE FX ASSETS
			if (_fxAssets) {
				for each (var fxLRU	: ObjectPoolLRU in _fxAssets)
				{
					for each (var fx: Fx in fxLRU.objects)
					{
						//RemoveGameObject(fx);
					}
					fxLRU.destroy();
				}
			}
			_fxAssets	= new Dictionary(true);
			
			Debugger.Log("RESETTING SCREEN MAPPER - POST");
			Debugger.Log("==================");
			Debugger.Log("GAME OBJECTS (" + _gameObjects.length + "): ");
			Debugger.Log("==================");
			for each(var gObj:GameObject in _gameObjects)
			{
				Debugger.Log("  ------> " + gObj);
				
			}
			Debugger.Log("==================");
			Debugger.Log("GAME CANVAS CHILDREN (" + _gameCanvas.numChildren + "): ");
			Debugger.Log("==================");
			var i : int = _gameCanvas.numChildren;
			while (i--)
			{
				var gObjView:DisplayObject = _gameCanvas.getChildAt(i);
				Debugger.Log("  ------> " + gObjView);
			}
			Debugger.Log("==================");
			Debugger.Log("SHADOW CANVAS CHILDREN (" + _shadowsCanvas.numChildren + "): ");
			Debugger.Log("==================");
			i = _shadowsCanvas.numChildren;
			while (i--)
			{
				var gObjShadowView:DisplayObject = _shadowsCanvas.getChildAt(i);
				Debugger.Log("  ------> " + gObjShadowView);
			}
		}
		
		public function PrepareForLevel(levelNumber:int):void {
			Debugger.Log("HIDE (" + levelNumber + "): " + ConfigLoader.Instance.Config["level_" + levelNumber + "_hide_shadows"]);
			var hideShadows: Boolean	= (ConfigLoader.Instance.Config["level_" + levelNumber + "_hide_shadows"] == "true");
			Debugger.Log("hideShadows: " + hideShadows);
			_shadowsCanvas.visible = !hideShadows;
		}
		
		
		static public function get instance():GameObjectToScreenMapper 
		{
			return _instance;
		}
		
		public function get gameCanvas():DisplayObjectContainer 
		{
			return _gameCanvas;
		}
		
		
		public function ShowHitFx(attacker:GameObject, victim:GameObject):void {
			
			// TMP - No muestro hitFx, lo ponemos en el objeto golpeado para que no aparezca en cualquier lugar.
			return;
			
			/*
				var fxRefAsset	: MovieClip	= attacker.hitFxRef;
				if (!fxRefAsset) {
					Debugger.Log("Warning: Trying to show Hit Fx but no ref found for " + attacker);
					return;
				}
				//var fxYPos	: Number	= (attacker.y > victim.y)?attacker.y + 1:victim.y + 1;
				var fxZPos	: Number	= (attacker.z > victim.z)?attacker.z + 1:victim.z + 1;
				//fxRefAsset.stop();
				
				var scaleFactor	: int = 1;
				var parent	: DisplayObjectContainer	= fxRefAsset.parent;
				while (parent) {
					if (parent.scaleX < 0) scaleFactor *= -1;
					parent = parent.parent;
				}
				var fxScaleX	: Number	= fxRefAsset.scaleX;
				var fxScaleY	: Number	= fxRefAsset.scaleY;
				
				//var newPosition	: Point	= MovieClipUtils.GetRelativePos(fxRefAsset, _gameCanvas);
				var newPosition	: GameAreaPoint	= Camera.Instance.ConvertScreenPosToGamePosWithFixedZ(MovieClipUtils.ScreenPosition(fxRefAsset), fxZPos);
				
				MovieClipUtils.Remove(fxRefAsset);
				
				var constructor	: Object		= (fxRefAsset as Object).constructor;
				var fxAssetName	: String	=	"GolpeAsset";
				var fxLRU		: ObjectPoolLRU	= _fxAssets[fxAssetName];
				var fxToShow	: FxHit			= null;
				//var fxAsset		: MovieClip		= null;
				if (!fxLRU) {
					Debugger.Log("SHOW FX [" + fxAssetName + " - NO HAY LRU - Creo LRU");
					fxLRU		= new ObjectPoolLRU();
					//fxAsset		= ExternalLoader.Instance.GetNewInstance(fxAssetName, "ben.swf") as MovieClip; //TMP
					fxToShow	= new FxHit(fxRefAsset);
					fxLRU.Add(fxToShow);
					AddGameObject(fxToShow);
					_fxAssets[fxAssetName]	= fxLRU;
				} else {
					fxToShow = fxLRU.GetNext() as FxHit;
					if (!fxToShow.ready) {
						Debugger.Log("SHOW FX [" + fxAssetName + " - HAY LRU - NO hay Fx sin usar - Creo Fx");
						//fxAsset		= ExternalLoader.Instance.GetNewInstance(fxAssetName, "ben.swf") as MovieClip; //TMP
						fxToShow	= new FxHit(fxRefAsset);
						fxLRU.Add(fxToShow);
						AddGameObject(fxToShow);
					} else {
						Debugger.Log("SHOW FX [" + fxAssetName + " - HAY LRU y Hay Fx sin usar");
					}
				}
				
				// Place & Show Fx
				if (!fxToShow) {
					Debugger.Log("WARNING! - No Fx To Show [" + fxAssetName + "]");
					return;
				}
				//Debugger.Log("Fx New Local Pos: " + StringUtils.ToCoord(newPosition.x, newPosition.y) + " - ScaleFactor: " + scaleFactor + " -- fxScaleX: " + fxScaleX + " - fxScaleY: " + fxScaleY);
				fxToShow.View.scaleX	= fxScaleX;
				fxToShow.View.scaleY	= fxScaleY;
				if (scaleFactor < 0 && fxToShow.View.scaleX > 0) fxToShow.View.scaleX *= -1;
				else if (scaleFactor > 0 && fxToShow.View.scaleX < 0) fxToShow.View.scaleX *= -1;
				fxToShow.Show(newPosition.x, newPosition.y, newPosition.z);
			*/
		}
		/*
		private function showFx(fxEvent:FxEvent):void {
			//Debugger.Log("__FX__ Showing FX "  + fxEvent + "  --  " + fxEvent.fxAsset + " ( " + fxEvent.fxAsset.name + ")");
			var fxAsset	: FxAsset	= fxEvent.fxAsset;
			fxAsset.stop();
			var parent	: DisplayObjectContainer	= fxAsset.parent;
			var rotation		: Number = fxAsset.rotation;
			var scaleFactorX	: Number = fxAsset.scaleX;
			var scaleFactorY	: Number = fxAsset.scaleY;
			//Debugger.Log("parent: " + parent);
			while (parent) {
				//Debugger.Log("parent.scaleX: " + parent.scaleX);
				scaleFactorX	*= parent.scaleX;
				scaleFactorY	*= parent.scaleY;
				rotation		+= parent.rotation;
				parent = parent.parent;
			}
			var fxScaleX	: Number	= fxAsset.scaleX;
			var fxScaleY	: Number	= fxAsset.scaleY;
			
			// TODO: Por ahora, sólo en el piso
			//var newPosition	: Point	= MovieClipUtils.GetRelativePos(fxAsset, _gameCanvas);
			var fxYPos	: Number	= Config.floor_y;
			//Debugger.Log("MovieClipUtils.ScreenPosition(fxAsset): " + MovieClipUtils.ScreenPosition(fxAsset));
			var newScreenPosition	: Point	= MovieClipUtils.ScreenPosition(fxAsset);
			var newPosition	: GameAreaPoint	= Camera.Instance.ConvertScreenPosToGamePos(newScreenPosition, fxYPos);
			var newScreenPosX	: Number	= newScreenPosition.x;
			var newScreenPosY	: Number	= newScreenPosition.y;
			var newPosX	: Number	= newPosition.x;
			var newPosY	: Number	= newPosition.y;
			var newPosZ	: Number	= newPosition.z;
			
			MovieClipUtils.Remove(fxAsset);
			
			var constructor	: Object		= (fxAsset as Object).constructor;
			var fxLRU		: ObjectPoolLRU	= _fxAssets[constructor];
			var fxToShow	: Fx			= null;
			if (!fxLRU) {
				//Debugger.Log("SHOW FX [" + constructor + " - NO HAY LRU - Creo LRU");
				fxLRU		= new ObjectPoolLRU();
				fxToShow	= new Fx(fxAsset, null, "FX-" + getQualifiedClassName(fxAsset));
				fxLRU.Add(fxToShow);
				AddGameObject(fxToShow);
				_fxAssets[constructor]	= fxLRU;
			} else {
				fxToShow = fxLRU.GetNext() as Fx;
				if (!fxToShow.ready) {
					//Debugger.Log("SHOW FX [" + constructor + " - HAY LRU - NO hay Fx sin usar - Creo Fx");
					fxToShow	= new Fx(fxAsset, null, "FX-" + getQualifiedClassName(fxAsset));
					fxLRU.Add(fxToShow);
					AddGameObject(fxToShow);
				} else {
					//Debugger.Log("SHOW FX [" + constructor + " - HAY LRU y Hay Fx sin usar");
				}
			}
			
			// Place & Show Fx
			if (!fxToShow) {
				Debugger.Log("WARNING! - No Fx To Show [" + getQualifiedClassName(fxAsset) + "]");
				return;
			}
			//Debugger.Log("Fx New Pos: " + newPosition);
			//Debugger.Log("Fx New Local Pos: " + StringUtils.ToCoord(newPosition.x, newPosition.y) + " - scaleFactorX: " + scaleFactorX  + " - scaleFactorY: " + scaleFactorY + " -- fxScaleX: " + fxScaleX + " - fxScaleY: " + fxScaleY + " Floor Y: " + Config.floor_y);
			fxToShow.View.scaleX	= fxScaleX * scaleFactorX;
			fxToShow.View.scaleY	= fxScaleY * scaleFactorY;
			fxToShow.View.rotation	= rotation;
			//if (scaleFactor < 0 && fxToShow.View.scaleX > 0) fxToShow.View.scaleX *= -1;
			//else if (scaleFactor > 0 && fxToShow.View.scaleX < 0) fxToShow.View.scaleX *= -1;
			
			// Para corregir que se muestra primero en el último frame anterior
			//Debugger.Log("newScreenPosition: " + newScreenPosition);
			fxToShow.View.x = newScreenPosX;
			fxToShow.View.y = newScreenPosY;
			fxToShow.Show(newPosX, newPosY, newPosZ);
		}
		*/
	}

}
