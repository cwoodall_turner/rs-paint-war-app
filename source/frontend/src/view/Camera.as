﻿package view{

import com.greensock.TweenLite;
import com.greensock.TweenMax;
import com.greensock.easing.EaseLookup;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.DisplayObjectContainer;
import flash.display.MovieClip;
import flash.events.TimerEvent;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Timer;

import players.Player;

import scroll.ScrollableScreenElement;

import utils.StringUtils;

/**
	 * ...
	 * @author alfred
	 */
	public class Camera {
		
		private static var _instance	:Camera = null;
		
		/* Scrollable Layers */
		private var _scrollableLayers	: Vector.<ScrollableScreenElement>		= null;
		
		/* Scroll config */
		private var _scrollFrozen	: Boolean	= false;
		
		/* Background Bitmap */
		protected 	var _backgroundBitmap	: Bitmap	= null;
		private 	var _extraFunction		: Function	= null;
		private 	var _defaultParent		: DisplayObjectContainer	= null;
		
		/* Position */
		protected var _x		: Number	= 0;
		protected var _y		: Number	= 0;
		protected var _width	: Number	= 0;
		protected var _height	: Number	= 0;
		
		protected var _screenMarginLeft		: Number	= 100;
		protected var _screenMarginRight	: Number	= 100;
		protected var _screenMarginTop		: Number	= 100;
		protected var _screenMarginBottom	: Number	= 100;
		
		/* Aux. Positions */
		//protected var _playerGameAreaPos				: GameAreaPoint	= new GameAreaPoint();
		//protected var _playerScreenPos					: Point	= new Point();
		protected var _playerGameAreaPosAtFloorLevel	: GameAreaPoint	= new GameAreaPoint();
		protected var _playerScreenPosAtFloorLevel		: Point	= new Point();
		
		/* Scroll */
		protected var _maxScrollStepPx				: Number	= 10;
		protected var _timeToStartScrollingMs		: int 		= 0;
		protected var _timeLeftToStartScrollingMs	: int 		= 0;
		protected var _lastFacing 					: String	= "";
		
		/* Return positions */
		protected var _screenPosToReturn		: Point			= new Point();
		protected var _gameAreaPointToReturn	: GameAreaPoint = new GameAreaPoint();
		
		/* Pan */
		private var _freeMoving:Boolean	= false;
		private var _panToXY:Point		= null;
		
		/* Destination using player pos */
		private var _destinationPos : Point	= null;
		
		/**
		 * Constructor.
		 * 
		 * @param   blocker 
		 */
		public function Camera(blocker :SingletonBlocker = null) {
			
			if(blocker == null)
				throw new Error("Public construction not allowed. Use Instance");
			
			_backgroundBitmap = new Bitmap();
		}
		
		/**
		* Singleton implementation.
		*/
		public static function get Instance() :Camera {
		
			if(!(Camera._instance is Camera))
				Camera._instance = new Camera(new SingletonBlocker());
				
			return _instance;
		}
		
		
		public function Initialize():void {
			
			_backgroundBitmap	= new Bitmap();
			_defaultParent	= LayerFactory.Instance.GetLayer(LayerFactory.HUD);
			_defaultParent.addChildAt(_backgroundBitmap,0);
			_backgroundBitmap.visible = false;
			_scrollableLayers	= new Vector.<ScrollableScreenElement>();
			
			_width			= Config.game_width;
			_height			= Config.game_height;
			
			_screenMarginLeft	= ConfigLoader.Instance.Config["screen_margin_left"];
			_screenMarginRight	= ConfigLoader.Instance.Config["screen_margin_right"];
			_screenMarginTop	= ConfigLoader.Instance.Config["screen_margin_top"];
			_screenMarginBottom	= ConfigLoader.Instance.Config["screen_margin_bottom"];
			
			_maxScrollStepPx	= ConfigLoader.Instance.Config["max_scroll_step_px"];
			
			_timeToStartScrollingMs	= ConfigLoader.Instance.Config["time_to_start_scrolling_ms"];
			
			_destinationPos	= new Point();
		}
		
		public function PrepareLevel(levelNumber:int):void {
			
			//var initX	: Number	= ConfigLoader.Instance.Config["camera_init_x_level_" + levelNumber.toString()];
			//var initY	: Number	= ConfigLoader.Instance.Config["camera_init_y_level_" + levelNumber.toString()];
			//SetPos(initX, initY, true);
			Update(33, true, true);
			
			
			var scrollFrozenTime	: Number = ConfigLoader.Instance.Config["init_scroll_freeze_time_ms_level_" + levelNumber.toString()];
			if (!isNaN(scrollFrozenTime) && scrollFrozenTime > 0) {
				Debugger.Log("scrollFrozenTime: " + scrollFrozenTime);
				FreezeScroll();
				var tmpTimer	: Timer	= new Timer(scrollFrozenTime, 1);
				tmpTimer.addEventListener(TimerEvent.TIMER, doUnfreeze);
				tmpTimer.start();
			}
			
			_timeLeftToStartScrollingMs	= _timeToStartScrollingMs;
			_lastFacing					= "";
		}
		
		private function doUnfreeze(e:TimerEvent):void 
		{
			e.currentTarget.removeEventListener(TimerEvent.TIMER, doUnfreeze);
			UnfreezeScroll();
		}
		
		public function Update(dtMs:int, immediateScroll:Boolean = false, forceFacingOffset:Boolean = false) : void {
			//Debugger.Log("Camera Update - _panToXY: " + _panToXY + "  --  freeMoving: " + freeMoving);
			if (_panToXY) UpdatePanToXY(dtMs);
			if (!freeMoving) {
				UpdateOnePlayer(dtMs, immediateScroll, forceFacingOffset);
				//if (GameManager.Instance.numactivePlayers == 1) UpdateOnePlayer(dtMs, immediateScroll);
				//else UpdateTwoPlayers(dtMs, immediateScroll);
			}
		}
		
		
		/**
		 * Updates Camera position till it gets to _panToXY
		 * 
		 * @param	dtMs
		 */
		private function UpdatePanToXY(dtMs:int, immediateScroll:Boolean = false) : void {
			
			var cameraNewX	: Number	= _panToXY.x;
			var cameraNewY	: Number	= _panToXY.y;
			
			if (cameraNewX != leftX || cameraNewY != upY) {
				//SetPos(cameraNewX, cameraNewY, immediateScroll);
			} else {
				_panToXY = null;
			}
		}
		
		/**
		 * Updates Camera position -> if ben passes screen limits, it starts scrolling
		 * 
		 * @param	dtMs
		 */
		//public function Update(dtMs:int, velocity:Number) : void {
		private function UpdateOnePlayer(dtMs:int, immediateScroll:Boolean = false, forceFacingOffset:Boolean = false) : void {
			
			if (_scrollFrozen) return;
			
			// CAMERA SCROLL TO FACING SIDE
			var thePlayer	: Player	= GameManager.Instance.activePlayer;
			var nowFacing:String = thePlayer.IsFacingRight?"right":thePlayer.IsFacingLeft?"left":thePlayer.IsFacingBack?"back":"front";
			if (_lastFacing != nowFacing)
			{
				_lastFacing = nowFacing;
				_timeLeftToStartScrollingMs = _timeToStartScrollingMs;
			}
			if (_timeLeftToStartScrollingMs >= 0)
			{
				_timeLeftToStartScrollingMs -= dtMs;
			}
			
			updateDestinationPoint(forceFacingOffset || (_timeLeftToStartScrollingMs <= 0));
			
			if (_destinationPos.x != leftX || _destinationPos.y != upY) {
				SetPos(_destinationPos.x, _destinationPos.y, immediateScroll);
			}
			//Debugger.Log("==== CAMERA UPDATE ===");
			//Debugger.Log("^^^^^^^^^^^^^^^^^^^^^^");
			//Debugger.Log("PlayerPos: "  + StringUtils.ToCoord(GameManager.Instance.activePlayer.x, GameManager.Instance.activePlayer.y, 1) + "PlayerScreenPos: "  + StringUtils.ToCoord(_playerScreenPosAtFloorLevel.x, _playerScreenPosAtFloorLevel.y, 1) + "Camera: " + this + " Camera limits: " + cameraLimits);
		}
		
		
		private function updateDestinationPoint(useFacingOffset:Boolean = true):void 
		{
			//Debugger.Log("useFacingOffset: " + useFacingOffset);
			var thePlayer	: Player	= GameManager.Instance.activePlayer;
			
			_playerGameAreaPosAtFloorLevel.x = thePlayer.xForCamera;
			_playerGameAreaPosAtFloorLevel.y = Config.floor_y;
			_playerGameAreaPosAtFloorLevel.z = thePlayer.zForCamera;
			
			_playerScreenPosAtFloorLevel	= ConvertGamePosToScreenPos(_playerGameAreaPosAtFloorLevel);
			
			var cameraLimits	: Absolute2DArea	= GameManager.Instance.currentLevel.cameraLimits;
			
			//Debugger.Log("VVVVVVVVVVVVVVVVVVVVVV");
			//Debugger.Log("==== CAMERA UPDATE ===");
			//Debugger.Log("_playerGameAreaPosAtFloorLevel: " + _playerGameAreaPosAtFloorLevel);
			//Debugger.Log("_playerScreenPosAtFloorLevel: " + _playerScreenPosAtFloorLevel);
			//Debugger.Log("_screenMarginLeft: " + _screenMarginRight + "   ---   _screenMarginRight: " + _screenMarginRight);
			//Debugger.Log("cameraLimits: " + cameraLimits);
			//Debugger.Log("Camera Pos: " + StringUtils.ToCoord(cameraNewX, cameraNewY));
			//Debugger.Log("CAMERA UPD - _playerScreenPosAtFloorLevel: " + _playerScreenPosAtFloorLevel + " -- cameraLimits: " + cameraLimits);
			
			if (GameManager.Instance.currentLevel.scenery.hasScroll) {
				if(useFacingOffset)
				{
					if (GameManager.Instance.activePlayer.IsFacingRight)
					{
						// FACING RIGHT
						// Target X: Player's left
						_destinationPos.x	= _playerGameAreaPosAtFloorLevel.x - _screenMarginLeft;
					}
					else if (GameManager.Instance.activePlayer.IsFacingLeft)
					{
						// FACING LEFT
						// Target X: Player's right
						_destinationPos.x	= _playerGameAreaPosAtFloorLevel.x - width + _screenMarginRight;
					}
					else if (GameManager.Instance.activePlayer.IsFacingBack)
					{
						// FACING BACK
						// Target Y: Player's front
						_destinationPos.y	= Config.floor_y + _playerGameAreaPosAtFloorLevel.z - height + _screenMarginBottom;
					}
					else if (GameManager.Instance.activePlayer.IsFacingFront)
					{
						// FACING FRONT
						// Target Y: Player's back
						_destinationPos.y	= Config.floor_y + _playerGameAreaPosAtFloorLevel.z - _screenMarginTop;
					}
				}
				
				
				// MARGIN LIMITS
				// Check Left limit
				if (_playerScreenPosAtFloorLevel.x < _screenMarginLeft && _destinationPos.x > (_playerGameAreaPosAtFloorLevel.x - _screenMarginLeft)) {
					//Debugger.Log("To the left");
					//Calculate new camera X
					_destinationPos.x	= _playerGameAreaPosAtFloorLevel.x - _screenMarginLeft;
				}
				// Check right limit
				else if (_playerScreenPosAtFloorLevel.x > width - _screenMarginRight && _destinationPos.x < (_playerGameAreaPosAtFloorLevel.x - width + _screenMarginRight)) {
					//Debugger.Log("To the right");
					// Calculate new camera X
					_destinationPos.x	= _playerGameAreaPosAtFloorLevel.x - width + _screenMarginRight;
				}
				
				// Check top limit
				if (_playerScreenPosAtFloorLevel.y < _screenMarginTop && _destinationPos.y > (Config.floor_y + _playerGameAreaPosAtFloorLevel.z - _screenMarginTop)) {
					//Debugger.Log("*** TOP LIMIT");
					//Debugger.Log("To the top");
					//Calculate new camera y
					_destinationPos.y	= Config.floor_y + _playerGameAreaPosAtFloorLevel.z - _screenMarginTop;
				}
				// Check bottom limit
				else if (_playerScreenPosAtFloorLevel.y > height - _screenMarginBottom && _destinationPos.y < (Config.floor_y + _playerGameAreaPosAtFloorLevel.z - height + _screenMarginBottom)) {
					//Debugger.Log("*** BOTTOM LIMIT");
					// Calculate new camera Y
					_destinationPos.y	= Config.floor_y + _playerGameAreaPosAtFloorLevel.z - height + _screenMarginBottom;
				}
				
			}
			
			// Check camera limits
			if (_destinationPos.x < cameraLimits.leftX) _destinationPos.x = cameraLimits.leftX;
			else if (_destinationPos.x + width > cameraLimits.rightX) _destinationPos.x = cameraLimits.rightX - width;
			if (_destinationPos.y < cameraLimits.upY) _destinationPos.y = cameraLimits.upY;
			else if (_destinationPos.y + height > cameraLimits.downY) _destinationPos.y = cameraLimits.downY - height;
		}
		
		
		/**
		 * Updates Camera position -> if ben passes screen limits, it starts scrolling
		 * 
		 * @param	dtMs
		
		private function UpdateTwoPlayers(dtMs:int, immediateScroll:Boolean = false) : void {
			
			if (_scrollFrozen) return;
			
			
			var centerGameAreaX	: Number	= (GameManager.Instance.thePlayers[0].x + GameManager.Instance.thePlayers[1].x) * .5;
			var centerGameAreaZ	: Number	= (GameManager.Instance.thePlayers[0].z + GameManager.Instance.thePlayers[1].z) * .5;
			
			
			var cameraLimits	: AbsoluteRangeArea	= GameManager.Instance.currentLevel.cameraLimits;
			
			var cameraNewX	: Number	= centerGameAreaX - width * .5;
			var cameraNewY	: Number	= Config.floor_y + centerGameAreaZ - height * .5;
			
			if (cameraNewX < cameraLimits.leftX) cameraNewX = cameraLimits.leftX;
			if (cameraNewX + width > cameraLimits.rightX) cameraNewX = cameraLimits.rightX - width;
			if (cameraNewY < cameraLimits.upY) cameraNewY = cameraLimits.upY;
			if (cameraNewY + height > cameraLimits.downY) cameraNewY = cameraLimits.downY - height;
				
			
			if (cameraNewX != leftX || cameraNewY != upY) {
				SetPos(cameraNewX, cameraNewY, immediateScroll);
			}
		}
		 */
		public function SetPos(newLeftX:Number, newUpY:Number, immediateScroll:Boolean = false):void 
		{
			var dX	: Number	= newLeftX - leftX;
			var dY	: Number	= newUpY - upY;
			
			//Debugger.Log("CAMERA - SET NEW POS " + StringUtils.ToCoord(newLeftX, newUpY, 1) + " StepX: " + dX.toFixed(1) + " - StepY: " + dY.toFixed(1));
			if (!immediateScroll) {
				if (dX > 0 && dX > _maxScrollStepPx) {
					newLeftX = leftX + _maxScrollStepPx;
				} else if (dX < 0 && dX < -_maxScrollStepPx) {
					newLeftX = leftX - _maxScrollStepPx;
				}
				if (dY > 0 && dY > _maxScrollStepPx) {
					newUpY = upY + _maxScrollStepPx;
				} else if (dY < 0 && dY < -_maxScrollStepPx) {
					newUpY = upY - _maxScrollStepPx;
				}
			}
			
			_x	= newLeftX;
			_y	= newUpY;
			
			for each (var scrollableLayer :ScrollableScreenElement in _scrollableLayers) {
				scrollableLayer.SetGameLevelPos(newLeftX, newUpY);
			}
		}
		
		
		public function Reset() : void {
			for each (var scrollableLayer : ScrollableScreenElement in _scrollableLayers) {
				scrollableLayer.Reset();
			}
			SetPos(0, 0, true);
			_scrollFrozen	= false;
		}
		
		
		public function Finalize() : void {
			//Debugger.Log("CAMERA Finalize");
			//Debugger.LogStack();
			for each (var scrollableLayer : ScrollableScreenElement in _scrollableLayers) {
				scrollableLayer.Destroy();
			}
			_scrollableLayers	= new Vector.<ScrollableScreenElement>();
		}
		
		public function AddScrollableLayer( scrollableElement : ScrollableScreenElement) : void {
			Debugger.Log("CAMERA - ADD SCROLLABLE LAYER (" + scrollableElement + ")  --  Num Layers: " + _scrollableLayers.length);
			_scrollableLayers.push(scrollableElement);
		}
		
		public function RemoveScrollable( scrollableElement : ScrollableScreenElement ) : void {
			
			_scrollableLayers.splice(_scrollableLayers.indexOf(scrollableElement), 1);
		}
		
		public function FreezeScroll() 		: void	{	_scrollFrozen	= true;	}
		public function UnfreezeScroll() 	: void	{	_scrollFrozen	= false;	}
		
		
		public function get leftX():Number 
		{
			return _x;
		}
		
		public function get upY():Number 
		{
			return _y;
		}
		
		public function get rightX():Number 
		{
			return _x + width;
		}
		
		public function get downY():Number 
		{
			return _y + height;
		}
		
		public function get width():Number 
		{
			return _width;
		}
		
		public function get height():Number 
		{
			return _height;
		}
		
		
		public function ConvertScreenPosToGamePos(screenPos:Point, gamePosY:Number):GameAreaPoint {
			_gameAreaPointToReturn.x	= screenPos.x + leftX;
			_gameAreaPointToReturn.y	= gamePosY;
			_gameAreaPointToReturn.z	= upY + screenPos.y - gamePosY;
			return _gameAreaPointToReturn;
		}
		
		public function ConvertScreenPosToGamePosWithFixedZ(screenPos:Point, gamePosZ:Number):GameAreaPoint {
			_gameAreaPointToReturn.x	= screenPos.x + leftX;
			_gameAreaPointToReturn.y	= upY + screenPos.y - gamePosZ; //TODO: Probar
			_gameAreaPointToReturn.z	= gamePosZ;
			return _gameAreaPointToReturn;
		}
		
		
		public function ConvertGamePosToScreenPos(gamePos:GameAreaPoint, debug:Boolean = false):Point {
			_screenPosToReturn.x = gamePos.x - leftX;
			_screenPosToReturn.y = gamePos.y - upY + gamePos.z;
			if(debug) Debugger.Log("ConvertGamePosToScreenPos( " + gamePos + ")  --  CameraPos: " + StringUtils.ToCoord(leftX, upY) + "  --  ScreenPos: " + _screenPosToReturn);
			return _screenPosToReturn;
		}
		
		public function CaptureBackground(fixedBackground:MovieClip = null, fixedParent:DisplayObjectContainer = null):void {
			
			var localBounds	: Rectangle	= new Rectangle( -50, -50, GameContext.GameWidth + 100, GameContext.GameWidth + 100);
			var bitmapDataOriginal	: BitmapData	= new BitmapData(localBounds.width, localBounds.height, false, 0x0);
			
			var m:Matrix = new Matrix();
			m.translate( -localBounds.x, -localBounds.y);
			
			if (!fixedBackground)	bitmapDataOriginal.draw(GameContext.GameSprite, m);
			else bitmapDataOriginal.draw(fixedBackground, m);
			
			_backgroundBitmap.bitmapData	= bitmapDataOriginal;
			_backgroundBitmap.x = localBounds.x;
			_backgroundBitmap.y = localBounds.y;
			_backgroundBitmap.visible = true;
			
			if (fixedParent) fixedParent.addChildAt(_backgroundBitmap, 0);
			else _defaultParent.addChildAt(_backgroundBitmap, 0);
		}
		private var _blurTween		: TweenMax	= null;
		private var _unBlurTween	: TweenMax	= null;
		public function BlurBitmap(time:Number = 1) : void {
			if (_unBlurTween != null) {
				_unBlurTween.pause();
				_unBlurTween = null;
			}
			_blurTween	= TweenMax.to(_backgroundBitmap, time, { blurFilter: { blurX:9, blurY:9, quality:3 }} );
		}
		public function UnBlurBitmap(time:Number = .6, extraFunction:Function = null) : void {
			if (_blurTween != null) {
				_blurTween.pause();
				_blurTween = null;
			}
			_extraFunction = extraFunction;
			_unBlurTween = TweenMax.to(_backgroundBitmap, time, { blurFilter: { blurX:0, blurY:0, quality:3, remove:true}, onComplete:HideBitmap} );
		}
		public function BWBitmap(time:Number = 1) : void {
			TweenMax.to(_backgroundBitmap, time, {colorMatrixFilter:{contrast:1.2, saturation:0}});
		}
		public function UnBWBitmap(time:Number = .6) : void {
			TweenMax.to(_backgroundBitmap, time, {colorMatrixFilter:{contrast:1, saturation:1, remove:true, onComplete:HideBitmap}});
		}
		
		private function HideBitmap() :void {
			Debugger.Log("HIDE BITMAP!!! - _extraFunction: " + _extraFunction);
			if (_extraFunction is Function) _extraFunction();
			_extraFunction = null;
			_backgroundBitmap.visible = false;
		}
		
		public function get backgroundCaptured():Boolean {
			return _backgroundBitmap && _backgroundBitmap.parent && _backgroundBitmap.visible;
		}
		
		public function toString():String {
			return "[leftX: " + leftX.toFixed(1) + ", upY: " + upY.toFixed(1) + "]";
		}
		
		
		public function PanToXY($leftX:Number, $upY:Number, $time:Number = 3, $easingFunctionName: String = ""):void 
		{
			_panToXY	= new Point($leftX, $upY);
			var easingFunction	: Function	= $easingFunctionName?EaseLookup.find($easingFunctionName):TweenLite.defaultEase;
			Debugger.Log("easingFunction: " + easingFunction);
			TweenLite.to(this, $time, { justForTweenUseX:$leftX, justForTweenUseY:$upY, ease:easingFunction } );
		}
		public function EndPan():void 
		{
			_panToXY	= null;
		}
		
		public function PanToPlayerPos():void 
		{
			var timeToPan 			: Number	= Config.getValue("camera_pan_after_player_kill_time_seconds");
			var easingFunctionStr	: String	= Config.getValue("camera_pan_after_player_kill_easing_function");
			updateDestinationPoint(true);
			PanToXY(_destinationPos.x, _destinationPos.y, timeToPan, easingFunctionStr);
		}
		
		public function get freeMoving():Boolean	{
			return _freeMoving;
		}
		
		public function set freeMoving(value:Boolean):void 
		{
			_freeMoving = value;
		}
		
		public function get justForTweenUseX():Number {
			return leftX;
		}
		public function set justForTweenUseX($x:Number):void 
		{
			SetPos($x, upY, true);
		}
		
		public function set justForTweenUseY($y:Number):void 
		{
			SetPos(leftX, $y, true);
		}
		public function get justForTweenUseY():Number {
			return upY;
		}
	}
	
}

internal class SingletonBlocker { }