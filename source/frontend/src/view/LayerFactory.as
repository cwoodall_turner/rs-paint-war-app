﻿package view {

import flash.display.DisplayObjectContainer;
import flash.display.MovieClip;
import flash.utils.Dictionary;

public class LayerFactory extends MovieClip {
		
		public static const	BACKGROUND		:	String	= "background";
		public static const	SPECIALS		:	String	= "specials";
		public static const	FX_GAME_BACK	:	String	= "fxGameBack";
		public static const	GAME_COMPLEX	:	String	= "GameComplex";
		public static const	DEBUG_PATH_MAP	:	String	= "debugPathMap";
		public static const	DEBUG_CURRENT_PATH	:	String	= "debugCurrentPath";
		public static const	DEBUG_INIT_PATH	:	String	= "debugInitPath";
		public static const	GAME_SHADOWS	:	String	= "game_shadows";
		public static const	GAME			:	String	= "game";
		public static const	FX_GAME_FRONT	:	String	= "fxGameFront";
		public static const FOREGROUND		:	String 	= "foreground";
		public static const	HUD				:	String	= "hud";
		public static const	UI				:	String	= "ui";
		
		private static var _instance:LayerFactory	= null;
		
		protected var _container	: DisplayObjectContainer	= null;
		protected var _layers		: Dictionary				= null;
		protected var _initialized	: Boolean					= false;

		public function LayerFactory(lock:SingletonEnforcer){}

		public static function get Instance():LayerFactory{
			if ( _instance == null ) {
				_instance = new LayerFactory(new SingletonEnforcer());
			}
			return _instance;
		}		
		/*
		 * Initialize
		 */
		public function Initialize(container:DisplayObjectContainer, a:Array = null):void {
			if(!_initialized){
				_initialized = true;
				_container	= container;
				_layers = new Dictionary();
				var layersLength	: int	= a.length;
				for (var i:int = 0; i < layersLength; i++) {
					var key			: String	= a[i] as String;
					var subLayers	: Array 	= a[i] as Array
					if (key != null) {
						_layers[key] = new MovieClip();
						(_layers[key] as MovieClip).name = key;
						_container.addChild(_layers[key]);
					} else if (subLayers != null) {
						key	= subLayers[0] as String;
						_layers[key]	= CreateComplexLayer(subLayers.splice(1));
						_container.addChild(_layers[key]);
						(_layers[key] as MovieClip).name = key;
					}
				}
			}
		}
		
		public function CreateComplexLayer(a:Array = null) : MovieClip {
			
			var complexLayer	: MovieClip	= new MovieClip();
			var layersLength	: int	= a.length;
			for (var i:int = 0; i < layersLength; i++) {
				var key			: String	= a[i] as String;
				var subLayers	: Array 	= a[i] as Array
				if (key != null) {
					_layers[key] = new MovieClip();
					(_layers[key] as MovieClip).name = key;
					complexLayer.addChild(_layers[key]);
				} else if (subLayers != null) {
					_layers[key]	= CreateComplexLayer(subLayers);
					complexLayer.addChild(_layers[key]);
				}
			}
			
			return complexLayer;
		}
		
		public function GetLayer(layerName:String):MovieClip{
			return _layers[layerName] as MovieClip;
		}
	}
}
internal class SingletonEnforcer { }
