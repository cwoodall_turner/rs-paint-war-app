package 
{
import flash.display.DisplayObjectContainer;
import flash.display.MovieClip;
import flash.events.EventDispatcher;
import flash.utils.Dictionary;

import utils.MovieClipUtils;

/**
	 * ...
	 * @author ...
	 */
	public class GameObject extends EventDispatcher
	{
		public static const FACING_SIDE		: String	= "side";
		public static const FACING_BACK		: String	= "back";
		public static const FACING_FRONT	: String	= "front";
		
		public static const ST_IDLE		: String	= "idle";
		public static const ST_READY	: String	= "ready";
		
		private var _teamId	: String	= "1";
		
		protected var _stateMoveDefault	: String	= "walk";
		
		protected var _view				: GameObjectView	= null;
		protected var _innerMc			: MovieClip	= null;
		protected var _shadow			: MovieClip	= null;
		protected var _shadowInnerMc	: MovieClip	= null;
		protected var _x	: Number	= 0;
		protected var _y	: Number	= 0;
		protected var _z	: Number	= 0;
		protected var _prevX	: Number	= 0;
		protected var _prevY	: Number	= 0;
		protected var _prevZ	: Number	= 0;
		protected var _prevFacingRight	: Boolean	= true;
		protected var _speedX	: Number	= 0;
		protected var _speedY	: Number	= 0;
		protected var _speedZ	: Number	= 0;
		protected var _rotation	: Number	= 0;
		
		// HIT
		protected var _hitBoundariesMc	: MovieClip	= null;
		protected var _hitHeightMc		: MovieClip	= null;
		protected var _hitBoundaries	: Boundaries	= null;
		protected var _lastHitBy		: GameObject	= null;
		
		protected var _hitArea	: MovieClip	= null;
		//protected var _useInnerHitBoundaries	: Boolean	= false;
		
		// ENERGY
		protected var _energy		: Number	= 100;
		protected var _maxEnergy	: Number	= 100;
		
		// INVULNERABILITY
		protected var _invulnerabilityTimeLeftMs	: int	= -1;
		
		// CAMERA REF
		protected var _cameraRef	: MovieClip	= null;
		
		
		protected var _states		: Dictionary		= null;
		protected var _currentState	: GameObjectState	= null;
		
		/* Hit seq */
		protected var _lastHitByAttackSeq	: int	= 0;
		
		/* Attack seq */
		private var _attackSeq	: int	= 0;
		
		/* Id */
		protected var _id		: String	= "";
		
		/* Active */
		protected var _active	: Boolean	= true;
		
		/* Reuse view */
		protected var _removeFromCanvasOnReset	: Boolean	= true;
		
		/* Attack */
		protected var _canHit		: uint	= 0;
		protected var _canBeHit		: Boolean	= false;
		protected var _attackPower	: Number	= 10;
		
		/* Powerups */
		protected var _canGrabPowerups	: Boolean	= false;
		
		/* Facing */
		protected var _facing	: String	= "side";
		
		/* Target Id */
		protected var _targetId	: uint	= 0;
		
		/* Config suffix */
		protected var _configPrefix	: String	= "shot_";
		
		protected var _prevBlockedDirections	: uint	= 0;
		protected var _blockedDirections		: uint	= 0;
		
		protected var _grabbedPowerup	: Boolean	= false;
		
		protected var _heightForFire	: Number	= 0;
		
		public function GameObject(view:GameObjectView, shadow:MovieClip = null, $id:String="1") 
		{
			_view	= view;
			_shadow	= shadow;
			_id				= $id;
			_configPrefix = $id + "_";
			CreateHitBoundaries();
			
			_view.mouseChildren = false;
			_view.mouseEnabled	= false;
			
			if (_shadow) {
				_shadow.mouseChildren 	= false;
				_shadow.mouseEnabled	= false;
			}
			
			CreateStates();
		}
		
		protected function CreateStates():void 
		{
			_states	= new Dictionary();
		}
		
		protected function SetNewView(newView:GameObjectView, newShadow:MovieClip = null):void 
		{
			var prevView		: MovieClip					= _view;
			var prevViewParent	: DisplayObjectContainer	= prevView.parent;
			newView.x = prevView.x;
			newView.y = prevView.y;
			prevViewParent.addChild(newView);
			MovieClipUtils.Remove(prevView);
			_view	= newView;
			_view.mouseChildren = false;
			_view.mouseEnabled	= false;
			
			if (_shadow && newShadow) {
				var prevShadow			: MovieClip					= _shadow;
				var prevShadowParent	: DisplayObjectContainer	= prevShadow.parent;
				newShadow.x = prevShadow.x;
				newShadow.y = prevShadow.y;
				prevShadowParent.addChild(newShadow);
				MovieClipUtils.Remove(prevShadow);
				_shadow	= newShadow;
				_shadow.mouseChildren 	= false;
				_shadow.mouseEnabled	= false;
			}
			
			_innerMc	= _shadowInnerMc	= _hitBoundariesMc	= _hitArea = _hitHeightMc	= _cameraRef	=	null;
			
			GetInnerMcs();
			
			CreateHitBoundaries();
		}
		
		protected function CreateHitBoundaries():void {
			// This has to be overridden
			_hitBoundaries = new Boundaries( -30, 30, -30, 30, -1000, -900);
		}
		
		public function TeleportTo(x:Number, y:Number, z:Number):void {
			_x = x;
			_y = y;
			_z = z;
		}
		
		public function TeleportToX(x:Number):void {
			_x = x;
		}
		
		public function TeleportToY(y:Number):void {
			_y = y;
		}
		
		public function TeleportToZ(z:Number):void {
			_z = z;
		}
		
		public function Update(dtMs:int):void {
			
			// Prev Pos
			_prevX	= x;
			_prevY	= y;
			_prevZ	= z;
			_prevFacingRight	= IsFacingRight;
			
			//GetInnerMcs();
			//_currentState.Update(dtMs);
			
			// INVULNERABILITY
			if (_invulnerabilityTimeLeftMs > 0) {
				_invulnerabilityTimeLeftMs -= dtMs;
			}
			
			// Update inner hit boundaries
			//if (_useInnerHitBoundaries) {
				//RecalculateHitBoundaries();
			//}
		}
		
		protected function GetInnerMcs():void 
		{
			//Debugger.Log("Getting inner mcs");
			//Debugger.LogStack();
			//Debugger.Log("GETTING INNER MCs ANTES - _innerMc: " + _innerMc + " - _shadow: " + _shadow + " - _hitBoundariesMc: " + _hitBoundariesMc + " - _hitHeightMc: " + _hitHeightMc + " - canBeHit: " + canBeHit);
			// INNER MCs
			if (!_innerMc) {
				_innerMc = _view.getChildByName("inner_mc") as MovieClip;
			}
			if (_shadow && !_shadowInnerMc) {
				_shadowInnerMc = _shadow.getChildByName("inner_mc") as MovieClip;
			}
			
			
			// HIT BOUNDARIES
			if ((canBeHit || canStopPlayer) && !(_hitBoundariesMc && _hitHeightMc)) {
				_hitBoundariesMc	= _view.getChildByName("hitBoundaries_mc") as MovieClip;
				_hitHeightMc		= _view.getChildByName("hitHeight_mc") as MovieClip;
				_hitArea			= _view.getChildByName("hitArea_mc") as MovieClip;
				//if (_useInnerHitBoundaries) {
					//Debugger.Log("GET INNER MCS - _innerMc: " + _innerMc);
					//if (_innerMc) {
						//_hitBoundariesMc	= _innerMc.getChildByName("hitBoundariesInner_mc") as MovieClip;
						//_hitHeightMc		= _innerMc.getChildByName("hitHeightInner_mc") as MovieClip;
						//
						//var numChildren : int = _innerMc.numChildren;
						//Debugger.Log("numChildren: " + numChildren);
						//while (numChildren--) {
							//Debugger.Log("Child: " + _innerMc.getChildAt(numChildren));
						//}
						//var mc : MovieClip = _innerMc.getChildByName("rect1_mc") as MovieClip;
						//Debugger.Log("mc: " + mc);
						//MonsterDebugger.inspect(_innerMc);
						//Debugger.Log("_hitHeightMc: " + _hitHeightMc);
						//Debugger.Log("_hitBoundariesMc: " + _hitBoundariesMc);
					//}
				//} else {
					//_hitBoundariesMc	= _view.getChildByName("hitBoundaries_mc") as MovieClip;
					//_hitHeightMc		= _view.getChildByName("hitHeight_mc") as MovieClip;
				//}
			}
			
			// CAMERA REF
			if (_innerMc) {
				_cameraRef	= _innerMc.getChildByName("cameraRef_mc") as MovieClip;
			} else {
				_cameraRef	= null;
			}
		}
		
		public function MoveX(dX:Number):void {
			_x += dX;
		}
		
		public function MoveY(dY:Number):void {
			_y += dY;
		}
		
		public function MoveZ(dZ:Number):void {
			_z += dZ;
		}
		
		public function FaceRight():void {
			if (_view && _view.scaleX < 0) _view.scaleX *= -1;
			if (_shadow && _shadow.scaleX < 0) _shadow.scaleX *= -1;
			
			_facing = FACING_SIDE;
		}
		
		public function FaceLeft():void {
			if (_view && _view.scaleX > 0) _view.scaleX *= -1;
			if(_shadow && _shadow.scaleX > 0) _shadow.scaleX *= -1;
			
			_facing = FACING_SIDE;
		}
		
		public function FaceFront():void {
			if (_view && _view.scaleX < 0) _view.scaleX *= -1;
			if (_shadow && _shadow.scaleX < 0) _shadow.scaleX *= -1;
			
			_facing = FACING_FRONT;
		}
		
		public function FaceBack():void {
			if (_view && _view.scaleX < 0) _view.scaleX *= -1;
			if (_shadow && _shadow.scaleX < 0) _shadow.scaleX *= -1;
			
			_facing = FACING_BACK;
		}
		
		public function RecalculateHitBoundaries():void 
		{
			//Debugger.Log("_hitBoundariesMc: " + _hitBoundariesMc);
			//Debugger.Log("_hitHeightMc: " + _hitHeightMc);
			if (!_hitArea && (!_hitBoundariesMc || !_hitHeightMc)) {
				GetInnerMcs();
			}
			if (_hitBoundariesMc && _hitHeightMc) {
				_hitBoundaries.SetLimits(
											_hitBoundariesMc.x,
											_hitBoundariesMc.x + _hitBoundariesMc.width,
											_hitHeightMc.y - _hitHeightMc.height,
											_hitHeightMc.y,
											//-(_hitBoundariesMc.y + _hitBoundariesMc.height),
											//-_hitBoundariesMc.y
											_hitBoundariesMc.y,
											_hitBoundariesMc.y + _hitBoundariesMc.height
											);
				//Debugger.Log("_hitBoundaries (" + id + "): " + _hitBoundaries);
			} else if (_hitArea) {
				_hitBoundaries.SetLimits(
											_hitArea.x,
											_hitArea.x + _hitArea.width,
											_hitArea.y,
											_hitArea.y + _hitArea.height,
											int.MIN_VALUE,
											int.MAX_VALUE
											);
				//Debugger.Log("_hitBoundaries 2: " + _hitBoundaries);
			}
			
			//if (_useInnerHitBoundaries) {
				//_innerMc = null;
				//Debugger.Log("RECALCULATE INNER @" + _view.currentLabel);
			//}
		}
		
		public function ViewGoToAndPlay(frame:Object):void {
			_innerMc		= null;
			_shadowInnerMc	= null;
			_cameraRef		= null;
			_view.gotoAndPlay(frame);
			if(_shadow)	_shadow.gotoAndPlay(frame);
			GetInnerMcs();
		}
		
		public function ViewGoToAndStop(frame:Object):void {
			//Debugger.Log("VIEW (" + _view + ") GOTOANDSTOP - " + _view.currentLabel + "(" + _view.currentFrame + ") -> " + frame);
			
			if (_view.currentFrame == frame || _view.currentLabel == frame) return;
			
			_innerMc		= null;
			_shadowInnerMc	= null;
			_cameraRef		= null;
			_view.gotoAndStop(frame);
			if(_shadow)	_shadow.gotoAndStop(frame);
			GetInnerMcs();
		}
		
		public function InnerMcGoToAndPlay(frame:Object):void {
			if (!_innerMc) _innerMc = _view.getChildByName("inner_mc") as MovieClip;
			//Debugger.Log("_innerMc: " + _innerMc);
			if (_innerMc) {
				//Debugger.Log("_innerMc @" + _innerMc.currentFrame);
				_innerMc.gotoAndPlay(frame);
			}
			
			if (_shadow) {
				if (!_shadowInnerMc) _shadowInnerMc = _shadow.getChildByName("inner_mc") as MovieClip;
				//Debugger.Log("_shadowInnerMc: " + _shadowInnerMc + " -- frame: " + frame);
				if (_shadowInnerMc) {
					//MovieClipUtils.Remove(_shadowInnerMc);
					_shadowInnerMc.gotoAndPlay(frame);
					//Debugger.Log("PLAY! _shadowInnerMc: " + _shadowInnerMc);
				}
			}
		}
		
		public function InnerMcGoToAndStop(frame:Object):void {
			if (!_innerMc) _innerMc = _view.getChildByName("inner_mc") as MovieClip;
			if (_innerMc) _innerMc.gotoAndStop(frame);
			
			if (_shadow) {
				if (!_shadowInnerMc) _shadowInnerMc = _shadow.getChildByName("inner_mc") as MovieClip;
				if (_shadowInnerMc) {
					//Debugger.Log("++ SHADOW en " + this + " A " + frame);
					_shadowInnerMc.gotoAndStop(frame);
				}
			}
		}
		
		public function ResetInnerMc():void {
			_innerMc = null;
			_cameraRef	= null;
		}
		
		public function PassedLevelLimitLeft(limitValue:Number) : void {
			_currentState.PassedLevelLimitLeft(limitValue);
		}
		
		public function PassedLevelLimitRight(limitValue:Number) : void {
			_currentState.PassedLevelLimitRight(limitValue);
		}
		
		public function PassedLevelLimitBack(limitValue:Number) : void {
			_currentState.PassedLevelLimitBack(limitValue);
		}
		
		public function PassedLevelLimitFront(limitValue:Number) : void {
			_currentState.PassedLevelLimitFront(limitValue);
		}
		
		public function Reset():void { }
		
		public function Destroy():void {
			
			_view				= null;
			_innerMc			= null;
			_shadow				= null;
			_shadowInnerMc		= null;
			_hitBoundaries		= null;
			_hitBoundariesMc	= null;
			_hitArea			= null;
			_hitHeightMc		= null;
			_lastHitBy			= null;
			_currentState		= null;
			_cameraRef			= null;
			if (_states) {
				for each (var state:GameObjectState in _states) {
					state.Destroy();
				}
				_states	= null;
			}
		}
		
		public function IsCurrentState(isStateId:String):Boolean 
		{
			return (_currentState && _currentState == _states[isStateId]);
		}
		
		public function SetState(newStateId:String, args:Array = null):void 
		{
			if (Config.debugThisGameObject(this.id)) {
				Debugger.Log("** " + id + " - Set state [" + this + "]:::  { " + _currentState + " ==> " + newStateId + " ( " + args + " ) }");
				//Debugger.LogStack();
			} else {
				//Debugger.Log("GOBJ " + this.id + "  --  DO NOT DEBUG");
			}
			
			if (IsCurrentState(newStateId)) {
				if (_currentState.canReEnterSameState) {
					//Debugger.Log("Puede reentrar");
					if (args) {
						_currentState.ReEnterWithArgs(args);
					} else {
						_currentState.ReEnter();
					}
					return;
				} else {
					//Debugger.Log("NO puede reentrar");
					return;
				}
			}
			
			if (_currentState) {
				_currentState.Leave();
			}
			
			_currentState = _states[newStateId];
			if (args) {
				_currentState.EnterWithArgs(args);
			} else {
				_currentState.Enter();
			}
			_innerMc	= null;
			_cameraRef	= null;
			GetInnerMcs();
			RecalculateHitBoundaries();
		}
		
		
		
/* =============================================*/
/* ============= SETTERS & GETTERS =============*/
/* =============================================*/
		
		public function get ST_MOVE_DEFAULT():String 
		{
			return _stateMoveDefault;
		}
		
		public function set ST_MOVE_DEFAULT(value:String):void 
		{
			_stateMoveDefault = value;
		}
		
		public function get teamId():String 
		{
			return _teamId;
		}
		
		public function set teamId(value:String):void 
		{
			_teamId = value;
		}
		
		public function set attackPower(value:Number):void 
		{
			_attackPower = value;
		}
		
		public function get View():GameObjectView 
		{
			return _view;
		}
		
		public function get x():Number 
		{
			return _x;
		}
		
		public function get y():Number 
		{
			return _y;
		}
		
		public function get z():Number 
		{
			return _z;
		}
		
		public function get xForCamera():Number 
		{
			if (_innerMc && _cameraRef) return _x + _innerMc.x + _cameraRef.x;
			return _x;
		}
		
		public function get yForCamera():Number 
		{
			if (_innerMc && _cameraRef) return _y + _innerMc.y + _cameraRef.y;
			return _y;
		}
		
		public function get zForCamera():Number 
		{
			return _z;
		}
		public function get IsFacingRight():Boolean {
			return (_facing == FACING_SIDE && _view.scaleX > 0);
		}
		public function get IsFacingLeft():Boolean {
			return (_facing == FACING_SIDE && _view.scaleX < 0);
		}
		public function get IsFacingFront():Boolean {
			return (_facing == FACING_FRONT);
		}
		public function get IsFacingBack():Boolean {
			return (_facing == FACING_BACK);
		}
		
		public function get shadow():MovieClip 
		{
			return _shadow;
		}
		
		public function get innerMc():MovieClip 
		{
			if (!_innerMc) GetInnerMcs();
			return _innerMc;
		}
		
		public function get hitFxRef():MovieClip {
			if (!_innerMc) GetInnerMcs();
			return _innerMc.getChildByName("hitRef_mc") as MovieClip;
		}
		
		public function get speedX():Number 
		{
			return _speedX;
		}
		
		public function set speedX(value:Number):void 
		{
			_speedX = value;
		}
		
		public function get speedY():Number 
		{
			return _speedY;
		}
		
		public function set speedY(value:Number):void 
		{
			_speedY = value;
		}
		
		public function get speedZ():Number 
		{
			return _speedZ;
		}
		
		public function set speedZ(value:Number):void 
		{
			_speedZ = value;
		}
		
		public function get hitBoundaries():Boundaries 
		{
			return _hitBoundaries;
		}
		
		public function get canBeHit():Boolean {
			return (!invulnerable && _canBeHit);
		}
		
		public function set canBeHit(value:Boolean):void 
		{
			_canBeHit = value;
		}
		
		public function get canStopPlayer():Boolean {
			return false;
		}
		
		public function get canHit():uint {
			return _canHit;
		}
		
		public function set canHit(value:uint):void 
		{
			_canHit = value;
		}
		
		public function get attackBoundaries():Boundaries {
			return _currentState.attackBoundaries;
		}
		
		public function get attackPower():Number {
			return _attackPower;
		}
		
		public function get lastHitByAttackSeq():int 
		{
			return _lastHitByAttackSeq;
		}
		
		public function set lastHitByAttackSeq(value:int):void 
		{
			_lastHitByAttackSeq = value;
		}
		
		public function get attackSeq():uint 
		{
			return _attackSeq;
		}
		
		public function set attackSeq(value:uint):void 
		{
			_attackSeq = value;
		}
		
		public function get lastHitBy():GameObject 
		{
			return _lastHitBy;
		}
		
		public function get invulnerable():Boolean {
			return _invulnerabilityTimeLeftMs > 0;
		}
		
		public function get energy():Number 
		{
			return _energy;
		}
		
		public function set energy(value:Number):void 
		{
			//Debugger.Log("value: " + value);
			if (value < 0) value = 0;
			_energy = value;
		}
		
		public function get killed():Boolean {
			return _energy <= 0;
		}
		
		public function get maxEnergy():Number 
		{
			return _maxEnergy;
		}
		
		public function set maxEnergy(value:Number):void 
		{
			_maxEnergy = value;
		}
		
		public function get currentState():GameObjectState 
		{
			return _currentState;
		}
		public function SetInvulnerabilityTimeMs(timeMs:int):void {
			_invulnerabilityTimeLeftMs = timeMs;
		}
		
		public function get visible() : Boolean {
			return View.visible;
		}
		
		public function set visible(value:Boolean):void {
			View.visible	= value;
			if (shadow) shadow.visible = value;
		}
		
		public function get checkScreenLimits():Boolean {
			return _currentState.checkScreenLimits;
		}
		
		public function get checkEnterScreen():Boolean {
			return _currentState.checkEnterScreen;
		}
		
		public function get prevX():Number 
		{
			return _prevX;
		}
		
		public function get prevY():Number 
		{
			return _prevY;
		}
		
		public function get prevZ():Number 
		{
			return _prevZ;
		}
		
		public function get wasFacingRight():Boolean 
		{
			return _prevFacingRight;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function get active():Boolean 
		{
			return _active;
		}
		
		public function set active(value:Boolean):void 
		{
			_active = value;
		}
		
		public function get removeFromCanvasOnReset():Boolean 
		{
			return _removeFromCanvasOnReset;
		}
		
		public function get hitArea():MovieClip 
		{
			return _hitArea;
		}
		
		public function get rotation():Number 
		{
			return _rotation;
		}
		
		public function set rotation(value:Number):void 
		{
			_rotation = value;
			_view.rotation = _rotation;
			//if (shadow) {
				//shadow.rotation = _rotation;
			//}
		}
		
		public function get facing():String 
		{
			return _facing;
		}
		
		public function get configPrefix():String 
		{
			return _configPrefix;
		}
		
		public function set configPrefix(value:String):void 
		{
			_configPrefix = value;
		}
		
		public function get targetId():uint 
		{
			return _targetId;
		}
		
		public function get canGrabPowerups():Boolean 
		{
			return _canGrabPowerups;
		}
		
		public function set canGrabPowerups(value:Boolean):void 
		{
			_canGrabPowerups = value;
		}
		
		public function get blockedDirections():uint 
		{
			return _blockedDirections;
		}
		
		public function set blockedDirections(value:uint):void 
		{
			_blockedDirections = value;
		}
		
		public function get prevBlockedDirections():uint 
		{
			return _prevBlockedDirections;
		}
		
		public function set prevBlockedDirections(value:uint):void 
		{
			_prevBlockedDirections = value;
		}
		
		public function get heightForFire():Number 
		{
			return _heightForFire;
		}
		
		public function get grabbedPowerup():Boolean 
		{
			return _grabbedPowerup;
		}
		
		public function set grabbedPowerup(value:Boolean):void 
		{
			_grabbedPowerup = value;
		}
		
		//public function set useInnerHitBoundaries(value:Boolean):void 
		//{
			//_useInnerHitBoundaries = value;
			//_innerMc = _hitBoundariesMc	= _hitHeightMc	= null;
			//RecalculateHitBoundaries();
		//}
		
	}

}