﻿package {

import flash.utils.getTimer;

public class Debugger {
		
		private static var _debugging			: Boolean	= true;
		private static var _logCount			: int		= 1;
		
		public function Debugger() { }
		
		public static function Initialize(debugging:Boolean) : void {
			
			_debugging			= debugging;
			_logCount	= 0;
		}
		
		public static function Log(o:Object, printTime:Boolean	= false):void {
			
			if (!_debugging) return;
			
			var log : String	= o.toString();
			
			if (printTime) log	= String("@ " + getTimer() + " ms  -->  ").concat(log);
			//log	= log.concat("\n");
			
			log	= String(++_logCount).concat(") ").concat(log);
			trace(log);
		}
		
		public static function LogStack(printTime:Boolean	= false) : void {
			
			if (!_debugging) return;
			
			var e : Error	= new Error();
			var log : String	= String("============ STACK ============\n").concat(e.getStackTrace());
			if (printTime) log	= String("@ " + getTimer() + " ms  -->  ").concat(log);
			//log	= log.concat("\n===============================\n");
			log	= log.concat("\n===============================");
			log	= String(++_logCount).concat(") ").concat(log);
			trace(log);
		}
		
		public static function ResetCounter() : void {
			
			_logCount	= 0;
		}
		
		static public function set debugging(value:Boolean):void 
		{
			_debugging = value;
		}
		
		static public function get debugging():Boolean 
		{
			return _debugging;
		}
	}
	
}