package levels 
{
import obstacles.Bullseye;
import obstacles.Obstacle;
import obstacles.ObstaclesManager;

import shots.Shot;

import ui.HUDManager;

/**
	 * ...
	 * @author ...
	 */
	public class LevelBullseye extends Level
	{
		
		protected var _capturables	: Vector.<Capturable>	= null;
		
		public function LevelBullseye($number:int, $difficulty:String, $config:String) {
			super($number, $difficulty, $config);
		}
		
		override public function onBullseyePainted(bullseye:Bullseye, shot:Shot):void { 
			
			// Si lo pinta el jugador, suma puntos
			if (shot.teamId == GameManager.Instance.activePlayer.teamId)
			{
				objectiveValue++;
				HUDManager.Instance.showPlusScore(new GameAreaPoint(bullseye.x, bullseye.y, bullseye.z), 1);
				
				increaseCombo();
			}
			/*
			// Si todos los blancos están del mismo color, 1000 puntos para Gryffindor
			var allTargetsSameColor	: Boolean = true;
			var obstacle : Obstacle = null;
			for each(obstacle in ObstaclesManager.instance.theObstacles)
			{
				if (obstacle is Bullseye && !obstacle.IsCurrentState(GameObject.ST_READY))
				{
					if ((obstacle as Bullseye).paintedByTeamId != shot.teamId)
					{
						allTargetsSameColor = false;
					}
				}
			}
			
			if (allTargetsSameColor)
			{
				// Reset all bullseyes
				for each(obstacle in ObstaclesManager.instance.theObstacles)
				{
					if (obstacle is Bullseye && !obstacle.IsCurrentState(GameObject.ST_READY))
					{
						(obstacle as Bullseye).resetPaintedBy();
					}
				}
				
				// If player team painted them, score!
				if (shot.teamId == GameManager.Instance.activePlayer.teamId)
				{
					var extraScore : int = Config.getValue("all_bulseyes_extra_score");
					objectiveValue += extraScore;
				}
			}
			*/
		}
		
		override public function onBullseyePaintedTimeEnd(bullseye:Bullseye):void { 
			
			// Si lo había pintado el jugador, suma puntos
			if (bullseye.paintedByTeamId == GameManager.Instance.activePlayer.teamId)
			{
				objectiveValue++;
				HUDManager.Instance.showPlusScore(new GameAreaPoint(bullseye.x, bullseye.y, bullseye.z), 1);
			}
		}
		
		override public function prepareLevelAfterPreparingManagers():void
		{
			_capturables	= new Vector.<Capturable>();
			for each(var obstacle:Obstacle in ObstaclesManager.instance.theObstacles)
			{
				if (obstacle is Bullseye)
				{
					_capturables.push(obstacle);
				}
			}
		}
		
		public function get capturables():Vector.<Capturable> 
		{
			return _capturables;
		}
		
		override public function Destroy():void
		{
			super.Destroy();
			_capturables	= null;
		}
		
	}

}
