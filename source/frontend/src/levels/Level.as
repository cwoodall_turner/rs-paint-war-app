package levels 
{
import communication.database.DBManager;

import enemies.Enemy;

import flash.display.MovieClip;
import flash.geom.Point;
import flash.utils.Dictionary;

import obstacles.Bullseye;

import players.Player;

import powerups.Flag;
import powerups.Powerup;

import sceneries.Scenery;
import sceneries.Scenery_Regular;

import shots.Shot;

import ui.HUDManager;

import utils.MathUtils;
import utils.MovieClipUtils;

import view.Camera;
import view.GameObjectToScreenMapper;

/**
	 * ...
	 * @author ...
	 */
	public class Level 
	{
		public static const	LEVEL_MODE_1	: String	= "mode_1";
		
		protected var _number		: int		= 0;
		protected var _difficulty	: String	= "a";
		protected var _config		: String	= "1";
		protected var _scenery	: Scenery	= null;
		
		
		protected var _playerLimits	: AbsoluteRangeArea	= new AbsoluteRangeArea(-int.MAX_VALUE, int.MAX_VALUE, -int.MAX_VALUE, int.MAX_VALUE, -int.MAX_VALUE, int.MAX_VALUE);
		protected var _cameraLimits	: Absolute2DArea	= new Absolute2DArea( -int.MAX_VALUE, int.MAX_VALUE, -int.MAX_VALUE, int.MAX_VALUE);
		
		protected var _spawnAreas	: Dictionary	= null;
		
		protected var _coinsGrabbed		: int	= 0;
		protected var _objectiveValue	: int	= 0;
		
		protected var _id	: String	= "";
		protected var _teamIdToRefId	: Dictionary	= null;
		
		protected var _levelAsset	: MovieClip	= null;
		
		// COMBO
		protected var _currentCombo 		: int	= 0;
		protected var _timeLeftToEndComboMs : int	= 0;
		protected var _timeToEndComboMs 	: int	= 0;
		
		public function Level($number:int, $difficulty:String, $config:String) {
			_number		= $number;
			_difficulty	= $difficulty;
			_config 	= $config;
		}
		
		public function PrepareFromSwf():void {
			
			_id	= _number.toString() + "_" + _config;
			
			// SET TEAMS REF IDS
			_teamIdToRefId	= new Dictionary();
			var possiblePlayerTeamsStr	: String	= Config.getValue("level_" + _id + "_possible_player_teams_ref");
			if (possiblePlayerTeamsStr == null || possiblePlayerTeamsStr == "") possiblePlayerTeamsStr = "A,B,C";
			var possiblePlayerTeamsArr : Array = possiblePlayerTeamsStr.split(",");
			
			var playerTeamId	: String	= GameManager.Instance.activePlayer.teamId;
			var playerTeamRef	: String	= possiblePlayerTeamsArr[int(MathUtils.random(0, possiblePlayerTeamsArr.length - 1, true))];
			_teamIdToRefId[playerTeamId] = playerTeamRef;
			var possibleTeamRefs	: Array = ["A", "B", "C"];
			possibleTeamRefs.splice(possibleTeamRefs.indexOf(playerTeamRef), 1);
			var possibleTeamIds		: Array = ["1", "2", "3"];
			possibleTeamIds.splice(possibleTeamIds.indexOf(playerTeamId), 1);
			for each(var tmpTeamId:String in possibleTeamIds)
			{
				_teamIdToRefId[tmpTeamId] = possibleTeamRefs.shift();
			}
			
			
			// LOAD ASSET
			_levelAsset	= ExternalLoader.Instance.GetSingleton("Config_" + _config, "level" + _number.toString() + ".swf") as MovieClip;
			
			// SCENERY (hardcodeado temporal)
			var sceneryName	: String		= Config.getValue("scenery_name",_number);
			var sceneryType	: String	= Config.getValue("scenery_type",_number);
			switch(sceneryType) {
				case Scenery.REGULAR:
					_scenery	= new Scenery_Regular(sceneryName);
					break;
				default:
					Debugger.Log("WARNING: Scenery type " + sceneryType + " for level " + _number + " not found!");
					return;
			}
			_scenery.Prepare();
			
			// CAMERA LIMITS
			var cameraLimitsMc	: MovieClip	= levelAsset.getChildByName("cameraLimits_mc") as MovieClip;
			_cameraLimits	= new Absolute2DArea(cameraLimitsMc.x, cameraLimitsMc.x + cameraLimitsMc.width, cameraLimitsMc.y, cameraLimitsMc.y + cameraLimitsMc.height);
			
			// PLAYER LIMITS
			var playerLimitsMc	: MovieClip	= levelAsset.getChildByName("playerLimits_mc") as MovieClip;
			
			var playerLimitsX	: Number = Config.getValue("player_limits_x");
			if (isNaN(playerLimitsX))
			{
				playerLimitsX = (playerLimitsMc != null)?playerLimitsMc.x:_cameraLimits.leftX;
			}
			var playerLimitsWidth	: Number = Config.getValue("player_limits_width");
			if (isNaN(playerLimitsWidth))
			{
				playerLimitsWidth = (playerLimitsMc != null)?playerLimitsMc.width:(_cameraLimits.rightX - _cameraLimits.leftX);
			}
			var playerLimitsY	: Number = Config.getValue("player_limits_y");
			if (isNaN(playerLimitsY))
			{
				playerLimitsY = (playerLimitsMc != null)?playerLimitsMc.y:_cameraLimits.upY;
			}
			var playerLimitsHeight	: Number = Config.getValue("player_limits_height");
			if (isNaN(playerLimitsHeight))
			{
				playerLimitsHeight = (playerLimitsMc != null)?playerLimitsMc.height:(_cameraLimits.downY - _cameraLimits.upY);
			}
			
			_playerLimits	= new AbsoluteRangeArea(	playerLimitsX, playerLimitsX + playerLimitsWidth,
														-int.MAX_VALUE, int.MAX_VALUE,
														playerLimitsY - Config.floor_y, playerLimitsY - Config.floor_y + playerLimitsHeight);
														
			// SPAWN AREAS
			_spawnAreas = new Dictionary();
			var i : int = (levelAsset.getChildByName("spawnAreas_mc") as MovieClip).numChildren;
			while (i--)
			{
				var teamSpawnArea 	: MovieClip = (levelAsset.getChildByName("spawnAreas_mc") as MovieClip).getChildAt(i) as MovieClip;
				var teamRefId		: String	= teamSpawnArea.name.substr(13, 1);
				var teamSpawnAreaScreenPos : Point = MovieClipUtils.GetRelativePos(teamSpawnArea, levelAsset);
				var spawnArea : AbsoluteRangeArea = new AbsoluteRangeArea(	teamSpawnAreaScreenPos.x, teamSpawnAreaScreenPos.x + teamSpawnArea.width, 
																			Config.floor_y, Config.floor_y, 
																			teamSpawnAreaScreenPos.y - Config.floor_y, teamSpawnAreaScreenPos.y - Config.floor_y + teamSpawnArea.height);
				var teamId : String = getTeamIdFromRef(teamRefId);
				if (_spawnAreas[teamId] == undefined)
				{
					_spawnAreas[teamId] = new Vector.<AbsoluteRangeArea>();
				}
				(_spawnAreas[teamId] as Vector.<AbsoluteRangeArea>).push(spawnArea);
			}
			
			
			//for (var i : int = 1; i <= 3; i++)
			//{
				//var teamSpawnArea : MovieClip = (levelAsset.getChildByName("spawnAreas_mc") as MovieClip).getChildByName("spawnAreaTeam" + getTeamRefId(i.toString()) + "_mc") as MovieClip;
				//var teamSpawnAreaScreenPos : Point = MovieClipUtils.GetRelativePos(teamSpawnArea, levelAsset);
				//var spawnArea : AbsoluteRangeArea = new AbsoluteRangeArea(	teamSpawnAreaScreenPos.x, teamSpawnAreaScreenPos.x + teamSpawnArea.width, 
																			//Config.floor_y, Config.floor_y, 
																			//teamSpawnAreaScreenPos.y - Config.floor_y, teamSpawnAreaScreenPos.y - Config.floor_y + teamSpawnArea.height);
				//_spawnAreas[i.toString()] = spawnArea;
			//}
			//
			
			var numExtraLifeSlots	: int = DBManager.instance.user.getVar("extra_life_slots");
			var playerMaxEnergyConfigStr : String	= (numExtraLifeSlots > 0)?(GameManager.Instance.activePlayer.configPrefix + "max_energy_" + numExtraLifeSlots.toString() + "_extra_life_slot"):(GameManager.Instance.activePlayer.configPrefix + "max_energy");
			var playerMaxEnergy : Number = Config.getValue(playerMaxEnergyConfigStr);
			GameManager.Instance.activePlayer.maxEnergy = playerMaxEnergy;
			GameManager.Instance.activePlayer.energy = GameManager.Instance.activePlayer.maxEnergy;
			teleportToSpawnArea(GameManager.Instance.activePlayer);
			//GameManager.Instance.activePlayer.SetState(Player.ST_SPAWN);
			
			_timeToEndComboMs	= Config.getValue("level_" + number.toString() + "_time_to_clear_combo_ms");
			_timeLeftToEndComboMs	= -1;
			_currentCombo		= 0;
			
			GameObjectToScreenMapper.instance.Update();
		}
		
		public function teleportToSpawnArea(gObj:GameObject):void 
		{
			var possibleSpawnAreas : Vector.<AbsoluteRangeArea> = _spawnAreas[gObj.teamId];
			var spawnAreaId	: int	= MathUtils.random(0, possibleSpawnAreas.length - 1, true);
			var spawnArea : AbsoluteRangeArea = possibleSpawnAreas[spawnAreaId];
			var spawnX : Number = MathUtils.random(spawnArea.leftX, spawnArea.rightX);
			var spawnY : Number = MathUtils.random(spawnArea.upY, spawnArea.downY);
			var spawnZ : Number = MathUtils.random(spawnArea.backZ, spawnArea.frontZ);
			gObj.TeleportTo(spawnX, spawnY, spawnZ);
			
			// Face the center of the screen
			var gameAreaCenter : GameAreaPoint = Camera.Instance.ConvertScreenPosToGamePos(new Point((_cameraLimits.leftX + _cameraLimits.rightX) * .5, (_cameraLimits.upY + _cameraLimits.downY) * .5), Config.floor_y);
			if (spawnX <= gameAreaCenter.x) gObj.FaceRight();
			else gObj.FaceLeft();
		}
		
		public function Start():void {
			//if (_tutor) {
				//_tutor.Start();
			//}
			
			HUDManager.Instance.showPlayerSign();
		}
		
		private function OnLevelWon():void 
		{
			//Debugger.Log("LEVEL WON");
			//GameManager.Instance.OnLevelWonWaiting();
		}
		
		private function OnGameWon():void 
		{
			//Debugger.Log("GAME WON");
			//GameManager.Instance.OnGameWon();
		}
		
		public function Update(dtMs:int):void {
			
			scenery.Update(dtMs);
			
			if (_timeLeftToEndComboMs > 0)
			{
				_timeLeftToEndComboMs -= dtMs;
				if (_timeLeftToEndComboMs <= 0)
				{
					resetCombo();
				}
			}
			//if (_tutor) {
				//_tutor.Update(dtMs);
				//if (_tutor.complete) {
					//_tutor.End();
					//_tutor.Destroy();
					//_tutor	= null;
				//}
			//}
		}
		
		public function Destroy():void {
			
			_scenery.Destroy();
			_scenery	= null;
			
			//if (_tutor) {
				//_tutor.End();
				//_tutor.Destroy();
				//_tutor	= null;
			//}
		}
		

		public function OnEnemyKilled(enemy:Enemy, theKiller:GameObject):void 
		{
			// Segun el modo es lo que pasa
		}
		
		public function OnEnemyKilledAnimationEnd(enemy:Enemy):void 
		{
			// Segun el modo es lo que pasa
			teleportToSpawnArea(enemy);
			enemy.SetState(Enemy.ST_SPAWN);
			enemy.UpdateVars();
		}
		
		
		public function OnPlayerKilled($player:Player, theKiller:GameObject):void 
		{
			// Segun el modo es lo que pasa
		}
		
		public function OnPlayerKilledAnimationEnd($player:Player):void 
		{
			// Segun el modo es lo que pasa
			teleportToSpawnArea($player);
			$player.SetState(Player.ST_SPAWN);
			$player.energy = $player.maxEnergy;
			
			HUDManager.Instance.showPlayerSign();
			//Camera.Instance.Update(33, true);
			Camera.Instance.PanToPlayerPos();
		}
		
		public function PlayerGrabbedPowerup(thePlayer:Player, powerup:Powerup):void { }
		public function EnemyGrabbedPowerup(enemy:Enemy, powerup:Powerup):void { }
		
		public function onBullseyePainted(bullseye:Bullseye, shot:Shot):void { }
		public function onBullseyePaintedTimeEnd(bullseye:Bullseye):void { }
		public function onFlagGrabbed(flag:Flag, gObj:GameObject):void { }
		public function onFlagGrabbedTimeEnd(flag:Flag):void { }
		
		protected function increaseCombo(deltaCombo : int = 1):void
		{
			_currentCombo += deltaCombo;
			if (_currentCombo >= 2)
			{
				HUDManager.Instance.ComboFeedbackFx(_currentCombo);
			}
			_timeLeftToEndComboMs = _timeToEndComboMs;
		}
		
		protected function resetCombo():void
		{
			if (_currentCombo > 1)
			{
				HUDManager.Instance.ComboFeedbackEnd();
			}
			_currentCombo = 0;
			_timeLeftToEndComboMs = -1;
		}
		
		
		
		
/* =============================================*/
/* ============= SETTERS & GETTERS =============*/
/* =============================================*/

		public function get mode():String {
			return LEVEL_MODE_1;
		}
		
		public function get difficulty():String 
		{
			return _difficulty;
		}
		
		public function get playerLimits():AbsoluteRangeArea 
		{
			return _playerLimits;
		}
		
		public function get cameraLimits():Absolute2DArea 
		{
			return _cameraLimits;
		}
		
		public function get number():int 
		{
			return _number;
		}
		
		public function get scenery():Scenery 
		{
			return _scenery;
		}
		
		public function getScore():int 
		{
			var scoreFactor	: Number	= Config.getValue("objective_value_to_score_factor_level_" + number.toString());
			return Math.round(objectiveValue * scoreFactor);
		}
		
		public function getCoinsPerObjective():int
		{
			var coinsFactor	: Number	= Config.getValue("objective_value_to_coins_factor_level_" + number.toString());
			
			// CODE: DOUBLE COINS
			var coinsMultiplier	: int = 1;
			if (CheatsManager.isActive(CheatsManager.DOUBLE_COINS))
			{
				coinsMultiplier = 2;
			}
			
			
			return Math.round(objectiveValue * coinsFactor * coinsMultiplier);
		}
		
		public function get coinsGrabbed():int
		{
			return _coinsGrabbed;
		}
		
		public function get objectiveValue():int 
		{
			return _objectiveValue;
		}
		
		public function set objectiveValue(value:int):void 
		{
			_objectiveValue = value;
			
			if (value >= 1)
			{
				Sfx.PlaySound("PointEarned");
			}
		}
		
		public function set coinsGrabbed(value:int):void 
		{
			_coinsGrabbed = value;
		}
		
		public function get levelAsset():MovieClip 
		{
			return _levelAsset;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function get config():String 
		{
			return _config;
		}
		
		public function getTeamRefId(teamId:String):String
		{
			if (_teamIdToRefId == null || _teamIdToRefId[teamId] == undefined) return "";
			return _teamIdToRefId[teamId];
		}
		
		public function getTeamIdFromRef(refId:String):String
		{
			for (var teamId : String in _teamIdToRefId)
			{
				if (_teamIdToRefId[teamId] == refId) return teamId;
			}
			return "";
		}
		
		public function prepareLevelAfterPreparingManagers():void { }
		
		
	}

}