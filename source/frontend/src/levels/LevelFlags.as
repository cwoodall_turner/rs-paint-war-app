package levels 
{
import powerups.Flag;
import powerups.Powerup;
import powerups.PowerupsManager;

import ui.HUDManager;

/**
	 * ...
	 * @author ...
	 */
	public class LevelFlags extends Level
	{
		
		protected var _capturables	: Vector.<Capturable>	= null;
		
		public function LevelFlags($number:int, $difficulty:String, $config:String) {
			super($number, $difficulty, $config);
		}
		
		override public function onFlagGrabbed(flag:Flag, gObj:GameObject):void { 
			
			// Si la agarra el jugador, suma puntos
			if (gObj.teamId == GameManager.Instance.activePlayer.teamId)
			{
				objectiveValue++;
				HUDManager.Instance.showPlusScore(new GameAreaPoint(flag.x, flag.y, flag.z), 1);
				
				increaseCombo();
			}
			/*
			// Si todas als banderas están del mismo color, 1000 puntos para Gryffindor
			var allFlagsSameColor	: Boolean = true;
			var powerup : Powerup = null;
			for each(powerup in PowerupsManager.instance.thePowerups)
			{
				if (powerup is Flag && !powerup.IsCurrentState(GameObject.ST_READY))
				{
					if ((powerup as Flag).grabbedByTeamId != gObj.teamId)
					{
						allFlagsSameColor = false;
					}
				}
			}
			
			if (allFlagsSameColor)
			{
				// Reset all bullseyes
				for each(powerup in PowerupsManager.instance.thePowerups)
				{
					if (powerup is Flag && !powerup.IsCurrentState(GameObject.ST_READY))
					{
						(powerup as Flag).resetGrabbedBy();
					}
				}
				
				// If player team painted them, score!
				if (gObj.teamId == GameManager.Instance.activePlayer.teamId)
				{
					var extraScore : int = Config.getValue("all_flags_extra_score");
					objectiveValue += extraScore;
				}
			}
			*/
		}
		
		override public function onFlagGrabbedTimeEnd(flag:Flag):void { 
			
			// Si la había agarrado el jugador, suma puntos
			if (flag.grabbedByTeamId == GameManager.Instance.activePlayer.teamId)
			{
				objectiveValue++;
				HUDManager.Instance.showPlusScore(new GameAreaPoint(flag.x, flag.y, flag.z), 1);
			}
		}
		
		override public function prepareLevelAfterPreparingManagers():void
		{
			_capturables	= new Vector.<Capturable>();
			for each(var powerup:Powerup in PowerupsManager.instance.thePowerups)
			{
				if (powerup is Flag)
				{
					_capturables.push(powerup);
				}
			}
		}
		
		public function get capturables():Vector.<Capturable> 
		{
			return _capturables;
		}
		
		override public function Destroy():void
		{
			super.Destroy();
			_capturables	= null;
		}
		
	}

}
