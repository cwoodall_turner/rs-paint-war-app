﻿package levels {

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.net.URLLoader;
import flash.net.URLRequest;

public class LevelsLoader extends EventDispatcher {
		
		/***** Public Constants ******/
		public static const LOAD_COMPLETE	: String = "LoadComplete";
		
		private	static var _instance	: LevelsLoader	= null;
		
		/***** Private variables *****/
		private var _loader		: URLLoader;
		private var _levels	: XML	= null;
		
		
		/**
		* Singleton implementation.
		*/
		public static function get Instance() :LevelsLoader {
		
			if(!(LevelsLoader._instance is LevelsLoader))
				LevelsLoader._instance = new LevelsLoader(new SingletonBlocker());
				
			return _instance;
		}
		
		/**
		* Constructor
		*/
		public function LevelsLoader(singletonBlocker:SingletonBlocker) {
		
			if (singletonBlocker == null) return;
		}
		
		public function get levelsXML():XML 
		{
			return _levels;
		}
		
		
		public function Load(path :String) :void {
			
			_loader	= new URLLoader();
			_loader.addEventListener(Event.COMPLETE, OnXmlLoaded, false, 0 , true);
			_loader.load(new URLRequest(path));
		}
		
		private function OnXmlLoaded(e:Event):void {
			_loader.removeEventListener(Event.COMPLETE, OnXmlLoaded);
			_levels	= new XML(e.target.data);
			dispatchEvent(new Event(LOAD_COMPLETE));
		}
		
		//private function ParseConfig(config:Dictionary, xml:XML):void {
			//
			//for each(var term:XML in xml.term) {
				//config[term.@key.toString()] = term.toString();
			//}
		//}
		
	}
	
}

internal class SingletonBlocker { }
