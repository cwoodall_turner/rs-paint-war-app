package levels 
{
import enemies.Enemy;

import players.Player;

import powerups.DogTag;
import powerups.Powerup;
import powerups.PowerupsManager;

import ui.HUDManager;

/**
	 * ...
	 * @author ...
	 */
	public class LevelDogTags extends Level
	{
		
		public function LevelDogTags($number:int, $difficulty:String, $config:String) {
			super($number, $difficulty, $config);
		}
		

		override public function OnEnemyKilled(enemy:Enemy, theKiller:GameObject):void 
		{
			super.OnEnemyKilled(enemy, theKiller);
		}
		
		override public function OnEnemyKilledAnimationEnd(enemy:Enemy):void 
		{
			PowerupsManager.instance.AddPowerup(Powerup.ID_DOG_TAG, enemy);
			super.OnEnemyKilledAnimationEnd(enemy);
		}
		
		
		override public function OnPlayerKilled($player:Player, theKiller:GameObject):void 
		{
			// Segun el modo es lo que pasa
			super.OnPlayerKilled($player, theKiller);
		}
		
		override public function OnPlayerKilledAnimationEnd($player:Player):void 
		{
			// Segun el modo es lo que pasa
			PowerupsManager.instance.AddPowerup(Powerup.ID_DOG_TAG, $player);
			objectiveValue--;
			if (objectiveValue < 0) objectiveValue = 0;
			super.OnPlayerKilledAnimationEnd($player);
		}
		
		override public function PlayerGrabbedPowerup(thePlayer:Player, powerup:Powerup):void 
		{
			if (powerup is DogTag)
			{
				objectiveValue++;
				HUDManager.Instance.showPlusScore(new GameAreaPoint(powerup.x, powerup.y, powerup.z), 1);
				
				increaseCombo();
			}
		}
		
	}

}
