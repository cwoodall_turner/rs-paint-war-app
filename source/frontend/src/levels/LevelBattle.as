package levels 
{
import enemies.Enemy;

import players.Player;

import ui.HUDManager;

/**
	 * ...
	 * @author ...
	 */
	public class LevelBattle extends Level
	{
		
		public function LevelBattle($number:int, $difficulty:String, $config:String) {
			super($number, $difficulty, $config);
		}
		

		override public function OnEnemyKilled(enemy:Enemy, theKiller:GameObject):void 
		{
			super.OnEnemyKilled(enemy, theKiller);
			
			if (theKiller is Player)
			{
				objectiveValue++;
				HUDManager.Instance.showPlusScore(new GameAreaPoint(enemy.x, enemy.y, enemy.z), 1);
				
				increaseCombo();
			}
		}
		
		override public function OnEnemyKilledAnimationEnd(enemy:Enemy):void 
		{
			super.OnEnemyKilledAnimationEnd(enemy);
		}
		
		
		override public function OnPlayerKilled($player:Player, theKiller:GameObject):void 
		{
			// Segun el modo es lo que pasa
			super.OnPlayerKilled($player, theKiller);
		}
		
		override public function OnPlayerKilledAnimationEnd($player:Player):void 
		{
			// Segun el modo es lo que pasa
			super.OnPlayerKilledAnimationEnd($player);
		}
		
	}

}
