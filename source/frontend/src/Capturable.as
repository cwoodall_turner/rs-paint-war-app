package 
{
	
	/**
	 * ...
	 * @author alfred
	 */
	public interface Capturable 
	{
		function get timeLeftToClearMs():int;
		function get timeToClearMs():int;
		
		function get gObj():GameObject;
	}
}
