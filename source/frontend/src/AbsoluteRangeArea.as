﻿package {
	/**
	 * ...
	 * @author ahodes
	 */
	public class AbsoluteRangeArea {
		
		protected var _leftX	: Number	= 0;
		protected var _rightX	: Number	= 0;
		protected var _upY		: Number	= 0;
		protected var _downY	: Number	= 0;
		protected var _backZ	: Number	= 0;
		protected var _frontZ	: Number	= 0;
		
		public function AbsoluteRangeArea($leftX:Number, $rightX:Number, $upY:Number, $downY:Number, $backZ:Number, $frontZ:Number) {
			SetLimits($leftX, $rightX, $upY, $downY, $backZ, $frontZ);
		}
		
		public function SetLimits($leftX:Number, $rightX:Number, $upY:Number, $downY:Number, $backZ:Number, $frontZ:Number): void {
			_leftX	= $leftX;
			_rightX = $rightX;
			_upY	= $upY;
			_downY 	= $downY;
			_backZ 	= $backZ;
			_frontZ = $frontZ;
		}
		
		public function get leftX():Number 
		{
			return _leftX;
		}
		
		public function get rightX():Number 
		{
			return _rightX;
		}
		
		public function get backZ():Number 
		{
			return _backZ;
		}
		
		public function get frontZ():Number 
		{
			return _frontZ;
		}
		
		public function get width() : Number {
			return rightX - leftX;
		}
		
		public function get depth() : Number {
			return frontZ - backZ;
		}
		
		public function get upY():Number 
		{
			return _upY;
		}
		
		public function get downY():Number 
		{
			return _downY;
		}
		
		public function toString():String {
			return "[leftX: " + leftX.toFixed(1) + ", rightX: " + rightX.toFixed(1) + ", backZ: " + backZ.toFixed(1) + ", frontZ: " + frontZ.toFixed(1) + "]";
		}
		
		
		public function Move(dX:Number, dY:Number, dZ:Number):void {
			_leftX	+= dX;
			_rightX	+= dX;
			_upY	+= dY;
			_downY 	+= dY;
			_backZ	+= dZ;
			_frontZ	+= dZ;
		}
		
		
		public function SetLimitsLeftX($leftX:Number):void {
			_leftX	= $leftX;
		}
		
	}

}