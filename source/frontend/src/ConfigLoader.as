﻿package {

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.utils.Dictionary;

public class ConfigLoader extends EventDispatcher {
		
		/***** Public Constants ******/
		public static const LOAD_COMPLETE	: String = "LoadComplete";
		
		private	static var _instance	: ConfigLoader	= null;
		
		/***** Private variables *****/
		private var _loader	: URLLoader;
		private var _config	: Dictionary	= null;
		
		
		/**
		* Singleton implementation.
		*/
		public static function get Instance() :ConfigLoader {
		
			if(!(ConfigLoader._instance is ConfigLoader))
				ConfigLoader._instance = new ConfigLoader(new SingletonBlocker());
				
			return _instance;
		}
		
		/**
		* Constructor
		*/
		public function ConfigLoader(singletonBlocker:SingletonBlocker) {
		
			if (singletonBlocker == null) return;
			
			_config	= new Dictionary();
		}
		
		
		/**
		 * Config Dictionary
		 * 
		 */
		public function get Config() : Dictionary	{	return	_config;	}
		
		
		public function LoadConfig(path :String) :void {
			
			_loader	= new URLLoader();
			_loader.addEventListener(Event.COMPLETE, OnXmlLoaded, false, 0 , true);
			_loader.load(new URLRequest(path));
		}
		
		private function OnXmlLoaded(e:Event):void {
			_loader.removeEventListener(Event.COMPLETE, OnXmlLoaded);
			ParseConfig(_config, new XML(e.target.data));
			dispatchEvent(new Event(LOAD_COMPLETE));
		}
		
		private function ParseConfig(config:Dictionary, xml:XML):void {
			
			for each(var term:XML in xml.term) {
				config[term.@key.toString()] = term.toString();
			}
		}
		
	}
	
}

internal class SingletonBlocker { }
