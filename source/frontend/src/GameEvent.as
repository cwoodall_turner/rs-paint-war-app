﻿package {

import flash.events.Event;

public class GameEvent extends Event {
		
		/***** Constants *****/
		
		public static const MILESTONE	:String = "milestone";
		
		
		
		/***** Public Variables *****/
		
		public var name :String;
		
		
		
		/***** Public Methods *****/
		
		/**
		 * Constructor.
		 * 
		 * @param	type
		 * @param	name
		 * @param	bubbles
		 * @param	cancelable
		 */
		public function GameEvent (type:String, name:String = null, bubbles:Boolean = false, cancelable:Boolean = false) {
			
			super(type, bubbles, cancelable);
			this.name = name;
		}

	}
	
}
