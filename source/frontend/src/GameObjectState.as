package  
{
import flash.display.MovieClip;
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.utils.Dictionary;
import flash.utils.getQualifiedClassName;

/**
	 * ...
	 * @author ...
	 */
	public class GameObjectState extends EventDispatcher
	{
		
		public static const LEAVE	: String	= "Leave";
		public static const ENTER	: String	= "Enter";
		
		protected var _gObj	: GameObject	= null;
		
		protected var 	_canReEnterSameState			: Boolean	= false;
		protected var	_onReEnterPlayInnerMcFromStart	: Boolean	= true;
		
		protected var _attackBoundaries			: Boundaries	= null;
		
		protected var _attackBoundariesMc	: MovieClip	= null;
		protected var _attackHeightMc		: MovieClip	= null;
		
		protected var _checkScreenLimits	: Boolean	= true;
		protected var _checkEnterScreen		: Boolean	= false;
		
		private var _enterEvt	: Event	= null;
		private var _leaveEvt	: Event	= null;
		
		protected var _possibleNextStatesExternal : Dictionary = null;
		
		protected var _id	: String	= "default-id";
		
		public function GameObjectState(gObj:GameObject) 
		{
			_gObj = gObj;
			_attackBoundaries			= new Boundaries(0, 1, 0, 1, 0, 1);
			_canReEnterSameState		= false;
			_enterEvt					= new Event(ENTER);
			_leaveEvt					= new Event(LEAVE);
			_possibleNextStatesExternal	= new Dictionary();
			_id							= getQualifiedClassName(this).split("_").pop();
			//Debugger.Log("_id: " + _id);
		}
		
		public function EnterWithArgs(args:Array):void {}
		public function Enter():void {
			updateCanHitAndCanBeHit();
			if (_gObj.canHit) _gObj.attackSeq++;
			if (_gObj.canBeHit) _gObj.RecalculateHitBoundaries();
			dispatchEvent(_enterEvt);
		}
		
		protected function updateCanHitAndCanBeHit():void 
		{
			Debugger.Log("WARNING! - UPDATING CAN HIT AND CAN BE HIT WITH DEFAULT VALUES  --  " + this);
			_gObj.canHit	= Targets.NONE;
			_gObj.canBeHit	= false;
		}
		public function Leave():void {
			_attackBoundariesMc = null;
			dispatchEvent(_leaveEvt);
		}
		
		public function AddPossibleNextStatesExternal(...states):void {
			var i : int = states.length;
			while (i--) {
				//Debugger.Log("Transicion permitida: " + this + " ------> " + states[i]);
				_possibleNextStatesExternal[states[i]]	= true;
			}
		}
		
		
		public function Update(dtMs:int):void {
			if (_gObj.canHit && !(_attackBoundariesMc && _attackHeightMc) && _gObj.innerMc) {
				_attackBoundariesMc = _gObj.innerMc.getChildByName("attackBoundaries_mc") as MovieClip;
				_attackHeightMc		= _gObj.innerMc.getChildByName("attackHeight_mc") as MovieClip;
			}
		}
		
		public function get attackBoundaries():Boundaries 
		{
			//Debugger.Log("_gObj: " + _gObj);
			//Debugger.Log("_gObj.innerMc: " + _gObj.innerMc);
			//if (_gObj.innerMc) {
				//Debugger.Log("InnerMc @" + _gObj.innerMc.currentFrame);
				//Debugger.Log("View @" + _gObj.View.currentFrame);
			//} else {
				//Debugger.Log("NO INNER MC");
				//Debugger.Log("View @" + _gObj.View.currentFrame);
				//var newInnerMc	: MovieClip = _gObj.View.getChildByName("inner_mc") as MovieClip;
				//Debugger.Log("newInnerMc: " + newInnerMc);
			//}
			if (!_attackBoundariesMc || !_attackHeightMc) {
				var tempInnerMc	: MovieClip	= _gObj.innerMc;
				if (!tempInnerMc) {
					Debugger.Log("WARNING! No InnerMc trying to get attackBoundaries for " + _gObj + " at State " + this);
					return null;
				}
				_attackBoundariesMc = tempInnerMc.getChildByName("attackBoundaries_mc") as MovieClip;
				_attackHeightMc		= tempInnerMc.getChildByName("attackHeight_mc") as MovieClip;
				if (!_attackBoundariesMc || !_attackHeightMc) {
					Debugger.Log("WARNING! No attack MCs for " + _gObj + " at State " + this);
					return null;
				}
			}
			
			if (_attackBoundariesMc && _attackHeightMc && _attackBoundariesMc.parent && _attackHeightMc.parent) {
				var parent	: MovieClip	= _attackBoundariesMc.parent as MovieClip;
				var scaleX	: Number	= parent.scaleX;
				var scaleY	: Number	= parent.scaleY;
				_attackBoundaries.SetLimits(
												parent.x + _attackBoundariesMc.x * scaleX,
												parent.x + (_attackBoundariesMc.x + _attackBoundariesMc.width) * scaleX,
												parent.y + (_attackHeightMc.y - _attackHeightMc.height) * scaleY,
												parent.y + _attackHeightMc.y * scaleY,
												parent.y + _attackBoundariesMc.y * scaleY,
												parent.y + (_attackBoundariesMc.y + _attackBoundariesMc.height) * scaleY
											);
											
				return _attackBoundaries;
			}
			return null;
		}
		
		public function get canReEnterSameState():Boolean 
		{
			return _canReEnterSameState;
		}
		
		public function get gameObject():GameObject 
		{
			return _gObj;
		}
		
		public function ReEnterWithArgs(args:Array):void {
			Leave();
			EnterWithArgs(args);
			if(_onReEnterPlayInnerMcFromStart)	_gObj.InnerMcGoToAndPlay(1);
		}
		
		public function ReEnter():void {
			Leave();
			Enter();
			if(_onReEnterPlayInnerMcFromStart)	_gObj.InnerMcGoToAndPlay(1);
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function CanSetNextStateExternal(newStateId:String):Boolean 
		{
			return _possibleNextStatesExternal[newStateId];
		}
		
		public function updateViewFacing():void { }
		
		public function PassedLevelLimitLeft(limitValue:Number) : void { }
		
		public function PassedLevelLimitRight(limitValue:Number) : void { }
		
		public function PassedLevelLimitBack(limitValue:Number) : void { }
		
		public function PassedLevelLimitFront(limitValue:Number) : void { }
		
		public function Destroy() : void {
			_attackBoundariesMc		= null;
			_attackHeightMc			= null;
			_possibleNextStatesExternal	= null;
			_gObj	= null;
		}
		
		public function Reset() : void { }
		
		
		public function get checkScreenLimits():Boolean {
			return _checkScreenLimits;
		}
		
		public function get checkEnterScreen():Boolean {
			return _checkEnterScreen;
		}
	}

}