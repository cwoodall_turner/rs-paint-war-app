package  behaviors {
	
	/**
	 * ...
	 * @author ...
	 */
	public class RelativeRangeAreaValueGenerator extends ValueGenerator
	{
		protected var _relativeRangeArea	: RelativeRangeArea	= null;
		
		public function RelativeRangeAreaValueGenerator($leftX:Number, $rightX:Number, $backZ:Number, $frontZ:Number) {
			_relativeRangeArea	= new RelativeRangeArea($leftX, $rightX, $backZ, $frontZ);
		}
		
		override public function GetNewValue():Object {
			return _relativeRangeArea;
		}
		
		public function toString():String {
			return "[Relative Range Area: " + _relativeRangeArea + "]";
		}
	}
}