package behaviors 
{
import obstacles.Bullseye;
import obstacles.Obstacle;
import obstacles.ObstaclesManager;

import powerups.DogTag;
import powerups.Flag;
import powerups.Powerup;
import powerups.PowerupsManager;

import shots.Shot;
import shots.ShotsManager;

/**
	 * ...
	 * @author alfred
	 */
	public class RangeCondition 
	{
		protected var _nextActionIdValueGen		: ValueGenerator	= null;
		protected var _checkOutOfRangeRect		: Boolean			= false;
		protected var _rangeTargetStr			: String			= "";
		protected var _withinRectRangeValueGen	: ValueGenerator	= null;
		protected var _withinRectRangeArea		: RelativeRangeArea	= null;
		protected var _debugStr					: String 	= "";
		protected var _targetStr				: String	= "";
		protected var _targetChecked			: GameObject	= null;
		
		public function RangeCondition(back:Number, front:Number, left:Number, right:Number, checkOutOfRange:Boolean, nextActionIdValueGen:ValueGenerator, targetStr:String, debugStr:String) 
		{
			_withinRectRangeArea = new RelativeRangeArea(back, front, left, right);
			_checkOutOfRangeRect	= checkOutOfRange;
			_nextActionIdValueGen = nextActionIdValueGen;
			_targetStr = targetStr;
			_debugStr = debugStr;
		}
		
		public function get nextActionIdValueGen():ValueGenerator 
		{
			return _nextActionIdValueGen;
		}
		
		public function get debugStr():String 
		{
			return _debugStr;
		}
		
		public function get targetChecked():GameObject 
		{
			return _targetChecked;
		}
		
		public function check(_gObj:GameObjectWithBehavior):Boolean
		{
			// Set horizontal range limits (it depends on where the gObj is facing)
			var leftRangeX	: Number = 0;
			var rightRangeX	: Number = 0;
			var backRangeZ	: Number = 0;
			var frontRangeZ	: Number = 0;
			if (_gObj.IsFacingRight)
			{
				
				leftRangeX	= _gObj.x - _withinRectRangeArea.leftX;
				rightRangeX	= _gObj.x + _withinRectRangeArea.rightX;
				backRangeZ	= _gObj.z - _withinRectRangeArea.backZ;
				frontRangeZ	= _gObj.z + _withinRectRangeArea.frontZ;
			}
			else if (_gObj.IsFacingLeft)
			{
				leftRangeX	= _gObj.x - _withinRectRangeArea.rightX;
				rightRangeX	= _gObj.x + _withinRectRangeArea.leftX;
				backRangeZ	= _gObj.z - _withinRectRangeArea.frontZ;
				frontRangeZ	= _gObj.z + _withinRectRangeArea.backZ;
			}
			else if (_gObj.IsFacingBack)
			{
				leftRangeX	= _gObj.x - _withinRectRangeArea.backZ;
				rightRangeX	= _gObj.x + _withinRectRangeArea.frontZ;
				backRangeZ	= _gObj.z - _withinRectRangeArea.rightX;
				frontRangeZ	= _gObj.z + _withinRectRangeArea.leftX;
			}
			else if (_gObj.IsFacingFront)
			{
				leftRangeX	= _gObj.x - _withinRectRangeArea.frontZ;
				rightRangeX	= _gObj.x + _withinRectRangeArea.backZ;
				backRangeZ	= _gObj.z - _withinRectRangeArea.leftX;
				frontRangeZ	= _gObj.z + _withinRectRangeArea.rightX;
			}
			
			if(Config.debug_attack_area && _debugStr != null && _debugStr != "no"){
				DrawBoundaries(leftRangeX, rightRangeX, backRangeZ, frontRangeZ);
			}
			
			// Is rangeTarget within range?
			var rangeTarget : GameObject	= null;
			var checkOk		: Boolean = false;
			switch(_targetStr)
			{
				case "player":
					checkOk = checkRange(GameManager.Instance.activePlayer, leftRangeX, rightRangeX, backRangeZ, frontRangeZ);
					if (checkOk) return true;
					break;
				case "all-dog-tags":
					for each(var dogTag : Powerup in PowerupsManager.instance.thePowerups )
					{
						if (dogTag == null || !(dogTag is DogTag)) continue;
						checkOk = checkRange(dogTag, leftRangeX, rightRangeX, backRangeZ, frontRangeZ);
						if (checkOk) return true;
					}
					break;
				case "all-possible-flags":
					for each(var flag : Powerup in PowerupsManager.instance.thePowerups )
					{
						if (flag == null || !(flag is Flag)) continue;
						var canBeGrabbed : Boolean = (flag as Flag).canBeGrabbedBy(_gObj);
						if (!canBeGrabbed) continue;
						checkOk = checkRange(flag, leftRangeX, rightRangeX, backRangeZ, frontRangeZ);
						if (checkOk) return true;
					}
					break;
				case "all-player-flags":
					for each(var flag : Powerup in PowerupsManager.instance.thePowerups )
					{
						if (flag == null || !(flag is Flag)) continue;
						var isPlayerFlag : Boolean = ((flag as Flag).grabbedByTeamId == GameManager.Instance.activePlayer.teamId);
						if (!isPlayerFlag) continue;
						checkOk = checkRange(flag, leftRangeX, rightRangeX, backRangeZ, frontRangeZ);
						if (checkOk) return true;
					}
					break;
				case "all-possible-bullseyes":
					for each(var bullseye : Obstacle in ObstaclesManager.instance.theObstacles)
					{
						if (bullseye == null || !(bullseye is Bullseye)) continue;
						var canBePainted : Boolean = (bullseye as Bullseye).paintedByTeamId != _gObj.teamId;
						if (!canBePainted) continue;
						checkOk = checkRange(bullseye, leftRangeX, rightRangeX, backRangeZ, frontRangeZ);
						if (checkOk) return true;
					}
					break;
				case "all-player-bullseyes":
					for each(var bullseye : Obstacle in ObstaclesManager.instance.theObstacles)
					{
						if (bullseye == null || !(bullseye is Bullseye)) continue;
						var isPlayerBullseye : Boolean = ((bullseye as Bullseye).paintedByTeamId == GameManager.Instance.activePlayer.teamId);
						if (!isPlayerBullseye) continue;
						checkOk = checkRange(bullseye, leftRangeX, rightRangeX, backRangeZ, frontRangeZ);
						if (checkOk) return true;
					}
					break;
				case "all-opponents":
					for each(var participant:GameObject in GameManager.Instance.participants)
					{
						if (participant.teamId == _gObj.teamId) continue;
						checkOk = checkRange(participant, leftRangeX, rightRangeX, backRangeZ, frontRangeZ);
						if (checkOk) return true;
					}
					break;
				case "all-shots":
					for each(var shot:Shot in ShotsManager.instance.theShots)
					{
						if (shot.canHit & _gObj.targetId)
						{
							checkOk = checkRange(shot, leftRangeX, rightRangeX, backRangeZ, frontRangeZ);
							if (checkOk) return true;
						}
					}
					break;
				default:
					checkOk = checkRange(_gObj.currentBehavior.target, leftRangeX, rightRangeX, backRangeZ, frontRangeZ);
					if (checkOk) return true;
					break;
			}
			
			return false;
		}
		
		private function checkRange(rangeTarget:GameObject, leftRangeX:Number, rightRangeX:Number, backRangeZ:Number, frontRangeZ:Number):Boolean 
		{
			_targetChecked	= rangeTarget;
			if (rangeTarget != null) {
				if (	rangeTarget.z <= frontRangeZ && rangeTarget.z >= backRangeZ
					&&	rangeTarget.x >= leftRangeX && rangeTarget.x <= rightRangeX) {
					//if (_gObj is Sidekick) Debugger.Log("CHECKING RANGE - AAADENTRO -- _checkOutOfRangeRect: " + _checkOutOfRangeRect);
					if (!_checkOutOfRangeRect) return true;
					//return;
				} else {
					//if (_gObj is Sidekick) Debugger.Log("CHECKING RANGE - AFUERAAA -- _checkOutOfRangeRect: " + _checkOutOfRangeRect);
					if (_checkOutOfRangeRect) return true;
				}
			}
			return false;
		}
		
		protected function DrawBoundaries(leftX:Number, rightX:Number, backZ:Number, frontZ:Number):void {
			
			if (!Config.debug_attack_area) return;
			AreaDebugger.instance.DrawBoundaries(leftX, rightX, backZ, frontZ);
		}
		
	}

}