package behaviors 
{
import behaviors.enemies.EnemyBehavior;

import enemies.Enemy;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorsManager 
	{
		
		private static var _instance:BehaviorsManager = null
		
		private var _behaviors:Vector.<Behavior> = null;
		
		public function BehaviorsManager() 
		{
			_instance	= this;
			_behaviors = new Vector.<Behavior>();
		}
		
		public function Reset():void {
			var i:int = _behaviors.length;
			while (i--)
			{
				_behaviors[i].Reset();
			}
		}
		
		
		public function Update(dtMs:int):void
		{
			var i:int = _behaviors.length;
			while (i--)
			{
				_behaviors[i].Update(dtMs);
			}
		}
		
		public function SetBehavior(gObj:GameObjectWithBehavior, behaviorId:String, firstAction: String = null, aggressiveness:Number = 0):void {
			
			Debugger.Log("SETTING BEHAVIOR - GOBJ: " + gObj + " (" + behaviorId + ")[" + firstAction + "]");
			
			// Si no tiene valores válidos, manda por defecto
			if (!behaviorId) {
				Debugger.Log("WARNING! GObj " + gObj + " NO tiene behavior. Seteando por-defecto.");
				behaviorId = Behavior.BEHAVIOR_ID_DEFAULT;
			}
			if (!firstAction) {
				Debugger.Log("WARNING! GObj " + gObj + " NO tiene firstAction. Seteando por-defecto.");
				firstAction = Behavior.ACTION_ID_DEFAULT;
			}
			
			// Busca reutilizar un Behavior ya creado (no asignado a ningún gObj). Si no hay, lo crea.
			var newBehavior:Behavior = null;
			for each (var behavior:Behavior in _behaviors) {
				if (behavior.id == behaviorId && !behavior.gameObject) {
					newBehavior	= behavior;
					break;
				}
			}
			if (!newBehavior) {
				newBehavior	= CreateNewBehavior(gObj, behaviorId, firstAction, aggressiveness);
				Debugger.Log("New behavior: " + newBehavior);
				if (!newBehavior) {
					Debugger.Log("ERROR - Behavior no creado");
					return;
				}
			} else {
				Debugger.Log("Reusing behavior " + newBehavior);
				newBehavior.SetFirstAction(firstAction);
				//theBehavior.aggressiveness	= aggressiveness;
			}
			
			// Si tenía behavior anterior, toma de este el target y luego lo resetea.
			if (gObj.currentBehavior) {
				var indexOfOldBehavior	: int	= _behaviors.indexOf(gObj.currentBehavior);
				if (indexOfOldBehavior >= 0) {
					Debugger.Log("indexOfOldBehavior: " + indexOfOldBehavior);
					// Remove old behavior
					var oldBehavior : Behavior	= _behaviors[indexOfOldBehavior];
					//_behaviors.splice(indexOfOldBehavior, 1); TMP: Probar si funciona sin eliminarlo -reutilización-
					newBehavior.SetGameObject(gObj);
					newBehavior.SetTarget(oldBehavior.target);
					//newBehavior.Start();
					oldBehavior.Reset();
					// TODO: oldBehavior.Destroy();
				} else {
					newBehavior.SetGameObject(gObj);
				}
			} else {
				newBehavior.SetGameObject(gObj);
			}
		}
		
		
		private function CreateNewBehavior(gObj:GameObject, behaviorId:String = "por-defecto", firstAction:String = "por-defecto", aggressiveness:Number = .5, addToCollection:Boolean = true):Behavior {
			
			var newBehavior = null;
			if (gObj is Enemy)	newBehavior = new EnemyBehavior(behaviorId, firstAction, aggressiveness);
			
			if (addToCollection) {
				_behaviors.push(newBehavior);
			}
			return newBehavior;
		}

		
		
		
/* =============================================*/
/* ============= SETTERS & GETTERS =============*/
/* =============================================*/
		
		static public function get instance():BehaviorsManager 
		{
			return _instance;
		}
		
	}

}