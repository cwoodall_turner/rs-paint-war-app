package behaviors 
{
import enemies.EnemiesManager;

import flash.display.Graphics;
import flash.display.MovieClip;

import players.Player;

/**
	 * ...
	 * @author alfred
	 */
	public class ConditionCheck 
	{
		private var _conditionId			: String			= "";
		private var _value					: String			= "";
		private var _nextActionIdValueGen	: ValueGenerator	= null;
		private var _conditionStatusToCheck	: Boolean			= true;
		
		private var _targetStr	: String	= "";
		private var _targetChecked	: GameObject	= null;
		
		public function ConditionCheck($conditionId:String, $value:String, $nextActionIdValueGen:ValueGenerator, $conditionStatusToCheck:Boolean = true, $targetStr:String = "") 
		{
			_conditionId				= $conditionId;
			_value					= $value;
			_nextActionIdValueGen	= $nextActionIdValueGen;
			_conditionStatusToCheck	= $conditionStatusToCheck;
			_targetStr				= $targetStr;
		}
		
		public function get conditionId():String 
		{
			return _conditionId;
		}
		
		public function get value():String 
		{
			return _value;
		}
		
		public function get nextActionIdValueGen():ValueGenerator 
		{
			return _nextActionIdValueGen;
		}
		
		public function toString():String {
			return "[" + _conditionId + "(" + _value + ") -> " + _nextActionIdValueGen + "]";
		}
		
		public function get valueAsBool():Boolean 
		{
			return (value == "true");
		}
		
		public function get conditionStatusToCheck():Boolean 
		{
			return _conditionStatusToCheck;
		}
		
		public function get targetStr():String 
		{
			return _targetStr;
		}
		
		public function get targetChecked():GameObject 
		{
			return _targetChecked;
		}
		
		public function set targetChecked(value:GameObject):void 
		{
			_targetChecked = value;
		}
		
		static public function check(checkCondition:ConditionCheck, gObj:GameObjectWithBehavior):Boolean
		{
			var target	: GameObject	= gObj.currentBehavior.target;
			checkCondition.targetChecked	= target;
			switch(checkCondition.conditionId) {
				case "targetAlive":
					if (!target) return false;
					var targetAlive	: Boolean	= gObj.visible && !gObj.currentBehavior.target.killed;
					return ((gObj.currentBehavior.target && targetAlive == checkCondition.valueAsBool) == checkCondition.conditionStatusToCheck);
					break;
				case "targetCanHit":
					if (!target) return false;
					var targetCanHit	: Boolean	= gObj.currentBehavior.target.canHit > 0;
					return ((gObj.currentBehavior.target && targetCanHit == checkCondition.valueAsBool) == checkCondition.conditionStatusToCheck);
					break;
				case "targetCanBeHit":
					if (!target) return false;
					//Debugger.Log("gObj.currentBehavior.target.canBeHit: " + gObj.currentBehavior.target.canBeHit + "  --  Target: " + gObj.currentBehavior.target);
					return ((gObj.currentBehavior.target && gObj.currentBehavior.target.canBeHit == checkCondition.valueAsBool) == checkCondition.conditionStatusToCheck);
					break;
				case "targetId":
					if (!target) return false;
					//Debugger.Log("gObj.currentBehavior.target.id: " + gObj.currentBehavior.target.id + "  --  Target: " + gObj.currentBehavior.target);
					return ((gObj.currentBehavior.target && gObj.currentBehavior.target.id == checkCondition.value) == checkCondition.conditionStatusToCheck);
					break;
				case "enemyAlive":
					return (((EnemiesManager.instance.aliveEnemy != null) == checkCondition.valueAsBool) == checkCondition.conditionStatusToCheck);
					break;
				case "enemyOnScreen":
					//Debugger.Log("checkCondition.conditionStatusToCheck: " + checkCondition.conditionStatusToCheck);
					//Debugger.Log("((EnemiesManager.instance.enemyOnScreen != null) == checkCondition.valueAsBool): " + ((EnemiesManager.instance.enemyOnScreen != null) == checkCondition.valueAsBool));
					return (((EnemiesManager.instance.enemyOnScreen != null) == checkCondition.valueAsBool) == checkCondition.conditionStatusToCheck);
					break;
				case "targetAttacking":
					if (!target) return false;
					//Debugger.Log("checkCondition.conditionStatusToCheck: " + checkCondition.conditionStatusToCheck);
					//Debugger.Log("((EnemiesManager.instance.enemyOnScreen != null) == checkCondition.valueAsBool): " + ((EnemiesManager.instance.enemyOnScreen != null) == checkCondition.valueAsBool));
					return (target.IsCurrentState(Player.ST_FIRE) || target.IsCurrentState(Player.ST_GRENADE));
					break;
				case "targetLooking":
					//Debugger.Log("targetLooking - target: " + target);
					if (!target) return false;
					//Debugger.Log("target.IsFacingRight: " + target.IsFacingRight);
					//Debugger.Log("target.x: " + target.x);
					//Debugger.Log("gObj.x: " + gObj.x);
					var looking:Boolean = ((target.IsFacingRight && gObj.x > target.x) || (target.IsFacingLeft && gObj.x < target.x) || (target.IsFacingFront && gObj.z > target.z) || (target.IsFacingBack && gObj.z < target.z));
					//Debugger.Log("looking: " + looking);
					return ((looking == checkCondition.valueAsBool) == checkCondition.conditionStatusToCheck);
					break;
				case "withinFireRange":
					if (Config.debug_attack_area) DrawShotRangeBoundaries(gObj, "fire");
					var targets:Vector.<GameObject> = new Vector.<GameObject>();
					switch(checkCondition.targetStr)
					{
						case "player":
							targets.push(GameManager.Instance.activePlayer);
							break;
						case "all-opponents":
							for each(var participant:GameObject in GameManager.Instance.participants)
							{
								if (participant.teamId == gObj.teamId) continue;
								targets.push(participant);
							}
							break;
						default:
							if (target == null) return false;
							targets.push(target);
							break;
					}
					for each(var tmpTarget:GameObject in targets)
					{
						checkCondition.targetChecked	= tmpTarget;
						var withinFireRange	: Boolean	= isWithinShotRange(tmpTarget, gObj, "fire");
						//Debugger.Log("withinFireRange: " + withinFireRange);
						if ((withinFireRange == checkCondition.valueAsBool) == checkCondition.conditionStatusToCheck) return true;
					}
					return false;
					break;
				case "withinGrenadeRange":
					if (Config.debug_attack_area) DrawShotRangeBoundaries(gObj, "grenade");
					var targets:Vector.<GameObject> = new Vector.<GameObject>();
					switch(checkCondition.targetStr)
					{
						case "player":
							targets.push(GameManager.Instance.activePlayer);
							break;
						case "all-opponents":
							for each(var participant:GameObject in GameManager.Instance.participants)
							{
								if (participant.teamId == gObj.teamId) continue;
								targets.push(participant);
							}
							break;
						default:
							if (target == null) return false;
							targets.push(target);
							break;
					}
					for each(var tmpTarget:GameObject in targets)
					{
						checkCondition.targetChecked	= tmpTarget;
						var withinGrenadeRange	: Boolean	= isWithinShotRange(tmpTarget, gObj, "grenade");
						//Debugger.Log("withinGrenadeRange: " + withinGrenadeRange);
						if ((withinGrenadeRange == checkCondition.valueAsBool) == checkCondition.conditionStatusToCheck) return true;
					}
					return false;
					break;
			}
			Debugger.Log("WARNING - Trying to check inexistent condition: " + checkCondition + "  --  (" + gObj.currentBehavior + ")");
			return false;
		}
		
		static private function isWithinShotRange(target:GameObject, gObj:GameObject, shotId:String):Boolean 
		{
			var minDistance	: Number	= Config.getValue(gObj.configPrefix + shotId + "_range_min_distance");
			var maxDistance	: Number	= Config.getValue(gObj.configPrefix + shotId + "_range_max_distance");
			var rangeWidth	: Number	= Config.getValue(gObj.configPrefix + shotId + "_range_width");
				
			var halfRangeWidth	: Number	= rangeWidth * .5;
			
			// RIGHT
			if (withinRange(target, gObj.x + minDistance, gObj.x + maxDistance, gObj.z - halfRangeWidth, gObj.z + halfRangeWidth)) return true;
			// LEFT
			if (withinRange(target, gObj.x - maxDistance, gObj.x - minDistance, gObj.z - halfRangeWidth, gObj.z + halfRangeWidth)) return true;
			// FRONT
			if (withinRange(target, gObj.x - halfRangeWidth, gObj.x + halfRangeWidth, gObj.z + minDistance, gObj.z + maxDistance)) return true;
			// BACK
			if (withinRange(target, gObj.x - halfRangeWidth, gObj.x + halfRangeWidth, gObj.z - maxDistance, gObj.z - minDistance)) return true;
			
			return false;
		}
		
		
		static private function DrawShotRangeBoundaries(gObj:GameObjectWithBehavior, shotId:String):void 
		{
			var minDistance	: Number	= Config.getValue(gObj.configPrefix + shotId + "_range_min_distance");
			var maxDistance	: Number	= Config.getValue(gObj.configPrefix + shotId + "_range_max_distance");
			var rangeWidth	: Number	= Config.getValue(gObj.configPrefix + shotId + "_range_width");
			var halfRangeWidth	: Number	= rangeWidth * .5;
			
			//(gObj.View.parent as MovieClip).graphics.clear();
			
			// RIGHT
			DrawBoundaries(gObj.x + minDistance, gObj.x + maxDistance, gObj.z - halfRangeWidth, gObj.z + halfRangeWidth, (gObj.View.parent as MovieClip).graphics);
			// LEFT
			DrawBoundaries(gObj.x - maxDistance, gObj.x - minDistance, gObj.z - halfRangeWidth, gObj.z + halfRangeWidth, (gObj.View.parent as MovieClip).graphics);
			// FRONT
			DrawBoundaries(gObj.x - halfRangeWidth, gObj.x + halfRangeWidth, gObj.z + minDistance, gObj.z + maxDistance, (gObj.View.parent as MovieClip).graphics);
			// BACK
			DrawBoundaries(gObj.x - halfRangeWidth, gObj.x + halfRangeWidth, gObj.z - maxDistance, gObj.z - minDistance, (gObj.View.parent as MovieClip).graphics);
		}
		
		static private function withinRange(rangeTarget:GameObject, leftRangeX:Number, rightRangeX:Number, backRangeZ:Number, frontRangeZ:Number):Boolean 
		{
			if (rangeTarget != null) {
				
				if (	rangeTarget.z <= frontRangeZ && rangeTarget.z >= backRangeZ
					&&	rangeTarget.x >= leftRangeX && rangeTarget.x <= rightRangeX) {
					return true;
				}
			}
			
			return false;
		}
		
		static private function DrawBoundaries(leftX:Number, rightX:Number, backZ:Number, frontZ:Number, canvas:Graphics):void {
			
			if (!Config.debug_attack_area) return;
			AreaDebugger.instance.DrawBoundaries(leftX, rightX, backZ, frontZ);
			/*
			//Debugger.Log("DRAW BOUNDARIES - leftX: " + leftX + " rightX: " + rightX + " backZ: " + backZ + " frontZ: " + frontZ + " Config.floor_y: " + Config.floor_y);
			
			//canvas.clear();
			
			canvas.lineStyle(3, 0x00ff00);
			canvas.beginFill(0xFFFF00, .5);
			canvas.drawRect(leftX, Config.floor_y + backZ, rightX - leftX, frontZ - backZ);
			canvas.endFill();
			*/
		}
		
	}

}