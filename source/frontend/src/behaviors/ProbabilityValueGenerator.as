package  behaviors {
	/**
	 * ...
	 * @author ...
	 */
	public class ProbabilityValueGenerator extends ValueGenerator
	{
		protected var _valueGenerators	: Array	= null;
		protected var _probabilites		: Array	= null;
		
		public function ProbabilityValueGenerator(valueGenerators:Array, probabilities:Array) {
			_valueGenerators	= valueGenerators;
			_probabilites		= probabilities;
		}
		
		override public function GetNewValue():Object {
			var refProbabilityValue	: Number = Math.random();
			var length : int = _valueGenerators.length;
			var i : int = 0;
			var acumProb	: Number	= 0;
			while (i < (length -1)) {
				acumProb	+= _probabilites[i];
				if (acumProb >= refProbabilityValue) return (_valueGenerators[i] as ValueGenerator).GetNewValue();
				i++;
			}
			return (_valueGenerators[length -1] as ValueGenerator).GetNewValue();
		}
		
		public function toString():String {
			var s	: String = "[Possibilities]\n";
			var len : int = _valueGenerators.length;
			for (var i : int = 0; i < len; i++) {
				s	+=	"	-- " + i + " (prob: " + _probabilites[i] + ") -> " + _valueGenerators[i];
				if (i < len -1) s += "\n";
			}
			return s;
		}
	}
}