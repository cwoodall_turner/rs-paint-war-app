﻿package behaviors {

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.net.URLLoader;
import flash.net.URLRequest;

public class BehaviorsLoader extends EventDispatcher {
		
		/***** Public Constants ******/
		public static const LOAD_COMPLETE	: String = "LoadComplete";
		
		private	static var _instance	: BehaviorsLoader	= null;
		
		/***** Private variables *****/
		private var _loader		: URLLoader;
		private var _behaviors	: XML	= null;
		
		
		/**
		* Singleton implementation.
		*/
		public static function get Instance() :BehaviorsLoader {
		
			if(!(BehaviorsLoader._instance is BehaviorsLoader))
				BehaviorsLoader._instance = new BehaviorsLoader(new SingletonBlocker());
				
			return _instance;
		}
		
		/**
		* Constructor
		*/
		public function BehaviorsLoader(singletonBlocker:SingletonBlocker) {
		
			if (singletonBlocker == null) return;
		}
		
		public function get theBehaviors():XML 
		{
			return _behaviors;
		}
		
		
		public function Load(path :String) :void {
			
			_loader	= new URLLoader();
			_loader.addEventListener(Event.COMPLETE, OnXmlLoaded, false, 0 , true);
			_loader.load(new URLRequest(path));
		}
		
		private function OnXmlLoaded(e:Event):void {
			_loader.removeEventListener(Event.COMPLETE, OnXmlLoaded);
			_behaviors	= new XML(e.target.data);
			dispatchEvent(new Event(LOAD_COMPLETE));
		}
		
		//private function ParseConfig(config:Dictionary, xml:XML):void {
			//
			//for each(var term:XML in xml.term) {
				//config[term.@key.toString()] = term.toString();
			//}
		//}
		
	}
	
}

internal class SingletonBlocker { }
