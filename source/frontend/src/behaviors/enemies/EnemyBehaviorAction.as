package behaviors.enemies 
{
import behaviors.BehaviorAction;
import behaviors.GameObjectWithBehavior;

import enemies.Enemy;

/**
	 * ...
	 * @author ...
	 */
	public class EnemyBehaviorAction extends BehaviorAction {
		
		protected var _enemy	: Enemy = null;
		
		public function EnemyBehaviorAction() 
		{
			super();
		}
		
		override public function SetGameObject(gObj:GameObjectWithBehavior):void {
			super.SetGameObject(gObj);
			_enemy	= gObj as Enemy;
		}
		
		override public function Reset():void {
			super.Reset();
			_enemy	= null;
		}
		
	}

}
