package behaviors.enemies 
{
import behaviors.GamePathFinder;
import behaviors.Path;

import enemies.Enemy;

import flash.events.Event;

import obstacles.Bullseye;
import obstacles.ObstaclesManager;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_FireBullseye extends EnemyBehaviorAction
	{
		protected var _dXFromTarget	: Number	= 0;
		protected var _dYFromTarget	: Number	= 0;
		protected var _dZFromTarget	: Number	= 0;
		
		protected var _initialPath				: Path	= null;
		protected var _initialPathCurrentIdx	: int	= 0;
		
		public function BehaviorAction_FireBullseye() {
			super();
		}
		
		/**
		 * 
		 * @param	args
		 * 0: Attack Id
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - FIRE BULLSEYE  -- _timeLeftToIncompleteMs: " + _timeLeftToIncompleteMs);
			
			// If current target is a paintable bullseye, use it
			if (_target is Bullseye && (_target as Bullseye).paintedByTeamId != _gObj.teamId)
			{
				Debugger.Log("Using already found bullseye");
			}
			else
			{
				Debugger.Log("Finding new bullseye");
				var bullseye : Bullseye	= ObstaclesManager.instance.getNearestPaintableBullseye(_gObj);
				if (bullseye == null)
				{
					// All bullseyes belong to this team
					CompleteNo();
					return;
				}
				_enemy.currentBehavior.SetTarget(bullseye);
			}
			
			_dYFromTarget	= 0;
			switch((_target as Bullseye).direction)
			{
				case CollisionManager.LEFT:
					_dXFromTarget	= -Config.getValue("bullseye_side_distance_to_shoot_height_" + (int(_gObj.heightForFire)).toString());
					_dZFromTarget	= 0;
					break;
				case CollisionManager.RIGHT:
					_dXFromTarget	= Config.getValue("bullseye_side_distance_to_shoot_height_" + (int(_gObj.heightForFire)).toString());
					_dZFromTarget	= 0;
					break;
				case CollisionManager.FRONT:
					_dXFromTarget	= 0;
					_dZFromTarget	= Config.getValue("bullseye_front_distance_to_shoot_height_" + (int(_gObj.heightForFire)).toString());
					break;
			}
			
			if (Config.use_pathfinding_to_avoid_obstacles && _target != null)
			{
				var initPos	: GameAreaPoint	= new GameAreaPoint(_gObj.x, _gObj.y, _gObj.z);
				var destPos	: GameAreaPoint	= new GameAreaPoint(_target.x + _dXFromTarget, _target.y + _dYFromTarget, _target.z + _dZFromTarget);
				_initialPath	= GamePathFinder.instance.getPath(initPos, destPos);
				_initialPathCurrentIdx	= 0;
				
				if (_initialPath != null && _initialPath.length == 0)
				{
					Debugger.Log("INITIAL PATH LENGTH 0");
					_initialPath = null;
				}
			}
		}
		
		override public function Leave():void {
			super.Leave();
			_gObj.ResetDestination();
			_enemy.removeEventListener(Enemy.FIRE_FINISH_COMPLETE, OnFireFinishComplete);
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			if (!_gObj || !_target) {
				if (Config.debugThisGameObject(_gObj.id)) Debugger.Log("Warning: @Behavior " + this + ": Enemy -> " + _gObj + "  --  " + " Target -> " + _target  );
				CompleteOk();
				return;
			}
			
			if (!(_target is Bullseye) || (_target as Bullseye).paintedByTeamId == _gObj.teamId)
			{
				Debugger.Log("Target bullseye painted by same team -> Action complete NO");
				CompleteNo();
				return;
			}
			SetAndCheckDestinationPoint();
		}
		
		private function SetAndCheckDestinationPoint():void 
		{
			var finalTargetX	: Number	= 0;
			var finalTargetY	: Number	= 0;
			var finalTargetZ	: Number	= 0;
			
			if (_initialPath != null)
			{
				finalTargetX	= _initialPath.getNode(_initialPathCurrentIdx).x;
				finalTargetY	= _initialPath.getNode(_initialPathCurrentIdx).y;
				finalTargetZ	= _initialPath.getNode(_initialPathCurrentIdx).z;
			}
			else
			{
				finalTargetX	= _target.x + _dXFromTarget;
				finalTargetY	= _target.y + _dYFromTarget;
				finalTargetZ	= _target.z + _dZFromTarget;
			}
			
			_gObj.SetDestinationPoint(finalTargetX, finalTargetY, finalTargetZ);
			
			if (_gObj.isAtDestination && (_gObj.IsCurrentState(GameObject.ST_IDLE) || _gObj.IsCurrentState(_gObj.ST_MOVE_DEFAULT))) {
				if (_initialPath != null)
				{
					_initialPathCurrentIdx++;
					if (_initialPathCurrentIdx >= _initialPath.length)
					{
						Debugger.Log("Initial Path complete!");
						_initialPath = null;
					}
				}
				else
				{
					aimAndFire();
				}
				return;
			}
		}
		
		private function aimAndFire():void 
		{
			_enemy.FaceTarget();
			_enemy.addEventListener(Enemy.FIRE_FINISH_COMPLETE, OnFireFinishComplete);
			_enemy.SetStateExternal(Enemy.ST_FIRE);
		}
		
		private function OnFireFinishComplete(e:Event):void 
		{
			_enemy.removeEventListener(Enemy.FIRE_FINISH_COMPLETE, OnFireFinishComplete);
			CompleteOk();
		}
		
	}

}
