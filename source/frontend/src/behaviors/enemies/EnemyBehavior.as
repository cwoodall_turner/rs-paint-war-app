package behaviors.enemies 
{
import behaviors.Behavior;

/**
	 * ...
	 * @author ...
	 */
	public class EnemyBehavior extends Behavior {
		
		protected var _aggressiveness	: Number	= 0;
		
		public function EnemyBehavior(type:String = "por-defecto", firstAction:String = "por-defecto", aggressiveness:Number = .5) 
		{
			_aggressiveness	= aggressiveness;
			_stateClassNameBase	= "enemies::EnemyState_";
			super(type, firstAction);
		}
		
		
		
/* =============================================*/
/* ============= SETTERS & GETTERS =============*/
/* =============================================*/

		public function get aggressiveness():Number 
		{
			return _aggressiveness;
		}
		
		public function set aggressiveness(value:Number):void 
		{
			_aggressiveness = value;
		}
		
	}

}