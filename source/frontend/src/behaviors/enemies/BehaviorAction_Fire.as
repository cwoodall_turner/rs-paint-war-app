package behaviors.enemies 
{
import enemies.Enemy;

import flash.events.Event;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_Fire extends EnemyBehaviorAction
	{
		
		public function BehaviorAction_Fire() {
			super();
		}
		
		/**
		 * 
		 * @param	args
		 * 0: Attack Id
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - FIRE  -- _timeLeftToIncompleteMs: " + _timeLeftToIncompleteMs + "  --  Target: " + _target);
			
			_enemy.FaceTarget();
			_enemy.addEventListener(Enemy.FIRE_FINISH_COMPLETE, OnFireFinishComplete);
			_enemy.SetStateExternal(Enemy.ST_FIRE);
		}
		
		override public function Leave():void {
			super.Leave();
			_enemy.removeEventListener(Enemy.FIRE_FINISH_COMPLETE, OnFireFinishComplete);
		}
		
		private function OnFireFinishComplete(e:Event):void 
		{
			_enemy.removeEventListener(Enemy.FIRE_FINISH_COMPLETE, OnFireFinishComplete);
			CompleteOk();
		}
		
		override public function Reset():void {
			if (_enemy) {
				_enemy.removeEventListener(Enemy.FIRE_FINISH_COMPLETE, OnFireFinishComplete);
			} else {
				Debugger.Log("WARNING! - Trying to remove event liseners @action " + this + " without _enemy");
			}
			super.Reset();
		}
		
	}

}
