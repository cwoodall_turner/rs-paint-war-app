package behaviors.enemies 
{
import behaviors.BehaviorAction;
import behaviors.GamePathFinder;
import behaviors.Path;

import enemies.Enemy;

import obstacles.Obstacle;
import obstacles.ObstaclesManager;

import view.LayerFactory;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_TakeCover extends BehaviorAction
	{
		private static const RIGHT	: String	= "right";
		private static const LEFT	: String	= "left";
		private static const BACK	: String	= "back";
		private static const FRONT	: String	= "front";
		
		private static const RIGHT_COVER	: String 	= "right_cover";
		private static const LEFT_COVER		: String 	= "left_cover";
		private static const BACK_COVER		: String 	= "back_cover";
		private static const FRONT_COVER	: String 	= "front_cover";
		
		private static const DIAG_RIGHT_BACK	: String	= "right_back";
		private static const DIAG_RIGHT_FRONT	: String	= "right_front";
		private static const DIAG_LEFT_BACK		: String	= "left_back";
		private static const DIAG_LEFT_FRONT	: String	= "left_front";
		
		protected var _obstacle				: Obstacle	= null;
		protected var _finalCoverPosition	: String	= "";
		protected var _nextTmpPosition		: String	= "";
		protected var _positionsPath		: Array		= null;
		
		protected var _gObjWidth	: Number	= 0;
		protected var _gObjDepth	: Number	= 0;
		
		protected var _numCoversLeft	: int	= 0;
		
		protected var _initialPath				: Path	= null;
		protected var _initialPathCurrentIdx	: int	= 0;
		
		public function BehaviorAction_TakeCover() {
			super();
		}
		
		
		/**
		 * 
		 * @param	args
		 * 0: path name
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			
			var obstacleName : String	= args[0];
			
			if (args.length < 2 || isNaN(args[1]) || int(args[1]) <= 0) _numCoversLeft = int.MAX_VALUE;
			else _numCoversLeft	= args[1];
			
			_finalCoverPosition	= "";
			
			
			// If reentering, keep same obstacle
			Debugger.Log("_reEntering: " + _reEntering);
			//if (!_reEntering)
			//{
				_obstacle = null;
				
				switch (obstacleName)
				{
					case "[nearest]":
						_obstacle	= ObstaclesManager.instance.getNearestObstacle(_gObj);
						break;
					case "[farthest]":
						_obstacle	= ObstaclesManager.instance.getFarthestObstacle(_gObj);
						break;
					case "[random]":
						_obstacle	= ObstaclesManager.instance.getRandomObstacle();
						break;
					default:
						_obstacle	= ObstaclesManager.instance.getObstacle((obstacleName));
						break;
				}
				if (_obstacle == null)
				{
					Debugger.Log("WARNING! - Unable to find obstacle [" + obstacleName + "]");
					CompleteNo();
				}
			//}
			
			
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - TAKE COVER (obstacle:" + obstacleName + ") -- _timeLeftToIncompleteMs: " + _timeLeftToIncompleteMs);
			
			if (!_gObj.IsCurrentState(GameObject.ST_IDLE) && !_gObj.IsCurrentState(_gObj.ST_MOVE_DEFAULT) && !_gObj.IsCurrentState(Enemy.ST_HIDE)) _gObj.SetStateExternal(GameObject.ST_IDLE);
			
			var gObjtHitAbsBoundaries	: Boundaries	= CollisionManager.instance.getAbsoluteHitBoundaries(_gObj);
			_gObjWidth = gObjtHitAbsBoundaries.width;
			_gObjDepth = gObjtHitAbsBoundaries.depth;
			
			if (!_reEntering)
			{
				if (Config.use_pathfinding_to_avoid_obstacles)
				{
					var initPos	: GameAreaPoint	= new GameAreaPoint(_gObj.x, _gObj.y, _gObj.z);
					var destPos	: GameAreaPoint	= getCoverPosForInitPath();
					_initialPath	= GamePathFinder.instance.getPath(initPos, destPos);
					_initialPathCurrentIdx	= 0;
					
					if (_initialPath != null && _initialPath.length == 0)
					{
						Debugger.Log("INITIAL PATH LENGTH 0");
						_initialPath = null;
					}
				}
				
				if (Config.debug_pathfinding)
				{
					Debugger.Log("_initialPath: " + _initialPath);
					AreaDebugger.instance.drawPath(_initialPath, LayerFactory.Instance.GetLayer(LayerFactory.DEBUG_INIT_PATH).graphics, 0);
				}
			}
		}
		
		override public function Leave():void {
			super.Leave();
			_gObj.ResetDestination();
			_initialPath	= null;
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			if (!_gObj || !_target) {
				if (Config.debugThisGameObject(_gObj.id)) Debugger.Log("Warning: @Behavior " + this + ": Enemy -> " + _gObj + "  --  " + " Target -> " + _target  );
				CompleteOk();
				return;
			}
			SetAndCheckDestinationPoint();
		}
		
		private function SetAndCheckDestinationPoint():void 
		{
			if (_initialPath != null)
			{
				var finalTargetX	: Number	= _initialPath.getNode(_initialPathCurrentIdx).x;
				var finalTargetY	: Number	= _initialPath.getNode(_initialPathCurrentIdx).y;
				var finalTargetZ	: Number	= _initialPath.getNode(_initialPathCurrentIdx).z;
				_gObj.SetDestinationPoint(finalTargetX, finalTargetY, finalTargetZ);
				
				if (_gObj.isAtDestination)
				{
					_initialPathCurrentIdx++;
					if (_initialPathCurrentIdx >= _initialPath.length)
					{
						Debugger.Log("Initial Path complete!");
						_initialPath = null;
					}
				}
			}
			else
			{
				// CHECK TARGET / COVER POSITION
				// =============================
				var coverPosition	: String	= "";
				var relativeDirectionTargetObst	: String	= getRelativeDirection(_target, _obstacle);
				
				switch(relativeDirectionTargetObst)
				{
					case RIGHT:
						coverPosition = LEFT;
						break;
					case LEFT:
						coverPosition = RIGHT;
						break;
					case BACK:
						coverPosition = FRONT;
						break;
					case FRONT:
						coverPosition = BACK;
						break;
				}
				//Debugger.Log("coverPosition: " + coverPosition);
				

				if (coverPosition != _finalCoverPosition)
				{
					_finalCoverPosition = coverPosition;
					_nextTmpPosition	= "";
					setNewPath(getRelativeDirection(_gObj, _obstacle), coverPosition);
				}
				
				if (_positionsPath != null && _positionsPath.length > 0)
				{
					var firstTmpPosition : String = _positionsPath[0];
					//Debugger.Log("firstTmpPosition: " + firstTmpPosition);
					//Debugger.Log("_nextTmpPosition: " + _nextTmpPosition);
					if (_nextTmpPosition != firstTmpPosition)
					{
						setNextTmpPos(firstTmpPosition);
					}
					
					//Debugger.Log("_gObj.isAtDestination: " + _gObj.isAtDestination);
					if (_gObj.isAtDestination)
					{
						//Debugger.Log("_positionsPath.length: " + _positionsPath.length);
						if (_positionsPath.length > 1)
						{
							_positionsPath.shift();
						}
						else
						{
							takeCover();
						}
					}
				}
			}
		}
		
		private function setNextTmpPos(posStr:String):void 
		{
			_nextTmpPosition	= posStr;
			var obstHitAbsBoundaries	: Boundaries	= CollisionManager.instance.getAbsoluteHitBoundaries(_obstacle);
			var gObjtHitAbsBoundaries	: Boundaries	= CollisionManager.instance.getAbsoluteHitBoundaries(_gObj);
			
			var finalTargetX	: Number = 0;
			var finalTargetY	: Number = Config.floor_y;
			var finalTargetZ	: Number = 0;
			
			switch(posStr)
			{
				case RIGHT:
					finalTargetX = obstHitAbsBoundaries.x1 + _gObjWidth + 1;
					finalTargetZ = _obstacle.z + 1;
					break;
				case RIGHT_COVER:
					//finalTargetX = obstHitAbsBoundaries.x1 + (_gObj.x - gObjtHitAbsBoundaries.x0);
					finalTargetX = obstHitAbsBoundaries.x1 + _gObjWidth * .5;
					//finalTargetZ = (obstHitAbsBoundaries.z0 + obstHitAbsBoundaries.z1) * .5;
					finalTargetZ = _obstacle.z + 1;
					break;
				case LEFT:
					finalTargetX = obstHitAbsBoundaries.x0 - _gObjWidth - 1;
					finalTargetZ = _obstacle.z + 1;
					break;
				case LEFT_COVER:
					//finalTargetX = obstHitAbsBoundaries.x0 - (_gObj.x - gObjtHitAbsBoundaries.x0);
					finalTargetX = obstHitAbsBoundaries.x0 - _gObjWidth * .5;
					//finalTargetZ = (obstHitAbsBoundaries.z0 + obstHitAbsBoundaries.z1) * .5;
					finalTargetZ = _obstacle.z + 1;
					break;
				case BACK:
					finalTargetX = _obstacle.x;
					finalTargetZ = obstHitAbsBoundaries.z0 - _gObjDepth - 1;
					break;
				case BACK_COVER:
					//finalTargetX = (obstHitAbsBoundaries.x0 + obstHitAbsBoundaries.x1) * .5;
					finalTargetX = _obstacle.x;
					//finalTargetZ = obstHitAbsBoundaries.z0 - (_gObj.z - gObjtHitAbsBoundaries.z0);
					finalTargetZ = obstHitAbsBoundaries.z0 - _gObjDepth * .5;
					break;
				case FRONT:
					finalTargetX = _obstacle.x;
					finalTargetZ = obstHitAbsBoundaries.z1 + _gObjDepth + 1;
					break;
				case FRONT_COVER:
					//finalTargetX = (obstHitAbsBoundaries.x0 + obstHitAbsBoundaries.x1) * .5;
					finalTargetX = _obstacle.x;
					//finalTargetZ = obstHitAbsBoundaries.z1 + (_gObj.z - gObjtHitAbsBoundaries.z0);
					finalTargetZ = obstHitAbsBoundaries.z1 + _gObjDepth * .5;
					break;
				case DIAG_RIGHT_BACK:
					finalTargetX = obstHitAbsBoundaries.x1 + _gObjWidth + 1;
					finalTargetZ = obstHitAbsBoundaries.z0 - _gObjDepth - 1;
					break;
				case DIAG_RIGHT_FRONT:
					finalTargetX = obstHitAbsBoundaries.x1 + _gObjWidth + 1;
					finalTargetZ = obstHitAbsBoundaries.z1 + _gObjDepth + 1;
					break;
				case DIAG_LEFT_BACK:
					finalTargetX = obstHitAbsBoundaries.x0 - _gObjWidth - 1;
					finalTargetZ = obstHitAbsBoundaries.z0 - _gObjDepth - 1;
					break;
				case DIAG_LEFT_FRONT:
					finalTargetX = obstHitAbsBoundaries.x0 - _gObjWidth - 1;
					finalTargetZ = obstHitAbsBoundaries.z1 + _gObjDepth + 1;
					break;
			}
			
			//Debugger.Log("_gObj.SetDestinationPoint [" + posStr + "]: " + StringUtils.ToCoord3D(finalTargetX, finalTargetY, finalTargetZ));
			_gObj.SetDestinationPoint(finalTargetX, finalTargetY, finalTargetZ);
		}
		
		private function setNewPath(relativeDirectionInit:String, relativeDirectionFinal:String):void 
		{
			_nextTmpPosition	= "";
			switch(relativeDirectionInit)
			{
				case RIGHT:
					switch(relativeDirectionFinal)
					{
						case RIGHT:
							_positionsPath = [RIGHT];
							break;
						case LEFT:
							if (_gObj.z > _obstacle.z)	_positionsPath = [FRONT, LEFT];
							else	_positionsPath = [BACK, LEFT];
							break;
						case BACK:
							_positionsPath = [BACK];
							break;
						case FRONT:
							_positionsPath = [FRONT];
							break;
					}
					break;
				case LEFT:
					switch(relativeDirectionFinal)
					{
						case RIGHT:
							//if (_gObj.z > _obstacle.z)	_positionsPath = [FRONT, RIGHT];
							if (_gObj.z > _obstacle.z)	_positionsPath = [DIAG_LEFT_FRONT, DIAG_RIGHT_FRONT, RIGHT];
							//else	_positionsPath = [BACK, RIGHT];
							else	_positionsPath = [DIAG_LEFT_BACK, DIAG_RIGHT_BACK, RIGHT];
							break;
						case LEFT:
							_positionsPath = [LEFT];
							break;
						case BACK:
							_positionsPath = [BACK];
							break;
						case FRONT:
							_positionsPath = [FRONT];
							break;
					}
					break;
				case BACK:
					switch(relativeDirectionFinal)
					{
						case RIGHT:
							//_positionsPath = [RIGHT];
							_positionsPath = [DIAG_RIGHT_BACK, RIGHT];
							break;
						case LEFT:
							_positionsPath = [LEFT];
							break;
						case BACK:
							_positionsPath = [BACK];
							break;
						case FRONT:
							if (_gObj.x > _obstacle.x)	_positionsPath = [RIGHT, FRONT];
							else	_positionsPath = [LEFT, FRONT];
							break;
					}
					break;
				case FRONT:
					switch(relativeDirectionFinal)
					{
						case RIGHT:
							//_positionsPath = [RIGHT];
							_positionsPath = [DIAG_RIGHT_FRONT, RIGHT];
							break;
						case LEFT:
							_positionsPath = [LEFT];
							break;
						case BACK:
							if (_gObj.x > _obstacle.x)	_positionsPath = [RIGHT, BACK];
							else	_positionsPath = [LEFT, BACK];
							break;
						case FRONT:
							_positionsPath = [FRONT];
							break;
					}
					break;
			}
			
			_positionsPath.push(relativeDirectionFinal + "_cover");
			//Debugger.Log("setNewPath (" + relativeDirectionInit + " -> " + relativeDirectionFinal + "): " + _positionsPath);
		}
		
		private function getRelativeDirection(gObjToCheck:GameObject, refGObj:GameObject):String 
		{
			var relativeDirection:String	= "";
			
			var dX	: Number	= gObjToCheck.x - refGObj.x;
			var dZ	: Number	= gObjToCheck.z - refGObj.z;
			
			if (dZ == 0)
			{
				if (dX > 0) relativeDirection = RIGHT;
				else relativeDirection	= LEFT;
			}
			else
			if (dX == 0)
			{
				if (dZ > 0) relativeDirection = FRONT;
				else relativeDirection	= BACK;
			}
			else
			{
				var pendienteZXGbjToCheck	: Number		= dZ / dX;
				var refGObjHitAbsBoundaries	: Boundaries	= CollisionManager.instance.getAbsoluteHitBoundaries(refGObj);
				var pendienteZXRefGObjDiag 		: Number		= refGObjHitAbsBoundaries.height / refGObjHitAbsBoundaries.width;
				
				if (Math.abs(pendienteZXGbjToCheck) - pendienteZXRefGObjDiag > 0)
				{
					if (dZ > 0) relativeDirection = FRONT;
					else relativeDirection	= BACK;
				}
				else
				{
					if (dX > 0) relativeDirection = RIGHT;
					else relativeDirection	= LEFT;
				}
			}
			
			return relativeDirection;
		}
		
		override public function againstObstacle(positionRelativeToObstacle:String, obstacle:Obstacle):void
		{
			super.againstObstacle(positionRelativeToObstacle, obstacle);
			if (_finalCoverPosition == positionRelativeToObstacle)
			{
				takeCover();
			}
		}
		
		private function takeCover():void 
		{
			_numCoversLeft--;
			//Debugger.Log("TAKE COVER! - " + _numCoversLeft + " left");
			_positionsPath		= null;
			_gObj.ResetDestination();
			_gObj.SetStateExternal(Enemy.ST_HIDE, [_finalCoverPosition]);
			if (_numCoversLeft <= 0)
			{
				CompleteOk();
			}
		}
		
		
		
		// SOLO PARA PATHFINDING
		private function getCoverPosForInitPath():GameAreaPoint 
		{
			var coverPosition	: String	= "";
			var relativeDirectionTargetObst	: String	= getRelativeDirection(_target, _obstacle);
			
			switch(relativeDirectionTargetObst)
			{
				case RIGHT:
					coverPosition = LEFT;
					break;
				case LEFT:
					coverPosition = RIGHT;
					break;
				case BACK:
					coverPosition = FRONT;
					break;
				case FRONT:
					coverPosition = BACK;
					break;
			}
			
			var obstHitAbsBoundaries	: Boundaries	= CollisionManager.instance.getAbsoluteHitBoundaries(_obstacle);
			var gObjtHitAbsBoundaries	: Boundaries	= CollisionManager.instance.getAbsoluteHitBoundaries(_gObj);
			
			var finalTargetX	: Number = 0;
			var finalTargetY	: Number = Config.floor_y;
			var finalTargetZ	: Number = 0;
			
			switch(coverPosition)
			{
				case RIGHT:
					finalTargetX = obstHitAbsBoundaries.x1 + _gObjWidth + 1;
					finalTargetZ = _obstacle.z + 1;
					break;
				case LEFT:
					finalTargetX = obstHitAbsBoundaries.x0 - _gObjWidth - 1;
					finalTargetZ = _obstacle.z + 1;
					break;
				case BACK:
					finalTargetX = _obstacle.x;
					finalTargetZ = obstHitAbsBoundaries.z0 - _gObjDepth - 1;
					break;
				case FRONT:
					finalTargetX = _obstacle.x;
					finalTargetZ = obstHitAbsBoundaries.z1 + _gObjDepth + 1;
					break;
			}
			
			return new GameAreaPoint(finalTargetX, finalTargetY, finalTargetZ);
		}
		
	}

}
