package behaviors.enemies 
{
import enemies.Enemy;

import flash.events.Event;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_Grenade extends EnemyBehaviorAction
	{
		
		public function BehaviorAction_Grenade() {
			super();
		}
		
		/**
		 * 
		 * @param	args
		 * 0: Attack Id
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - GRENADE  -- _timeLeftToIncompleteMs: " + _timeLeftToIncompleteMs);
			
			_enemy.FaceTarget();
			_enemy.addEventListener(Enemy.GRENADE_FINISH_COMPLETE, OnGrenadeFinishComplete);
			_enemy.SetStateExternal(Enemy.ST_GRENADE);
		}
		
		override public function Leave():void {
			super.Leave();
			_enemy.removeEventListener(Enemy.GRENADE_FINISH_COMPLETE, OnGrenadeFinishComplete);
		}
		
		private function OnGrenadeFinishComplete(e:Event):void 
		{
			_enemy.removeEventListener(Enemy.GRENADE_FINISH_COMPLETE, OnGrenadeFinishComplete);
			CompleteOk();
		}
		
		override public function Reset():void {
			if (_enemy) {
				_enemy.removeEventListener(Enemy.GRENADE_FINISH_COMPLETE, OnGrenadeFinishComplete);
			} else {
				Debugger.Log("WARNING! - Trying to remove event liseners @action " + this + " without _enemy");
			}
			super.Reset();
		}
		
	}

}
