package  behaviors {
	
	/**
	 * ...
	 * @author ...
	 */
	public class Path
	{
		private var _name			: String	= "";
		private var _nodes			: Vector.<GameAreaPoint> 	= null;
		private var _possibleTeams	: Vector.<String> 			= null;
		
		private var _followedBy	: GameObject	= null;
		
		public function Path($possibleTeams:Vector.<String>, name:String) {
			_nodes 			= new Vector.<GameAreaPoint>();
			_name			= name;
			_possibleTeams	= $possibleTeams;
			_followedBy		= null;
		}
		
		public function unshift(o:GameAreaPoint):uint 
		{
			return _nodes.unshift(o as GameAreaPoint);
		}
		
		public function getNode(idx:int):GameAreaPoint 
		{
			return _nodes[idx];
		}
		
		public function get length():int 
		{
			return _nodes.length;
		}
		
		public function get nodes():Vector.<GameAreaPoint> 
		{
			return _nodes;
		}
		
		public function get possibleTeams():Vector.<String> 
		{
			return _possibleTeams;
		}
		
		public function get name():String 
		{
			return _name;
		}
		
		public function get followedBy():GameObject 
		{
			return _followedBy;
		}
		
		public function set followedBy(value:GameObject):void 
		{
			_followedBy = value;
		}
	}
}