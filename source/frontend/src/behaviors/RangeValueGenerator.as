package  behaviors {

import utils.MathUtils;

/**
	 * ...
	 * @author ...
	 */
	public class RangeValueGenerator extends ValueGenerator
	{
		protected var _min		: Number	= 0;
		protected var _max		: Number	= 1;
		protected var _forceInt	: Boolean	= false;
		
		public function RangeValueGenerator(min:Number, max:Number, forceInt:Boolean = false) {
			_min		= min;
			_max		= max;
			_forceInt	= forceInt;
		}
		
		override public function GetNewValue():Object {
			return MathUtils.random(_min, _max, _forceInt);
		}
		
		public function toString():String {
			return "[Range: (" + _min + ", " + _max + ") - int: " + _forceInt + "]";
		}
	}
}