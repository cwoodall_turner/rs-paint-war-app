package behaviors.common 
{
import behaviors.BehaviorAction;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_Wait extends BehaviorAction
	{
		
		protected var _waitTimeMs	: int = 0;
		protected var _timeLeftMs	: int = 0;
		
		public function BehaviorAction_Wait() {
			super();
		}
		
		/**
		 * 
		 * @param	args
		 * 0: wait Time (ms)
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			_waitTimeMs	= args[0];
			_timeLeftMs	= _waitTimeMs;
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - WAIT (" + _timeLeftMs + ") -- _timeLeftToIncompleteMs: " + _timeLeftToIncompleteMs + " -- args: " + args);
			_gObj.SetStateExternal(GameObject.ST_IDLE);
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			//if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION UPDATE - WAIT (" + _timeLeftMs + ")");
			_timeLeftMs -= dtMs;
			if (_timeLeftMs <= 0) {
				CompleteOk();
				return;
			}
		}
		
	}

}