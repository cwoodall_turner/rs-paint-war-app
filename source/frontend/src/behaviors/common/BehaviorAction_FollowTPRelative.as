package behaviors.common 
{
import behaviors.BehaviorAction;
import behaviors.GamePathFinder;
import behaviors.Path;

import view.LayerFactory;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_FollowTPRelative extends BehaviorAction
	{
		
		protected var _dXFromTarget	: Number	= 0;
		protected var _dYFromTarget	: Number	= 0;
		protected var _dZFromTarget	: Number	= 0;
		
		protected var _checkRect		: Boolean	= false;
		protected var _relativeLeftX 	: Number	= 10;
		protected var _relativeRightX 	: Number	= 10;
		protected var _relativeBackZ 	: Number	= 10;
		protected var _relativeFrontZ 	: Number	= 10;
		
		protected var _firstLoop	: Boolean	= true;
		
		protected var _initialPath				: Path	= null;
		protected var _initialPathCurrentIdx	: int	= 0;
		
		public function BehaviorAction_FollowTPRelative() {
			super();
		}
		
		
		/**
		 * 
		 * @param	args
		 * 0: dX from target
		 * 1: dZ from target
		 * 1: relativeLeftX 	(opcional)
		 * 1: relativeRightX 	(opcional)
		 * 1: relativeBackZ		(opcional)
		 * 1: relativeFrontZ	(opcional)
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			_dXFromTarget	= (args.length > 0 && !isNaN(args[0]))?args[0]:0;
			_dYFromTarget	= 0;
			_dZFromTarget	= (args.length > 1 && !isNaN(args[1]))?args[1]:0;
			
			if (args.length >= 6) {
				_checkRect		= true;
				_relativeLeftX 	= args[2];
				_relativeRightX = args[3];
				_relativeBackZ 	= args[4];
				_relativeFrontZ	= args[5];
			} else {
				_checkRect	= false;
			}
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - FOLLOW RELATIVE (dX:" + _dXFromTarget + " , dY: " +  _dYFromTarget + ", dZ: " + _dZFromTarget + ") -- _timeLeftToIncompleteMs: " + _timeLeftToIncompleteMs);
			if (_checkRect) {
				//Debugger.Log("- EXTRA PARMS! (" + "X: -" + _relativeLeftX + ", " + _relativeRightX + "  -  Z: -" + _relativeBackZ + ", " + _relativeFrontZ + ") -- _timeLeftToIncompleteMs: " + _timeLeftToIncompleteMs);
			} else {
				//Debugger.Log("DO NOT CHECK RECT");
			}
			if (!_gObj.IsCurrentState(GameObject.ST_IDLE) && !_gObj.IsCurrentState(_gObj.ST_MOVE_DEFAULT)) _gObj.SetStateExternal(GameObject.ST_IDLE);
			
			_firstLoop	= true;
			//SetAndCheckDestinationPoint();
			
			if (Config.use_pathfinding_to_avoid_obstacles && _target != null)
			{
				var initPos	: GameAreaPoint	= new GameAreaPoint(_gObj.x, _gObj.y, _gObj.z);
				var destPos	: GameAreaPoint	= new GameAreaPoint(_target.x + _dXFromTarget, _target.y + _dYFromTarget, _target.z + _dZFromTarget);
				_initialPath	= GamePathFinder.instance.getPath(initPos, destPos);
				_initialPathCurrentIdx	= 0;
				
				if (_initialPath != null && _initialPath.length == 0)
				{
					Debugger.Log("INITIAL PATH LENGTH 0");
					_initialPath = null;
				}
				
				if (Config.debug_pathfinding)
				{
					Debugger.Log("_initialPath: " + _initialPath);
					AreaDebugger.instance.drawPath(_initialPath, LayerFactory.Instance.GetLayer(LayerFactory.DEBUG_INIT_PATH).graphics, 0);
				}
			}
		}
		
		override public function Leave():void {
			super.Leave();
			_gObj.ResetDestination();
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			if (!_gObj || !_target || _target.IsCurrentState(GameObject.ST_READY)) {
				if (Config.debugThisGameObject(_gObj.id)) Debugger.Log("Warning: @Behavior " + this + ": Enemy -> " + _gObj + "  --  " + " Target -> " + _target  );
				CompleteOk();
				return;
			}
			SetAndCheckDestinationPoint();
		}
		
		private function SetAndCheckDestinationPoint():void 
		{
			var finalTargetX	: Number	= 0;
			var finalTargetY	: Number	= 0;
			var finalTargetZ	: Number	= 0;
			
			if (_initialPath != null)
			{
				finalTargetX	= _initialPath.getNode(_initialPathCurrentIdx).x;
				finalTargetY	= _initialPath.getNode(_initialPathCurrentIdx).y;
				finalTargetZ	= _initialPath.getNode(_initialPathCurrentIdx).z;
			}
			else
			{
				finalTargetX	= _target.x + _dXFromTarget;
				finalTargetY	= _target.y + _dYFromTarget;
				finalTargetZ	= _target.z + _dZFromTarget;
			}
			
			//if (_initialPath != null && !_firstLoop && !_gObj.destinationActive) {
				//Debugger.Log("NEXT LOOPS _gObj.isAtDestination (1): " + _gObj.isAtDestination);
				//CompleteOk();
				//return;
			//} else {
				//Debugger.Log("FIRST LOOP _gObj.isAtDestination (1): " + _gObj.isAtDestination);
			//}
			_firstLoop	= false;
			_gObj.SetDestinationPoint(finalTargetX, finalTargetY, finalTargetZ);
			//Debugger.Log("_gObj.isAtDestination (2): " + _gObj.isAtDestination);
			if(!_checkRect){
				if (_gObj.isAtDestination && (_gObj.IsCurrentState(GameObject.ST_IDLE) || _gObj.IsCurrentState(_gObj.ST_MOVE_DEFAULT))) {
					if (_initialPath != null)
					{
						_initialPathCurrentIdx++;
						if (_initialPathCurrentIdx >= _initialPath.length)
						{
							Debugger.Log("Initial Path complete!");
							_initialPath = null;
						}
					}
					else
					{
						CompleteOk();
					}
					return;
				}
			} else {
				// Set horizontal range limits (it depends on where the enemy is facing)
				var leftRangeX	: Number	= _gObj.IsFacingRight?_gObj.x - _relativeLeftX:_gObj.x - _relativeRightX;
				var rightRangeX	: Number	= leftRangeX + _relativeRightX + _relativeLeftX;
				
				
				if(Config.debug_attack_area){
					DrawBoundaries(leftRangeX, rightRangeX, _gObj.z - _relativeBackZ, _gObj.z + _relativeFrontZ);
				}
				
				// Is _target within range?
				if (	finalTargetZ <= (_gObj.z + _relativeFrontZ) && finalTargetZ >= (_gObj.z - _relativeBackZ)
					&&	finalTargetX >= leftRangeX && finalTargetX <= rightRangeX) {
					if (_initialPath != null)
					{
						_initialPathCurrentIdx++;
						if (_initialPathCurrentIdx >= _initialPath.length)
						{
							Debugger.Log("Initial Path complete!");
							_initialPath = null;
						}
					}
					else
					{
						CompleteOk();
					}
					return;
				}
			}
		}
		
	}

}
