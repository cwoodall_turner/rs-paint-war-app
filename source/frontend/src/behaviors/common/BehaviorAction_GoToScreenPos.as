package behaviors.common 
{
import behaviors.BehaviorAction;

import flash.geom.Point;

import view.Camera;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_GoToScreenPos extends BehaviorAction
	{
		protected var _targetScreenPos		: Point 		= new Point();
		protected var _targetGameAreaPos	: GameAreaPoint = new GameAreaPoint();
		
		public function BehaviorAction_GoToScreenPos() {
			super();
		}
		
		
		/**
		 * 
		 * @param	args
		 * 0: Screen X
		 * 1: Screen Y
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			
			_targetScreenPos.x	= args[0];
			_targetScreenPos.y	= args[1];
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - GO TO SCREEN POS (screenX:" + _targetScreenPos.x.toFixed(2) + " , screenY: " +  _targetScreenPos.y.toFixed(2) + ") -- _timeLeftToIncompleteMs: " + _timeLeftToIncompleteMs);
			if (!_gObj.IsCurrentState(GameObject.ST_IDLE) && !_gObj.IsCurrentState(_gObj.ST_MOVE_DEFAULT)) _gObj.SetStateExternal(GameObject.ST_IDLE);
			//SetAndCheckDestinationPoint();
		}
		
		override public function Leave():void {
			super.Leave();
			_gObj.ResetDestination();
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			if (!_gObj || !_target) {
				if (Config.debugThisGameObject(_gObj.id)) Debugger.Log("Warning: @Behavior " + this + ": GObj -> " + _gObj + "  --  " + " Target -> " + _target  );
				return;
			}
			SetAndCheckDestinationPoint();
		}
		
		private function SetAndCheckDestinationPoint():void {
			_targetGameAreaPos	= Camera.Instance.ConvertScreenPosToGamePos(_targetScreenPos, Config.floor_y);
			//Debugger.Log("_targetGameAreaPos: " + _targetGameAreaPos);
			_gObj.SetDestinationPoint(_targetGameAreaPos.x, _targetGameAreaPos.y, _targetGameAreaPos.z);
			if (_gObj.isAtDestination && (_gObj.IsCurrentState(GameObject.ST_IDLE) || _gObj.IsCurrentState(_gObj.ST_MOVE_DEFAULT))) {
				CompleteOk();
				return;
			}
		}
		
	}

}
