package behaviors.common 
{
import behaviors.BehaviorAction;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_CheckRange extends BehaviorAction
	{
		
		protected var _distanceToCheckPxSquared : Number	= 0;
		protected var _checkWithinRange			: Boolean	= true;
		
		public function BehaviorAction_CheckRange() {
			super();
		}
		
		/**
		 * 
		 * @param	args
		 * 0: distancePx:Number
		 * 2: checkOutsideRange:Boolean = false
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			var d : Number = args[0];
			_distanceToCheckPxSquared	= d * d;
			var checkOutsideRange : Boolean = (args.length >= 2 && args[1]);
			_checkWithinRange			= !checkOutsideRange;
			var insideOrOutside	: String = _checkWithinRange?"inside":"outside";
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - CHECK " + insideOrOutside + " RANGE (" + d + ") -- _timeLeftToIncompleteMs: " + _timeLeftToIncompleteMs);
			if (_timeLeftToIncompleteMs > 0) _gObj.SetStateExternal(GameObject.ST_IDLE);
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			CheckDistance();
		}
		
		private function CheckDistance():void 
		{
			var distanceSquared	: Number 	= (_gObj.x - _target.x) * (_gObj.x - _target.x) + (_gObj.z - _target.z) * (_gObj.z - _target.z);
			var isWithinRange	: Boolean	= (distanceSquared <= _distanceToCheckPxSquared);
			//Debugger.Log("distanceSquared: " + distanceSquared + " - vs - _distanceToCheckPxSquared:  " + _distanceToCheckPxSquared + " - _checkWithinRange: " + _checkWithinRange + " - isWithinRange: " + isWithinRange);
			if ((_checkWithinRange && isWithinRange) ||	(!_checkWithinRange && !isWithinRange)) {
				CompleteOk();
			} else {
				if(_timeLeftToIncompleteMs < 0) CompleteNo();
			}
		}
		
	}

}
