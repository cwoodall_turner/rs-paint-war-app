package behaviors.common 
{
import behaviors.BehaviorAction;

import enemies.EnemiesManager;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_CheckNumEnemiesGreaterThan extends BehaviorAction
	{
		
		protected var _numEnemiesToCheck	: int = 1;
		
		public function BehaviorAction_CheckNumEnemiesGreaterThan() {
			super();
		}
		
		/**
		 * 
		 * @param	args
		 * 0: numEnemiesToCheck:int
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			_numEnemiesToCheck = args[0];
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - CHECK NUM ENEMIES > (" + _numEnemiesToCheck + ")");
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			
			if (EnemiesManager.instance.numAliveEnemies > _numEnemiesToCheck) {
				CompleteOk();
			} else {
				CompleteNo();
			}
		}
		
	}

}
