package behaviors.common 
{
import behaviors.BehaviorAction;

import flash.geom.Point;

import view.Camera;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_CheckScreenPosXGreaterThan extends BehaviorAction
	{
		
		protected var _screenPosToCheck	: Point	= new Point();
		
		public function BehaviorAction_CheckScreenPosXGreaterThan() {
			super();
		}
		
		/**
		 * 
		 * @param	args
		 * 0: screenPosXToCheck:Number
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			_screenPosToCheck.x = args[0];
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - CHECK SCREEN POS >= (" + _screenPosToCheck.x + ")");
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			
			var gamePosToCheck	: GameAreaPoint	= Camera.Instance.ConvertScreenPosToGamePos(_screenPosToCheck,Config.floor_y);
			if (_gObj.x >= gamePosToCheck.x) {
				CompleteOk();
			} else {
				CompleteNo();
			}
		}
		
	}

}
