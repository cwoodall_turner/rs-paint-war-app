package behaviors.common 
{
import behaviors.BehaviorAction;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_CheckTargetWithinRectRange extends BehaviorAction
	{
		
		protected var _relativeLeftX 	: Number	= 0;
		protected var _relativeRightX 	: Number	= 0;
		protected var _relativeBackZ 	: Number	= 0;
		protected var _relativeFrontZ 	: Number	= 0;
		
		public function BehaviorAction_CheckTargetWithinRectRange() {
			super();
		}
		
		/**
		 * 
		 * @param	args
		 * 0: relativeLeftX:Number
		 * 1: relativeRightX:Number
		 * 2: relativeBackZ:Number
		 * 3: relativeFrontZ:Number
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			_relativeLeftX 	= args[0];
			_relativeRightX = args[1];
			_relativeBackZ 	= args[2];
			_relativeFrontZ	= args[3];
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - TARGET WITHIN RECT RANGE (" + "X: -" + _relativeLeftX.toFixed(2) + ", " + _relativeRightX.toFixed(2) + "  -  Z: -" + _relativeBackZ.toFixed(2) + ", " + _relativeFrontZ.toFixed(2) + ") -- _timeLeftToIncompleteMs: " + _timeLeftToIncompleteMs);
			if (_timeLeftToIncompleteMs > 0) _gObj.SetStateExternal(GameObject.ST_IDLE);
		}
		
		override public function Leave():void {
			super.Leave();
			//if (Config.debug_attack_area) {
				//ClearBoundaries();
			//}
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			
			// Is _target also within horizontal range?  (CompleteNo only if no timelimit is set)
			var leftRangeX	: Number	= _gObj.IsFacingRight?_gObj.x - _relativeLeftX:_gObj.x - _relativeRightX;
			var rightRangeX	: Number	= leftRangeX + _relativeRightX + _relativeLeftX;
			
			
			if(Config.debug_attack_area){
				DrawBoundaries(leftRangeX, rightRangeX, _gObj.z - _relativeBackZ, _gObj.z + _relativeFrontZ);
			}
			
			
			// Is _target within vertical range? (CompleteNo only if no timelimit is set)
			if (_target.z > (_gObj.z + _relativeFrontZ) || _target.z < (_gObj.z - _relativeBackZ)) {
				if(_timeLeftToIncompleteMs < 0) CompleteNo();
				return;
			}
			
			if (_target.x < leftRangeX || _target.x > rightRangeX) {
				if(_timeLeftToIncompleteMs < 0) CompleteNo();
				return;
			} else {
				CompleteOk();
			}
		}
		
	}

}
