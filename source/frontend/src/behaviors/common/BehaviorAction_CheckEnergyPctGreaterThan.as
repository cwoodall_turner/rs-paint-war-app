package behaviors.common 
{
import behaviors.BehaviorAction;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_CheckEnergyPctGreaterThan extends BehaviorAction
	{
		
		protected var _energyPctToCheck	: Number = 50;
		
		public function BehaviorAction_CheckEnergyPctGreaterThan() {
			super();
		}
		
		/**
		 * 
		 * @param	args
		 * 0: _energyPctToCheck:Number
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			_energyPctToCheck = args[0];
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - CHECK ENERGY PCT > (" + _energyPctToCheck + ")");
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			
			var energyPct	: Number	= _gObj.energy *  100 / _gObj.maxEnergy;
			if (energyPct > _energyPctToCheck) {
				CompleteOk();
			} else {
				CompleteNo();
			}
		}
		
	}

}
