package behaviors.common 
{
import behaviors.BehaviorAction;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_Bridge extends BehaviorAction
	{
		
		public function BehaviorAction_Bridge() {
			super();
		}
		
		/**
		 * 
		 * @param	args
		 * 0: next action id
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			var nextActionId	: String	= args[0];
			//if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION BRIDGE - (" + nextActionId + ")");
			_gObj.currentBehavior.GotoAction(nextActionId);
		}
		
	}

}
