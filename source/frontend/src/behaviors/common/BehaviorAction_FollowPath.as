package behaviors.common 
{
import behaviors.BehaviorAction;
import behaviors.GamePathFinder;
import behaviors.Path;

import enemies.EnemiesManager;

import obstacles.Obstacle;

import view.LayerFactory;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_FollowPath extends BehaviorAction
	{
		protected var _currentPath		: Path	= null;
		protected var _currentPathIdx	: int	= 0;
		protected var _numRoundsLeft	: int	= 0;
		
		protected var _initialPath				: Path	= null;
		protected var _initialPathCurrentIdx	: int	= 0;
		
		protected var _obstacleToSurround		: Obstacle	= null;
		protected var _sideNextToObstacle		: String	= CollisionManager.LEFT;
		protected var _lastCollisionPosition	: String	= CollisionManager.LEFT;
		
		
		
		public function BehaviorAction_FollowPath() {
			super();
		}
		
		
		/**
		 * 
		 * @param	args
		 * 0: path name
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			
			var pathName : String	= args[0];
			//Debugger.Log("pathName: " + pathName);
			
			switch (pathName)
			{
				case "[nearest]":
					_currentPath	= EnemiesManager.instance.getNearestPath(_gObj, true);
					break;
				case "[farthest]":
					_currentPath	= EnemiesManager.instance.getFarthestPath(_gObj, true);
					break;
				case "[random]":
					_currentPath	= EnemiesManager.instance.getRandomPath(_gObj);
					Debugger.Log("_currentPath: " + _currentPath);
					break;
				default:
					_currentPath	= EnemiesManager.instance.getPath(pathName);
					break;
			}
			if (_currentPath == null)
			{
				Debugger.Log("WARNING! - Unable to find path [" + pathName + "]");
				CompleteOk();
				return;
			}
			if (_currentPath.length == 0)
			{
				Debugger.Log("WARNING! - path " + _currentPath + " length 0");
				CompleteOk();
				return;
			}
			
			_currentPath.followedBy = _gObj;
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - FOLLOW PATH (path:" + pathName + ") -- _timeLeftToIncompleteMs: " + _timeLeftToIncompleteMs);
			
			if (!_gObj.IsCurrentState(GameObject.ST_IDLE) && !_gObj.IsCurrentState(_gObj.ST_MOVE_DEFAULT)) _gObj.SetStateExternal(GameObject.ST_IDLE);
			
			_currentPathIdx	= 0;
			var nearestNodeIdx : int = 0;
			if (_currentPath != null)
			{
				// Find nearest node
				var tmpIdx : int = _currentPath.length;
				var shortestDistance : Number = Number.MAX_VALUE;
				while (tmpIdx--)
				{
					var node : GameAreaPoint = _currentPath.getNode(tmpIdx);
					var distance : Number = (node.x - _gObj.x) * (node.x - _gObj.x) + (node.z - _gObj.z) * (node.z - _gObj.z);
					if (distance < shortestDistance)
					{
						shortestDistance 	= distance;
						nearestNodeIdx		= tmpIdx;
					}
				}
			}
			_currentPathIdx	= nearestNodeIdx;
			
			if (args.length < 2 || isNaN(args[1]) || int(args[1]) <= 0) _numRoundsLeft = int.MAX_VALUE;
			else _numRoundsLeft	= args[1];
			
			
			if (Config.use_pathfinding_to_avoid_obstacles)
			{
				var initPos	: GameAreaPoint	= new GameAreaPoint(_gObj.x, _gObj.y, _gObj.z);
				var destPos	: GameAreaPoint	= new GameAreaPoint(_currentPath.getNode(_currentPathIdx).x, _currentPath.getNode(_currentPathIdx).y, _currentPath.getNode(_currentPathIdx).z);
				_initialPath	= GamePathFinder.instance.getPath(initPos, destPos);
				_initialPathCurrentIdx	= 0;
				
				if (_initialPath != null && _initialPath.length == 0)
				{
					Debugger.Log("INITIAL PATH LENGTH 0");
					_initialPath = null;
				}
			}
			
			if (Config.debug_pathfinding)
			{
				Debugger.Log("_currentPath: " + _currentPath);
				Debugger.Log("_initialPath: " + _initialPath);
				AreaDebugger.instance.drawPath(_initialPath, LayerFactory.Instance.GetLayer(LayerFactory.DEBUG_INIT_PATH).graphics, 0);
				AreaDebugger.instance.drawPath(_currentPath, LayerFactory.Instance.GetLayer(LayerFactory.DEBUG_INIT_PATH).graphics, 0xffffff);
			}
		}
		
		override public function Leave():void {
			super.Leave();
			_gObj.ResetDestination();
			if (_currentPath != null)
			{
				_currentPath.followedBy = null;
			}
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			if (!_gObj) {
				if (Config.debugThisGameObject(_gObj.id)) Debugger.Log("Warning: @Behavior " + this + ": Enemy -> " + _gObj + "  --  " + " Target -> " + _target  );
				CompleteOk();
				return;
			}
			SetAndCheckDestinationPoint();
		}
		
		private function SetAndCheckDestinationPoint():void 
		{
			//Debugger.Log("NODE [" + _currentPathIdx + "]: " + _currentPath[_currentPathIdx]);
			var finalTargetX	: Number	= 0;
			var finalTargetY	: Number	= 0;
			var finalTargetZ	: Number	= 0;
			
			if (_initialPath != null)
			{
				if (_initialPath.length == 0)
				{
					CompleteOk();
					return;
				}
				finalTargetX	= _initialPath.getNode(_initialPathCurrentIdx).x;
				finalTargetY	= _initialPath.getNode(_initialPathCurrentIdx).y;
				finalTargetZ	= _initialPath.getNode(_initialPathCurrentIdx).z;
			}
			else if (_obstacleToSurround != null)
			{
				// Moverse en función al sentido en que se rodea el obstáculo y la dirección de la última colisión
			}
			else
			{
				if (_currentPath == null || _currentPath.length == 0)
				{
					CompleteOk();
					return;
				}
				finalTargetX	= _currentPath.getNode(_currentPathIdx).x;
				finalTargetY	= _currentPath.getNode(_currentPathIdx).y;
				finalTargetZ	= _currentPath.getNode(_currentPathIdx).z;
			}
			
			//if (!_firstLoop && !_gObj.destinationActive) {
				//Debugger.Log("NEXT LOOPS _gObj.isAtDestination (1): " + _gObj.isAtDestination);
				//CompleteOk();
				//return;
			//} else {
				//Debugger.Log("FIRST LOOP _gObj.isAtDestination (1): " + _gObj.isAtDestination);
			//}
			//_firstLoop	= false;
			_gObj.SetDestinationPoint(finalTargetX, finalTargetY, finalTargetZ);
			if (_gObj.isAtDestination && (_gObj.IsCurrentState(GameObject.ST_IDLE) || _gObj.IsCurrentState(_gObj.ST_MOVE_DEFAULT))) {
				
				if (_initialPath != null)
				{
					_initialPathCurrentIdx++;
					if (_initialPathCurrentIdx >= _initialPath.length)
					{
						Debugger.Log("Initial Path complete!");
						_initialPath = null;
					}
				}
				else
				{
					_currentPathIdx++;
					if (_currentPathIdx >= _currentPath.length)
					{
						// Round complete
						_numRoundsLeft --;
						if (_numRoundsLeft <= 0)
						{
							CompleteOk();
						}
						else
						{
							// Let's go another round.
							_currentPathIdx = 0;
						}
					}
				}
			}
		}
		
		override public function get debugStr():String {
			var pathName : String = "";
			if (_initialPath != null) pathName += "<" + _initialPath.name + ">";
			if (_currentPath != null) pathName += _currentPath.name;
			return super.debugStr + "-[path:" + pathName + "]";
		}
		
	}

}
