package behaviors.common 
{
import behaviors.BehaviorAction;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction_CheckTargetEnergyPctGreaterThan extends BehaviorAction
	{
		
		protected var _energyPctToCheck	: Number = 50;
		
		public function BehaviorAction_CheckTargetEnergyPctGreaterThan() {
			super();
		}
		
		/**
		 * 
		 * @param	args
		 * 0: energyPctToCheck:Number
		 */
		override public function Enter(args:Array):void {
			super.Enter(args);
			_energyPctToCheck = args[0];
			
			if (Config.debugThisGameObject(_gObj.id))	Debugger.Log("** " + _gObj.id + " - ACTION ENTER - CHECK TARGET ENERGY PCT > (" + _energyPctToCheck + ")");
		}
		
		override public function Update(dtMs:int):void {
			super.Update(dtMs);
			if (!_active) return;
			
			var energyPct	: Number	= _target.energy *  100 / _target.maxEnergy;
			if (energyPct > _energyPctToCheck) {
				CompleteOk();
			} else {
				CompleteNo();
			}
		}
		
	}

}
