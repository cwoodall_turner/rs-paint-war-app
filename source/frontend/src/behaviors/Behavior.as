package behaviors 
{
import flash.system.ApplicationDomain;
import flash.utils.Dictionary;

import obstacles.Obstacle;

/**
	 * ...
	 * @author ...
	 */
	public class Behavior 
	{
		public static const ACTION_ID_DEFAULT	: String	= "por-defecto";
		public static const BEHAVIOR_ID_DEFAULT	: String	= "por-defecto";
		
		public static const ACTION_CLASS_DEFAULT_GROUP	: String	= "common";
		
		protected var _gObj					: GameObjectWithBehavior	= null;
		protected var _target				: GameObject				= null;
		protected var _actions				: Dictionary				= null;
		protected var _actionsParms			: Dictionary				= null;
		protected var _prevAction			: BehaviorAction			= null;
		protected var _currentAction		: BehaviorAction			= null;
		protected var _currentActionIdx		: String					= null;
		protected var _startingIdx			: String					= "default-start";
		protected var _id					: String					= "por-defecto";
		protected var _stateClassNameBase	: String = "";
		
		protected var _onStateEnterNextActionIdValGens		: Dictionary	= null;
		protected var _onStateLeaveNextActionIdValGens		: Dictionary	= null;
		protected var _onGenericEventNextActionIdValGens	: Dictionary	= null;
		
		protected var _nextActionId	: String	= null;
		
		public function Behavior(type:String = "por-defecto", firstAction:String = "por-defecto") 
		{
			_actions		= new Dictionary();
			_actionsParms	= new Dictionary();
			_onStateEnterNextActionIdValGens	= new Dictionary();
			_onStateLeaveNextActionIdValGens	= new Dictionary();
			_onGenericEventNextActionIdValGens	= new Dictionary();
			SetFirstAction(firstAction);
			LoadBehavior(type);
		}
		
		public function SetFirstAction(firstAction:String):void {
			_startingIdx	= firstAction;
		}
		
		private function LoadBehavior(type:String):void 
		{
			var theBehaviors : XMLList	= BehaviorsLoader.Instance.theBehaviors.behavior;
			var l : int = theBehaviors.length();
			while (l--) {
				if (theBehaviors[l].@id == type) ParseBehavior(theBehaviors[l]);
			}
		}
		
		private function ParseBehavior(behavior:XML):void {
			
			Debugger.Log("Parsing behavior " + behavior.@id);
			_id				= behavior.@id;
			
			// ACTIONS
			var actions:XMLList	= behavior.action;
			var length : int = actions.length();
			for (var actionIdx:int = 0; actionIdx < length; actionIdx++ ) {
				AddAction(actions[actionIdx]);
			}
			
			// EVENTS
			AddStateEvents(behavior.events.onEnterState, AddOnStateEnterNextActionIdValueGen);
			AddStateEvents(behavior.events.onLeaveState, AddOnStateLeaveNextActionIdValueGen);
			AddGenericEvents(behavior.events.onEvent, AddGenericEventNextActionIdValueGen);
		}
		
		
		private function AddAction(action:XML):void 
		{
			var actionId	: String	= action.@id;
			var actionClass	: String	= action.@type;
			//trace("------------------- VVVVVVVVVVVVVVVVV ---------------------");
			//trace("ACTION " + actionId + " (" + actionClass + ")");
			//trace(action);
			
			var behaviorActionGroup	: String	= action.@group;
			//Debugger.Log("behaviorActionGroup: " + behaviorActionGroup);
			if (!behaviorActionGroup) behaviorActionGroup = ACTION_CLASS_DEFAULT_GROUP;
			var behaviorActionClassName	: String	= "behaviors." + behaviorActionGroup + ".BehaviorAction_" + action.@type;
			//Debugger.Log("ACTION  --  behaviorActionClassName: " + behaviorActionClassName);
			var behaviorActionClass		: Class		= ApplicationDomain.currentDomain.getDefinition(behaviorActionClassName) as Class;
			//trace("behaviorActionClass: " + behaviorActionClass);
			var behaviorAction	: BehaviorAction	= new behaviorActionClass();
			//trace("behaviorAction: " + behaviorAction);
			
			var behaviorActionId	: String	= action.@id;
			//trace("behaviorActionId: " + behaviorActionId);
			_actions[behaviorActionId]	= behaviorAction;
			behaviorAction.id = behaviorActionId;
			
			
			// PARMS!
			// ======
			var args	: Array	= new Array();
			var arg		: ValueGenerator	= null;
			
			var parms 	: XMLList	= action.parm;
			var length	: int		= parms.length();
			var i		: int		= 0;
			for (i = 0; i < length; i++) {
				var parm : XML	= parms[i];
				if ("@value" in parm) {
					// Fixed Value
					//trace("FIJO! (" + parm.@value + ")");
					arg	= new FixedValueGenerator(parm.@value);
				} else if (("@min" in parm) && ("@max" in parm)) {
					// Range Value
					//trace("RANGO! (" + parm.@min + ", " + parm.@max + ", " + ("@integer" in parm) + ")");
					arg	= new RangeValueGenerator(parm.@min, parm.@max, ("@integer" in parm));
				} else if (parm.option.length()) {
					// Options
					var options			: XMLList	= parm.option;
					var numOptions		: int		= options.length();
					var argOptions		: Array		= new Array();
					var argOptionsProb	: Array		= new Array();
					for (var j : int = 0; j < numOptions; j++) {
						var option 			: XML		= options[j];
						var probability		: Number	= option.@prob;
						var argOption		: ValueGenerator	= null;
						if ("@value" in option) {
							// Option Fixed Value
							//trace("OPCION - FIJO! (" + option.@value + ")");
							argOption	= new FixedValueGenerator(option.@value);
						} else if (("@min" in option) && ("@max" in option)) {
							// Option Range Value
							//trace("OPCION - RANGO! (" + option.@min + ", " + option.@max + ", " + ("@integer" in option) + ")");
							argOption	= new RangeValueGenerator(option.@min, option.@max, ("@integer" in option));
						} else {
							// ERROR
							trace("WARNING - No value for option[" + j + "] in " + options);
						}
						//trace("probability: " + probability);
						//trace("argOptions[ " + j + "]: " + argOption);
						argOptions.push(argOption);
						argOptionsProb.push(probability);
					}
					//trace("argOptions: " + argOptions);
					arg	= new ProbabilityValueGenerator(argOptions, argOptionsProb);
				} else {
					// ERROR
					//trace("WARNING - No value for parm[" + i + "] in " + parms);
				}
				args.push(arg);
			}
			_actionsParms[behaviorActionId]	= args;
			
			
			// UPDATE TARGET?
			// ==============
			var newTargetId	: String	= action.@target;
			if (newTargetId) {
				behaviorAction.targetIdToSetOnEnter	= newTargetId;
			}
			
			
			// ACTION TRANSITIONS
			// ==================
			var valueGen	: ValueGenerator	= null;
			// On Complete OK
			behaviorAction.onCompleteOkNextActionIdValueGen	= GetNextActionIdValueGen(action.onCompleteOk);
			// On Complete NO
			behaviorAction.onCompleteNoNextActionIdValueGen	= GetNextActionIdValueGen(action.onCompleteNo);
			// On Incomplete
			behaviorAction.onIncompleteNextActionIdValueGen	= GetNextActionIdValueGen(action.onIncomplete);
			if (behaviorAction.onIncompleteNextActionIdValueGen) {
				valueGen	= GetValueGen(action.onIncomplete, "timeLimitMs");
				var tempXmlListTime : XMLList	= action.onIncomplete as XMLList;
				if (valueGen) behaviorAction.SetTimeLimitValueGen(valueGen);
				else {
					//trace("WARNING: No time limit for action " + action);
				}
			}
			
			// On Rect Range Check
			var rangeConditions:XMLList	= action.onWithinRectRange;
			var rangeConditionsQty: int = rangeConditions.length();
			for (var rangeConditionIdx:int = 0; rangeConditionIdx < rangeConditionsQty; rangeConditionIdx++ ) {
				var rangeCondition : XML	= rangeConditions[rangeConditionIdx];
				var back 	: Number	= rangeCondition.@back;
				var front 	: Number	= rangeCondition.@front;
				var left 	: Number	= rangeCondition.@left;
				var right 	: Number	= rangeCondition.@right;
				var checkOutOfRangeStr		: String	= rangeCondition.attribute("check-outside");
				var checkOutOfRange			: Boolean	= ((checkOutOfRangeStr != null) && (checkOutOfRangeStr == "true"));
				var nextActionIdValueGen	: ValueGenerator	= new FixedValueGenerator(rangeCondition.@id);
				var targetStr				: String	= rangeCondition.@target;
				var debugStr				: String	= rangeCondition.attribute("debug");
				
				behaviorAction.addRangeCondition(new RangeCondition(back, front, left, right, checkOutOfRange, nextActionIdValueGen, targetStr, debugStr));
			}
			
			// On Conditions check
			var checkConditions:XMLList	= action.onConditionCheck;
			var checkConditionsQty: int = checkConditions.length();
			for (var checkConditionIdx:int = 0; checkConditionIdx < checkConditionsQty; checkConditionIdx++ ) {
				var checkCondition : XML	= checkConditions[checkConditionIdx];
				var conditionId	: String	= checkCondition.@condition;
				var value		: String	= checkCondition.@value;
				var conditionStatusToCheckStr	: String	= checkCondition.@conditionStatusToCheck;
				var conditionStatusToCheck	: Boolean	= ((conditionStatusToCheckStr == "") || (conditionStatusToCheckStr == "true"));
				var targetStr	: String	= checkCondition.@target;
				Debugger.Log("checkCondition.@conditionStatusToCheck: " + checkCondition.@conditionStatusToCheck);
				Debugger.Log("conditionStatusToCheck: " + conditionStatusToCheck);
				var nextActionIdValueGen	: ValueGenerator	= new FixedValueGenerator(checkCondition.@id);
				behaviorAction.addCheckCondition(new ConditionCheck(conditionId, value, nextActionIdValueGen, conditionStatusToCheck, targetStr));
			}
			//trace("------------------- AAAAAAAAAAAAAAAAAAAAAAA ---------------------");
		}
		
		private function AddStateEvents(onStates:XMLList, valueGenSetFunction:Function):void 
		{
			var length : int	= onStates.length();
			if (length) {
				for (var i : int = 0; i < length; i++) {
					var onState	: XML	= onStates[i];
					var state	: String	= onState.@state;
					var stateClassName	: String	= _stateClassNameBase + state;
					valueGenSetFunction(stateClassName, GetNextActionIdValueGenXml(onState));
				}
			}
		}
		
		private function AddGenericEvents(onEvents:XMLList, valueGenSetFunction:Function):void 
		{
			var length : int	= onEvents.length();
			if (length) {
				for (var i : int = 0; i < length; i++) {
					var onEvent	: XML	= onEvents[i];
					var event	: String	= onEvent.@event;
					valueGenSetFunction(event, GetNextActionIdValueGenXml(onEvent));
				}
			}
		}
		
		private function GetNextActionIdValueGen(nextActionIdXMLList:XMLList):ValueGenerator {
			var valueGen	: ValueGenerator	= null;
			if ("@id" in nextActionIdXMLList) {
				valueGen	= new FixedValueGenerator(nextActionIdXMLList.@id);
			} else if (nextActionIdXMLList.option.length()) {
				var nextActionIdOptions	: XMLList	= nextActionIdXMLList.option;
				var len					: int		= nextActionIdOptions.length();
				var valueGenerators	: Array	= new Array();
				var probabilities	: Array	= new Array();
				for (var k : int = 0; k < len; k ++) {
					var nextActionIdOption	: XML		= nextActionIdOptions[k];
					if ("@id" in nextActionIdOption) {
						valueGenerators.push(new FixedValueGenerator(nextActionIdOption.@id));
						probabilities.push(nextActionIdOption.@prob);
					} else {
						trace("WARNING - No value for nextActionIdOption[" + k + "] in " + nextActionIdXMLList);
					}
				}
				if (valueGenerators.length > 0)	valueGen	= new ProbabilityValueGenerator(valueGenerators, probabilities);
			}
			return valueGen;
		}
		
		private function GetValueGen(valueXMLList:XMLList, valueName:String):ValueGenerator {
			//Debugger.Log("-- GET VALUE GEN (" + valueName + " en " + valueXMLList);
			var valueGen	: ValueGenerator	= null;
			if (("@" + valueName) in valueXMLList) {
				//Debugger.Log("Existe atributo " + valueName + ": " + valueXMLList.attribute(valueName));
				valueGen	= new FixedValueGenerator(valueXMLList.attribute(valueName));
			}  else if (("@min" in valueXMLList) && ("@max" in valueXMLList)) {
				// Range Value
				//Debugger.Log("RANGO! (" + valueXMLList.@min + ", " + valueXMLList.@max + ", " + ("@integer" in valueXMLList) + ")");
				valueGen	= new RangeValueGenerator(valueXMLList.@min, valueXMLList.@max, ("@integer" in valueXMLList));
			}   else if (("@back" in valueXMLList) && ("@front" in valueXMLList) && ("@left" in valueXMLList) && ("@right" in valueXMLList)) {
				// Range Area Value
				//Debugger.Log("RANGO AREA! (" + valueXMLList.@back + ", " + valueXMLList.@front + ", " + ", " + valueXMLList.@left + ", " + valueXMLList.@right);
				valueGen	= new RelativeRangeAreaValueGenerator(valueXMLList.@back, valueXMLList.@front, valueXMLList.@left, valueXMLList.@right);
			} else {
				Debugger.Log("WARNING - No value for attribute " + valueName + " in " + valueXMLList);
			}
			return valueGen;
		}
		
		private function GetNextActionIdValueGenXml(nextActionIdXMLList:XML):ValueGenerator {
			var valueGen	: ValueGenerator	= null;
			if ("@id" in nextActionIdXMLList) {
				valueGen	= new FixedValueGenerator(nextActionIdXMLList.@id);
			} else if (nextActionIdXMLList.option.length()) {
				var nextActionIdOptions	: XMLList	= nextActionIdXMLList.option;
				var len					: int		= nextActionIdOptions.length();
				var valueGenerators	: Array	= new Array();
				var probabilities	: Array	= new Array();
				for (var k : int = 0; k < len; k ++) {
					var nextActionIdOption	: XML		= nextActionIdOptions[k];
					if ("@id" in nextActionIdOption) {
						valueGenerators.push(new FixedValueGenerator(nextActionIdOption.@id));
						probabilities.push(nextActionIdOption.@prob);
					} else {
						trace("WARNING - No value for nextActionIdOption[" + k + "] in " + nextActionIdXMLList);
					}
				}
				if (valueGenerators.length > 0)	valueGen	= new ProbabilityValueGenerator(valueGenerators, probabilities);
			}
			return valueGen;
		}
		
		private function ShowActions():void 
		{
			//trace("SHOWING ACTIONS");
			//trace("===============");
			for (var actionId:String in _actions) {
				var action	: BehaviorAction	= _actions[actionId];
				//trace("Action: " + actionId + " (" + action + ")");
				var args	: Array	= _actionsParms[actionId];
				if (!args) continue;
				var len	: int = args.length;
				for (var i : int = 0; i < len; i++) {
					//trace("Arg(" + i + "): " + args[i]);
				}
			}
		}
		
		public function SetGameObject($gObj:GameObjectWithBehavior):void {
			Debugger.Log("--- SETTING GAME OBJECT TO BEHAVIOR " + this + " --> " + $gObj + " ---");
			_gObj = $gObj;
			for each (var action:BehaviorAction in _actions) {
				action.SetGameObject($gObj);
			}
			
			if(_gObj){
				_gObj.currentBehavior = this;
			}
		}
		
		public function SetTarget(target:GameObject):void {
			//Debugger.Log("!!! " + _gObj.id + " ** SetTarget: " + target);
			//Debugger.LogStack();
			_target = target;
			for each (var action:BehaviorAction in _actions) {
				action.SetTarget(target);
			}
		}
		
		public function Update(dtMs:int):void {
			//if (_gObj is Sidekick) Debugger.Log("SIDEKICK - UPD BEHAVIOR");
			if (!_gObj || !_gObj.active) {
				//Debugger.Log("Warning: Behavior " + this + " without gObj.");
				return;
			}
			if (!_actions) {
				if (Config.debugThisGameObject(_gObj.id)) Debugger.Log("Warning: Behavior " + this + " without actions.");
				return;
			}
			
			if (_nextActionId) {
				if (Config.debugThisGameObject(_gObj.id)) Debugger.Log("** " + _gObj.id + " ** PASANDO A NEXT ACTION POR EVENTO: " + _nextActionId);
				GotoAction(_nextActionId);
				_nextActionId	= null;
			}
			
			if (_gObj.killed) {
				//Debugger.Log("GObj killed");
				return;
			}
			if (_gObj.IsCurrentState(GameObject.ST_READY)) {
				return;
			}
			
			if (!_currentAction) {
				if (Config.debugThisGameObject(_gObj.id)) Debugger.Log("Warning: Behavior " + this + " without current action.");
				return;
			}
			
			//if (_gObj is Sidekick) Debugger.Log("PRE UPD ACTION");
			_currentAction.Update(dtMs);
			
		}
		
		public function Reset():void {
			_prevAction		= null;
			_currentAction	= null;
			_currentActionIdx	= "[[no current action idx]]";
			for each (var action:BehaviorAction in _actions) {
				action.Reset();
			}
			SetGameObject(null);
			SetTarget(null);
		}
		
		public function Start():void {
			GotoAction(_startingIdx);
		}
		
		public function GotoAction(actionIdx:String):void 
		{
			//Debugger.LogStack();
			if (_currentActionIdx == actionIdx) {
				if (!_gObj || Config.debugThisGameObject(_gObj.id))  Debugger.Log("** " + _gObj.id + " ** WARNING! Going to same action: " +  _currentActionIdx + "  ->  " + actionIdx);
				//return;
			}
			if (!_gObj || Config.debugThisGameObject(_gObj.id))  Debugger.Log("** " + _gObj.id + " ** ------ BEHAVIOR ACTION TRANSITION [" + _gObj.id + "]: " + _currentActionIdx + " -> " + actionIdx);
			
			if (!_actions[actionIdx]) {
				if (!_gObj || Config.debugThisGameObject(_gObj.id))  Debugger.Log("** " + _gObj.id + " ** WARNING!!! - Trying to go to inexistent action " + actionIdx + " in Behavior " + this.id + ". Using por_defecto.");
				actionIdx	= Behavior.ACTION_ID_DEFAULT;
			}
			
			if (_currentAction) _currentAction.Leave();
			
			_currentActionIdx	= actionIdx;
			_prevAction			= _currentAction;
			_currentAction		= _actions[_currentActionIdx];
			
			var valueGens	: Array	= _actionsParms[_currentActionIdx];
			var tempArgs	: Array = new Array();
			if (valueGens) {
				var len			: int = valueGens.length;
				while (len--) {
					tempArgs.unshift((valueGens[len] as ValueGenerator).GetNewValue());
				}
			}
			
			if (_prevAction != null && _prevAction.type == _currentAction.type)
			{
				Debugger.Log("GOING TO SAME ACTION TYPE!!");
				_currentAction.ReEnter(tempArgs);
			}
			else
			{
				_currentAction.Enter(tempArgs);
			}
		}
		
		public function GotoActionExternal(actionIdx:String):void 
		{
			if (_currentActionIdx == actionIdx) return;
			Debugger.Log("Trying to go to action external (" + _currentActionIdx + " -> " + actionIdx + ")");
			GotoAction(actionIdx);
		}
		
		public function OnStateEnter(stateEnter:GameObjectState):void {
			if (_onStateEnterNextActionIdValGens[stateEnter.id]) {
				var nextActionId	: String	= String((_onStateEnterNextActionIdValGens[stateEnter.id] as ValueGenerator).GetNewValue());
				Debugger.Log("BEHAVIOR - OnStateEnter(" + stateEnter + ") -> " + nextActionId);
				GotoAction(nextActionId);
			}
		}
		
		public function OnStateLeave(stateLeave:GameObjectState):void {
			//if (Config.debugThisGameObject(_gObj.id)) Debugger.Log("** " + _gObj.id + " ** ON STATE LEAVE ( " + stateLeave.id + ") --  _currentBehavior: " + this + "  --  _onStateLeaveNextActionIdValGen: " + _onStateLeaveNextActionIdValGens[stateLeave.id]);
			if (_onStateLeaveNextActionIdValGens[stateLeave.id]) {
				var nextActionId	: String	= String((_onStateLeaveNextActionIdValGens[stateLeave.id] as ValueGenerator).GetNewValue());
				Debugger.Log("BEHAVIOR - OnStateLeave(" + stateLeave + ") -> " + nextActionId);
				
				// TMP!!!
				if (nextActionId == "VOLVER-A-ANTERIOR") {
					Debugger.Log("*** Volviendo a acción anterior: " + _currentActionIdx);
					nextActionId	= _currentActionIdx;
				}
				_nextActionId	= nextActionId;
				//GotoAction(nextActionId);
			}
		}
		
		public function OnEvent(event:String):void {
			if (Config.debugThisGameObject(_gObj.id)) Debugger.Log("** " + _gObj.id + " ** ON EVENT ( " + event + ") --  _currentBehavior: " + this + "  --  _onGenericEventNextActionIdValGens: " + _onGenericEventNextActionIdValGens[event]);
			if (_onGenericEventNextActionIdValGens[event]) {
				var nextActionId	: String	= String((_onGenericEventNextActionIdValGens[event] as ValueGenerator).GetNewValue());
				Debugger.Log("BEHAVIOR - EnEvent(" + event + ") -> " + nextActionId);
				
				// TMP!!!
				if (nextActionId == "VOLVER-A-ANTERIOR") {
					Debugger.Log("*** Volviendo a acción anterior: " + _currentActionIdx);
					nextActionId	= _currentActionIdx;
				}
				_nextActionId	= nextActionId;
				//GotoAction(nextActionId);
			}
		}
		
		public function AddOnStateEnterNextActionIdValueGen(stateClassName:String, valueGen:ValueGenerator) {
			_onStateEnterNextActionIdValGens[stateClassName]	= valueGen;
			//trace("ON ENTER - " + stateClassName + " - ValueGen: " + valueGen);
		}
		
		public function AddOnStateLeaveNextActionIdValueGen(stateClassName:String, valueGen:ValueGenerator) {
			_onStateLeaveNextActionIdValGens[stateClassName]	= valueGen;
			//trace("ON LEAVE - " + stateClassName + " - ValueGen: " + valueGen);
		}
		
		public function AddGenericEventNextActionIdValueGen(event:String, valueGen:ValueGenerator) {
			_onGenericEventNextActionIdValGens[event]	= valueGen;
			//trace("ON EVENT - " + event + " - ValueGen: " + valueGen);
		}
		
		public function GoToEntryAction(entryPoint:String):void {
			var entryBehaviorId	: String	= entryPoint;
			GotoAction(entryBehaviorId);
		}
		
		public function toString():String {
			//return "{Behavior: " + id + " - Current action: " + _currentActionIdx + "(" + _currentAction + ") - Prev. action: " + _prevAction + " - GObj: " + _gObj + "}";
			return "{Behavior: " + id + " - Current action: " + _currentAction + " - Prev. action: " + _prevAction + "}";
		}
		
		public function againstObstacle(positionRelativeToObstacle:String, obstacle:Obstacle):void 
		{
			if(_currentAction != null) _currentAction.againstObstacle(positionRelativeToObstacle, obstacle);
		}
		
		public function onBlocked(destinationDirectionWhenBlocked:uint):void 
		{
			if(_currentAction != null) _currentAction.onBlocked(destinationDirectionWhenBlocked);
		}
		
		
		
		
/* =============================================*/
/* ============= SETTERS & GETTERS =============*/
/* =============================================*/

		
		public function get id():String 
		{
			return _id;
		}
		
		public function get target():GameObject 
		{
			return _target;
		}
		
		public function get prevAction():BehaviorAction 
		{
			return _prevAction;
		}
		
		public function get gameObject():GameObject
		{
			return _gObj;
		}
		
		public function get currentAction():BehaviorAction 
		{
			return _currentAction;
		}
		
		public function OnHit():void {
			_currentAction.OnHit();
		}
		
	}

}