package behaviors 
{
import flash.display.MovieClip;

/**
	 * ...
	 * @author ...
	 */
	public class GameObjectWithBehavior extends GameObject
	{
		/* Destination */
		protected var _destination			: GameAreaPoint	= null;
		protected var _destinationActive	: Boolean	= false;
		
		/* Behavior */
		protected var _currentBehavior	: Behavior	= null;
		
		public function GameObjectWithBehavior($view:GameObjectView, $shadow:MovieClip = null, $id:String="1") 
		{
			super($view, $shadow, $id)
			_destination		= new GameAreaPoint(0, 0, 0);
			_destinationActive	= false;
			
		}
		
		override public function Update(dtMs:int): void {
			super.Update(dtMs);
		}
		
		
		override public function Reset():void {
			super.Reset();
			currentBehavior		= null;
			SetState(ST_READY);
		}
		
		public function SetDestinationPoint(destinationAreaPointX:Number, destinationAreaPointY: Number, destinationAreaPointZ:Number):void {
			_destination.x = destinationAreaPointX;
			_destination.y = destinationAreaPointY;
			_destination.z = destinationAreaPointZ;
			
			//if (!GameManager.Instance.currentLevel.scenery.allowZMovement)_destination.z = z;
			
			_destinationActive	= true;
			if (isAtDestination) {
				GotToDestination();
			}
		}
		
		public function get isAtDestination(): Boolean 
		{
			if (!_destinationActive) return true;
			var xToDest	: Number	= _destination.x - x;
			if ((xToDest > 0  && xToDest > Config.default_destination_tolerance_x)
				|| (xToDest < 0  && xToDest < -Config.default_destination_tolerance_x)) return false;
			var yToDest	: Number	= _destination.y - y;
			if ((yToDest > 0  && yToDest > Config.default_destination_tolerance_y)
				|| (yToDest < 0  && yToDest < -Config.default_destination_tolerance_y)) return false;
			var zToDest	: Number	= _destination.z - z;
			if ((zToDest > 0  && zToDest > Config.default_destination_tolerance_z)
				|| (zToDest < 0  && zToDest < -Config.default_destination_tolerance_z)) return false;
			return true;
		}
		
		public function ResetDestination():void {
			_destinationActive = false;
		}
		
		public function GotToDestination():void {
			ResetDestination();
		}
		
		public function OnStateEnter(stateEnter:GameObjectState):void {
			if (_currentBehavior) _currentBehavior.OnStateEnter(stateEnter);
		}
		
		public function OnStateLeave(stateLeave:GameObjectState):void {
			//if (Config.debugThisGameObject(id)) Debugger.Log("** " + id + " ** STATE LEAVE ( " + stateLeave + ") --  _currentBehavior: " + _currentBehavior);
			if (_currentBehavior) _currentBehavior.OnStateLeave(stateLeave);
		}
		
		public function CanSetStateExternal(newStateId:String):Boolean {
			if (!_currentState) return true;
			if (IsCurrentState(newStateId)) return _currentState.canReEnterSameState;
			return _currentState.CanSetNextStateExternal(newStateId);
		}
		
		public function SetStateExternal(newStateId:String, args:Array = null):void {
			if (CanSetStateExternal(newStateId)) {
				SetState(newStateId, args);
			} else {
				if ((ConfigLoader.Instance.Config["debug_behavior"] == "true") && Config.debugThisGameObject(id))
				{
					Debugger.Log("WARNING! Transicion de estado externa prohibida: " + currentState + " -> " + newStateId + " - GObj: " + this);
					Debugger.LogStack();
				}
			}
		}
		
		public function RemoveBehavior():void {
			if (!currentBehavior) return;
			currentBehavior.Reset();
			currentBehavior	= null;
		}
		
		
		
		private static const RIGHT	: String	= "right";
		private static const LEFT	: String	= "left";
		private static const BACK	: String	= "back";
		private static const FRONT	: String	= "front";
		public function FaceTarget():void {
			var target : GameObject = currentBehavior.target;
			if (!target) {
				//Debugger.Log("Gobj [" + this + "] trying to FaceTarget without Target");
				return;
			}
			var relativeTargetDirection: String	= getRelativeDirection(target, this);
			Debugger.Log("target: " + target);
			Debugger.Log("relativeTargetDirection: " + relativeTargetDirection);
			switch(relativeTargetDirection)
			{
				case RIGHT:
					FaceRight();
					break;
				case LEFT:
					FaceLeft();
					break;
				case BACK:
					FaceBack();
					break;
				case FRONT:
					FaceFront();
					break;
			}
		}
		private function getRelativeDirection(gObjToCheck:GameObject, refGObj:GameObject):String 
		{
			var relativeDirection:String	= "";
			
			var dX	: Number	= gObjToCheck.x - refGObj.x;
			var dZ	: Number	= gObjToCheck.z - refGObj.z;
			
			if (dZ == 0)
			{
				if (dX > 0) relativeDirection = RIGHT;
				else relativeDirection	= LEFT;
			}
			else
			if (dX == 0)
			{
				if (dZ > 0) relativeDirection = FRONT;
				else relativeDirection	= BACK;
			}
			else
			{
				var pendienteZXGbjToCheck	: Number		= dZ / dX;
				//var refGObjHitAbsBoundaries	: Boundaries	= CollisionManager.instance.getAbsoluteHitBoundaries(refGObj);
				//var pendienteZXRefGObjDiag 		: Number		= refGObjHitAbsBoundaries.height / refGObjHitAbsBoundaries.width;
				var pendienteZXRefGObjDiag 		: Number		= 1;
				
				if (Math.abs(pendienteZXGbjToCheck) - pendienteZXRefGObjDiag > 0)
				{
					if (dZ > 0) relativeDirection = FRONT;
					else relativeDirection	= BACK;
				}
				else
				{
					if (dX > 0) relativeDirection = RIGHT;
					else relativeDirection	= LEFT;
				}
			}
			
			return relativeDirection;
		}
		
		
/* =============================================*/
/* ============= SETTERS & GETTERS =============*/
/* =============================================*/
		
		public function get destination():GameAreaPoint 
		{
			return _destination;
		}
		
		public function get destinationActive():Boolean 
		{
			return _destinationActive;
		}
		
		public function get currentBehavior():Behavior 
		{
			return _currentBehavior;
		}
		
		public function set currentBehavior(value:Behavior):void 
		{
			_currentBehavior = value;
		}
		
		override public function toString():String {
			//return("GObjWBeh (" + id + ") State -> " + _currentState + " -- X -> " + _x.toFixed(1) + " -- Y -> " + _y.toFixed(1) + " -- Z: " + _z.toFixed(1));
			return("GObjWBeh (" + id + ") State -> " + _currentState + " -- Behavior -> " + currentBehavior);
		}
	}

}