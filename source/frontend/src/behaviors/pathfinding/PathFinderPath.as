/**
* ...
* @author Default
* @version 0.1
*/

package behaviors.pathfinding {

	public class PathFinderPath {
		
		private var _path:Array	= null;
		
		public function PathFinderPath() {
			_path	= new Array();
		}
		
		
		public function GetTileY(idx :uint) :uint {
			if (_path.length > idx) {
				return _path[idx][0];
			}
			else {
				return null;
			}
		}
		
		
		public function GetTileX(idx :uint) :uint {
			if (_path.length > idx) {
				return _path[idx][1];
			}
			else {
				return null;
			}
		}
		
		
		public function toString() :String {
			var returnValue : String = "Best Path (y,x)[" + _path.length + "]: ";
			for (var i:int = 0; i < _path.length-1; i++ ) {
				returnValue += "("+_path[i][0]+","+_path[i][1]+"),";
				//returnValue += "("+_path[i]+"),";
			}
			returnValue += "(" + _path[i][0] + "," + _path[i][1] + ")";
			//returnValue += "(" + _path[_path.length-1] + ")";
			
			return returnValue;
		}
		
		public function unshift(o:Array):uint 
		{
			return _path.unshift(o);
		}
		
		public function push(o:Array):uint 
		{
			return _path.push(o);
		}
		
		public function get length():int
		{
			return _path.length;
		}
		
		
	}
	
}
