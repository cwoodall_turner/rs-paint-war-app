/**
* ...
* @author Default
* @version 0.1
*/

package  behaviors.pathfinding {
import flash.display.Sprite;
import flash.geom.Point;

public class Map {
		
		private var _map		: Sprite	= null;
		private var _tilesMap 	: Array		= null;
		private var _mapBitMap 	: Array		= null;
		
		
		private 	var TILE_DEFAULT_WIDTH: Number = 40;
		private 	var TILE_DEFAULT_HEIGHT: Number = 40;
		private 	var MAP_Y_OFFSET: Number = 0;
		private 	var MAP_X_OFFSET: Number = 0;
		
		private var _numColumns	: int = 10;
		private var _numRows	: int = 10;
		
		public function Map(numColumns:int, numRows:int, tileWidth:Number, tileDepth:Number, horizontalMargin:Number, depthMargin:Number) {
			
			// TMP
			TILE_DEFAULT_WIDTH = tileWidth;
			TILE_DEFAULT_HEIGHT = tileDepth;
			MAP_X_OFFSET = horizontalMargin;
			MAP_Y_OFFSET = depthMargin;
			_numColumns	= numColumns;
			_numRows	= numRows;
			
			// Init bitmap: All walkable
			var initBitmap:Array	= new Array();
			var column:Array	= new Array();
			for (var rowNbr:int = 0; rowNbr < numRows; rowNbr++)
			{
				var row	: Array = new Array();
				for (var colNbr:int = 0; colNbr < numColumns; colNbr++)
				{
					row.push(1);
				}
				initBitmap.push(row);
			}
			SetMapBitMap(initBitmap);
		}
		
		
		
		public function Initialize() {
			
		}
		
		
		public function SetMapBitMap(mapBitMap : Array) :void {
			
			//trace("Map SetMapBitMap");
			_map		= new Sprite();
			_mapBitMap	= new Array(mapBitMap.length);
			_tilesMap 	= new Array(mapBitMap.length);
			for (var iH :uint = 0; iH < mapBitMap.length; iH++ ) {
				_mapBitMap[iH]	= new Array(mapBitMap[0].length);
				_tilesMap[iH] 	= new Array(mapBitMap[0].length);
				for (var jW :uint = 0; jW < mapBitMap[0].length; jW++ ) {
					_mapBitMap[iH][jW]	= mapBitMap[iH][jW];
					_tilesMap[iH][jW] 	= new Tile(iH, jW, MAP_Y_OFFSET + iH * TILE_DEFAULT_HEIGHT, MAP_X_OFFSET + jW * TILE_DEFAULT_WIDTH, TILE_DEFAULT_HEIGHT,  TILE_DEFAULT_WIDTH , (mapBitMap[iH][jW] >= 1) );
				}
			}
			
			_map.y		= MAP_Y_OFFSET;
			_map.x		= MAP_X_OFFSET;
			_map.graphics.beginFill(0xFF);
			_map.graphics.drawRect(0, 0, mapBitMap[0].length * TILE_DEFAULT_WIDTH, mapBitMap.length * TILE_DEFAULT_HEIGHT);
			_map.graphics.endFill();
			_map.alpha = 0;
			_map.mouseChildren = false;
		}
		
		
		
		public function ResetMapBitMap(mapBitMap : Array) :void {
			
			//trace("Map ResetMapBitMap");
			for (var iH :uint = 0; iH < mapBitMap.length; iH++ ) {
				for (var jW :uint = 0; jW < mapBitMap[0].length; jW++ ) {
					_mapBitMap[iH][jW]	= mapBitMap[iH][jW];
					(_tilesMap[iH][jW] as Tile).walkable = (mapBitMap[iH][jW] >= 1);
				}
			}
		}
		
		
		public function GetMapBitMap() :Array {			return _mapBitMap;		}
		
		
		
		public function UpdateMapBitmap(modifiedTile :Tile): void {
			
			SetTileWalkable(modifiedTile.tileY, modifiedTile.tileX, modifiedTile.walkable);
		}
		
		
		
		public function SetTileWalkable(tileY:uint, tileX:uint, walkable:Boolean): void {
			if (_tilesMap[tileY][tileX] != null) {
				if (walkable) {
					_mapBitMap[tileY][tileX] = 1;
				}
				else {
					_mapBitMap[tileY][tileX] = 0;
				}
				GetTile(tileY, tileX).walkable = walkable;
			}
		}
		
		
		
		public function GetCenterPosAtTile(y: uint, x :uint) :Point {
			var posX	: Number	= MAP_X_OFFSET + TILE_DEFAULT_WIDTH * x + TILE_DEFAULT_WIDTH * .5;
			var posY	: Number	= MAP_Y_OFFSET + TILE_DEFAULT_HEIGHT * y + TILE_DEFAULT_HEIGHT * .5;
			
			return new Point(posX, posY);
		}
		
		
		
		public function GetTileAt(y: uint, x :uint, forceWalkable:Boolean = false) :Tile {
			var tileY : int = (y - MAP_Y_OFFSET) / TILE_DEFAULT_HEIGHT;
			var tileX : int = (x - MAP_X_OFFSET) / TILE_DEFAULT_WIDTH;
			var tileToReturn : Tile = null;
			if (tileY < 0 || tileY >= GetHeight() || tileX < 0 || tileX >= GetWidth()) {
				var borderTileX	: int = (tileX < 0)?0:(tileX>=GetWidth())?GetWidth()-1:tileX;
				var borderTileY	: int = (tileY < 0)?0:(tileY>=GetHeight())?GetHeight()-1:tileY;
				tileToReturn = _tilesMap[borderTileY][borderTileX];
				if (forceWalkable && !tileToReturn.walkable) return getWalkableTileNearTo(tileToReturn.tileY, tileToReturn.tileX);
				return tileToReturn;
			}
			else {
				tileToReturn = _tilesMap[tileY][tileX];
				if (forceWalkable && !tileToReturn.walkable) return getWalkableTileNearTo(tileToReturn.tileY, tileToReturn.tileX);
				return tileToReturn;
			}
			return null;
		}
		
		private function getWalkableTileNearTo(tileY:uint, tileX:uint):Tile 
		{
			var possibleTile : Tile	= GetTile(tileY, tileX);
			if (possibleTile != null && possibleTile.walkable) return possibleTile;
			
			// CHECK NEIGHBORS
			var distance:int = 1;
			while (distance < 100)
			{
				// RIGHT
				possibleTile = GetTile(tileY, tileX + distance);
				if (possibleTile != null && possibleTile.walkable) return possibleTile;
				// LEFT
				possibleTile = GetTile(tileY, tileX - distance);
				if (possibleTile != null && possibleTile.walkable) return possibleTile;
				// UP
				possibleTile = GetTile(tileY - distance, tileX);
				if (possibleTile != null && possibleTile.walkable) return possibleTile;
				// DOWN
				possibleTile = GetTile(tileY + distance, tileX);
				if (possibleTile != null && possibleTile.walkable) return possibleTile;
				
				distance++;
			}
			Debugger.Log("WARNING!!! COULDN'T FIND WALKABLE TILE NEAR TO TILE(x: " + tileX + ", y: " + tileY +")");
			return null;
		}
		
		
		
		public function GetTile(tileY: uint, tileX :uint) :Tile {
			if (tileY < 0 || tileY >= GetHeight() || tileX < 0 || tileX >= GetWidth()) {
				return null;
			}
			else {
				return _tilesMap[tileY][tileX];
			}
		}
		
		
		
		public function GetWidth() :uint {
			return _mapBitMap[0].length;
		}
		
		
		public function GetHeight() :uint {
			return _mapBitMap.length;
		}
		
		public function logBitmap():void 
		{
			for each(var row:Array in _mapBitMap)
			{
				Debugger.Log("row: " + row);
			}
		}
		
	}
	
}
