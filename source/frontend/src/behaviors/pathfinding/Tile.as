/**
* ...
* @author Default
* @version 0.1
*/

package  behaviors.pathfinding {

	public class Tile {
		
		private var _walkable	: Boolean	= true;
		private var _tileY	: int = 0;
		private var _tileX	: int = 0;
		private var _posY	: Number = 0;
		private var _posX	: Number = 0;
		private var _width	: Number = 0;
		private var _height	: Number = 0;
		
		public function Tile(tileY:Number, tileX:Number, posY:Number, posX:Number, height:Number, width:Number, walkable:Boolean) {
			
			_tileY		= tileY;
			_tileX		= tileX;
			_posY		= posY;
			_posX		= posX;
			_width		= width;
			_height		= height;
			_walkable	= walkable;
		}
		
		public function toString():String
		{
			return "Tile[y:" + tileY + ",x:" + tileX + "] (posY: " + posY + ",posX:" + posX + ") - Walkable: " + walkable;
		}
		
		public function get tileY():int 
		{
			return _tileY;
		}
		
		public function get tileX():int 
		{
			return _tileX;
		}
		
		public function get height():Number 
		{
			return _height;
		}
		
		public function get width():Number 
		{
			return _width;
		}
		
		public function get posY():Number 
		{
			return _posY;
		}
		
		public function get posX():Number 
		{
			return _posX;
		}
		
		public function get walkable():Boolean 
		{
			return _walkable;
		}
		
		public function set walkable(value:Boolean):void 
		{
			_walkable = value;
		}
	}
	
}
