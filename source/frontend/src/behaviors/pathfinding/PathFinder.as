/**
* ...
* @author Alfredo Hodes
* @version 0.1
*/

package behaviors.pathfinding {
	//import pathfinding.Path;

	public class PathFinder {
		
		private static var _instance	:PathFinder;

		public static const HV_COST	: int = 10; // Costo de movimiento Horizontal/Vertical
		public static const D_COST	: int = 14; // Costo de Movimiento Diagonal
		public static const ALLOW_DIAGONAL			: Boolean 	= true;  // Permite mov. diagonales
		public static const ALLOW_CORNERING			: Boolean 	= false; // Permite cortar a traves de esquinas
		// MS_: �ndices para almacenar en un Array los distintos atributos de cada MapSpace.
		public static const MS_HEURISTIC_IDX		: int	= 0;
		public static const MS_OPEN_IDX				: int	= 1;
		public static const MS_CLOSED_IDX			: int	= 2;
		public static const MS_PARENTY_IDX			: int	= 3;
		public static const MS_PARENTX_IDX			: int	= 4;
		public static const MS_MOVEMENT_COST_IDX	: int	= 5;
		public static const MS_TOTAL_ATTRS			: int	= 6;
		public static const UNDEFINED_int			: int	= 999999999;
		
		private var _bestPath	: PathFinderPath  = null;
		private var _mapStatus	: Array = null;
		private var _openList	: Array = null;
		private var _mapH		: int;
		private var _mapW		: int;
		
		
		public static function get instance() :PathFinder {
			if (_instance == null) {
				_instance = new PathFinder( new SingletonEnforcer() )
			}
			
			return _instance;
		}
		
		
		public function PathFinder(singletonEnforcer :SingletonEnforcer) {
			
		}
		
		
		/*
		 * map = Array bidimensional donde cada celda vale: 0 (unwalkable) o 1+
		 * 		(a mayor valor, m�s cuesta atravesarlo)
		 * 
		 * return: Array de pares [y,x], donde la pos(0) es la celda origen y
		 * 	 	   la pos(length-1) es el destino. Devuelve null si no hay path.
		 * */
		public function FindPath(map:Array, startY:int, startX:int, endY:int, endX:int):PathFinderPath {
			// Obtengo el tamanio del mapa
			_mapH = map.length;
			_mapW = map[0].length;
			
			// Inicializo el status de cada tile
			_mapStatus = new Array(_mapH);
			for (var ii:int = 0; ii < _mapH; ii++) {
				_mapStatus[ii] = new Array(_mapW);
				for (var jj:int = 0; jj < _mapW; jj++) {
					_mapStatus[ii][jj] = new Array(MS_TOTAL_ATTRS);
					_mapStatus[ii][jj][MS_HEURISTIC_IDX] 		= UNDEFINED_int;
					_mapStatus[ii][jj][MS_OPEN_IDX] 			= false;
					_mapStatus[ii][jj][MS_CLOSED_IDX] 			= false;
					_mapStatus[ii][jj][MS_PARENTY_IDX] 			= UNDEFINED_int;
					_mapStatus[ii][jj][MS_PARENTX_IDX] 			= UNDEFINED_int;
					_mapStatus[ii][jj][MS_MOVEMENT_COST_IDX]	= UNDEFINED_int;
				}
			}
			
			// Inicializo la lista de abiertos
			_openList 	= new Array();
			
			// Agrego a la lista de abiertos la celda inicial
			openSquare (startY, startX, UNDEFINED_int, UNDEFINED_int, 0, 0, false);
			
			// Cicla hasta que no haya camino por recorrer o encontrar la celda destino
			while (_openList.length > 0 && !_mapStatus[endY][endX][MS_CLOSED_IDX]) {
				// Busca la celda m�s "cercana" de las libres
				var i:int = nearerSquare();
				var nowY:int = _openList[i][0];
				var nowX:int = _openList[i][1];
				// Cierra la celda actual, ya recorrida y sin posibilidad de mejorar
				closeSquare (nowY, nowX);
				// Agrega a la lista de libres las celdas adyacentes que cumplan con las condiciones:
				for (var j:int = nowY - 1; j <= nowY + 1; j++) {
					for (var k:int = nowX - 1; k <= nowX + 1; k++) {
						if (
							// si No esta fuera del mapa
							(j >= 0 && j < _mapH && k >= 0 && k < _mapW)
							// y No es la celda actual
							&& !(j == nowY && k == nowX)
							// y No es diagonal si no se permite (!ALLOW_DIAGONAL)
							&& (ALLOW_DIAGONAL || j == nowY || k == nowX)							
							// y No corta una esquina si no se permite (!ALLOW_CORNERING)
							&& (ALLOW_CORNERING  ||  ( map[nowY][k] != 0 && map[j][nowX] != 0 ))							
							// y No es una celda unwalkable
							&& (map[j][k] != 0)
							// y No esta en la lista de Cerrados
							&& (!_mapStatus[j][k][MS_CLOSED_IDX])) {
								var movementCost:int = _mapStatus[nowY][nowX][MS_MOVEMENT_COST_IDX]
									+ ((j == nowY || k == nowX ? HV_COST : D_COST) * map[j][k]);
								if (_mapStatus[j][k][MS_OPEN_IDX]) {
									// Si ya est� abierta, analiza si no se llega "mejor" desde la celda actual
									if (movementCost < _mapStatus[j][k][MS_MOVEMENT_COST_IDX]) {
										// Si es mas corto desde actual, actualizo el costo y la celda padre
										// (heuristica: undefined, porque ya se calculo la 1ra vez que se abrio)
										openSquare (j, k, nowY, nowX, movementCost, undefined, true);
									}
								} else {
									// Agrego la celda a la lista de Abiertas
									var heuristic:int = (Math.abs (j - endY) + Math.abs (k - endX)) * HV_COST;
									openSquare (j, k, nowY, nowX, movementCost, heuristic, false);
								}
							}
					}
				}
			}
			// Terminado de procesar, me fijo si se encontro un path o no
			var pFound:Boolean = _mapStatus[endY][endX][MS_CLOSED_IDX];
			
			// Reseteo _bestPath a null
			_bestPath = null;
			if (pFound) {
				// Genera el path
				_bestPath = new PathFinderPath();
				var nowBestY:int = endY;
				var nowBestX:int = endX;
				var newY:int = UNDEFINED_int;
				var newX:int = UNDEFINED_int;
				while ((nowBestY != startY || nowBestX != startX)) {
					_bestPath.unshift([nowBestY, nowBestX]);
					newY = _mapStatus[nowBestY][nowBestX][MS_PARENTY_IDX];
					newX = _mapStatus[nowBestY][nowBestX][MS_PARENTX_IDX];
					nowBestY = newY;
					nowBestX = newX;
				}
				_bestPath.unshift([startY, startX]);
			}
			
			if (_bestPath == null)
			{
				Debugger.Log("PATH NOT FOUND! De [" + startX + ", " + startY + "] a [" + endX + "," + endY + "]");
			}
			return (_bestPath);
		};
		
		
		
		
		
		// Devuelve la celda de la lista de Abiertas que tenga menor (movementCost + heuristic distance)
		protected function nearerSquare():int {
			var minimum		:int = 999999;
			var indexFound	:int = 0;
			var thisF		:int = undefined;
			var thisSquare	:Array = null;
			var i			:int = _openList.length;
			while (i-- > 0) {
				thisSquare = _mapStatus[_openList[i][0]][_openList[i][1]];
				thisF = thisSquare[MS_HEURISTIC_IDX] + thisSquare[MS_MOVEMENT_COST_IDX];
				if (thisF <= minimum) {
					minimum = thisF;
					indexFound = i;
				}
			}
			return indexFound;
		};
		
		
		protected function closeSquare(y:int, x:int):void {
			// Quito la celda de la lista de Abiertas
			var len:int = _openList.length;
			for (var i:int = 0; i < len; i++) {
				if (_openList[i][0] == y) {
					if (_openList[i][1] == x) {
						_openList.splice(i, 1);
						break;
					}
				}
			}
			// La agrego a la lista de Cerradas
			_mapStatus[y][x][MS_OPEN_IDX] 	= false;
			_mapStatus[y][x][MS_CLOSED_IDX]	= true;
		};
		
		
		protected function openSquare(y:int, x:int, parentY:int, parentX:int, 
			movementCost:int, heuristic:int, replacing:Boolean):void {
			// Agrego la celda a la lista de Abiertas
			if (!replacing) {
				_openList.push([y, x]);
				_mapStatus[y][x][MS_HEURISTIC_IDX] 	= heuristic;
				_mapStatus[y][x][MS_OPEN_IDX] 		= true;
				_mapStatus[y][x][MS_CLOSED_IDX]		= false;
			}
			_mapStatus[y][x][MS_PARENTY_IDX] = parentY;
			_mapStatus[y][x][MS_PARENTX_IDX] = parentX;
			_mapStatus[y][x][MS_MOVEMENT_COST_IDX] = movementCost;
		};
		
		
		public function FindPathForFlyer(map:Array, startY:int, startX:int, endY:int, endX:int):PathFinderPath {
			
			// Obtengo el tamanio del mapa
			_mapH = map.length;
			_mapW = map[0].length;
			
			_bestPath = new PathFinderPath();
			
			_bestPath.push([startY, startX]);
			while (startY < endY) {
				startY++;
				if (startX < endX) startX++;
				else if (startX > endX) startX--;
				_bestPath.push([startY, startX]);
			}
			while (startY > endY) {
				startY--;
				if (startX < endX) startX++;
				else if (startX > endX) startX--;
				_bestPath.push([startY, startX]);
			}
			while (startX < endX) {
				startX++;
				_bestPath.push([startY, startX]);
			}
			while (startX > endX) {
				startX--;
				_bestPath.push([startY, startX]);
			}

			return (_bestPath);
		}
		
	}
	
}

internal class SingletonEnforcer{}