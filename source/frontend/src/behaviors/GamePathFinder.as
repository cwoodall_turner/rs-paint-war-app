package behaviors 
{
import behaviors.pathfinding.Map;
import behaviors.pathfinding.PathFinder;
import behaviors.pathfinding.PathFinderPath;
import behaviors.pathfinding.Tile;

import flash.display.Graphics;
import flash.geom.Point;

import obstacles.ObstaclesManager;

import view.LayerFactory;

/**
	 * ...
	 * @author alfred
	 */
	public class GamePathFinder 
	{
		private static var _instance : GamePathFinder	= null;
		
		
		private var _horizontalMargin	: Number = 10;
		private var _depthMargin		: Number = 10;
		private var _tileWidth	: Number = 40;
		private var _tileDepth	: Number = 40;
		
		private var _map	: Map	= null;
		
		public function GamePathFinder() 
		{
		}
		
		static public function get instance():GamePathFinder 
		{
			if (_instance == null)
			{
				_instance = new GamePathFinder();
			}
			return _instance;
		}
		
		public function get map():Map 
		{
			return _map;
		}
		
		public function initialize():void {
			
		}
		
		public function prepareLevel(levelId:String):void
		{
			_tileWidth	= Config.getValue("level_" + levelId + "_pathfinding_tile_width");
			if (isNaN(_tileWidth)) _tileWidth = Config.getValue("default_pathfinding_tile_width");
			_tileDepth	= Config.getValue("level_" + levelId + "_pathfinding_tile_height");
			if (isNaN(_tileDepth)) _tileDepth = Config.getValue("default_pathfinding_tile_height");
			
			var playerLimits	: AbsoluteRangeArea	= GameManager.Instance.currentLevel.playerLimits;
			var numColumns	: int = Math.ceil(playerLimits.width / _tileWidth);
			//_horizontalMargin	= (playerLimits.width - numColumns * _tileWidth) * .5;
			_horizontalMargin	= playerLimits.leftX;
			
			
			//Debugger.Log("playerLimits: " + playerLimits);
			//Debugger.Log("playerLimits.width : " + playerLimits.width );
			
			
			var numRows	: int = Math.ceil(playerLimits.depth / _tileDepth);
			//_depthMargin	= (playerLimits.depth - numRows * _tileDepth) * .5;
			_depthMargin	= Config.floor_y + playerLimits.backZ;
			
			_map	= new Map(numColumns, numRows, _tileWidth, _tileDepth, _horizontalMargin, _depthMargin);
			
			if (Config.debug_pathfinding)
			{
				LayerFactory.Instance.GetLayer(LayerFactory.DEBUG_PATH_MAP).graphics.clear();
			}
			
			for (var column:int = 0; column < numColumns; column++)
			{
				for (var row:int = 0; row < numRows; row++)
				{
					var leftX	: Number	= _horizontalMargin + column * _tileWidth;
					var backZ	: Number	= _depthMargin + row * _tileDepth - Config.floor_y;
					var tileArea	: AbsoluteRangeArea	= new AbsoluteRangeArea(leftX, leftX + _tileWidth, Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY, backZ, backZ + _tileDepth);
					var areaBoundaries	: Boundaries	= new Boundaries(leftX, leftX + _tileWidth, Number.NEGATIVE_INFINITY, Number.POSITIVE_INFINITY, backZ, backZ + _tileDepth);
					
					var hasObstacle:Boolean = ObstaclesManager.instance.intersectsAnyObstacle(areaBoundaries);
					_map.SetTileWalkable(row, column, !hasObstacle);
					
					if (Config.debug_pathfinding)
					{
						//AreaDebugger.instance.DrawBoundaries(tile.posX, tile.posX + tile.width, tile.posY - Config.floor_y, tile.posY - Config.floor_y + tile.height);
						var canvas : Graphics = LayerFactory.Instance.GetLayer(LayerFactory.DEBUG_PATH_MAP).graphics;
						var rightX	: Number = leftX + _tileWidth;
						var frontZ	: Number = backZ + _tileDepth;
						canvas.lineStyle(.5, 0x1224A1, .5);
						if (hasObstacle)	canvas.beginFill(0xFFFF00, .3);
						else	canvas.endFill();
						canvas.drawRect(leftX, Config.floor_y + backZ, rightX - leftX, frontZ - backZ);
						canvas.endFill();
					}
				}
			}
			_map.logBitmap();
		}
		
		public function getPath(initPos:GameAreaPoint, destPos:GameAreaPoint):Path 
		{
			// TMP
			//initPos = new GameAreaPoint(510.95, 300, 565.7);
			//destPos = new GameAreaPoint(393, 300, 772);
			//
			var initMapPos	: Point	= new Point(initPos.x, initPos.y + initPos.z);
			var destMapPos	: Point	= new Point(destPos.x, destPos.y + destPos.z);
			
			var initMapTile	: Tile	= _map.GetTileAt(initMapPos.y, initMapPos.x);
			var destMapTile	: Tile	= _map.GetTileAt(destMapPos.y, destMapPos.x);
			
			if (Config.debug_pathfinding)
			{
				Debugger.Log("initMapTile: " + initMapTile);
				Debugger.Log("destMapTile: " + destMapTile);
			}
			
			if (initMapTile == null || destMapTile == null)
			{
				Debugger.Log("WARNING! Map Tile Null...");
				Debugger.Log("initMapTile: " + initMapTile);
				Debugger.Log("destMapTile: " + destMapTile);
				return null;
			}
			if (!initMapTile.walkable)
			{
				if (Config.debug_pathfinding)
				{
					Debugger.Log("WARNING! Init tile NOT walkable!!!");
				}
				// Check neighbor tiles (horizontal)
				var dX	: Number	= initMapPos.x - initMapTile.posX;
				var dY	: Number	= initMapPos.y - initMapTile.posY;
				
				var initMapTileX	: int = initMapTile.tileX;
				var initMapTileY	: int = initMapTile.tileY;
				
				if (Math.abs(dX) >= Math.abs(dY))
				{
					// Check neighbor tiles - 1st horizontal, then (if horizontal neighbor is NOT walkable) vertical
					if (dX >= 0) initMapTile = _map.GetTile(initMapTileY, initMapTileX + 1);
					else initMapTile = _map.GetTile(initMapTileY, initMapTileX - 1);
					if (initMapTile == null || !initMapTile.walkable)
					{
						if (dY >= 0) initMapTile = _map.GetTile(initMapTileY + 1, initMapTileX);
						else initMapTile = _map.GetTile(initMapTileY - 1, initMapTileX);
					}
				}
				else
				{
					// Check neighbor tiles - 1st horizontal, then (if horizontal neighbor is NOT walkable) vertical
					if (dY >= 0) initMapTile = _map.GetTile(initMapTileY + 1, initMapTileX);
					else initMapTile = _map.GetTile(initMapTileY - 1, initMapTileX);
					if (initMapTile == null || !initMapTile.walkable)
					{
						if (dX >= 0) initMapTile = _map.GetTile(initMapTileY, initMapTileX + 1);
						else initMapTile = _map.GetTile(initMapTileY, initMapTileX - 1);
					}
				}
				if (initMapTile == null || !initMapTile.walkable)
				{
					Debugger.Log("RE-WARNING! Init neighbor tile NOT walkable!!!");
					initMapTile	= _map.GetTileAt(initMapPos.y, initMapPos.x, true);
				}
			}
			
			if (!destMapTile.walkable)
			{
				if (Config.debug_pathfinding)
				{
					Debugger.Log("WARNING! Dest tile NOT walkable!!!: " + destMapTile);
					Debugger.Log("Level " + GameManager.Instance.currentLevel.id + " - " + GameManager.Instance.currentLevel.config);
					Debugger.Log("initPos: " + initPos);
					Debugger.Log("destPos: " + destPos);
				}
				// Check neighbor tiles (horizontal)
				var dX	: Number	= destMapPos.x - destMapTile.posX;
				var dY	: Number	= destMapPos.y - destMapTile.posY;
				
				var destMapTileX	: int = destMapTile.tileX;
				var destMapTileY	: int = destMapTile.tileY;
				
				if (Math.abs(dX) >= Math.abs(dY))
				{
					// Check neighbor tiles - 1st horizontal, then (if horizontal neighbor is NOT walkable) vertical
					if (dX >= 0) destMapTile = _map.GetTile(destMapTileY, destMapTileX + 1);
					else destMapTile = _map.GetTile(destMapTileY, destMapTileX - 1);
					if (destMapTile == null || !destMapTile.walkable)
					{
						if (dY >= 0) destMapTile = _map.GetTile(destMapTileY + 1, destMapTileX);
						else destMapTile = _map.GetTile(destMapTileY - 1, destMapTileX);
					}
				}
				else
				{
					// Check neighbor tiles - 1st horizontal, then (if horizontal neighbor is NOT walkable) vertical
					if (dY >= 0) destMapTile = _map.GetTile(destMapTileY + 1, destMapTileX);
					else destMapTile = _map.GetTile(destMapTileY - 1, destMapTileX);
					if (destMapTile == null || !destMapTile.walkable)
					{
						if (dX >= 0) destMapTile = _map.GetTile(destMapTileY, destMapTileX + 1);
						else destMapTile = _map.GetTile(destMapTileY, destMapTileX - 1);
					}
				}
				if (destMapTile == null || !destMapTile.walkable)
				{
					Debugger.Log("RE-WARNING! Dest neighbor tile NOT walkable!!!");
					destMapTile	= _map.GetTileAt(destMapPos.y, destMapPos.x, true);
				}
			}
			
			if (initMapTile == null || destMapTile == null)
			{
				Debugger.Log("TILE NULL");
				return null;
			}
			
			var pathFinderPath : PathFinderPath	= PathFinder.instance.FindPath(_map.GetMapBitMap(), initMapTile.tileY, initMapTile.tileX, destMapTile.tileY, destMapTile.tileX);
			if (pathFinderPath == null || pathFinderPath.length == 0)
			{
				Debugger.Log("PATH NULL");
				Debugger.Log("initMapTile.walkable: " + initMapTile.walkable);
				Debugger.Log("destMapTile.walkable: " + destMapTile.walkable);
				return null;
			}
			
			var optimizedPathFinderPath	: PathFinderPath = getOptimizedPathFinderPath(pathFinderPath);
			
			var possibleTeamsVect : Vector.<String> = new Vector.<String>();
			possibleTeamsVect.push("A");
			possibleTeamsVect.push("B");
			possibleTeamsVect.push("C");
			var path : Path	= new Path(possibleTeamsVect, "init-path");
			var i : int = optimizedPathFinderPath.length;
			//Debugger.Log("Path [" + initPos + "] -> [" + destPos + "]");
			
			var initMapTilePos	: GameAreaPoint	= new GameAreaPoint(initMapTile.posX + initMapTile.width * .5, Config.floor_y, initMapTile.posY + initMapTile.height * .5 - Config.floor_y);
			//Debugger.Log("initMapTilePos: " + initMapTilePos);
			
			var destMapTilePos	: GameAreaPoint	= new GameAreaPoint(destMapTile.posX + destMapTile.width * .5, Config.floor_y, destMapTile.posY + destMapTile.height * .5 - Config.floor_y);
			//Debugger.Log("destMapTilePos: " + destMapTilePos);
			
			
			
			while (i--)
			{
				var tileY	: int = optimizedPathFinderPath.GetTileY(i);
				var tileX	: int = optimizedPathFinderPath.GetTileX(i);
				
				var centerMapPos	: Point	= _map.GetCenterPosAtTile(tileY, tileX);
				var centerGameAreaPoint	: GameAreaPoint = new GameAreaPoint(centerMapPos.x, Config.floor_y, centerMapPos.y - Config.floor_y);
				path.unshift(centerGameAreaPoint);
			}
			
			
			i = pathFinderPath.length;
			while (i--)
			{
				var tileY	: int = pathFinderPath.GetTileY(i);
				var tileX	: int = pathFinderPath.GetTileX(i);
				
				var centerMapPos	: Point	= _map.GetCenterPosAtTile(tileY, tileX);
				var centerGameAreaPoint	: GameAreaPoint = new GameAreaPoint(centerMapPos.x, Config.floor_y, centerMapPos.y - Config.floor_y);
			}
			
			if (path.length == 0)
			{
				Debugger.Log("WARNING! - Calculated path length 0");
				return null;
			}
			
			
			
			return path;
		}
		
		
		private static const RIGHT	: String	= "right";
		private static const LEFT	: String	= "left";
		private static const BACK	: String	= "back";
		private static const FRONT	: String	= "front";
		private static const NONE	: String	= "none";
		private static const DIAG_RIGHT_BACK	: String	= "right_back";
		private static const DIAG_RIGHT_FRONT	: String	= "right_front";
		private static const DIAG_LEFT_BACK		: String	= "left_back";
		private static const DIAG_LEFT_FRONT	: String	= "left_front";
		private function getOptimizedPathFinderPath(pathFinderPath:PathFinderPath):PathFinderPath 
		{
			var optimizedPath	: PathFinderPath = new PathFinderPath();
			var numNodes	: int = pathFinderPath.length;
			var prevDirection	: String 	= "";
			for (var i : int = 0; i < numNodes -1; i++)
			{
				var currentTileY	: int = pathFinderPath.GetTileY(i);
				var currentTileX	: int = pathFinderPath.GetTileX(i);
				
				var nextTileY	: int = pathFinderPath.GetTileY(i+1);
				var nextTileX	: int = pathFinderPath.GetTileX(i + 1);
				
				var direction	: String	= getDirection(currentTileY, currentTileX, nextTileY, nextTileX);
				
				//Debugger.Log("Moving (" + currentTileX + " , " + currentTileY + ") -> (" + nextTileX + " , " + nextTileY + "): " + direction);
				
				if (direction != prevDirection)
				{
					optimizedPath.push([currentTileY, currentTileX]);
					prevDirection	= direction;
				}
			}
			optimizedPath.push([pathFinderPath.GetTileY(numNodes - 1), pathFinderPath.GetTileX(numNodes - 1)]);
			
			return optimizedPath;
		}
		
		private function getDirection(currentTileY:int, currentTileX:int, nextTileY:int, nextTileX:int):String 
		{
			var sideIdx		: int	= (nextTileX > currentTileX)?2:(nextTileX < currentTileX)? 0:1;
			var depthIdx	: int	= (nextTileY > currentTileY)?2:(nextTileY < currentTileY)? 0:1;
			
			var directions : Array	= [
										[DIAG_LEFT_BACK, BACK, DIAG_RIGHT_BACK],
										[LEFT, NONE, RIGHT],
										[DIAG_LEFT_FRONT, FRONT, DIAG_RIGHT_FRONT]
										];
										
			return directions[depthIdx][sideIdx];
		}
		
	}

}