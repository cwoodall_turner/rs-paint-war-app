package  behaviors {
	/**
	 * ...
	 * @author ...
	 */
	public class FixedValueGenerator extends ValueGenerator
	{
		protected var _value	: Object	= null;
		
		public function FixedValueGenerator(value:Object) {
			_value	= value;
		}
		
		override public function GetNewValue():Object {
			return _value;
		}
		
		public function toString():String {
			return "[Fixed: " + _value + "]";
		}
	}
}