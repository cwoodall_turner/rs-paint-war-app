﻿package behaviors 
{
import enemies.EnemiesManager;

import flash.geom.Point;
import flash.utils.getQualifiedClassName;

import obstacles.Obstacle;

import powerups.Powerup;
import powerups.PowerupsManager;

import utils.MathUtils;

import view.Camera;

/**
	 * ...
	 * @author ...
	 */
	public class BehaviorAction
	{
		
		protected var _gObj		: GameObjectWithBehavior	= null;
		protected var _target	: GameObject				= null;
		
		protected var _id	: String	= "";
		protected var _type	: String	= "";
		
		protected var _expiringTimeMsValueGen	: ValueGenerator	= null;
		protected var _expiringTimeMs			: int		= 0;
		protected var _timeLeftToIncompleteMs	: int		= 0;
		
		protected var _active					: Boolean	= true;
		
		protected var _onCompleteOkNextActionIdValueGen	: ValueGenerator	= null;
		protected var _onCompleteNoNextActionIdValueGen	: ValueGenerator	= null;
		protected var _onIncompleteNextActionIdValueGen	: ValueGenerator	= null;
		protected var _checkConditions					: Vector.<ConditionCheck> = null;
		protected var _rangeConditions					: Vector.<RangeCondition> = null;
		
		protected var _targetIdToSetOnEnter		: String			= "";
		
		protected var _reEntering	: Boolean	= false;
		
		protected var _lastObstacleAgainst	: Obstacle	= null;
		
		
		public function BehaviorAction() 
		{
			_timeLeftToIncompleteMs = 0;
			_type	= getQualifiedClassName(this).split("_").pop();
		}
		
		public function SetGameObject(gObj:GameObjectWithBehavior):void {
			_gObj = gObj;
		}
		
		public function SetTarget($target:GameObject):void {
			//Debugger.Log("SetTarget (" + _gObj + ") --> " + $target);
			_target = $target;
		}
		
		public function ReEnter(args:Array):void {
			_reEntering	= true;
			Enter(args);
		}
		
		public function Enter(args:Array):void {
			_lastObstacleAgainst = null;
			_active	= true;
			if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
				//Debugger.Log("** " + _gObj.id + " **ACTION ENTER[" + this + "] -- GOBJ: " + _gObj + " -- _expiringTimeMsValueGen: " + _expiringTimeMsValueGen + " -- _withinRectRangeValueGen: " + _withinRectRangeValueGen);
				Debugger.Log("** " + _gObj.id + " **ACTION ENTER[" + this + "] -- _expiringTimeMsValueGen: " + _expiringTimeMsValueGen);
			}
			if (_expiringTimeMsValueGen) _timeLeftToIncompleteMs	= int(_expiringTimeMsValueGen.GetNewValue());
			else _timeLeftToIncompleteMs	= -1;
			
			if (_target == null && (targetIdToSetOnEnter == null || targetIdToSetOnEnter == ""))
			{
				//Debugger.Log("WARNING! Forcing target player for action " + this.id + " (Behavior: " + _gObj.currentBehavior.id + ")");
				targetIdToSetOnEnter = "player";
			}
			
			if (targetIdToSetOnEnter) {
				if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
					Debugger.Log("** " + _gObj.id + " ******SETTING NEW TARGET: " + targetIdToSetOnEnter);
				}
				var possibleTargetsIdxs	:Vector.<int> = null;
				var possibleTarget		: GameObject	= null;
				var currentIdx	: int = 0;
				switch(targetIdToSetOnEnter) {
					case "player":
						_gObj.currentBehavior.SetTarget(GameManager.Instance.activePlayer);
						break;
					case "firstEnemy":
						_gObj.currentBehavior.SetTarget(EnemiesManager.instance.aliveEnemy);
						break;
					case "anyEnemy":
						possibleTargetsIdxs = new Vector.<int>();
						currentIdx	= EnemiesManager.instance.theEnemies.length;
						while (currentIdx--)
						{
							possibleTarget	= EnemiesManager.instance.theEnemies[currentIdx] as GameObject;
							if (!possibleTarget.IsCurrentState(GameObject.ST_READY) && !possibleTarget.killed)
							{
								possibleTargetsIdxs.push(currentIdx);
							}
						}
						if (possibleTargetsIdxs.length > 0)
						{
							currentIdx	= MathUtils.random(0, possibleTargetsIdxs.length - 1, true);
							possibleTarget	= EnemiesManager.instance.theEnemies[possibleTargetsIdxs[currentIdx]] as GameObject;
							_gObj.currentBehavior.SetTarget(possibleTarget);
						}
						else 
						{
							_gObj.currentBehavior.SetTarget(null);
						}
						break;
					case "firstEnemyOnScreen":
						_gObj.currentBehavior.SetTarget(EnemiesManager.instance.enemyOnScreen);
						break;
					case "anyEnemyOnScreen":
						possibleTargetsIdxs = new Vector.<int>();
						currentIdx	= EnemiesManager.instance.theEnemies.length;
						while (currentIdx--)
						{
							possibleTarget	= EnemiesManager.instance.theEnemies[currentIdx] as GameObject;
							if (possibleTarget.visible && !possibleTarget.IsCurrentState(GameObject.ST_READY) && !possibleTarget.killed)
							{
								var enemyScreenPos	: Point	= Camera.Instance.ConvertGamePosToScreenPos(new GameAreaPoint(possibleTarget.x, possibleTarget.y, possibleTarget.z));
								if (enemyScreenPos.x > 0 && enemyScreenPos.x < Config.game_width && enemyScreenPos.y > 0 && enemyScreenPos.y < Config.game_height)
								{
									possibleTargetsIdxs.push(currentIdx);
								}
							}
						}
						if (possibleTargetsIdxs.length > 0)
						{
							currentIdx	= MathUtils.random(0, possibleTargetsIdxs.length - 1, true);
							possibleTarget	= EnemiesManager.instance.theEnemies[possibleTargetsIdxs[currentIdx]] as GameObject;
							_gObj.currentBehavior.SetTarget(possibleTarget);
						}
						else 
						{
							_gObj.currentBehavior.SetTarget(null);
						}
						break;
					case "lastHitBy":
						_gObj.currentBehavior.SetTarget(_gObj.lastHitBy);
						break;
					case "nearestDogTag":
						_gObj.currentBehavior.SetTarget(PowerupsManager.instance.getNearestPowerup(_gObj, Powerup.ID_DOG_TAG));
						break;
					default:
						Debugger.Log("WARNING! Trying to set new target " + targetIdToSetOnEnter);
						break;
				}
				possibleTargetsIdxs	= null;
				possibleTarget		= null;
			}
		}
		
		public function Update(dtMs:int):void {
			if (!_active) return;
			
			// Time out?
			if (_timeLeftToIncompleteMs > 0) {
				_timeLeftToIncompleteMs -= dtMs;
				if (_timeLeftToIncompleteMs <= 0) Incomplete();
			}
			
			// RANGE CONDITIONS
			if (_rangeConditions) {
				for each (var rangeCondition : RangeCondition in _rangeConditions) {
					if (rangeCondition.check(_gObj)) {
						//Debugger.Log("Checking " + rangeCondition + " on " + _target + "... TRUE");
						rangeConditionMet(rangeCondition);
						return;
					} else {
						//Debugger.Log("Checking " + rangeCondition + " on " + _target + "... FALSE");
					}
				}
			}
			
			
			// CHECK CONDITIONS
			if (_checkConditions) {
				for each (var checkCondition : ConditionCheck in _checkConditions) {
					if (ConditionCheck.check(checkCondition, _gObj)) {
						//Debugger.Log("Checking " + checkCondition + " on " + _target + "... TRUE");
						conditionMet(checkCondition);
						return;
					} else {
						//Debugger.Log("Checking " + checkCondition + " on " + _target + "... FALSE");
					}
				}
			}
		}
		
		public function Leave():void {
			_reEntering	= false;
			_active	= false;
			//if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
				//Debugger.Log("** " + _gObj.id + " **ACTION LEAVE " + this.id);
			//}
		}
		
		public function Reset():void {
			_gObj	= null;
			_target	= null;
			//ClearBoundaries();
		}
		
		public function SetTimeLimitValueGen(valueGen:ValueGenerator):void {
			_expiringTimeMsValueGen	= valueGen;
		}
		
		
		
		public function CompleteOk():void {
			if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
				Debugger.Log("** " + _gObj.id + " ** ACTION " + this + " COMPLETE [OK]");
			}
			if (_onCompleteOkNextActionIdValueGen == null) {
				if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
					Debugger.Log("Warning: Action " + this + " without _onCompleteOkNextActionIdValueGen");
				}
				return;
			}
			_active	= false;
			var nextStateId	: String	= String(_onCompleteOkNextActionIdValueGen.GetNewValue());
			_gObj.currentBehavior.GotoAction(nextStateId);
		}
		
		protected function CompleteNo():void {
			if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
				Debugger.Log("** " + _gObj.id + " ** ACTION " + this + " COMPLETE [NO]");
			}
			if (_onCompleteNoNextActionIdValueGen == null) {
				if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
					Debugger.Log("Warning: Action " + this + " without _onCompleteNoNextActionIdValueGen");
					Debugger.LogStack();
				}
				return;
			}
			_active	= false;
			var nextStateId	: String	= String(_onCompleteNoNextActionIdValueGen.GetNewValue());
			_gObj.currentBehavior.GotoAction(nextStateId);
		}
		
		protected function Incomplete():void {
			if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
				Debugger.Log("** " + _gObj.id + " ** ACTION " + this + " INCOMPLETE");
			}
			if (_onIncompleteNextActionIdValueGen == null) {
				if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
					Debugger.Log("Warning: Action " + this + " without _onIncompleteNextActionIdValueGen");
				}
				return;
			}
			_active	= false;
			var nextStateId	: String	= String(_onIncompleteNextActionIdValueGen.GetNewValue());
			_gObj.currentBehavior.GotoAction(nextStateId);
		}
		
		private function conditionMet(checkCondition:ConditionCheck):void 
		{
			if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
				Debugger.Log("** " + _gObj.id + " ** ACTION " + this + " CONDITION MET " + checkCondition);
			}
			if (checkCondition.nextActionIdValueGen == null) {
				if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
					Debugger.Log("Warning: Action " + this + " without checkCondition nextActionIdValueGen");
				}
				return;
			}
			_active	= false;
			var nextStateId	: String	= String(checkCondition.nextActionIdValueGen.GetNewValue());
			_gObj.currentBehavior.SetTarget(checkCondition.targetChecked);
			_gObj.currentBehavior.GotoAction(nextStateId);
		}
		
		private function rangeConditionMet(rangeCondition:RangeCondition):void 
		{
			if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
				Debugger.Log("** " + _gObj.id + " ** ACTION " + this + "RANGE CONDITION MET " + rangeCondition);
			}
			if (rangeCondition.nextActionIdValueGen == null) {
				if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
					Debugger.Log("Warning: Action " + this + " without rangeCondition nextActionIdValueGen");
				}
				return;
			}
			_active	= false;
			var nextStateId	: String	= String(rangeCondition.nextActionIdValueGen.GetNewValue());
			_gObj.currentBehavior.SetTarget(rangeCondition.targetChecked);
			_gObj.currentBehavior.GotoAction(nextStateId);
		}
		
		public function set onCompleteOkNextActionIdValueGen(value:ValueGenerator):void 
		{
			_onCompleteOkNextActionIdValueGen = value;
			//trace("_onCompleteOkNextActionIdValueGen: " + _onCompleteOkNextActionIdValueGen);
		}
		
		public function set onCompleteNoNextActionIdValueGen(value:ValueGenerator):void 
		{
			_onCompleteNoNextActionIdValueGen = value;
			//trace("_onCompleteNoNextActionIdValueGen: " + _onCompleteNoNextActionIdValueGen);
		}
		
		public function set onIncompleteNextActionIdValueGen(value:ValueGenerator):void 
		{
			_onIncompleteNextActionIdValueGen = value;
			//trace("_onIncompleteNextActionIdValueGen: " + _onIncompleteNextActionIdValueGen);
		}
		
		public function get onCompleteOkNextActionIdValueGen():ValueGenerator 
		{
			return _onCompleteOkNextActionIdValueGen;
		}
		
		public function get onCompleteNoNextActionIdValueGen():ValueGenerator 
		{
			return _onCompleteNoNextActionIdValueGen;
		}
		
		public function get onIncompleteNextActionIdValueGen():ValueGenerator 
		{
			return _onIncompleteNextActionIdValueGen;
		}
		
		public function OnHit():void {
		}
		
		public function againstObstacle(positionRelativeToObstacle:String, obstacle:Obstacle):void
		{
			_lastObstacleAgainst	= obstacle;
		}
		
		
		public function addCheckCondition(targetCheckCondition:ConditionCheck):void 
		{
			if (!_checkConditions) _checkConditions = new Vector.<ConditionCheck>();
			if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
				Debugger.Log("-- ADD CHECK CONDITION (" + this + "): " + targetCheckCondition);
			}
			_checkConditions.push(targetCheckCondition);
		}
		
		
		public function addRangeCondition(rangeCondition:RangeCondition):void 
		{
			if (!_rangeConditions) _rangeConditions = new Vector.<RangeCondition>();
			if (!_gObj || Config.debugThisGameObject(_gObj.id)) {
				Debugger.Log("-- ADD RANGE CONDITION (" + this + "): " + rangeCondition);
			}
			_rangeConditions.push(rangeCondition);
		}
		
		public function get targetIdToSetOnEnter():String 
		{
			return _targetIdToSetOnEnter;
		}
		
		public function set targetIdToSetOnEnter(value:String):void 
		{
			_targetIdToSetOnEnter = value;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function set id(value:String):void 
		{
			_id = value;
		}
		//public function OnHitAnimationComplete():void { }
		
		protected function DrawBoundaries(leftX:Number, rightX:Number, backZ:Number, frontZ:Number):void {
			
			if (!Config.debug_attack_area) return;
			AreaDebugger.instance.DrawBoundaries(leftX, rightX, backZ, frontZ);
		}
		
		public function toString():String {
			return "<" + id + ">(" + _type + ")";
		}
		
		public function onBlocked(destinationDirectionWhenBlocked:uint):void
		{
			Debugger.Log("ENEMY BLOCKED! -> FORCING ACTION COMPLETE! (" + this.debugStr + ")");
			CompleteOk();
		}
		
		
		public function get debugStr():String {
			return "<" + id + ">(" + _type + ")";
		}
		
		public function get type():String 
		{
			return _type;
		}
		
	}

}