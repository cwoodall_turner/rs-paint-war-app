﻿package {
import behaviors.Path;

import flash.display.Graphics;

public class AreaDebugger {
		
		private static var _instance : AreaDebugger	= null;
		
		private var _canvas	: Graphics	= null;
		
		public function AreaDebugger($canvas:Graphics) {
			_instance	= this;
			_canvas = $canvas;
		}
		
		public function update(dtMs:int):void
		{
			_canvas.clear();
		}
		
		public function DrawCollisionBoundaries( attacker: GameObject, attackAbsBoundaries: Boundaries, victim: GameObject, victimAbsHitBoundaries: Boundaries):void {
			
			if (!Config.debug_attack_area) return;
			
			// Attacker attack rect
			canvas.lineStyle(3, 0x00ff00);
			canvas.beginFill(0xFF0000, .5);
			canvas.drawRect(attackAbsBoundaries.x0, Config.floor_y + attackAbsBoundaries.z0, attackAbsBoundaries.width, attackAbsBoundaries.depth);
			canvas.endFill();
			
			// Attacker Attack height
			canvas.lineStyle(3, 0xFF0000);
			var heightMarkerX	:Number	= attacker.IsFacingRight?attackAbsBoundaries.x1 + 10:attackAbsBoundaries.x0 - 10;
			var useGobjZRef	: Boolean	= (ConfigLoader.Instance.Config["debug_attack_area_use_gobj_z_ref"] == "true");
			var zRef	: Number	= useGobjZRef?attacker.z:0;
			//canvas.moveTo(heightMarkerX, attacker.y + attacker.z);
			//canvas.lineTo(heightMarkerX, attacker.y + attacker.z - attackAbsBoundaries.height);
			canvas.moveTo(heightMarkerX, zRef + attackAbsBoundaries.y0);
			canvas.lineTo(heightMarkerX, zRef + attackAbsBoundaries.y1);
			
			// Attacker center
			canvas.beginFill(0x000000);
			canvas.drawRect(attacker.x - 3, Config.floor_y + attacker.z - 3, 6, 6);
			canvas.endFill();
			
			
			
			// Victim hit rect
			canvas.lineStyle(3, 0x00ff00);
			canvas.beginFill(0x0000FF);
			canvas.drawRect(victimAbsHitBoundaries.x0, Config.floor_y + victimAbsHitBoundaries.z0, victimAbsHitBoundaries.width, victimAbsHitBoundaries.depth);
			canvas.endFill();
			
			// Victim hit height
			canvas.lineStyle(3, 0x00FF00, .5);
			heightMarkerX	= victim.IsFacingRight?victimAbsHitBoundaries.x1 + 10:victimAbsHitBoundaries.x0 - 10;
			zRef	= useGobjZRef?victim.z:0;
			//canvas.moveTo(heightMarkerX, victim.y + victim.z);
			//canvas.lineTo(heightMarkerX, victim.y + victim.z - victimAbsHitBoundaries.height);
			canvas.moveTo(heightMarkerX, zRef + victimAbsHitBoundaries.y0);
			canvas.lineTo(heightMarkerX, zRef + victimAbsHitBoundaries.y1);
			
			// Victim center
			canvas.beginFill(0x000000);
			canvas.drawRect(victim.x - 3, Config.floor_y + victim.z - 3, 6, 6);
			canvas.endFill();
		}
		
		public function DrawBoundaries(leftX:Number, rightX:Number, backZ:Number, frontZ:Number, debugStr:String = "", forceDraw:Boolean = false):void {
			
			if (!Config.debug_attack_area && !forceDraw) return;
			
			canvas.lineStyle(3, 0x00ff00);
			canvas.beginFill(0xFFFF00, .5);
			canvas.drawRect(leftX, Config.floor_y + backZ, rightX - leftX, frontZ - backZ);
			canvas.endFill();
		}
		
		public function drawPath(path:Path, canvas:Graphics, color:uint, clearCanvas:Boolean = true):void
		{
			if (path == null) return;
			if (clearCanvas) canvas.clear();
			for (var j : int = 0; j < path.length; j++)
			{
				var pathNodeTmp:GameAreaPoint = path.getNode(j);
				var leftX	: Number = pathNodeTmp.x - 2;
				var rightX	: Number = pathNodeTmp.x + 2;
				var backZ	: Number = pathNodeTmp.z - 2;
				var frontZ	: Number = pathNodeTmp.z + 2;
				canvas.lineStyle(1.5, color);
				canvas.beginFill(0, .5);
				canvas.drawRect(leftX, Config.floor_y + backZ, rightX - leftX, frontZ - backZ);
				if (j < path.length-1)
				{
					var nextNode:GameAreaPoint = path.getNode(j+1);
					canvas.moveTo(pathNodeTmp.x, Config.floor_y + pathNodeTmp.z);
					canvas.lineTo(nextNode.x, Config.floor_y + nextNode.z);
				}
				canvas.endFill();
			}
		}
		
		public function drawAreaBoundaries(boundaries:Boundaries, canvas:Graphics, color:uint, clearCanvas:Boolean = true):void
		{
			if (boundaries == null) return;
			if (clearCanvas) canvas.clear();
			
			canvas.lineStyle(1, color, .3);
			canvas.beginFill(color, .3);
			canvas.drawRect(boundaries.x0, Config.floor_y + boundaries.z0, boundaries.width, boundaries.depth);
			canvas.endFill();
		}
		
		public function get canvas():Graphics 
		{
			return _canvas;
		}
		
		static public function get instance():AreaDebugger 
		{
			return _instance;
		}
	}
	
}