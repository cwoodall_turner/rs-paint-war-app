﻿package {
	/**
	 * ...
	 * @author ahodes
	 */
	public class RelativeRangeArea {
		
		protected var _leftX	: Number	= 0;
		protected var _rightX	: Number	= 0;
		protected var _backZ	: Number	= 0;
		protected var _frontZ	: Number	= 0;
		
		/**
		 * All positive values
		 * 
		 * @param	$leftX
		 * @param	$rightX
		 * @param	$backZ
		 * @param	$frontZ
		 */
		public function RelativeRangeArea($leftX:Number, $rightX:Number, $backZ:Number, $frontZ:Number) {
			_leftX	= $leftX;
			_rightX = $rightX;
			_backZ 	= $backZ;
			_frontZ = $frontZ;
		}
		
		public function get leftX():Number 
		{
			return _leftX;
		}
		
		public function get rightX():Number 
		{
			return _rightX;
		}
		
		public function get backZ():Number 
		{
			return _backZ;
		}
		
		public function get frontZ():Number 
		{
			return _frontZ;
		}
		
		public function get width() : Number {
			return leftX + rightX;
		}
		
		public function get height() : Number {
			return frontZ + backZ;
		}
		
		public function toString():String {
			return "[leftX: " + leftX.toFixed(1) + ", rightX: " + rightX.toFixed(1) + ", backZ: " + backZ.toFixed(1) + ", frontZ: " + frontZ.toFixed(1) + "]";
		}
		
	}

}