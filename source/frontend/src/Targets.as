﻿package {
	/**
	 * ...
	 * @author ahodes
	 */
	public class Targets {
		
		public static const NONE		: uint = 0;
		public static const PLAYER		: uint = 1;
		public static const ENEMY		: uint = 2;
		public static const CHARACTER	: uint = 3; // PLAYER or ENEMY
		public static const OBSTACLE	: uint = 4;
		
		
		public function Targets() {	}
		
	}
}