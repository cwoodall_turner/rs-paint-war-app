/*
 * Copyright (c) 2008, TouchMyPixel & contributors
 * Original author : Tony Polinelli <tonyp@touchmypixel.com> 
 * Contributers: Tarwin Stroh-Spijer 
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE TOUCH MY PIXEL & CONTRIBUTERS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE TOUCH MY PIXEL & CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.touchmypixel.utils 
{
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Matrix;
import flash.geom.Rectangle;
import flash.utils.Dictionary;
import flash.utils.getDefinitionByName;

public class Animation extends Sprite
	{
		public static const ANIMATION_CACHE_COMPLETE	: String	= "AnimationCacheComplete";
		
		public var bitmap:Bitmap;
		//public var clip:MovieClip;
		public var totalStates : int;
		public var totalFrames : int;
		public var frames:Array = [];
		
		public var repeat:Boolean = true;
		public var onEnd:Function;
		public var reverse:Boolean = false;
		public var speed:Number = 1;
		public var treatAsLoopedGraphic:Boolean = false;
		
		private var _playing:Boolean = false;
		private var _cache:Boolean = true;
		//private var _totalFrames: int;
		private var _currentFrame:Number = 1;
		
		private var _clip				: MovieClip	= null;
		private var _currentInnerClip	: MovieClip	= null;
		private var _rect				: Rectangle	= null;
		
		private var _initialFrameForState	: Dictionary	= null;
		private var _finalFrameForState		: Dictionary	= null;
		private var _currentStateLastFrame	: int			= 1;
		private var _currentStateFirstFrame	: int			= 1;
		private var _cachingComplete		: Boolean		= false;
		
		private var _repeatStates			: Dictionary	= null;
		
		
		public function Animation(identifier:String=null,getAssetFunction:Function=null) 
		{
			_cachingComplete	= false;
			bitmap = new Bitmap();
			bitmap.smoothing = false;
			addChild(bitmap);
			
			if (identifier != null) {
				buildCache(identifier,getAssetFunction);
			} else {
				_cachingComplete	= true;
			}
		}
		
		public function set bitmapData(value:BitmapData){ bitmap.bitmapData = value; }
		public function get bitmapData():BitmapData { return bitmap.bitmapData; }
		
		public function get playing():Boolean { return _playing; }
		//public function get totalFrames():Number { return clip.totalFrames; }
		//public function get totalFrames():Number { return _totalFrames; }
		public function get currentFrame():Number { return _currentFrame; }
		
		public function get initialFrameForState():Dictionary { return _initialFrameForState; }
		public function set initialFrameForState(value:Dictionary):void {	_initialFrameForState = value;	}
		
		public function get finalFrameForState():Dictionary { return _finalFrameForState; }
		public function set finalFrameForState(value:Dictionary):void {	_finalFrameForState = value;	}
		
		public function get repeatStates():Dictionary { return _repeatStates; }
		public function set repeatStates(value:Dictionary):void {	_repeatStates = value;	}
		
		public function buildCache(identifier:String,getAssetFunction:Function):void
		{
			Debugger.Log("Building Cache... " + identifier);
			
			if (getAssetFunction == null)
				_clip = new (getDefinitionByName(identifier))();
			else
				_clip = getAssetFunction(identifier);
				
			totalStates = _clip.totalFrames;
			
			addChild(_clip);
			
			var bounds : DisplayObject = _clip.getChildByName("bounds");
			if (bounds != null)
			{
				_rect = bounds.getRect(bounds.parent);
				bounds.visible = false;
			} else {
				_rect = _clip.getRect(_clip);
			}
			
			_initialFrameForState	= new Dictionary();
			_finalFrameForState		= new Dictionary();
			_repeatStates			= new Dictionary();
			
			CacheCurrentFrame(new Event("Dummy"));
		}
		
		private function CacheCurrentFrame(e:Event):void {
			removeEventListener(Event.ENTER_FRAME, CacheCurrentFrame);
			
			_currentInnerClip = _clip.getChildByName("innerMc_mc") as MovieClip;
			if (!_currentInnerClip) {
				Debugger.Log("Error getting child @ frame " + _clip.currentFrame + "/" + _clip.totalFrames + " - (numChildren: " + _clip.numChildren);
				addEventListener(Event.ENTER_FRAME, CacheCurrentFrame);
				return;
			}
			
			
			/*
			 * 
m.translate( -localBounds.x, -localBounds.y);
			
			if (!fixedBackground)	bitmapDataOriginal.draw(GameContext.GameSprite, m);
			else bitmapDataOriginal.draw(fixedBackground, m);
			
			_backgroundBitmap.bitmapData	= bitmapDataOriginal;
			_backgroundBitmap.x = localBounds.x;
			_backgroundBitmap.y = localBounds.y;
			 * 
			 * 
			 * */
			
			
			_initialFrameForState[_clip.currentFrame]	= frames.length + 1;
			_finalFrameForState[_clip.currentFrame]	= _initialFrameForState[_clip.currentFrame] + _currentInnerClip.totalFrames - 1;
			_repeatStates[_clip.currentFrame]	= true;
			
			//Debugger.Log("STATE " + _clip.currentFrame + ": [" + _initialFrameForState[_clip.currentFrame] + " - " + _finalFrameForState[_clip.currentFrame] + " ]");
			for (var i = 1; i <= _currentInnerClip.totalFrames; i++)
			{
				_currentInnerClip.gotoAndStop(i);
				makeAllChildrenGoToFrame(_currentInnerClip, i);
				var bitmapData:BitmapData = new BitmapData(Math.ceil(_rect.width), Math.ceil(_rect.height), true, 0x00000000);
				var m:Matrix = new Matrix();
				m.translate(-_rect.x, -_rect.y);
				m.scale(_clip.scaleX, _clip.scaleY);
				bitmapData.draw(_clip,m);
				frames.push(bitmapData);
			}
			
			if (_clip.currentFrame == _clip.totalFrames) {
				FinishBuildingCache();
			} else {
				_clip.gotoAndStop(_clip.currentFrame + 1);
				addEventListener(Event.ENTER_FRAME, CacheCurrentFrame);
			}
		}
		
		private function FinishBuildingCache():void {
			
			totalFrames	= frames.length;
			
			bitmap.x = _rect.x;
			bitmap.y = _rect.y;
			removeChild(_clip);
			_clip	= null;
			_rect	= null;
			
			dispatchEvent(new Event(Animation.ANIMATION_CACHE_COMPLETE));
			_cachingComplete	= true;
		}
		
		private function makeAllChildrenGoToFrame(m:MovieClip, f:int):void
		{
			for (var i:int = 0; i < m.numChildren; i++) {
				var c = m.getChildAt(i);
				if (c is MovieClip) {
					makeAllChildrenGoToFrame(c, f);
					c.gotoAndStop(f);
				}
				c = null;
			}
		}
		
		public function play():void
		{
			_playing = true;
			// TMP: ver si no se pasa a update() llamado desde gameManager.
			addEventListener(Event.ENTER_FRAME, enterFrame, false, 0, true);
		}
		
		public function stop():void
		{
			_playing = false;
			removeEventListener(Event.ENTER_FRAME, enterFrame)
		}
		
		public function gotoAndStop(frame:Number):void
		{
			/*
			if (treatAsLoopedGraphic) {
				if (frame > totalFrames) {
					frame = frame % totalFrames;
				}
			}*/
			if (frame > _currentStateLastFrame) {
				_currentFrame = _currentStateFirstFrame;
			}
			_currentFrame = frame;
			
			goto(_currentFrame);
			stop();
		}
		
		public function gotoAndPlay(frame:Number):void
		{
			_currentFrame = frame;
			goto(_currentFrame);
			play();
		}
		
		public function GoToAndPlayInnerFrame(innerFrame:Number):void
		{
			_currentFrame = _currentStateFirstFrame + innerFrame - 1;
			goto(_currentFrame);
			play();
		}
		
		public function gotoAndPlayRandomFrame():void
		{
			gotoAndPlay(Math.ceil(Math.random() * totalFrames));
		}
		
		public function nextFrame(useSpeed:Boolean = false):void
		{
			useSpeed ? _currentFrame += speed : _currentFrame++;
			
			/*TODO: Agregar previous frame para ver eventos y loop
			if (_currentFrame > totalFrames) _currentFrame = 1;*/
			if (_currentFrame > _currentStateLastFrame) _currentFrame = _currentStateFirstFrame;
			
			goto(Math.floor(_currentFrame));
		}
		
		public function prevFrame(useSpeed:Boolean = false):void
		{
			useSpeed ? _currentFrame -= speed : _currentFrame--;
			
			/*if (_currentFrame < 1) _currentFrame = totalFrames;*/
			if (_currentFrame < _currentStateFirstFrame) _currentFrame = _currentStateLastFrame;
			goto(Math.floor(_currentFrame));
		}
		
		private function goto(frame:Number):void
		{
			bitmap.bitmapData = frames[_currentFrame-1];
			bitmap.smoothing = true;
		}
		
		public function enterFrame(e:Event = null):void
		{
			if(reverse){
				prevFrame(true);
			}else {
				nextFrame(true);
			}
			
			//if (_currentFrame >= totalFrames) {
			if (_currentFrame >= _currentStateLastFrame) {
				
				if (!repeat) {
					stop();
				}
				dispatchEvent(new Event(Event.COMPLETE));
				if (onEnd != null) onEnd();
			}
			
			//Debugger.Log("_currentFrame: " + _currentFrame);
		}
		
		public function destroy()
		{
			stop();
			if (parent) parent.removeChild(this);
		}
		
		public function copy()
		{
			var newAnimation = new Animation();
			newAnimation.frames = frames;
			newAnimation.bitmap.x = bitmap.x;
			newAnimation.bitmap.y = bitmap.y;
			//newAnimation.clip = clip;
			
			newAnimation.initialFrameForState 	= initialFrameForState;
			newAnimation.finalFrameForState 	= finalFrameForState;
			newAnimation.repeatStates			= repeatStates;
			
			newAnimation.totalFrames = totalFrames;
			newAnimation.totalStates = totalStates;
			newAnimation.gotoAndStop(1);
			return newAnimation;
		}
		
		public function playState(state:int):void {
			//Debugger.Log("playState: " + state);
			_currentStateFirstFrame	= _initialFrameForState[state];
			_currentStateLastFrame	= _finalFrameForState[state];
			//Debugger.Log("_currentStateFirstFrame: " + _currentStateFirstFrame);
			//Debugger.Log("_currentStateLastFrame: " + _currentStateLastFrame);
			gotoAndPlay(_currentStateFirstFrame);
			
			repeat	= _repeatStates[state];
		}
		
		public function DispatchEventIfComplete():void {
			if (_cachingComplete) {
				_cachingComplete	= false;
				dispatchEvent(new Event(Animation.ANIMATION_CACHE_COMPLETE));
			}
		}
		
		public function SetLoopingState(state:int, loops:Boolean):void
		{
			_repeatStates[state]	= loops;
		}
	}
}