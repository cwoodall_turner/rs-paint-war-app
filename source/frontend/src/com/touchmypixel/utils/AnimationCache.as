/*
 * Copyright (c) 2008, TouchMyPixel & contributors
 * Original author : Tony Polinelli <tonyp@touchmypixel.com> 
 * Contributers: Tarwin Stroh-Spijer 
 * All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE TOUCH MY PIXEL & CONTRIBUTERS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE TOUCH MY PIXEL & CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.touchmypixel.utils
{
import flash.events.Event;
import flash.events.EventDispatcher;

public class AnimationCache extends EventDispatcher
	{	
		public static const CACHE_COMPLETE	: String	= "CacheComplete";
		
		private var animations:Object = {};
		private static var instance:AnimationCache;
		
		private var _identifiers		: Array	= null;
		private var _getAssetFunctions	: Array	= null;
		
		private var _animationClass	: Class	= null;
		
		public function AnimationCache(lock:AnimationCacheLock) 
		{
			instance = this;
		}	
		
		public static function get Instance():AnimationCache
		{
			return !instance ? new AnimationCache(new AnimationCacheLock()) : instance;
		}
		
		public function cacheAnimation(identifier:String):Animation
		{
			var animation:Animation
			if (!animations[identifier]) {
				animation = new Animation(identifier);
				animations[identifier] = animation;
			} else {
				animation = animations[identifier];
			}
			return animation;
		}
		
		public function dumpAllAnimations():void
		{
			animations = { };
		}
		
		/**
		 * 
		 * @param	identifiers
		 * @param	getAssetFunctions tiene que coincidir en cantidad con los identifiers
		 */
		public function cacheAnimations(identifiers:Array,getAssetFunctions:Array,animationClass:Class):void
		{
			_animationClass		= animationClass;
			_identifiers		= identifiers;
			if (getAssetFunctions == null)
				_getAssetFunctions	= new Array();
			else
				_getAssetFunctions	= getAssetFunctions;
			if (_getAssetFunctions.length < _identifiers.length) {
				var i : int = _identifiers.length - _getAssetFunctions.length;
				while (--i >= 0) {
					_getAssetFunctions.push(null);
				}
			}
			if (_identifiers.length > 0) CacheIndividualAnimation(_identifiers.shift() as String,_getAssetFunctions.shift() as Function);
		}
		
		private function CacheIndividualAnimation(identifier:String,getAssetFunction:Function):AbstractAnimation
		{
			var animation:AbstractAnimation
			if(!animations[identifier]){
				animation = new _animationClass(identifier,getAssetFunction);
				animation.addEventListener(AbstractAnimation.ANIMATION_CACHE_COMPLETE, OnCacheIndividualAnimationComplete);
				animations[identifier] = animation;
				animation.DispatchEventIfComplete();
			} else {
				animation = animations[identifier];
				OnCacheIndividualAnimationComplete(new Event("Dummy"));
			}
			return animation;
		}
		
		private function OnCacheIndividualAnimationComplete(e:Event):void {
			if(e.currentTarget != null)	e.currentTarget.removeEventListener(AbstractAnimation.ANIMATION_CACHE_COMPLETE, OnCacheIndividualAnimationComplete);
			if (_identifiers.length > 0) {
				CacheIndividualAnimation(_identifiers.shift() as String,_getAssetFunctions.shift() as Function);
			} else {
				FinishBuildingCaches();
			}
		}
		
		private function FinishBuildingCaches():void {
			
			dispatchEvent(new Event(AnimationCache.CACHE_COMPLETE));
		}
		
		public function getAnimation(id):AbstractAnimation
		{
			if (!animations[id]) {
				trace("MISSING ANIMATION: "+ id);
				return null;
			}
			
			return animations[id].copy();
		}
	}
}
internal class AnimationCacheLock{}
