package com.touchmypixel.utils {
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.Event;

/**
	 * ...
	 * @author alfred
	 */
	public class AbstractAnimation extends Sprite {
		
		public static const ANIMATION_CACHE_COMPLETE	: String	= "AnimationCacheComplete";
		
		protected var _cachingComplete	: Boolean	= false;
		protected var _clip				: MovieClip	= null;
		
		private static var _animations	: Vector.<AbstractAnimation>	= new Vector.<AbstractAnimation>();
		
		public function AbstractAnimation() {
			
		}
		
		public function DispatchEventIfComplete():void {
			if (_cachingComplete) {
				_cachingComplete	= false;
				dispatchEvent(new Event(ANIMATION_CACHE_COMPLETE));
			}
		}
		
		public static function Update(dtMs:int):void {
			
		}
		
		public function copy() {
			
		}
		
	}

}