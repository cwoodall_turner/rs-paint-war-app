﻿package com.reintroducing.sound
{
import com.greensock.TweenLite;
import com.greensock.plugins.SoundTransformPlugin;
import com.greensock.plugins.TweenPlugin;

import flash.events.Event;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;
import flash.system.ApplicationDomain;
import flash.utils.Dictionary;
import flash.utils.getQualifiedClassName;

/**
	 * The SoundManager is a singleton that allows you to have various ways to control sounds in your project.
	 * <p />
	 * The SoundManager can load external or library sounds, pause/mute/stop/control volume for one or more sounds at a time, 
	 * fade sounds up or down, and allows additional control to sounds not readily available through the default classes.
	 * <p />
	 * This class is dependent on TweenLite (http://www.tweenlite.com) to aid in easily fading the volume of the sound.
	 * 
	 * @author Matt Przybylski [http://www.reintroducing.com]
	 * @version 1.0
	 */
	public class SoundManager
	{
//- PRIVATE & PROTECTED VARIABLES -------------------------------------------------------------------------

		// singleton instance
		private static var _instance:SoundManager;
		private static var _allowInstance:Boolean;
		
		private var _soundsDict:Dictionary;
		//private var _sounds:Array;
		
		// (Un)Muting sounds
		private var _mutedSounds	: Array	= null;
		private var _mutedVolumes	: Array	= null;
		
		/* [ Load sounds from External swf */
		private var _applicationDomain	: ApplicationDomain = null;
		public function set MyApplicationDomain(myApplicationDomain : ApplicationDomain) : void	{	_applicationDomain	= myApplicationDomain;	}
		/* Load sounds from External swf ] */
		
//- PUBLIC & INTERNAL VARIABLES ---------------------------------------------------------------------------
		
		
		
//- CONSTRUCTOR	-------------------------------------------------------------------------------------------
	
		// singleton instance of SoundManager
		public static function getInstance():SoundManager 
		{
			if (SoundManager._instance == null)
			{
				SoundManager._allowInstance = true;
				SoundManager._instance = new SoundManager();
				SoundManager._allowInstance = false;
			}
			
			return SoundManager._instance;
		}
		
		public function SoundManager() 
		{
			this._soundsDict = new Dictionary(true);
			//this._sounds = new Array();
			
			if (!SoundManager._allowInstance)
			{
				throw new Error("Error: Use SoundManager.getInstance() instead of the new keyword.");
			}
			
			TweenPlugin.activate([SoundTransformPlugin]);
		}
		
//- PRIVATE & PROTECTED METHODS ---------------------------------------------------------------------------
		
		
//- PUBLIC & INTERNAL METHODS -----------------------------------------------------------------------------

		/**
		 * Adds the passed sound to the sounds dictionary
		 * 
		 * @param	$id
		 * @param	$sound
		 */
		public function AddSound($id:String, $sound:Sound):void {
			
			this._soundsDict[$id] = new SoundObject($id, $sound);
		}
		
		/**
		 * Adds an external sound to the sounds dictionary for playing in the future.
		 * 
		 * @param $path A string representing the path where the sound is on the server
		 * @param $name The string identifier of the sound to be used when calling other methods on the sound
		 * @param $buffer The number, in milliseconds, to buffer the sound before you can play it (default: 1000)
		 * @param $checkPolicyFile A boolean that determines whether Flash Player should try to download a cross-domain policy file from the loaded sound's server before beginning to load the sound (default: false) 
		 * 
		 * @return Boolean A boolean value representing if the sound was added successfully
		 
		public function addExternalSound($path:String, $name:String, $buffer:Number = 1000, $checkPolicyFile:Boolean = false):Boolean{
			//var LENGTH:int = this._sounds.length;
			//for (var i:int = 0; i < LENGTH; i++){
				//if (this._sounds[i].name == $name) return false;
			//}
			
			var sndObj:SoundObject = new SoundObject();
			var snd:Sound = new Sound(new URLRequest($path), new SoundLoaderContext($buffer, $checkPolicyFile));
			
			sndObj.name = $name;
			sndObj.sound = snd;
			//sndObj.channel = new SoundChannel();
			//sndObj.position = 0;
			//sndObj.played	= false;
			sndObj.paused = false;
			//sndObj.volume = 1;
			//sndObj.startTime = 0;
			//sndObj.loops = 0;
			//sndObj.pausedByAll = false;
			sndObj.channelsInfo = new Dictionary();
			sndObj.numPlays	= 0;
			
			this._soundsDict[$name] = sndObj;
			//this._sounds.push(sndObj);
			
			return true;
		}
		*/
		
		/**
		 * Removes a sound from the sound dictionary.  After calling this, the sound will not be available until it is re-added.
		 * 
		 * @param $name The string identifier of the sound to remove
		 * 
		 * @return void
		 */
		public function removeSound($name:String):void
		{
			delete this._soundsDict[$name];
		}
		
		/**
		 * Removes all sounds from the sound dictionary.
		 * 
		 * @return void
		 */
		public function removeAllSounds():void
		{
			this._soundsDict = new Dictionary(true);
		}

		/**
		 * Plays or resumes a sound from the sound dictionary with the specified name.
		 * 
		 * @param $name The string identifier of the sound to play
		 * @param $volume A number from 0 to 1 representing the volume at which to play the sound (default: 1)
		 * @param $startTime A number (in milliseconds) representing the time to start playing the sound at (default: 0)
		 * @param $loops An integer representing the number of times to loop the sound (default: 0)
		 * 
		 * @return void
		 */
		public function playSound($name:String, $volume:Number = 1, $startTime:Number = 0, $loops:int = 0, $panning:Number = 0, $onComplete:Function = null):void {
			
			var snd:SoundObject = this._soundsDict[$name];
			var sndChannel : SoundChannel = snd.sound.play($startTime, $loops, new SoundTransform($volume,$panning));
			
			sndChannel.addEventListener(Event.SOUND_COMPLETE, OnSoundComplete($name));
			
			var sndChannelInfo	: SoundChannelInfo	= new SoundChannelInfo();
			sndChannelInfo.position		= $startTime;
			sndChannelInfo.playing		= true;
			sndChannelInfo.paused		= false;
			sndChannelInfo.volume		= $volume;
			sndChannelInfo.startTime	= $startTime;
			sndChannelInfo.loops		= $loops;
			sndChannelInfo.pausedByAll	= false;
			sndChannelInfo.onComplete	= $onComplete;
			
			snd.channelsInfo[sndChannel] = sndChannelInfo;
			snd.numPlays++;
			snd.paused = false;
		}
		
		/**
		 * Stop a sound once it has finished playing
		 * 
		 * @param sound's class identifier in the library
		 * 
		 * @return function which handles the event [function(e :Event):void]
		 */ 
		private function OnSoundComplete($name :String):Function {
			
			return function(e :Event):void{
				
				var snd : SoundObject	= _soundsDict[$name];
				var sndChannelInfo	: SoundChannelInfo	= snd.channelsInfo[e.currentTarget as SoundChannel];
				if (sndChannelInfo.onComplete != null)
				{
					sndChannelInfo.onComplete();
				}
				stopSound($name, e.currentTarget as SoundChannel);
			}
		}
		
		/**
		 * Stops the specified sound.
		 * 
		 * @param $name The string identifier of the sound
		 * 
		 * @return void
		 */
		public function stopSound($name:String,$sndChannel:SoundChannel = null,numSndChannelsToStop:int = 999999):void
		{
			if ($sndChannel) {
				stopSndChannel($name,$sndChannel);
			} else { 
				// Detengo todos los canales
				var snd : SoundObject	= _soundsDict[$name];
				for (var sndChannel:Object in snd.channelsInfo) {
					stopSndChannel($name, sndChannel as SoundChannel);
					if (--numSndChannelsToStop == 0) return;
				}
			}
		}
		
		private function stopSndChannel($name:String, $sndChannel:SoundChannel):void {
			var snd				: SoundObject 		= this._soundsDict[$name];
			var sndChannelInfo	: SoundChannelInfo	= snd.channelsInfo[$sndChannel];
			if (snd != null && sndChannelInfo != null) {
				$sndChannel.stop();
				$sndChannel.removeEventListener(Event.SOUND_COMPLETE, OnSoundComplete($name));
				snd.numPlays--;
				delete snd.channelsInfo[$sndChannel];
			} else {
				//Debugger.Log("Couldn't stop sound " + $name);
			}
		}
		
		/**
		 * Pauses the specified sound.
		 * 
		 * @param $name The string identifier of the sound
		 * 
		 * @return void
		 */
		public function pauseSound($name:String,$sndChannel:SoundChannel = null,numSndChannelsToPause:int = 999999):void
		{
			if ($sndChannel) {
				pauseSndChannel($name,$sndChannel);
			} else { 
				// Pauso todos los canales
				var snd : SoundObject	= _soundsDict[$name];
				for (var sndChannel:Object in snd.channelsInfo) {
					pauseSndChannel($name, sndChannel as SoundChannel);
					if (--numSndChannelsToPause == 0) return;
				}
			}
			/*
			var snd:SoundObject = this._soundsDict[$name];
			snd.paused = true;
			snd.played = false;
			snd.position = snd.channel.position;
			snd.channel.stop();
			*/
		}
		
		
		private function pauseSndChannel($name:String, $sndChannel:SoundChannel):void {
			var snd				: SoundObject 		= this._soundsDict[$name];
			var sndChannelInfo	: SoundChannelInfo	= snd.channelsInfo[$sndChannel];
			if (snd != null && sndChannelInfo != null) {
				$sndChannel.stop();
				$sndChannel.removeEventListener(Event.SOUND_COMPLETE, OnSoundComplete($name));
				snd.numPlays--;
				delete snd.channelsInfo[$sndChannel];
			} else {
				//Debugger.Log("Couldn't stop sound " + $name);
			}
		}
		
		
		/**
		 * Plays all the sounds that are in the sound dictionary.
		 * 
		 * @param $useCurrentlyPlayingOnly A boolean that only plays the sounds which were currently playing before a pauseAllSounds() or stopAllSounds() call (default: false)
		 * 
		 * @return void
		 */
		public function playAllSounds($useCurrentlyPlayingOnly:Boolean = false):void
		{/*
			var LENGTH:int = this._sounds.length;
			for (var i:int = 0; i < LENGTH; i++)
			{
				var id:String = this._sounds[i].name;
				
				if ($useCurrentlyPlayingOnly)
				{
					if (this._soundsDict[id].pausedByAll)
					{
						this._soundsDict[id].pausedByAll = false;
						this.playSound(id);
					}
				}
				else
				{
					this.playSound(id);
				}
			}*/
		}
		
		/**
		 * Stops all the sounds that are in the sound dictionary.
		 * 
		 * @param $useCurrentlyPlayingOnly A boolean that only stops the sounds which are currently playing (default: true)
		 * 
		 * @return void
		 */
		public function stopAllSounds($useCurrentlyPlayingOnly:Boolean = true):void
		{
			for (var sndName:String in _soundsDict) {
				stopSound(sndName);
			}
		}
		
		/**
		 * Pauses all the sounds currently playing.
		 * 
		 * @return void
		 */
		public function pauseAllSounds():void
		{
			for each (var snd:SoundObject in _soundsDict) {
				for (var sndChannel:Object in snd.channelsInfo) {
					var scInfo : SoundChannelInfo	= snd.channelsInfo[sndChannel];
					var sc : SoundChannel = sndChannel as SoundChannel;
					sc.stop();
					scInfo.paused	= true;
				}
			}
		}
		
		/**
		 * Resumes all the sounds currently paused.
		 * 
		 * @return void
		 */
		public function resumeAllPausedSounds():void
		{
			for each (var snd:SoundObject in _soundsDict) {
				for (var sndChannel:Object in snd.channelsInfo) {
					var soundChannelOld : SoundChannel = sndChannel as SoundChannel;
					var scInfo	: SoundChannelInfo	= snd.channelsInfo[sndChannel];
					if (scInfo.paused) {
						playSound(snd.name, scInfo.volume, soundChannelOld.position, scInfo.loops);
						stopSndChannel(snd.name, soundChannelOld);
					}
				}
			}
		}
		
		/**
		 * Fades the sound to the specified volume over the specified amount of time.
		 * 
		 * @param $name The string identifier of the sound
		 * @param $targVolume The target volume to fade to, between 0 and 1 (default: 0)
		 * @param $fadeLength The time to fade over, in seconds (default: 1)
		 * 
		 * @return void
		 */
		public function fadeSound($name:String, $targVolume:Number = 0, $fadeLength:Number = 1, $onComplete:Function=null):void
		{
			var snd:SoundObject = _soundsDict[$name];
			for (var sndChannel:Object in snd.channelsInfo) {
				//TweenLite.to(sndChannel as SoundChannel, $fadeLength, {volume: $targVolume, onComplete:$onComplete});
				//TweenLite.to(sndChannel, 1,{soundTransform:{volume:0.2,pan:0.5}});
				TweenLite.to(sndChannel, $fadeLength, { soundTransform: { volume:$targVolume }, onComplete:$onComplete } );
			}
		}
		
		/**
		 * Mutes the volume for all sounds in the sound dictionary.
		 * 
		 * @return void
		 */
		public function muteAllSounds():void
		{
			/**/
			_mutedSounds  = new Array();
			_mutedVolumes = new Array();
			for (var sndName:String in _soundsDict) {
				if (IsPlaying(sndName)) {
					_mutedSounds.push(sndName);
					_mutedVolumes.push(getSoundVolume(sndName));
					setSoundVolume(sndName, 0);
				}
			}
		}
		
		/**
		 * Resets the volume to their original setting for all sounds in the sound dictionary.
		 * 
		 * @return void
		 */
		public function unmuteAllSounds():void
		{
			if (_mutedSounds == null) return;
			var i	: int = _mutedSounds.length;
			while (i--) {
				var sndName	: String = _mutedSounds[i];
				if (IsPlaying(sndName)) {
					setSoundVolume(sndName, _mutedVolumes[i]);
				}
			}
			_mutedSounds 	= null;
			_mutedVolumes	= null;
		}
		
		/**
		 * Sets the volume of the specified sound.
		 * 
		 * @param $name The string identifier of the sound
		 * @param $volume The volume, between 0 and 1, to set the sound to
		 * 
		 * @return void
		 */
		public function setSoundVolume($name:String, $volume:Number, $sndChannel:SoundChannel = null):void
		{
			if ($sndChannel) {
				setSoundVolumeChannel($volume,$sndChannel);
			} else {
				// Seteo volumen a todos los canales
				var snd : SoundObject	= _soundsDict[$name];
				if (snd == null) return;
				for (var sndChannel:Object in snd.channelsInfo) {
					setSoundVolumeChannel($volume,sndChannel as SoundChannel);
				}
			}
		}
		
		private function setSoundVolumeChannel($volume:Number, $soundChannel:SoundChannel):void {
			var curTransform:SoundTransform = $soundChannel.soundTransform;
			curTransform.volume = $volume;
			$soundChannel.soundTransform = curTransform;
		}
		
		/**
		 * Gets the volume of the specified sound.
		 * 
		 * @param $name The string identifier of the sound
		 * 
		 * @return Number The current volume of the sound
		 */
		public function getSoundVolume($name:String):Number
		{
			var snd	: SoundObject	= this._soundsDict[$name];
			for (var sndChannel:Object in snd.channelsInfo) {
				return (sndChannel as SoundChannel).soundTransform.volume;
			}
			return 1;
		}
		
		/**
		 * Gets the position of the specified sound.
		 * 
		 * @param $name The string identifier of the sound
		 * 
		 * @return Number The current position of the sound, in milliseconds
		 */
		public function getSoundPosition($name:String):Number
		{
			// TMP!
			var snd	: SoundObject	= this._soundsDict[$name];
			for (var sndChannel:Object in snd.channelsInfo) {
				return (sndChannel as SoundChannel).position;
			}
			return 0;
		}
		
		/**
		 * Gets the duration of the specified sound.
		 * 
		 * @param $name The string identifier of the sound
		 * 
		 * @return Number The length of the sound, in milliseconds
		 */
		public function getSoundDuration($name:String):Number
		{
			// TMP!
			var snd	: SoundObject	= this._soundsDict[$name];
			return snd.sound.length;
		}

//- EVENT HANDLERS ----------------------------------------------------------------------------------------
	
		

//- GETTERS & SETTERS -------------------------------------------------------------------------------------
	
		//public function get sounds():Array
		//{
		    //return this._sounds;
		//}
		
		public function IsPlaying($name:String):Boolean
		{
			var snd	: SoundObject	= this._soundsDict[$name];
			var playing : Boolean = false;
			if (snd != null && snd.numPlays > 0) playing = true;
			return playing;
		}
	
//- HELPERS -----------------------------------------------------------------------------------------------
	
		public function toString():String
		{
			return getQualifiedClassName(this);
		}
		
		public function TestUpdate() : void {
			for each (var snd:SoundObject in _soundsDict) {
				//if(snd.numPlays)	trace("SND " + snd.name + " (" + snd.numPlays + ")");
				for (var sndChannel:Object in snd.channelsInfo) {
					var s:SoundChannel = sndChannel as SoundChannel;
					//trace("s.position: " + s.position);
				}
			}
		}
	
//- END CLASS ---------------------------------------------------------------------------------------------
	}
}
