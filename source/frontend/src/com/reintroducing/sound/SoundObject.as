﻿package com.reintroducing.sound {
import flash.media.Sound;
import flash.utils.Dictionary;

/**
	 * ...
	 * @author alfred
	 */
	public class SoundObject {
		
		public var name			: String 		= "";
		public var sound		: Sound 		= null;
		public var channelsInfo	: Dictionary	= new Dictionary();
		public var numPlays		: int			= 0;
		public var paused		: Boolean		= false;
			
		public function SoundObject($name:String, $sound:Sound, $channelsInfo:Dictionary = null, $numPlays:int = 0, $paused:Boolean = false) {
			name 			= $name;
			sound			= $sound;
			channelsInfo 	= $channelsInfo?$channelsInfo:new Dictionary(true);
			numPlays 		= $numPlays;
			paused 			= $paused;
		}
		
	}
	
}