﻿package com.reintroducing.sound {
/**
	 * ...
	 * @author alfred
	 */
	public class SoundChannelInfo {

		//public var channel		: SoundChannel	= null;
		public var position		: Number		= 0;
		public var playing		: Boolean		= false;
		public var paused		: Boolean		= false;
		public var volume		: Number		= 1;
		public var startTime	: Number		= 0;
		public var loops		: int			= 0;
		public var pausedByAll	: Boolean		= false;
		public var onComplete	: Function		= null;
			
		public function SoundChannelInfo() {
			
		}
		
	}
	
}