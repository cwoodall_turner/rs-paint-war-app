package  
{
	/**
	 * ...
	 * @author ...
	 */
	public class Boundaries 
	{
		
		private var _x0	: Number	= 0;
		private var _x1	: Number	= 1;
		private var _y0	: Number	= 0;
		private var _y1	: Number	= 1;
		private var _z0	: Number	= 0;
		private var _z1	: Number	= 1;
		
		public function Boundaries($x0:Number, $x1:Number, $y0:Number, $y1:Number, $z0:Number, $z1:Number)
		{
			_x0 = $x0;
			_y0 = $y0;
			_z0 = $z0;
			_x1 = $x1;
			_y1 = $y1;
			_z1 = $z1;
		}
		
		public function SetLimits($x0:Number, $x1:Number, $y0:Number, $y1:Number, $z0:Number, $z1:Number):void
		{
			_x0 = $x0;
			_y0 = $y0;
			_z0 = $z0;
			_x1 = $x1;
			_y1 = $y1;
			_z1 = $z1;
		}
		
		public function get x0():Number 
		{
			return _x0;
		}
		
		public function get x1():Number 
		{
			return _x1;
		}
		
		public function get y0():Number 
		{
			return _y0;
		}
		
		public function get y1():Number 
		{
			return _y1;
		}
		
		public function get z0():Number 
		{
			return _z0;
		}
		
		public function get z1():Number 
		{
			return _z1;
		}
		
		public function toString():String {
			return "[" + _x0.toFixed(2) + " , "  + _x1.toFixed(2) + " ; " + _y0.toFixed(2) + " , " + _y1.toFixed(2) + " ; " + _z0.toFixed(2) + " , " + _z1.toFixed(2) + "]";
		}
		
		public function get width():Number 
		{
			return (_x1 > _x0)?_x1 - _x0:_x0 - _x1;
		}
		
		public function get height():Number 
		{
			return (_y1 > _y0)?_y1 - _y0:_y0 - _y1;
		}
		
		public function get depth():Number 
		{
			return (_z1 > _z0)?_z1 - _z0:_z0 - _z1;
		}
		
	}

}