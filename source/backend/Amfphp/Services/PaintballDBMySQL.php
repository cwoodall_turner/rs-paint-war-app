<?php
/**
 *  This file is part of amfPHP
 *
 * LICENSE
 *
 * This source file is subject to the license that is bundled
 * with this package in the file license.txt.
 * @package Amfphp_Services
 */


/**
 * This is a test/example service. Remove it for production use
 *
 * @package Amfphp_Services
 * @author Ariel Sommeria-klein
 */
class PaintballDBMySQL {

    private $secret_key = "queremos a los chili peppers"; 
	
    private $max_delta_score = 1000;
    private $max_delta_money = 1000;

    /**
     * Get user info
     * @param mixed cnId (item name)
     * @return "success","info","data"
     */
    public function getUserInfo($cnId) {
		
		require_once ('../meekrodb.2.1.class.php');
		
		$returnVars = array();
		
		$returnVars['success']	= 0;
		$returnVars['info']		=  "Init..";
		$returnVars['info']		.=   " - [".$cnId."]";
		
		$returnVars['data']	= DB::queryFirstRow("SELECT uo.*, (SELECT COUNT(*) + 1 FROM user ui WHERE ui.score_level_0 > uo.score_level_0) AS rank FROM user uo WHERE uo.cn_id = %s0", $cnId);
		
		
		$returnVars['success']	= 1;
		$returnVars['info']		.=   " - Complete";
		
		return $returnVars;
    }

    /**
     * Create new user
     * @param mixed cnId (item name)
     * @param mixed vars
     * @return "success","info","data"
     */
    public function createNewUser($vars, $hash) {
		
		require_once ('../meekrodb.2.1.class.php');
		
		$returnVars = array();
		$returnVars['success']	= 0;
		$returnVars['info']		=  "createNewUser()";
		
		$putAttributesRequest = array();
		foreach($vars as $attName => $attValue)
		{
			$returnVars['info']		.=   " - " . $attName . ": " . $attValue;
			$putAttributesRequest[$attName] = $attValue;
		}
		
		$cn_id	= $putAttributesRequest['cn_id'];
		
		if(md5($cn_id . $this->secret_key)===$hash)
		{
			$returnVars['data']		=  $returnVars['data']	= DB::insert('user', $putAttributesRequest);
			$returnVars['success']	= 1;
		}
		else
		{
			$returnVars['data']		=  "Hash inválido!";
			$returnVars['success']	= 0;
  		}
		
		
		
		$returnVars['success']	= 1;
		$returnVars['info']		.=   " - Complete";
		
		return $returnVars;
    }

    /**
     * Set user info
     * @param mixed cnId (item name)
     * @param mixed vars
     * @return "success","info","data"
     */
    public function setUserInfo($cnId, $vars, $hash) {
		
		require_once ('../meekrodb.2.1.class.php');
		
		$returnVars = array();
		$returnVars['success']	= 0;
		$returnVars['info']		=  "setUserInfo(".$cnId.")";
		
		$putAttributesRequest = array();
		foreach($vars as $attName => $attValue)
		{
			$returnVars['info']		.=   " - " . $attName . ": " . $attValue;
			$putAttributesRequest[$attName] = $attValue;
		}
		
		$hashStr = $putAttributesRequest['grenades'] . $putAttributesRequest['shots_power'] . $putAttributesRequest['shots_distance'] . $putAttributesRequest['extra_life_slots'] . $putAttributesRequest['money'] . $putAttributesRequest['score_level_1'] . $putAttributesRequest['score_level_2'] . $putAttributesRequest['score_level_3'] . $putAttributesRequest['score_level_4'] . $putAttributesRequest['score_level_0'];
		
		if(md5($cnId . $hashStr . $this->secret_key)===$hash)
		{
			$returnVars['data']	= DB::update('user', $putAttributesRequest, "cn_id=%s", $cnId);
			$returnVars['success']	= 1;
		}
		else
		{
			$returnVars['data']		=  "Hash inválido!";
			$returnVars['success']	= 0;
  		}
		
		$returnVars['success']	= 1;
		$returnVars['info']		.=   " - Complete";
		
		return $returnVars;
    }

    /**
     * Get teams info
     * @return 
     */
    public function getTeamsInfo() {
		
		require_once ('../meekrodb.2.1.class.php');
		
		$returnVars = array();
		
		$returnVars['success']	= 0;
		$returnVars['info']		=  "Init..";
		
		$returnVars['data']	= DB::query("SELECT team_id as id, SUM(score_level_1) as score_level_1, SUM(score_level_2) as score_level_2, SUM(score_level_3) as score_level_3, SUM(score_level_4) as score_level_4 FROM team_score WHERE date > DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE())+1 DAY) GROUP BY team_id");
		
		$returnVars['success']	= 1;
		$returnVars['info']		.=   " - Complete";
		
		return $returnVars;
    }

    /**
     * Get tlast week teams scores
     * @return 
     */
    public function getLastWeekResults() {
		
		require_once ('../meekrodb.2.1.class.php');
		
		$returnVars = array();
		
		$returnVars['success']	= 0;
		$returnVars['info']		=  "Init..";
		
		$returnVars['data']	= DB::query("SELECT team_id as id, SUM(score_level_1) as last_week_score_level_1, SUM(score_level_2) as last_week_score_level_2, SUM(score_level_3) as last_week_score_level_3, SUM(score_level_4) as last_week_score_level_4 FROM team_score WHERE date > DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE())+8 DAY) AND date < DATE_SUB(CURDATE(), INTERVAL WEEKDAY(CURDATE()) DAY) GROUP BY team_id");
		
		$returnVars['success']	= 1;
		$returnVars['info']		.=   " - Complete";
		
		return $returnVars;
    }

    /**
     * Get tlast day teams scores
     * @return 
     */
    public function getLastDayResults() {
		
		require_once ('../meekrodb.2.1.class.php');
		
		$returnVars = array();
		
		$returnVars['success']	= 0;
		$returnVars['info']		=  "Init..";
		
		$returnVars['data']	= DB::query("SELECT date, team_id as id, score_level_1 as last_day_score_level_1, score_level_2 as last_day_score_level_2, score_level_3 as last_day_score_level_3, score_level_4 as last_day_score_level_4 FROM team_score WHERE date=DATE_SUB(CURDATE(), INTERVAL 1 DAY)");
		
		$returnVars['success']	= 1;
		$returnVars['info']		.=   " - Complete";
		
		return $returnVars;
    }

    /**
     * Get ranking
     * @return 
     */
    public function getRankingLevel($cn_id, $level, $num_players_en_extremos) {
		
		require_once ('../meekrodb.2.1.class.php');
		
		$returnVars = array();
		
		$returnVars['success']	= 0;
		$returnVars['info']		=  "Init..";
		
		
		// Esto se podría hacer con 2 queries
		$player = DB::query("SELECT uo.name, uo.team_id, uo.score_level_%i0, (SELECT COUNT(*) + 1 FROM user ui WHERE ui.score_level_%i0 > uo.score_level_%i0) AS rank FROM user uo WHERE uo.cn_id = %s1 ORDER BY score_level_%i0 DESC", $level, $cn_id);
		$score_player = $player[0]["score_level_".$level];
		
		$score_menor_igual_desc = DB::query("SELECT name, team_id, score_level_%i0 FROM user WHERE score_level_%i0 <= %i1 AND cn_id <> %i2 ORDER BY score_level_%i0 DESC LIMIT %i3", $level, $score_player, $cn_id, $num_players_en_extremos);
		$score_mayor_asc = DB::query("SELECT name, team_id, score_level_%i0 FROM user WHERE score_level_%i0 > %i1 AND cn_id <> %i2 ORDER BY score_level_%i0 ASC LIMIT %i3", $level, $score_player, $cn_id, $num_players_en_extremos);
		
		
		$score_menor_igual_asc = array_reverse($score_menor_igual_desc);
		$returnVars['data']		=  array_merge($score_menor_igual_asc, $player, $score_mayor_asc);
		
		$returnVars['success']	= 1;
		$returnVars['info']		.=   " - Complete";
		
		return $returnVars;
    }

    /**
     * Add team score
     * @return 
     */
    public function addTeamScore($team_id, $level, $delta_score, $hash) {
		
		require_once ('../meekrodb.2.1.class.php');
		
		$returnVars = array();
		
		$returnVars['success']	= 0;
		$returnVars['info']		=  "Init..";
		
		if(md5($team_id . $level . $delta_score . $this->secret_key)===$hash)
		{
			if($delta_score > $this->max_delta_score)
			{
				$returnVars['info']		.=   " - Trying to add a lot of points!";
				$returnVars['data']		=  "Trying to add a lot of points!";
				$returnVars['success']	= 0;
			}
			elseif ($delta_score < 0)
			{
				$returnVars['info']		.=   " - Trying to add negative points!";
				$returnVars['data']		=  "Trying to add negative points!";
				$returnVars['success']	= 0;
			}
			else
			{
				$returnVars['data'] = DB::query("INSERT INTO team_score(team_id,date,score_level_%i0) VALUES(%s2,CURDATE(),%i1) ON DUPLICATE KEY UPDATE score_level_%i0=score_level_%i0+%i1", $level, $delta_score, $team_id);
				$returnVars['success']	= 1;
			}
		}
		else
		{
			$returnVars['data']		=  "Hash inválido!";
			$returnVars['success']	= 0;
  		}
		
		$returnVars['success']	= 1;
		$returnVars['info']		.=   " - Complete";
		
		return $returnVars;
    }

    /**
     * Add money to user
     * @return 
     */
    public function addMoneyToUser($cn_id, $delta_money, $hash) {
		
		require_once ('../meekrodb.2.1.class.php');
		
		$returnVars = array();
		
		$returnVars['success']	= 0;
		$returnVars['info']		=  "Init..";
		
		if(md5($cn_id . $delta_money . $this->secret_key)===$hash)
		{
			if($delta_money > $this->max_delta_money)
			{
				$returnVars['info']		.=   " - Trying to add a lot of money!";
				$returnVars['data']		=  "Trying to add a lot of money!";
				$returnVars['success']	= 0;
			}
			elseif ($delta_money < ($this->max_delta_money * -1))
			{
				$returnVars['info']		.=   " - Trying to remove a lot of money!";
				$returnVars['data']		=  "Trying to remove a lot of money!";
				$returnVars['success']	= 0;
			}
			else
			{				
				$returnVars['data']		=  DB::query("UPDATE user SET money=money+%i0 WHERE cn_id=%s1", $delta_money, $cn_id);
				$returnVars['success']	= 1;
			}
		}
		else
		{
			$returnVars['data']		=  "Hash inválido!";
			$returnVars['success']	= 0;
  		}
		
		
		$returnVars['success']	= 1;
		$returnVars['info']		.=   " - Complete";
		
		return $returnVars;
    }

    /**
     * Get game info
     * @return "success","info","data"
     */
    public function getGameInfo() {
		
		require_once ('../meekrodb.2.1.class.php');
		
		$returnVars = array();
		
		$returnVars['success']	= 0;
		$returnVars['info']		=  "Init..";
		$returnVars['info']		.=   " - []";
		
		$returnVars['data']	= DB::queryFirstRow("SELECT WEEKDAY(CURDATE())+1 as weekday, TIME_TO_SEC(CURTIME())*1000 as time_of_day_ms, CURDATE() as today, YEARWEEK(CURDATE(), 1) as week_number from game LIMIT 1");
		
		
		$returnVars['success']	= 1;
		$returnVars['info']		.=   " - Complete";
		
		return $returnVars;
    }

}
?>
