# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.6.14)
# Database: regshowpaintwardomestic
# Generation Time: 2013-10-03 17:40:33 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table game
# ------------------------------------------------------------

DROP TABLE IF EXISTS `game`;

CREATE TABLE `game` (
  `id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;

INSERT INTO `game` (`id`)
VALUES
	(1);

/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table team_score
# ------------------------------------------------------------

DROP TABLE IF EXISTS `team_score`;

CREATE TABLE `team_score` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `score_level_1` int(11) NOT NULL DEFAULT '0',
  `score_level_2` int(11) NOT NULL DEFAULT '0',
  `score_level_3` int(11) NOT NULL DEFAULT '0',
  `score_level_4` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_team_date` (`team_id`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `team_score` WRITE;
/*!40000 ALTER TABLE `team_score` DISABLE KEYS */;

INSERT INTO `team_score` (`id`, `team_id`, `date`, `score_level_1`, `score_level_2`, `score_level_3`, `score_level_4`)
VALUES
	(22,1,'2037-12-31',0,0,0,0),
	(23,2,'2037-12-31',0,0,0,0),
	(24,3,'2037-12-31',0,0,0,0);

/*!40000 ALTER TABLE `team_score` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cn_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `money` int(10) NOT NULL DEFAULT '0',
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `team_id` int(11) NOT NULL,
  `score_level_0` int(15) NOT NULL DEFAULT '0',
  `score_level_1` int(15) unsigned NOT NULL DEFAULT '0',
  `score_level_2` int(15) unsigned NOT NULL DEFAULT '0',
  `score_level_3` int(15) unsigned NOT NULL DEFAULT '0',
  `score_level_4` int(15) unsigned NOT NULL DEFAULT '0',
  `grenades` int(10) unsigned NOT NULL DEFAULT '0',
  `shots_power` int(10) unsigned NOT NULL DEFAULT '0',
  `shots_distance` int(10) unsigned NOT NULL DEFAULT '0',
  `extra_life_slots` int(10) unsigned NOT NULL DEFAULT '0',
  `last_day_results_seen` date NOT NULL,
  `last_week_results_seen` int(10) unsigned NOT NULL DEFAULT '0',
  `muscleman` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cn_id` (`cn_id`),
  KEY `score_level_1` (`score_level_1`),
  KEY `score_level_2` (`score_level_2`),
  KEY `score_level_3` (`score_level_3`),
  KEY `score_level_4` (`score_level_4`),
  KEY `score_level_0` (`score_level_0`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
